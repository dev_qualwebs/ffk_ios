//
//  DatePickerViewController.swift
//  FFK
//
//  Created by AM on 01/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    static var shared = DatePickerViewController()
    
    //MARK: IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        datePicker.timeZone = TimeZone(identifier: "America/Chicago")
        datePicker.maximumDate = Date()
    }
    
    
    //MARK: IBActions
    @IBAction func doneButton(_ sender: Any) {
        K_PICKER_DATE = datePicker.date
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: N_SELECT_DATE), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
}
