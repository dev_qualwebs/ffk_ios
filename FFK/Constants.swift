//
//  Constants.swift
//
//  Copyright © 2019 Qualwebs. All rights reserved.

import UIKit

//MARK: Constants
var ligntGreen = UIColor(red: 0/255, green: 133/255, blue: 119/255, alpha: 1) // 008577
var darkGreen = UIColor(red: 86/255, green: 153/255, blue: 88/255, alpha: 1) // 569958
var lightPink =  UIColor(red: 216/255, green: 27/255, blue: 96/255, alpha: 1) // D81B60
//var primaryColor = UIColor(red: 185/255, green: 0/255, blue: 6/255, alpha: 1) // b90006
var primaryColor = UIColor(red: 169/255, green: 34/255, blue: 25/255, alpha: 1) //A92219

var statusBarColor = UIColor(red: 149/255, green: 14/255, blue: 05/255, alpha: 1) // b90006
var yellowColor = UIColor(red: 246/255, green: 190/255, blue: 14/255, alpha: 1) // f6be0e
var grayColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1) // 666666
//var blueColoe = UIColor(red: 59/255, green: 85/255, blue: 160/255, alpha: 1) // 3B55A0
var smokeColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1) // eaeaea
var orangeColor = UIColor(red: 221/255, green: 75/255, blue: 57/255, alpha: 1) // DD4B39
var lightBrown = UIColor(red: 181/255, green: 101/255, blue: 29/255, alpha: 1) // DD4B39
var blueColor =    UIColor(red: 102/255, green: 153/255, blue: 204/255, alpha: 1)
//var secondaryColor = UIColor(red: 219/255, green: 183/255, blue: 55/255, alpha: 1)
var barBackgroundColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1)//d2d2d2
var backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1) //e8eaed
var darkBackgroundColor = UIColor(red: 213/255, green: 251/255, blue: 251/255, alpha: 1) //D5FBFB
var offWhiteColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
var shadowColor = UIColor(red: 227/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1)

//EDF3F9
//Keys


//MARK: API Constants
//Base Url
//var U_BASE = "https://admin.farmersfreshkitchen.com/api/"
 var U_BASE = "https://beta-api-v2.farmersfreshmeat.com/api/"
//var U_BASE = "https://v2api.farmersfreshkitchen.com/api/"
var U_IMAGE_BASE =  "http://54.219.128.62/api/public/storage/"
var U_CUSTOM_SIZE = "https://image.qualwebs.com/360x360/http://54.219.128.62/api/public/storage/"


//Url end-points
var U_REGISTER = "register"
var U_GET_MENU = "get-menu/"
var U_LOGIN = "login"
var U_GET_PREFERRED_RESTAURANT = "get-pref-restaurants"
var U_GET_PROFILE = "get-profile"
var U_UPDATE_PROFILE = "update-profile"
var U_GET_RESTAURENTS = "get-restaurants"
var U_GET_SUB_CATEGORIES = "get-main-menu-categories/"
var U_GET_SUBCATEGORIES = "get-main-menu-modifiers/"
var U_ADD_TO_CART = "add-to-cart"
var U_GET_MENU_TYPE = "get-menu-type/"
var U_GET_CART_DATA = "get-my-cart?cart_id="
var U_GET_MENU_MEALS = "get-menu-type-meal/"
var U_GET_RESTAURENT_DETAIL = "get-single-restaurant/"
var U_REMOVE_CART = "remove-from-cart/"
var U_SELECT_PREFERENCE = "select-preference"
var U_DUPLICATE_CART = "duplicate-item-in-cart"
var U_GET_EDIT_CART_MODIFIER = "get-edit-cart-modifiers/"
var U_UPDATE_MY_CART = "update-my-cart"
var U_CONFIRM_ORDER = "confirm-order"
var U_GET_ORDER_HISTORY = "get-order-history"
var U_FAVOURITE_LABEL = "get-favorite-label"
var U_MARK_FAVOURITE = "mark-as-favorite"
var U_EMPTY_CART = "empty-cart"
var U_GET_REWARDS = "get-my-rewards"
var U_RESTAURENT_FAVOURITE  = "favorite-restaurant"
var U_GET_REWARD_COUPEN = "get-coupons"
var U_GET_REWARD_ITEMS = "get-reward-items"
var U_GET_SIGLE_ORDER = "get-single-order/"
var U_SEND_OTP = "send-otp"
var U_RESET_PASSWORD = "reset-password"
var U_CHANGE_PASSWORD = "change-password"
var U_ORDER_FEEDBACK = "place-order-feedback"
var U_FB_LOGIN = "facebook-login"
var U_GOOGLE_LOGIN = "google-login"
var U_RESTAURENT_PREF = "restaurant-preference"
var U_SET_USER_ADDRESS = "set-user-address"
var U_DELETE_ACCOUNT = "delete-my-account"
var U_GET_PREFERENCE = "get-preference"
var U_SUBS_PREFERENCE = "subscription-preference"
var U_LOGOUT = "logout"
var U_REORDER_RECENT = "make-re-order"
var U_ADD_FAVOURITE_LABEL = "add-favorite-label"
var U_DEVICE_PREFER = "device-preference"
var U_BONUS_LIST = "get-bonus"
var U_APPLY_BONUS = "apply-bonus/"
var U_APPLY_REWARD = "apply-reward/"
var U_REMOVE_REWARD = "remove-reward?coupon_id="
var U_REMOVE_BONUS = "remove-bonus?bonus_id="
var U_DELETE_CARD = "delete-card"
var U_SAVE_CARD = "save-card"
var U_GET_CARD = "get-cards"
var U_GET_NEW_CARD = "get-cards-v2"
var U_GUEST_CHECKOUT = "guest-checkout"
var U_CHANGE_DEFAULT_CARD  = "set-card-default/"
var U_EMPTY_GUEST_CART = "guest-empty-cart"
var U_APPLE_LOGIN = "apple-login"
var U_VALIDATE_MENU_TIMING = "validate-menu-timing/"
var U_GET_NOTIFICATIONS = "user/notifications"

var U_CHECK_ITEM_IN_STOCK = "validate-cart-items"

var U_ADD_CARD = "square/add-card"

var U_WELCOME_ITEMS = "welcome-items/"

var U_GUEST_CHECKOUT_BRAINTREE = "guest-checkout-v2"
var U_CONFIRM_ORDER_BRAINTREE = "confirm-order-v2"
var U_ADD_CARD_BRAINTREE = "create-customer-paymethod"

var U_CARDKNOX_URL = "https://x1.cardknox.com/gatewayjson"
var U_CARDKNOX_ADD_CARD = "cardknox/add-card"
var U_CARDKNOX_KEY = "qualwebsdev0ca39ee2fa384011907ea65b64f07982"
var U_CARDKNOX_VERSION = "4.5.9"
var U_CARDKNOX_SOFTWARE_NAME = "FarmersFreshMeat"
var U_CARDKNOX_SOFTWARE_VERSION = "xSoftwareVersion"


//Constant
var K_PICKER_DATE: Date?

//Subscribe to topic
let K_TOPIC_FOR_ALL_USER = "ffk_all"
let K_TOPIC_FOR_LOGGEDIN_USER = "ffk_user"

//Handling realtime
var K_KITCHEN_ONLINE_ONE = "1"
var K_KITCHEN_ONLINE_TWO = "1"
var K_IS_MENU_ITEM_CHANGED = false

//Controller id's
var K_SPLASH = "splash"
var K_HOME_SLIDER = "home_slider"
var K_FIRST_NAME = "first_name"
var K_LAST_NAME = "last_name"
var K_EMAIL = "email"
var K_PHONE_NUMBER = "phone_number"
var K_PASSWORD = "password"
var K_AGE = "age"
var K_ROLE = "role"
var K_IMAGE_UPLOAD = "profile"
var K_CERT_UPLOAD = "certificate"
var k_USER_IMAGE = "profile_image"
var K_GENDER = "gender"
var K_RECENT_TAB = "recent"
var K_FAVOURITE_TAB = "favourite"
var K_LUNCH_ID = Int()
var K_BREAK_ID = Int()
//var K_CURRENT_REWARD = 0


//PAGE VIEW SCREEN
var K_MAP_PAGE_VIEW = "map_view"
var K_MENU_PAGE_VIEW = "menu_view"


//var K_SQUARE_APP_ID = "sq0idp-RvC4zckibDCNUU85Q-KAGQ"
var K_SQUARE_APP_ID = "sandbox-sq0idb-_ce09Ka5ei3BNUZ-09lizA"

var K_GOOGLE_API_KEY = "AIzaSyAu8XCn19KUmYjZD5zXgDqqdhrWtTjyAJQ"
var K_GOOGLE_CLIENT_ID = "448534530350-gkdqfc95vmgj2u3tc4udsni247kokl7f.apps.googleusercontent.com"

var K_APPLE_MERCHANT_ID_SQUARE = "merchant.com.ffk.applepaysquare.client"
var K_APPLE_MERCHANT_ID_CARDKNOX = "merchant.com.ffkcardknox"


//PLACE_API_KEY="AIzaSyBOKcao5g8_DE4y6UkAXsc-qJC1PlLpvBQ"
//SQUARE_SANDBOX_LOCATION_ID = "LVFPQMM945H3E"
//SQUARE_SANDBOX_KEY = "sandbox-sq0idb-_ce09Ka5ei3BNUZ-09lizA"
//SQUARE_CULLEN_LOCATION_ID  = "648YXECK0Q7RZ"
//SQUARE_MESA_LOCATION_ID = "L41Q7BKZ0HJZJ"

//OLD_FACEBOOK_ID = 660550354516294

//Button Id's
var BUTTON_BACK = 0
var BUTTON_MENU = 1
var MENU_TYPE = Int()

//User Default's
var UD_FCM_TOKEN = "fcm_token"
var UD_TOKEN = "access_token"
var UD_USER_DETAIl = "userDetail"
var UD_ADD_TO_CART = "addtocart"
var UD_CARTITEM_COUNT = "cart_item_count"
var UD_ACTIVE_ORDER_EXIST = "active_order_exist"
var UD_FFK_RESTAURENT = "ffk_restaurent"
var UD_CATEGORY_TYPE = "cart_selected_category"
var UD_GUEST_LOGIN_DETAIL = "guest_login_detail"
var UD_LAUNCH_FIRST_TIME = "launch_first_time"
var UD_APP_RATED = "app_rated_before"
//let UD_RESTAURANT_PREF = "restaurant_preferances"
let UD_BREAkFAST_ITEMS = "breakfast_items"
let UD_LUNCH_ITEMS = "lunch_items"
let UD_MENU_TYPE = "menu_type"
let UD_SELECTED_LOCATION_PREF = "selected_location_preference"
let UD_RESTAURANT_SELECTED = "restaurant_selected"
let UD_SINGLE_ACTIVE_ORDER = "UD_SINGLE_ACTIVE_ORDER"

//NSNotifications
var N_BF_TAB_COLOR = "breakfast_tab_color"
var N_LUNCH_TAB_COLOR = "lunch_tab_color"
var N_SELECT_DATE = "select_date"
var N_SCROLL_TABLE = "location_table_scroll"
var N_CONTAINER_SIZE = "reduce_container_size"
var N_LOCATION_CONTAINER = "location_contaier_height"
var N_LOCATION_DATA = "location_table_data"
var N_SELECT_ADDRESS_SEND = "selected_address"
var N_UPDATE_REST_LOCATION = "update_location"
var N_SELECTED_RESTAURENT_MAP = "show_selected_restaurent_map"
var N_LUNCH_DATA = "reload_lunch_data"
var N_BREAK_DATA = "reload_breakfast_data"
var N_UPDATE_BAG_COUNT = "update_bag_count"
var N_SOCIAL_LOGIN = "handle_social_login"
var N_ORDER_RATED = "handel_order_rated"
var N_SHOW_REWARD_VIEW = "show_free_reward_view"
var N_TIME_OBSERVER = "menu_time_observer"
var N_BONUS_REWARD_COUNT = "bonus_reward_count"
var N_RESET_PAY_BUTTON = "reset_pay_button"
var N_SHOW_ERROR_MESSAGE = "show_error_message"
var N_CHANGE_SELECTED_CARD = "change_payment_selected_card"
var N_APPLE_CARD_SELECTED = "apple_card_selected"
var N_SELECT_RESTAURANT_BUTTON = "location_detail_buttton"
var N_ADD_NEW_CARD = "add_new_payment_card"
var N_LOGOUT_USER = "logout_user"
var N_HANDLE_ITEM_OUTOFSTOCK = "N_HANDLE_ITEM_OUTOFSTOCK"
var N_RESET_WELCOME_GESTURE = "N_RESET_WELCOME_GESTURE"

var N_UPDATE_RESTAURANT_TAB = "update_restaurant_tab"
var N_CHANGE_WELCOME_VIEW = "order_completed_welcome"
var N_CHANGE_RECENT_VIEW = "order_completed_recent" 
var N_ORDER_COMPLETED = "order_completed"
var N_CHANGE_ORDERCART_VIEW = "order_completed_single_order"
var N_NEW_REWARD = "2000_point_reward"
var N_NOTIFICATION_REWARD = "notification_for_reward"
var N_NOTIFICATION_BONUS = "notification_for_bonus"
var N_NOTIFICATION_REWARD_POINTS = "notification_for_reward_points"
var N_NOTIFICATION_SIGNUP = "notification_for_signup"
