//
//  PaymentMethodViewController.swift
//  Farmers
//
//  Created by qw on 12/02/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import SquareInAppPaymentsSDK

class PaymentMethodViewController: UIViewController,AddCard, Confirmation {
    func handleGuestCard(nonce: String?, zip: String?, street: String?) {
        
    }
    
    func confirmationSelection() {
         ActivityIndicator.show(view: self.view)
                  let param = [
                    "card_id": self.selectedId
                  ] as! [String:Any]
                  SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CARD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_DELETE_CARD, userToken: nil) { (response) in
                    ActivityIndicator.hide()
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                       myVC.modalPresentationStyle = .overFullScreen
                       myVC.heading = "Card Deleted"
                       self.navigationController?.present(myVC, animated: false, completion: nil)
                   self.getCards()
                  }
    }
    //MARK: IBOutlets
    @IBOutlet weak var cardTable: ContentSizedTableView!
    @IBOutlet weak var addNewView: UIView!
    @IBOutlet weak var dashView: UIView!
    @IBOutlet weak var dashSubview: UIView!

    
    
    var cardData = [GetCardResponse]()
    var selectedId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = primaryColor
 
        self.cardTable.tableFooterView = UIView()
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
     //   swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        if(Singleton.shared.paymentCards.count == 0){
            self.getCards()
        }else {
          self.cardData = Singleton.shared.paymentCards
          self.cardTable.reloadData()
        }
    }
   
    func getCards(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_CARD, method: .get, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_NEW_CARD, userToken: nil) { (response) in
              
            Singleton.shared.paymentCards = response.response
            self.cardData = response.response
            self.cardTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func showPopup(title: String, msg: String, id: Int) {
        
         let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.confirmationText = "Are you sure? \n You're removing a card."
        self.selectedId = id
        myVC.isConfirmationViewHidden = false
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
    }
    
    func addCard() {
        self.getCards()
    }
    
    
    //MARK: IBActions
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        if(Singleton.shared.selectedLocation.payment_gateway == "cardknox"){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            myVC.cardDelegate = self
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let controller = self.makeCardEntryViewController()
            controller.collectPostalCode = true
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension PaymentMethodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.cardData.count == 0){
            //self.addNewView.isHidden = true
            self.dashView.isHidden = false
            self.cardTable.isHidden = true
        }else {
          // self.addNewView.isHidden = false
            self.dashView.isHidden = true
            self.cardTable.isHidden = false
        }
        return self.cardData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "AddCardCell") as! AddCardCell
        let val = self.cardData[indexPath.row]
        cell.mainView.borderColor = .darkGray
        switch val.card_type?.lowercased() {
            case "visa":
                cell.visaImage.image = UIImage(named: "visa")
                break
                case "mastercard":
                cell.visaImage.image = UIImage(named: "mastercard")
                break
                
                case "jcb":
                cell.visaImage.image = UIImage(named: "jcb")
                break
                case "discover":
                cell.visaImage.image = UIImage(named: "discover")
                break
                case "UNKNOWN":
                cell.visaImage.image = nil
                 break
                case "amex":
                cell.visaImage.image = UIImage(named: "american-express")
                break
            default:
                cell.visaImage.image = nil
            }
        cell.cardNumber.text = val.masked_card_number
        cell.cardHolderName.text = val.name?.uppercased()
        let year = String(val.expiry_year!.suffix(2))
        cell.expiryDate.text = (val.expiry_month ?? "") + "/" + (year ?? "")
        cell.removeCard = {
            self.showPopup(title: "Delete Card", msg: "Are you sure?", id: val.id ?? 0)
        }
        return cell
    }
}


extension PaymentMethodViewController: UINavigationControllerDelegate, SQIPCardEntryViewControllerDelegate{
    func navigationControllerSupportedInterfaceOrientations(
        _: UINavigationController
    ) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    func makeCardEntryViewController() -> SQIPCardEntryViewController {
        let theme = SQIPTheme()

        let cardEntry = SQIPCardEntryViewController(theme: theme)
        cardEntry.collectPostalCode = false
        cardEntry.delegate = self
        return cardEntry
    }
    
    func cardEntryViewController(
        _: SQIPCardEntryViewController,
        didCompleteWith _: SQIPCardEntryCompletionStatus
    ) {
        self.dismiss(animated: true, completion: nil)

    }
    
    func cardEntryViewController(_ val: SQIPCardEntryViewController,
                                 didObtain cardDetail: SQIPCardDetails,
                                 completionHandler: @escaping (Error?) -> Void) {
        // Send card nonce to your server to store or charge the card.
        // When a response is received, call completionHandler with `nil` for success,
        // or an error to indicate failure.
            
        /*
         MyAPIClient.shared.chargeCard(withNonce: cardDetails.nonce) { transaction, chargeError in
         
         if let chargeError = chargeError {
         completionHandler(chargeError)
         }
         else {
         completionHandler(nil)
         }
         }
         */
       self.dismiss(animated: true, completion: nil)
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CARD, method: .post, parameter: ["nonce":cardDetail.nonce], objectClass: SuccessResponse.self, requestCode: U_ADD_CARD, userToken: nil) { response in
            self.getCards()
            ActivityIndicator.hide()
        }
    }
}


class AddCardCell: UITableViewCell{
    //MARK:IBOutlets
    @IBOutlet weak var cardNumber: DesignableUILabel!
    @IBOutlet weak var cardHolderName: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var visaImage: UIImageView!
    @IBOutlet weak var mainView: View!
    
    var removeCard:(()-> Void)? = nil
    
    @IBAction func removeAction(_ sender: Any) {
        if let removeCard = self.removeCard{
            removeCard()
        }
    }
}


