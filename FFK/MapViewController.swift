//
//  MapViewController.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import GoogleMaps


var isNavigationFromMenu = false

protocol RestaurantSelected{
    func setRestaurant()
}

class MapViewController: UIViewController,UIGestureRecognizerDelegate,CLLocationManagerDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var viewCrossButton: View!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var gestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var mapLayer: UIView!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderRestaurantButton: UIView!
    @IBOutlet weak var orderBottomDistance: NSLayoutConstraint!
    
    var location = [MKPointAnnotation]()
    var initialTouchPoint = CGPoint(x: 0, y: 0)
    var scroll: UIPanGestureRecognizer?
    var restaurentsData = [LocationResponse]()
    var hideCrossView = false
    // let locationManager = CLLocationManager()
    var translation: CGPoint!
    var startPosition: CGPoint!
    var originalHeight: CGFloat = 0
    var difference: CGFloat!
    var intialContainerHeight:CGFloat = 0
    static var restaurantDelegate:RestaurantSelected? = nil
    
    enum CardState {
        case expanded
        case collapsed
    }
    
    var cardHeight:CGFloat = 600
    var cardHandleAreaHeight:CGFloat = 65
    
    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    var visualEffectView:UIVisualEffectView!
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted:CGFloat = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRestaurentAPI()
        self.view.backgroundColor = primaryColor
        // scroll?.delegate = self
        self.cardHeight = self.view.frame.height*0.75
        self.cardHandleAreaHeight = self.containerView.frame.height
        mapView.mapType = .mutedStandard
        originalHeight = containerView.frame.height
        mapView.subviews[1].isHidden = true
        mapView.subviews[2].isHidden = true
        self.mapView.isRotateEnabled = false
        self.mapView.isScrollEnabled = true
        scroll = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        
        //To decrease container on Tapping outside container
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.compressContainer))
        tap.numberOfTapsRequired = 1
        self.mapLayer.addGestureRecognizer(tap)
        
        
        self.containerView.addGestureRecognizer(scroll!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.compressContainer), name: NSNotification.Name(N_CONTAINER_SIZE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.increaseContaierHeight(_:)), name: NSNotification.Name(N_LOCATION_CONTAINER), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.updateRestaurentLocation), name: NSNotification.Name(N_UPDATE_REST_LOCATION), object: nil)   
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMapCenter), name: NSNotification.Name(N_SELECTED_RESTAURENT_MAP), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatepageControl), name: NSNotification.Name("page_controller"), object: nil)
        self.mapView.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleCardTap(recognzier:)))
        containerView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        PageViewController.index_delegate = self
        if(hideCrossView){
            self.viewCrossButton.isHidden = true
        }else {
            self.viewCrossButton.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(UIScreen.main.nativeBounds.height < 1500){
            self.intialContainerHeight = self.containerView.frame.height + 20
            self.containerView.frame = CGRect(x: 0, y: self.view.frame.height - (self.intialContainerHeight), width: self.view.frame.width, height: self.intialContainerHeight)
            self.containerView.translatesAutoresizingMaskIntoConstraints = true
        }else {
            self.intialContainerHeight = self.containerView.frame.height
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    @objc
    func handleCardTap(recognzier:UITapGestureRecognizer) {
        switch recognzier.state {
        case .ended:
            animateTransitionIfNeeded(state: nextState, duration: 0.5)
        default:
            break
        }
    }
    
    @objc func updatepageControl(_ notif: Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("page_controller"), object: nil)
        if let id = notif.userInfo?["id"] as? Int{
            self.pageControl.currentPage = id
            if(id==1){
                self.orderRestaurantButton.isHidden = false
            }else {
                self.orderRestaurantButton.isHidden = true
            }
            if !(UIDevice.current.hasNotch){
                self.orderBottomDistance.constant = 0
            }else {
                self.orderBottomDistance.constant = 0
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatepageControl), name: NSNotification.Name("page_controller"), object: nil)
    }
    
    @objc func updateRestaurentLocation(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_REST_LOCATION), object: nil)
        K_LUNCH_ID = Int()
        K_BREAK_ID = Int()
        UserDefaults.standard.removeObject(forKey: UD_MENU_TYPE)
        Singleton.shared.mainMenuData = []
        Singleton.shared.breakfastMenu = []
        Singleton.shared.lunchMenu = []
        NavigationController.shared.pushHome(controller: self)
    }
    
    @objc func updateMapCenter(_ notif: Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SELECTED_RESTAURENT_MAP), object: nil)
        if let val = notif.userInfo as? [String:String] {
            let restaurantLocation = CLLocationCoordinate2D(latitude: Double(val["lat"]!) as! CLLocationDegrees, longitude:Double(val["lng"]!) as! CLLocationDegrees)
            //Center the map on the place location
            //            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta:0.01)
            //            let region = MKCoordinateRegion(center: restaurantLocation, span: span)
            //            mapView.setRegion(MKCoordinateRegion(center: restaurantLocation, latitudinalMeters: 500, longitudinalMeters: 500), animated: true)
            //             mapView.setCenter(restaurantLocation, animated: true)
            
            let span = MKCoordinateSpan.init(latitudeDelta: 0.01, longitudeDelta:
                0.01)
            let coordinate = restaurantLocation
            let region = MKCoordinateRegion.init(center: coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMapCenter), name: NSNotification.Name(N_SELECTED_RESTAURENT_MAP), object: nil)
    }
    
    @objc  func increaseContaierHeight(_ notif: Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_LOCATION_CONTAINER), object: nil)
        if let val = notif.userInfo as? [String:String] {
            if(val["increaseHeight"] == "true"){
                self.animateTransitionIfNeeded(state: .expanded, duration: 0.5)
            }else if(val["increaseHeight"] == "false"){
                self.animateTransitionIfNeeded(state: .collapsed, duration: 0.5)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.increaseContaierHeight(_:)), name: NSNotification.Name(N_LOCATION_CONTAINER), object: nil)
    }
    
    func getRestaurentAPI(){
        if(Singleton.shared.restaurentLocation.count != 0){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: N_LOCATION_DATA), object: nil,userInfo: ["data": Singleton.shared.restaurentLocation])
            self.restaurentsData = Singleton.shared.restaurentLocation
            DispatchQueue.main.async {
                self.showRestaurentsMarker()
            }
        }else {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_RESTAURENTS, method: .get, parameter: nil, objectClass: GetLocation.self, requestCode: U_GET_RESTAURENTS, userToken: nil) { (response) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: N_LOCATION_DATA), object: nil,userInfo: ["data": response.response])
                self.restaurentsData = response.response
                Singleton.shared.restaurentLocation = response.response
                if(response.response.count > 0 && Singleton.shared.selectedLocation.id == nil){
                    Singleton.shared.selectedLocation = response.response[0]
                }
                self.showRestaurentsMarker()
            }
        }
    }
    
    func showRestaurentsMarker(){
        for val in self.restaurentsData {
            let location = MKPointAnnotation()
            location.title = val.name
            location.coordinate = CLLocationCoordinate2D(latitude: Double(val.latitude!) as! CLLocationDegrees, longitude:Double(val.longitude!) as! CLLocationDegrees)
            self.location.append(location)
        }
        mapView.showAnnotations(self.location, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            let pinImage = #imageLiteral(resourceName: "map_location_1_01")
            let size = CGSize(width:35, height: 55)
            UIGraphicsBeginImageContext(size)
            pinImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            
            annotationView?.image = resizedImage
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func compressContainer() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CONTAINER_SIZE), object: nil)
        view.endEditing(true)
        if let nav = self.navigationController {
            nav.view.endEditing(true)
        }
        UIView.animate(withDuration: 0.2, delay: 0 , options: .curveEaseInOut, animations: { () -> Void in
            self.containerView.frame = CGRect(x: 0, y:self.view.frame.height - (self.intialContainerHeight), width: self.view.frame.width, height: self.intialContainerHeight)
            self.pageControl.frame.origin.y = self.view.frame.height - (self.intialContainerHeight)
            self.pageControl.isHidden = false
        },completion: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.compressContainer), name: NSNotification.Name(N_CONTAINER_SIZE), object: nil)
    }
    
    @IBAction func restaurantChangeaction(_ sender: Any) {
        MapViewController.restaurantDelegate?.setRestaurant()
        Singleton.shared.recentOrderHistory = []
        Singleton.shared.activeOrderData = []
        currentTab = K_RECENT_TAB
       // NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_RECENT_VIEW), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(N_RESET_WELCOME_GESTURE), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        NavigationController.shared.getRecentOrder(nil)
    }
    
    @IBAction func viewDidDrag(_ sender: UIPanGestureRecognizer) {
        
    }
    
    
    
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer){
        switch sender.state {
        case .began:
            startInteractiveTransition(state: nextState, duration: 0.5)
        case .changed:
            let translation = sender.translation(in: self.containerView)
            var fractionComplete = translation.y / cardHeight
            fractionComplete = cardVisible ? fractionComplete : -fractionComplete
            updateInteractiveTransition(fractionCompleted: fractionComplete)
        case .ended:
            continueInteractiveTransition()
        default:
            break
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        currentPageView = K_MENU_PAGE_VIEW
        if(isNavigationFromMenu){
            isNavigationFromMenu = false
            NavigationController.shared.pushHome(controller: self)
        }else {
            NavigationController.shared.addTransition(direction: .fromTop, controller: self)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension MapViewController: GMSAutocompleteResultsViewControllerDelegate,MKMapViewDelegate{
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error){
        
    }
}

extension MapViewController: ControllerIndexDelegate {
    func getControllerIndex(index: Int) {
        pageControl.currentPage = index
        pageControl.isHidden = false
        if index == 0 {
            
        }else {
            
        }
    }
    
    
    
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                let screenSize: CGRect = UIScreen.main.bounds
                switch state {
                case .expanded:
                    self.containerView.translatesAutoresizingMaskIntoConstraints = true
                    self.containerView.frame = CGRect(x: 0, y:self.view.frame.height - self.cardHeight, width: screenSize.width, height: screenSize.height * 0.75)
                    self.pageControl.frame.origin.y = self.view.frame.height - (self.view.frame.height * 0.75)
                    self.mapLayer.alpha = 0.5
                    
                case .collapsed:
                    self.containerView.translatesAutoresizingMaskIntoConstraints = true
                    self.containerView.frame = CGRect(x: 0, y:self.view.frame.height - self.intialContainerHeight, width: self.view.frame.width, height: self.intialContainerHeight)
                    self.pageControl.frame.origin.y = self.view.frame.height - (self.intialContainerHeight)
                    self.mapLayer.alpha = 0
                    
                }
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                if(self.runningAnimations.count > 0){
                    self.runningAnimations.removeAll()
                }
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
            
            
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    break
                // self.cardViewController.view.layer.cornerRadius = 12
                case .collapsed:
                    break
                    // self.cardViewController.view.layer.cornerRadius = 0
                }
            }
            
            cornerRadiusAnimator.startAnimation()
            runningAnimations.append(cornerRadiusAnimator)
            
            let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    break
                //  self.visualEffectView.effect = UIBlurEffect(style: .dark)
                case .collapsed:
                    break
                    // self.visualEffectView.effect = nil
                }
            }
            
            blurAnimator.startAnimation()
            runningAnimations.append(blurAnimator)
            
        }
    }
    
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        for animator in runningAnimations {
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        for animator in runningAnimations {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    func continueInteractiveTransition (){
        for animator in runningAnimations {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
}




