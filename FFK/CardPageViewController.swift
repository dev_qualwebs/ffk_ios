//
//  CardPageViewController.swift
//  Farmers
//
//  Created by qw on 15/02/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
protocol CardIndexDelegate {
    func getContentIndex(index: Int)
}

class CardPageViewController: UIPageViewController {

    static var dataSource1: UIPageViewControllerDataSource?
    static var indexDelegate: CardIndexDelegate? = nil
    
    var currentIndex: Int = 0
    var arrayContent: [GetCardResponse]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        CardPageViewController.dataSource1 = self
        arrayContent = Singleton.shared.paymentCards
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getContent()
        
    }
        
   public func getContent() {
        if let content = contentAtIndex(index: currentIndex){
            self.setViewControllers([content], direction: .forward, animated: false, completion: nil)
           // CardPageViewController.indexDelegate?.getContentIndex(index: currentIndex)
        }
    }
    
    func contentAtIndex(index: Int) -> UIViewController? {
        if (arrayContent?.count == 0) || (index >= (arrayContent?.count)!) {
            return nil
        }
        
            let splashContent = self.storyboard?.instantiateViewController(withIdentifier: "MultiCardViewController") as! MultiCardViewController
            guard let content = arrayContent as? [GetCardResponse] else { return nil }
            splashContent.content = content[index]
            MultiCardViewController.currentIndex = index
            return splashContent
    }
    
    func viewAtIndex(_ viewController: UIViewController, next: Bool) -> UIViewController? {
        var index:Int = 0
        if let pageContent: MultiCardViewController = viewController as? MultiCardViewController{
            index = MultiCardViewController.currentIndex!
            if next {
                if (index == NSNotFound) {
                    return nil
                }
                index += 1
                if index == arrayContent?.count {
                    return nil
                }
            } else {
                if ((index == 0) || (index == NSNotFound)) {
                    return nil
                }
                index -= 1
            }
            return contentAtIndex(index: index)
        }else {
            return nil
        }
    }
}
extension CardPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: false)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: true)
    }
}
extension CardPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            let currentVC: MultiCardViewController = pageViewController.viewControllers?[0] as! MultiCardViewController
        currentIndex = MultiCardViewController.currentIndex!
        CardPageViewController.indexDelegate?.getContentIndex(index: currentIndex)
    }
    
//    func goToPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil, next: Bool) {
//        if let currentViewController = viewControllers?[0] {
//            if next{
//                if let nextPage = CardPageViewController.dataSource1?.pageViewController(self, viewControllerAfter: currentViewController) {
//                    setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
//                }
//            }else{
//                if let previousPage = CardPageViewController.dataSource1?.pageViewController(self, viewControllerBefore: currentViewController) {
//                    setViewControllers([previousPage], direction: .reverse, animated: animated, completion: completion)
//                }
//            }
//        }
//    }
    
//    func removeSwipeGesture(){
//        for view in self.view.subviews {
//            if let subView = view as? UIScrollView {
//                subView.isScrollEnabled = false
//            }
//        }
//    }
}
