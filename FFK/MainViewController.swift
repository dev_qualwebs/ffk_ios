//
//  MainViewController.swift
//  FFK
//
//  Created by AM on 20/09/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig

var isNavigationFromRewardItem =  false
var isNavigationFromCheckout = false

class MainViewController: UIViewController, RewardPopup {
    
    //IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    var remoteConfig: RemoteConfig!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")
        fetchConfig()
        
        self.view.backgroundColor = primaryColor
        if(isNavigationFromRewardItem){
            let pageControl = MainPageViewController.customDataSource as! MainPageViewController
            isNavigationFromRewardItem =  false
            pageControl.setThirdController()
        }else {
            let pageControl = MainPageViewController.customDataSource as! MainPageViewController
            isNavigationFromRewardItem =  false
            pageControl.setSecondController()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_ORDER_COMPLETED), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NEW_REWARD), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_REWARD_POINTS), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_SIGNUP), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_ORDER_COMPLETED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.rewardEarned(_:)), name: NSNotification.Name(N_NEW_REWARD), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForReward), name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForBonus(_:)), name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForRewardPoints(_:)), name: NSNotification.Name(N_NOTIFICATION_REWARD_POINTS), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForSignup(_:)), name: NSNotification.Name(N_NOTIFICATION_SIGNUP), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        currentPageView = K_MENU_PAGE_VIEW
    }
    
    @objc func notificationForSignup(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_SIGNUP), object: nil)
        
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForSignup(_:)), name: NSNotification.Name(N_NOTIFICATION_SIGNUP), object: nil)
    }
    
    @objc func notificationForRewardPoints(_ notif: Notification){
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_REWARD_POINTS), object: nil)
              let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
              self.navigationController?.pushViewController(myVC, animated: true)
    
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForRewardPoints(_:)), name: NSNotification.Name(N_NOTIFICATION_REWARD_POINTS), object: nil)
      }
    
   @objc func notificationForReward(_ notif: Notification){
       NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil)
       
           let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedRewardsViewController") as! SavedRewardsViewController
           self.navigationController?.pushViewController(myVC, animated: true)
 
    NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForReward(_:)), name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil)
   }
    
    @objc func notificationForBonus(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil)
        
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BonusViewController") as! BonusViewController
            self.navigationController?.pushViewController(myVC, animated: true)
 
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationForBonus(_:)), name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil)
    }
    
    @objc func orderCompleted(_ notif: Notification){
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_ORDER_COMPLETED), object: nil)
        if let id = notif.userInfo?["orderId"] as? Int{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderReceivedViewController") as! OrderReceivedViewController
            myVC.orderId = id
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
          NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_ORDER_COMPLETED), object: nil)
    }
    
    
    
    @objc func rewardEarned(_ notif: Notification){
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_NEW_REWARD), object: nil)
       
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
            // myVC.orderId = Int(id)!
            myVC.rewardDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.rewardEarned(_:)), name: NSNotification.Name(N_NEW_REWARD), object: nil)
    }
    
    
    func showRewardPopup() {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedRewardsViewController") as! SavedRewardsViewController
        self.navigationController?.pushViewController(myVC, animated: true)

    }
}

extension MainViewController{
    
    func fetchConfig() {
        //        if(remoteConfig["update_soft_ios"].stringValue == "false"){
        //            self.showPopup(title: "Update", msg: "New Version is available on App Store",action:2)
        //        }
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                self.remoteConfig.activate { changed, error in
                }
            } else {
            }
            self.display()
        }

    }
    
    func display() {
        var softUpdate =  "update_soft_ios"
        var updateRequired = "force_update_required_ios"
        var currentVersion = "force_update_current_version_ios"
        var updateUrl = "force_update_store_url_ios"
        guard let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return
        }
        if(remoteConfig[updateRequired].stringValue == "true"){
            if(remoteConfig[currentVersion].stringValue == appVersion){
                return
            }else{
                if(remoteConfig[softUpdate].stringValue == "false"){
                    self.showPopup(title: "Update", msg: "New Version is available on App Store",action:2)
                }else{
                    self.showPopup(title: "Update", msg:  "New Version is available on App Store",action: 1)
                }
            }
        }else {
            return
        }
    }
    
    func showPopup(title: String, msg: String,action:Int?) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Update", style: .default) { (UIAlertAction) in
            self.yesButtonAction()
        }
        let noBtn = UIAlertAction(title: "Cancel", style: .default){
            (UIAlertAction) in
            if(action != 1){
                self.forceUpdate()
            }
        }
        
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        
        present(alert, animated: true, completion: nil)
    }
    
    func yesButtonAction(){
        let string = remoteConfig["force_update_store_url_ios"].stringValue!
        let url  = NSURL(string: string)//itms   https
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.openURL(url! as URL)
        }
    }
    
    func forceUpdate() {
        exit(0);
    }
}
