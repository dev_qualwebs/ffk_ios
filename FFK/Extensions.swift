//
//  Extemsions.swift
//
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit
import SideMenu


extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UIViewController {
    
    func showToast(message : String,isAddToBag:Bool) {
        var toastLabel = UILabel(frame: CGRect.zero)
        toastLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
        toastLabel.text = message
        toastLabel.sizeToFit()
        toastLabel.frame.size = CGSize(width: toastLabel.frame.width + 30, height: toastLabel.frame.height + 20)
        if(UIScreen.main.bounds.height > 700){
            toastLabel.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY - 110)
        }else {
            toastLabel.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY - 70)
        }
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 5
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 0, delay: 1.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showAlert(title: String?, message: String?, action1Name: String?, action2Name: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //alertController.view.backgroundColor = lightPink
        //        alertController.title?.withBoldText(text: <#T##String#>, font: <#T##UIFont?#>)
        alertController.addAction(UIAlertAction(title: action1Name, style: .default, handler: nil))
        if action2Name != nil {
            alertController.addAction(UIAlertAction(title: action2Name, style: .default, handler: nil))
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            if let navController = navigationController {
                navController.popViewController(animated: true)
            } else {
                dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func getImage(str: String?) -> UIImage {
        //        var image: UIImage
        //        guard let url = URL(string: U_IMAGEBASE + (str as! String)) else {
        //            return UIImage(named: "defaultProfile")!
        //        }
        //        var myData: Any?
        //        do {
        //            myData = try  Data(contentsOf: url)
        //        }  catch let error {
        //            print("Error: \(error)")
        //        }
        //        if let data = myData {
        //            image = UIImage(data: data as! Data )!
        //            return image
        //        }else {
        return UIImage(named: "defaultProfile")!
        //  }
    }
    
    @objc func menu() {
        guard let sideMenuNavController =  SideMenuManager.defaultManager.menuLeftNavigationController else {
            let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            SideMenuManager.defaultManager.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
            SideMenuManager.defaultManager.menuWidth = self.view.frame.width
            SideMenuManager.defaultManager.menuLeftNavigationController?.setNavigationBarHidden(true, animated: false)
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
            return
        }
        present(sideMenuNavController, animated: true, completion: nil)
    }
    
    func pushController(controller: UIViewController) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "\(controller)")
        nextViewController.modalPresentationStyle = .overFullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func presentController(controller: UIViewController) {
        DispatchQueue.main.async {
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier: "America/Chicago")
        
        //dateFormatter.locale = TimeZone.current
        return dateFormatter.string(from: date)
    }
    
    func convertDateToTimestamp(_ date: String, to format: String) -> Int{
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = format
        dfmatter.timeZone = TimeZone(identifier: "America/Chicago")
        let date = dfmatter.date(from: date)
        var dateStamp:TimeInterval?
        if let myDate = date {
            dateStamp = myDate.timeIntervalSince1970
            let dateSt:Int = Int(dateStamp!)
            return dateSt
        }else {
            return Int(Date().timeIntervalSince1970)
        }
    }
    
    func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    func todaysDataToTimezone() -> Int{
        let dfmatter = DateFormatter()
        dfmatter.timeZone = TimeZone(identifier: "America/Chicago")
        let date = dfmatter.defaultDate 
        return Int(date!.timeIntervalSince1970)
    }
    
    func openUrl(urlStr: String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[0-9])[a-zA-Z0-9]{8,}")
        return passwordTest.evaluate(with: password)
    }
}

extension UIView {
    func setCircularShadow() {
        self.layer.shadowColor = UIColor(red: 111/255, green: 113/255, blue: 121/255, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1
        self.layer.cornerRadius = self.frame.width / 2
    }
    
    func addDashedBorder() {
        let color = UIColor.lightGray.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = (self.frame.size)
        let shapeRect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [2,2]
        shapeLayer.path = UIBezierPath(rect: shapeRect).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func addConstraintsWithFormatString(formate: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formate, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
        
    }
    
    func animShow(){
        if(self.isHidden == false){
            return
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                       animations: {
            self.center.y -= self.bounds.height
            self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    
    func animHide(){
        if(self.isHidden == true){
            return
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear],
                       animations: {
            self.center.y += self.bounds.height
            self.layoutIfNeeded()
            
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
}

extension UILabel {
    var isTruncated: Bool {
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
}

extension UIImageView {
    func changeTint(color: UIColor) {
        let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIView
{
    func setShadow(cornerRadius: CGFloat)
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 1
    }
}

extension String {
    func withBoldText(text: String, font: UIFont? = nil) -> NSAttributedString {
        let _font = font ?? UIFont.systemFont(ofSize: 14, weight: .regular)
        let fullString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: _font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: _font.pointSize)]
        let range = (self as NSString).range(of: text)
        fullString.addAttributes(boldFontAttribute, range: range)
        return fullString
    }
    
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
    }
    
    
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
}

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}

class UnderlinedLabel: UILabel {
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}


extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}


extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(
            red:   .random(),
            green: .random(),
            blue:  .random(),
            alpha: 1.0
        )
    }
}

extension UISlider {
    var currentPresentationValue: Float {
        guard let presentation = layer.presentation(),
              let thumbSublayer = presentation.sublayers?.max(by: {
                  $0.frame.height < $1.frame.height
              })
        else { return self.value }
        
        let bounds = self.bounds
        let trackRect = self.trackRect(forBounds: bounds)
        let minRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: 0)
        let maxRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: 1)
        let value = (thumbSublayer.frame.minX - minRect.minX) / (maxRect.minX - minRect.minX)
        return Float(value)
    }
}


extension UINavigationController: UIGestureRecognizerDelegate {
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let isSystemSwipeToBackEnabled = interactivePopGestureRecognizer?.isEnabled == true
        let isThereStackedViewControllers = viewControllers.count > 1
        return isSystemSwipeToBackEnabled && isThereStackedViewControllers
    }
    
    // 6
    func addCustomTransitioning() {
        var fullWidthBackGestureRecognizer = UIPanGestureRecognizer()
        
        // The trick here is to wire up our full-width `fullWidthBackGestureRecognizer` to execute the same handler as
        // the system `interactivePopGestureRecognizer`. That's done by assigning the same "targets" (effectively
        // object and selector) of the system one to our gesture recognizer.
//        guard
//            let interactivePopGestureRecognizer = interactivePopGestureRecognizer,
//            let targets = interactivePopGestureRecognizer.value(forKey: "targets")
//        else {
//            return
//        }
//
//        fullWidthBackGestureRecognizer.setValue(targets, forKey: "targets")
//        fullWidthBackGestureRecognizer.delegate = self
//        view.addGestureRecognizer(fullWidthBackGestureRecognizer)
    }
}
