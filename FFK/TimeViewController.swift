//
//  TimeViewController.swift
//  FFK
//
//  Created by AM on 17/09/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

protocol TimeProtocol {
    func selectPickupTime(current:Int,previous: Int,selectedTime: String)
}

class TimeViewController: UIViewController {
    
    //MARK: IBOutlts
    @IBOutlet weak var timeCollectionView: UICollectionView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    
    var timeData = [String]()
    var previousTime = Int()
    var currentTime = Int()
    var timeDelegate: TimeProtocol? = nil
    var currentIndex = Int()
    var isCardInfo = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.timeData = self.timeData.filter{$0 != "more options"}
        pickerView.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //MARK: IBActions
    
    @IBAction func okAction(_ sender: Any) {
        Singleton.shared.seletedTime = self.timeData[self.currentIndex].lowercased()
        self.timeDelegate?.selectPickupTime(current: self.currentTime, previous: self.currentTime, selectedTime: self.timeData[self.currentIndex].lowercased())
        self.dismiss(animated: true, completion: nil)
    }
        
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension TimeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.timeData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(self.isCardInfo){
          return self.timeData[row]
        }else {
          return self.timeData[row].lowercased()
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currentIndex = row
    }
}

