////
////  Router.swift
////  Diamonium
////
////  Copyright © 2019 Qualwebs. All rights reserved.
////
//
import UIKit
import Foundation
//
class Router {
    
    static func launchHome() {
        let myVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
       // myVC.addTransition(direction:.fromTop, controller: myVC)
        getRootViewController().pushViewController(myVC, animated: false)
    }
    
//    static func tutorVC() {
////        let tutorController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
////        getRootViewController().pushViewController(tutorController, animated: false)
//    }
//
//static func tuteeVC() {
//    let tuteeController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
//    getRootViewController().pushViewController(tuteeController, animated: false)
//}
    
    static func getRootViewController() -> UINavigationController {
        let navController = UINavigationController()
        self.getWindow()?.rootViewController = navController
        return navController
    }
    
    static private func getWindow() -> UIWindow? {
        var window: UIWindow? = nil
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            window = appDelegate.window
        }
        return window
    }
}
