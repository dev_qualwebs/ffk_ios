//
//  Singleton.swift
//  Diamonium
//
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

class Singleton {
    
    static let shared = Singleton()
    var userDetail = getUser()
    var cityName:String?
    var address:String?
    var seletedTime:String?
    var selectedRewardCoupenId: Int?
    var selectedLocation = getSelectedRestaurant()
    var selectedTimeArr = [String]()
    var restaurentLocation = getLocation()
    var selectedLocationPref = getSelectedPref()
    var mainMenuData = getMenuTypeData()
    var breakfastTimeMessage = String()
    var lunchTimeMessage = String()
    var lunchMenu = getLunchData()
    var breakfastMenu = getBreakfastData()
    var recentOrderHistory = [OrderResponse]()
    var activeOrderData = [OrderResponse]()
    var singleArrayContent = [SplashContent]()
    var favouriteLabel = [GetFavouriteResponse]()
    var myRewards = GetRewardResponse()
    var orderTotal = String()
    var orderSubTotal = String()
    var sideMenuBonus = [GetBonusResponse]()
    var sideMenuReward = RewardResponse()
    var prefrenceData = GetPreferenceResponse()
    //var selectedBonus = GetBonusResponse()
    var mealsData = [GetMealsResponse]()
    var paymentCards = [GetCardResponse]()
    var cardDetails = GetCart()
    
    
    public static func getLocation() -> [LocationResponse]{
        if let info = UserDefaults.standard.data(forKey: UD_FFK_RESTAURENT){
            let locData = info
            let loc = try! JSONDecoder().decode([LocationResponse].self, from: locData)
            return loc
        }else {
            return [LocationResponse]()
        }
    }
    
    public static func getSelectedRestaurant() -> LocationResponse{
        if let info = UserDefaults.standard.data(forKey: UD_RESTAURANT_SELECTED){
            let locData = info
            let loc = try! JSONDecoder().decode(LocationResponse.self, from: locData)
            return loc
        }else {
            return LocationResponse()
        }
    }
    
    public static func getSelectedPref() -> Int{
        if let info = UserDefaults.standard.data(forKey: UD_SELECTED_LOCATION_PREF) as? Int{
            return info
        }else {
            return 1
        }
    }
    
    public static func getBreakfastData() -> [GetBreakfastLunch]{
        if let info = UserDefaults.standard.data(forKey: UD_BREAkFAST_ITEMS){
            let breakfastData = info
            let data = try! JSONDecoder().decode([GetBreakfastLunch].self, from: breakfastData)
            return data
        }else {
            return [GetBreakfastLunch]()
        }
    }
   
    
    public static func getLunchData() -> [GetBreakfastLunch]{
        if let info = UserDefaults.standard.data(forKey: UD_LUNCH_ITEMS){
            let lunchData = info
            let lunch = try! JSONDecoder().decode([GetBreakfastLunch].self, from: lunchData)
            return lunch
        }else {
            return [GetBreakfastLunch]()
        }
    }
    
    public static func getMenuTypeData() -> [GetMenuResponse]{
        if let info = UserDefaults.standard.data(forKey: UD_MENU_TYPE){
            let menuData = info
            let data = try! JSONDecoder().decode([GetMenuResponse].self, from: menuData)
            return data
        }else {
            return [GetMenuResponse]()
        }
    }
    
    public static func getUser() -> SignupResponse{
        if let info = UserDefaults.standard.data(forKey: UD_USER_DETAIl){
            let placeData = info
            if let placeArray = try? JSONDecoder().decode(SignupResponse.self, from: placeData){
            return placeArray
            }else {
                return SignupResponse()
            }
        }else {
            return SignupResponse()
        }
    }
    
    func initialiseValues(){
        UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        UserDefaults.standard.removeObject(forKey: UD_USER_DETAIl)
        UserDefaults.standard.removeObject(forKey: UD_CATEGORY_TYPE)
        UserDefaults.standard.removeObject(forKey: UD_ADD_TO_CART)
        UserDefaults.standard.removeObject(forKey: UD_SELECTED_LOCATION_PREF)
        UserDefaults.standard.removeObject(forKey: UD_RESTAURANT_SELECTED)
        UserDefaults.standard.removeObject(forKey: UD_SINGLE_ACTIVE_ORDER)
        UserDefaults.standard.removeObject(forKey: UD_ACTIVE_ORDER_EXIST)
        
        userDetail = Singleton.getUser()
        cityName = String()
        address = String()
        selectedRewardCoupenId = nil
        seletedTime = String()
        recentOrderHistory = [OrderResponse]()
        activeOrderData = []
        singleArrayContent = []
        paymentCards = []
        mealsData = []
        cardDetails = GetCart()
        favouriteLabel = [GetFavouriteResponse]()
        myRewards = GetRewardResponse()
        //selectedLocation = Singleton.getLocation()
        orderTotal = String()
        prefrenceData = GetPreferenceResponse()
        
        sideMenuBonus = [GetBonusResponse]()
        sideMenuReward = RewardResponse()
    }
}
