
import UIKit
protocol PageContentIndexDelegate {
    func getContentIndex(index: Int)
}

class SinglePageViewController: UIPageViewController {

    static var dataSource1: UIPageViewControllerDataSource?
    static var indexDelegate: PageContentIndexDelegate? = nil
    
    var currentIndex: Int = 0
    var arrayContent: [SplashContent]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        arrayContent = Singleton.shared.singleArrayContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getContent()
        SinglePageViewController.dataSource1 = self
    }
        
    func getContent() {
        if let content = contentAtIndex(index: currentIndex){
            self.setViewControllers([content], direction: .forward, animated: false, completion: nil)
            SinglePageViewController.indexDelegate?.getContentIndex(index: currentIndex)
        }
    }
    
    func contentAtIndex(index: Int) -> UIViewController? {
        if (arrayContent?.count == 0) || (index >= (arrayContent?.count)!) {
            return nil
        }
        
            let splashContent = self.storyboard?.instantiateViewController(withIdentifier: "SplashContentViewController") as! SplashContentViewController
            guard let content = arrayContent as? [SplashContent] else { return nil }
            splashContent.content = content[index]
            splashContent.currentIndex = index
            return splashContent
    }
    
    func viewAtIndex(_ viewController: UIViewController, next: Bool) -> UIViewController? {
        var index:Int = 0
        if let pageContent: SplashContentViewController = viewController as? SplashContentViewController{
            index = pageContent.currentIndex
            if next {
                if (index == NSNotFound) {
                    return nil
                }
                index += 1
                if index == arrayContent?.count {
                    return nil
                }
            } else {
                if ((index == 0) || (index == NSNotFound)) {
                    return nil
                }
                index -= 1
            }
            return contentAtIndex(index: index)
        }else {
            return nil
        }
    }
}
extension SinglePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: false)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: true)
    }
}
extension SinglePageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            let currentVC: SplashContentViewController = pageViewController.viewControllers?[0] as! SplashContentViewController
            currentIndex = currentVC.currentIndex
        SinglePageViewController.indexDelegate?.getContentIndex(index: currentIndex)
    }
}

extension UIPageViewController {
    func goToPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil, next: Bool) {
        if let currentViewController = viewControllers?[0] {
            if next{
                if let nextPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerAfter: currentViewController) {
                    setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
                }
            }else{
                if let previousPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerBefore: currentViewController) {
                    setViewControllers([previousPage], direction: .reverse, animated: animated, completion: completion)
                }
            }
        }
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
}
