//
//  APIManager.swift
//
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit
import Alamofire


class SessionManager: NSObject {
    
    static var shared = SessionManager()
    
    var createWallet: Bool = true
    
    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode, userToken: userToken))")
        
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode, userToken: userToken), interceptor: nil).responseString { (dataResponse) in
            
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")
            
            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode != 400 && requestCode != U_GUEST_CHECKOUT){
                    object = self.convertDataToObject(response: dataResponse.data, T.self)
                }else if(statusCode == 200 && requestCode == U_GUEST_CHECKOUT){
                    object = self.convertDataToObject(response: dataResponse.data, T.self)
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    completionHandler(object!)
                } else if statusCode == 404  {
                    if(errorObject?.message != "" || errorObject?.message != nil){
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    }
                    //  NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                    //self.showAlert(msg: errorObject?.message)
                }else if statusCode == 400{
                    if(requestCode == U_GET_CART_DATA){
//                        ActivityIndicator.hide()
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil, userInfo: ["loginType": "facebook"])
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    }else if(requestCode == U_FB_LOGIN){
                        ActivityIndicator.hide()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil, userInfo: ["loginType": "facebook"])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    }else if(requestCode == U_GOOGLE_LOGIN){
                        ActivityIndicator.hide()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil, userInfo: ["loginType": "google"])
                    }else if(requestCode == U_APPLE_LOGIN){
                        ActivityIndicator.hide()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil, userInfo: ["loginType": "apple"])
                    }else if(requestCode == U_CHECK_ITEM_IN_STOCK){
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_HANDLE_ITEM_OUTOFSTOCK), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    }else if(requestCode == U_VALIDATE_MENU_TIMING){
                        if(errorObject?.message != ""){
                            if(Singleton.shared.restaurentLocation.count > 0){
                            if(Singleton.shared.selectedLocation.id == Singleton.shared.restaurentLocation[0].id){
                                if(K_KITCHEN_ONLINE_ONE == "1"){
                                    if(MENU_TYPE == K_BREAK_ID){
                                        if(errorObject?.message?.contains("Lunch") == false){
                                        Singleton.shared.breakfastTimeMessage = errorObject?.message ?? ""
                                        }
                                    }else {
                                        if(errorObject?.message?.contains("Breakfast") == false){
                                        Singleton.shared.lunchTimeMessage = errorObject?.message ?? ""
                                        }
                                    }
                                    NotificationCenter.default.post(name: NSNotification.Name(N_TIME_OBSERVER), object: ["message": errorObject?.message ?? ""])
                                }else {
                                    Singleton.shared.breakfastTimeMessage = errorObject?.message ?? ""
                                    Singleton.shared.lunchTimeMessage = errorObject?.message ?? ""
                                    NotificationCenter.default.post(name: NSNotification.Name(N_TIME_OBSERVER), object: ["message": errorObject?.message ?? ""])
                                }
                            }else {
                                if(K_KITCHEN_ONLINE_TWO == "1"){
                                    if(MENU_TYPE == K_BREAK_ID){
                                        if(errorObject?.message?.contains("Lunch") == false){
                                        Singleton.shared.breakfastTimeMessage = errorObject?.message ?? ""
                                        }
                                    }else {
                                        if(errorObject?.message?.contains("Breakfast") == false){
                                        Singleton.shared.lunchTimeMessage = errorObject?.message ?? ""
                                        }
                                    }
                                    NotificationCenter.default.post(name: NSNotification.Name(N_TIME_OBSERVER), object: ["message": errorObject?.message ?? ""])
                                }else {
                                    Singleton.shared.breakfastTimeMessage = errorObject?.message ?? ""
                                    Singleton.shared.lunchTimeMessage = errorObject?.message ?? ""
                                    NotificationCenter.default.post(name: NSNotification.Name(N_TIME_OBSERVER), object: ["message": errorObject?.message ?? ""])
                                }
                            }
                            }
                        }
                    }else {
                        if(errorObject?.message != "" || errorObject?.message != nil){
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                        }
                    }
                }else if(statusCode == 401){
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_LOGOUT_USER), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                }else if (statusCode == 429) {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": "Too many requests"])
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
                ActivityIndicator.hide()
                if(statusCode == 200 && requestCode == U_VALIDATE_MENU_TIMING){
                    if(Singleton.shared.restaurentLocation.count > 0){
                    if(Singleton.shared.selectedLocation.id == Singleton.shared.restaurentLocation[0].id){
                        if(K_KITCHEN_ONLINE_ONE == "1"){
                            if(MENU_TYPE == K_LUNCH_ID){
                                Singleton.shared.lunchTimeMessage = ""
                            }else if(MENU_TYPE == K_BREAK_ID){
                                Singleton.shared.breakfastTimeMessage = ""
                            }
                            completionHandler(SuccessResponse(message: "test", status: 200) as! T)
                        }
                    }else {
                        if(K_KITCHEN_ONLINE_TWO == "1"){
                            if(MENU_TYPE == K_LUNCH_ID){
                                Singleton.shared.lunchTimeMessage = ""
                            }else if(MENU_TYPE == K_BREAK_ID){
                                Singleton.shared.breakfastTimeMessage = ""
                            }
                            completionHandler(SuccessResponse(message: "test", status: 200) as! T)
                        }
                    }
                    }
                  
                    break
                }
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])
                    }
                    //  NavigationController.shared.showAlertScreen(message: error ?? "")
                    //self.showAlert(msg: error)
                    
                } else {
                    //Showing error message on alert
                    // self.showAlert(msg: error)
                    
                }
                break
            }
        }
    }
    
    private func showAlert(msg: String?) {
        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title:"", message: msg, action1Name: "Ok", action2Name: nil)
        
    }
    
    
    private func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    
    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        var token = UserDefaults.standard.string(forKey: UD_TOKEN)
        if (reqCode != U_LOGIN) && (reqCode != U_REGISTER) && (reqCode != U_GET_MENU) && (reqCode != U_GET_RESTAURENTS) && (reqCode != U_LOGIN) && (reqCode != U_GET_MENU_TYPE){
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + token!]
            }
        } else {
            return nil
        }
    }
}
