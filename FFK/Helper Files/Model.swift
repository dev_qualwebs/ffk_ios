//
//  Model.swift
//
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit

struct MenuObject {
    var image: UIImage?
    var name: String?
}

struct Response: Codable {
    var message: String?
    var response: String?
    var status: String?
}

struct SuccessResponse: Codable {
    var message: String?
    var status: Int?
}

struct CarKnoxResponse: Codable {
    var xToken: String?
    var xCardType: String?
    var xError: String?
}

struct ErrorResponse: Codable {
    var message: String?
    var status: Int?
}


struct SignUp: Codable {
    var message: String?
    var status: Int?
    var response: SignupResponse
}

struct SignupResponse: Codable {
    var id:Int?
    var first_name: String?
    var last_name: String?
    var email: String?
    var mobile: String?
    var zip_code: String?
    //var date_of_birth:Int?
    var date_of_birth:String?
    var is_account_deleted: Int?
    var google_id: String?
    var facebook_id: String?
    var latitude: String?
    var longitude: String?
    var created_at: String?
    var restaurant_preference: Int?
    var token: String?
}

struct UserInfo: Codable {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var email: String?
    var mobile: String?
    var zip_code: String?
   // var date_of_birth: Int?
    var date_of_birth: String?
}

struct GetMenu: Codable {
    var response: [GetMenuResponse]
    var message: String?
    var status: Int?
}

struct GetMenuResponse: Codable{
    var menu_id:Int?
    var restaurant_id: Int?
    var menu_name:String?
    var from:Int?
    var slot_start: Int?
    var to:Int?
}


struct GetUserInfo: Codable {
    var response: UserInfo
    var message: String?
    var status: Int?
}

struct GetBreakLunchResonse: Codable{
    var response: [GetBreakfastLunch]
    var message: String?
    var status: Int?
}

struct GetBreakfastLunch: Codable{
    var menu_id:Int?
    var category_id:Int?
    var menu_type_id: Int?
    var side_menu_id: Int?
    var category_name:String?
    var menus:[getMenusList]
}

struct getMenusList: Codable
{
    var item_id:Int?
    var category_id:Int?
    var item_name:String?
    var item_price:Double?
    var item_image:String?
    var modifiers: Int?
    var thumbnail:String?
    var is_common: Int?
   // var tax_rate:Double?
    var item_description:String?
    var reward_item: Int?
    //var its_own:Int?
   // var complete_meal_of: Int?
}




struct GetSubmenu: Codable {
    var response: [SubMenuResponse]
    var message: String?
    var status: Int?
}

struct SubMenuResponse: Codable {
    var id:Int?
    var welcome_bonus_item_id: Int?
    var modifier_group_id:Int?
    var item_id:Int?
    var added_from: Int?
    var item_exactly:Int?
    var item_range_from: Int?
    var item_count: Int?
    var item_price: Double?
    var item_range_to: Int?
    var restaurant_id: Int?
    var item_maximum: Int?
    var single_item_maximum: Int?
    var modifier_group_name:String?
    var is_rule:Int?
    var sectionItemCount: Int?
    var meals:[SubmenuSubCategory]
}

struct SubmenuSubCategory: Codable {
    var id:Int?
    var cart_item_id:Int?
    var item_id: Int?
    var side_menu_id:Int?
    var modifier_group_id: Int?
    var category_id:Int?
    var item_name:String?
    var item_price:QuantumValue?
    var item_image:String?
    var thumbnail:String?
  //  var tax_rate:Double?
    var item_description:String?
    var its_own:Int?
    var isSelected:Int?
    var itemCount:Int?
    var count: Int?
    var restaurant_id: Int?
   // var modifier_item_count: Int?
    var item_count: Int?
}

struct GetLocation: Codable {
    var response: [LocationResponse]
   // var message: String?
    var status: Int?
}

struct GetLocationDetail: Codable {
    var response: LocationResponse
    var message: String?
    var status: Int?
}

struct LocationResponse: Codable {
    var id: Int?
    var placeId: String?
    var name: String?
    var address: String?
    var latitude: String?
    var longitude: String?
    var isSelected: Bool?
    var price: Double?
    var image: String?
    var payment_gateway: String?
    var is_opened: Int?
    //var tax_rate: Double?
    var favorite: FavoriteMark?
    var description: String?
    var complete_meal_of: String?
    var contact_number: String?
    var distance: Double?
    //var restaurant_id: Int?
    var additional_info: String?
}

struct FavoriteMark: Codable {
    var is_favorite: Int?
}

struct CardItems: Codable {
        var user_id:Int
        var cart_data:[CartData]
        var country:String
    init(user_id: Int,cart_data:[CartData],country: String) {
        self.user_id = user_id
        self.cart_data  = cart_data
        self.country = country
    }
}

struct CartData: Codable {
   var  main_menu_id:Int
    var side_menu: [SideMenu]
    init(main_menu_id: Int,side_menu:[SideMenu]) {
        self.main_menu_id = main_menu_id
        self.side_menu  = side_menu
    }
}

struct SideMenu: Codable {
    var id: Int
    var category_id: Int
    init(id: Int,category_id:Int) {
        self.id = id
        self.category_id  = category_id
    }
}

struct ConfirmOrder: Codable {
    var menu_id: Int?
    var modifier_items: [AddToCart]
}

struct JsonConfirmOrder: Codable {
    var menu_id: Int?
    var modifier_items: [String]
}

struct AddToCart: Codable{
    var modifier_id: String?
    var modifier_item_id: String?
    var modifier_item_count:String?
}

struct AddToCartResponse: Codable {
    var response:String
    var message: String
    var status: Int?
}

struct GetCart: Codable {
    var response = MyCartData()
    var status = Int()
    var message = String()
}

struct MyCartData: Codable {
    var cart_data = [GetCartResponse]()
    var cart_details = CartDetailResponse()
}

struct CartDetailResponse: Codable {
    var cart_list_id: Int?
    var cart_id: String?
    var total_tax: String?
    var order_total: String?
    var total_amount: String?
    var is_birthday: Int?
    var is_reward_coupon: Int?
    var is_admin_reward: Int?
    var is_bonus: Int?
    var bonus_id: Int?
    var is_reward_added: Int?
    var discount_amount: String?
    var coupon_id: Int?
    var coupon_type: Int?
}

struct GetCartResponse: Codable {
    var id: Int?
    var cart_item_id: Int?
    var receiver_name: String?
    var cart_list_id: Int?
    var item_id: Int?
    var user_id: Int?
    var menu_id: Int?
    var menu_price: Int?
    var item_flag: Int?
    var bonus_id: Int?
    var cart_id: String?
    var modifier_id: Int?
    var modifier_item_id: Int?
    var modifier_item_count: Int?
    var modifier_item_price: Int?
    var item_name: String?
    var item_price: Double?
    var item_image: String?
   // var tax_rate: Double?
    
    var item_description: String?
   // var meals: [SubmenuSubCategory]
    var details: [SubmenuSubCategory]
}


struct GetMeals: Codable{
    var response: [GetMealsResponse]
    var message: String?
    var status: Int?
}

struct GetMealsResponse: Codable{
    var id:Int?
    var cart_item_id: Int?
    var cart_list_id: Int?
    var item_flag: Int?
    var item_id: Int?
    var menu_id: Int?
    var meal: OrderMeals?
}

struct OrderMeals: Codable
{
    var item_id:Int?
    var modifiers:Int?
    var side_menu_id:Int?
    var item_name:String?
    var item_price:Double?
    var item_image:String?
    var thumbnail:String?
   // var tax_rate:Double?
    var description:String?
    var isSelected: Int?
    var its_own:Int?
}

struct GetOrder: Codable {
    var response = CurrentOrderStatus()
    var message: String?
    var status: Int?
}

struct CurrentOrderStatus: Codable {
    var active = [OrderResponse]()
    var past = [OrderResponse]()
}

struct GetSingleOrder: Codable {
    var response = [OrderResponse]()
    var message: String?
}

struct GuestCheckoutResponse: Codable{
    var response : OrderResponse?
      var message: String?
}

struct OrderResponse: Codable{
    var order_id: Int?
    var menu_name: String?
    var reference_id: String?
    var restaurant_name: String?
    var restaurant_address: String?
    var restaurant_contact_number: String?
    var pickup_time: Int?
    var menu_id: Int?
    var total_tax: String?
    var order_total: String?
    var discount_amount: String?
    var total_amount: String?
    var status: Int?
    var address: String?
    var item_price: Double?
    var is_favorite: Int?
    var favorite_order_id: Int?
    var label_name: String?
    var favorite_label_id: Int?
    var total_rewards: Double?
    var reward_coupons_status: Int?
    var coupon_type: Int?
    var coupon_id: Int?
    var bonus_id: Int?
    var order_date: Int?
    var is_common: Int?
    var is_in_stock:Int?
    var order_details: [OrderDetail]?
    var feedback: FeedbackResponse?
}

struct FeedbackResponse: Codable {
    var feedback: Int?
    var order_id: Int?
    var review: String?
}

struct OrderDetail: Codable{
    var order_detail_id: Int?
    var order_id: Int?
    var restaurant_id: Int?
    var item_id: Int?
    var item_count: Int?
    var item_name: String?
    var item_price: QuantumValue?
   // var tax_rate: Double?
    var item_image: String?
    var order_item: [OrderDetail]?
}

struct GetFavouriteLabel: Codable {
    var response: [GetFavouriteResponse]
}

struct GetFavouriteResponse: Codable {
    var favorite_label_id: Int?
    var label_name: String?
}

struct GetRewards: Codable {
    var response = GetRewardResponse()
    var message: String?
    var status: Int?
}

struct GetRewardResponse: Codable {
    var total_rewards: TotalReward?
    var list = [RewardList]()
}

struct TotalReward: Codable {
    var points: String?
}

struct RewardList: Codable {
    var month: Int?
    var month_name: String?
    var rewards = [GetRewardList]()
}

struct GetRewardList: Codable {
    var order_date: Int?
    var reward_id: Int?
    var order_id: Int?
    var user_id: Int?
    var month: Int?
    var type: Int?
    var total_rewards: Int?
}

struct OrderConfiremed: Codable {
    var response: OrderConfirmedResponse?
    var message: String?
}

struct OrderConfirmedResponse: Codable {
    var order_id: Int?
    var total_rewards: Double?
}


struct GetRewardCoupen: Codable {
    var response = RewardResponse()
    var message:String?
    var status: Int?
}

struct RewardResponse: Codable{
  var order_rewards_coupons =  [RewardCoupenRespose]()
    var birthday_coupons =  [RewardCoupenRespose]()
    var admin_reward_coupon = [RewardCoupenRespose]()
}


struct RewardCoupenRespose: Codable {
    var coupon_id: Int?
    var user_id: Int?
    var expiry: Int?
    var name: String?
   // var created_at: String?
    var status: Int?
    var order_date: Int?
}

struct GetRewardItems: Codable {
    var response = [RewardItemsRespose]()
    var message: String?
    var status: Int?
}

struct RewardItems: Codable {
    var order_rewards_items = [RewardItemsRespose]()
    var birthday_items = [RewardItemsRespose]()
    var admin_reward_items = [RewardItemsRespose]()
}

struct RewardItemsRespose: Codable {
    var reward_item_id: Int?
    var item_id: Int?
    var restaurant_id: Int?
    var is_enable: Int?
    var flag: Int?
    var category_id: Int?
    var item: getMenusList?
}

struct GetPReference: Codable {
    var response = GetPreferenceResponse()
    var message: String?
    var status: Int?
}

struct GetPreferenceResponse: Codable {
    var subscription_preference:SubscriptionPreferenceResponse?
    var restaurant_preference: Int?
    var device_preference: DevicePreferenceResponse?
    
}

struct DevicePreferenceResponse: Codable {
    var push_notification: Int?
}

struct SubscriptionPreferenceResponse: Codable {
    var email_subscription: Int?
    var phone_number_subscription: Int?
}

struct GetBonus: Codable{
    var response = BonusOfferData()
    var message: String?
    var status: Int?
}
struct BonusOfferData: Codable {
    var bonus = [GetBonusResponse]()
    var offer = [GetBonusResponse]()
}

struct GetBonusResponse: Codable {
    var bonus_id: Int?
    var user_id: Int?
    var bonus_type: Int?
    var bonus_condition_type: Int?
    var bonus_name: String?
    var bonus_expiry: String?
    var bonus_expiry_unix: String?
    var notification_text: String?
    var description: String?
    var term_and_condition: String?
    var bonus_free_item_id: String?
    var bonus_extra_point: String?
    var bonus_points_multiplier: String?
    var bonus_discount: String?
    var bonus_orders_no: String?
    var bonus_start_date: String?
    var bonus_end_date: String?
    var bonus_plates_no: String?
    var bonus_user_points: String?
    var item_name: String?
    var item_price: Double?
    var item_image: String?
    var item_description: String?
    var restaurant_id: Int?
}

struct SplashContent: Codable {
    var pickupTime: Int?
    var address: String?
}

struct GetCard: Codable {
    var response: [GetCardResponse]
    var message: String?
    var status: Int?
}

struct GetCardResponse: Codable {
    var id: Int?
    var user_id: Int?
    var token: String?
    var type: String?
    var name: String?
    var is_default: Int?
    var expiry_month: String?
    var expiry_year: String?
    var issue_number: String?
    var start_month: String?
    var start_year: String?
    var card_type: String?
    var masked_card_number: String?
    var card_scheme_type: String?
    var card_scheme_name: String?
    var card_issuer: String?
    var country_code: String?
    var card_class: String?
}

struct ApplePay: Codable{
    var version: String?
    var data: String?
    var signature: String?
    var header:AppleHeader
}

struct AppleHeader: Codable{
    var ephemeralPublicKey: String?
    var publicKeyHash: String?
    var transactionId: String?
}

struct NotificationType: Codable {
    var notification_type: Int?
    var order_id:Int?
}

struct GetBonusItem: Codable {
    var response: [BonusItemResponse]
    var message: String?
    var status: Int?
}

struct BonusItemResponse: Codable {
    var id:Int?
    var welcome_bonus_item_id: Int?
    var modifier_group_id:Int?
    var item_id:Int?
    var item_name: String?
    var item_image: String?
    var item_description: String?
    var added_from: Int?
    var item_exactly:Int?
    var item_range_from: Int?
    var item_count: Int?
    var item_range_to: Int?
    var restaurant_id: Int?
    var item_maximum: Int?
    var single_item_maximum: Int?
    var modifier_group_name:String?
    var is_rule:Int?
    var sectionItemCount: Int?
}

struct GetNotification: Codable {
    var response = [NotificationResponse]()
    var message: String?
    var status: Int?
}

struct NotificationResponse: Codable {
    var id: Int?
    var user_id:  Int?
    var notification_title: String?
    var notification_content: String?
    var type_id: String?
    var type_text: String?
    var url: String?
    var created_at: String?
    var order_id: Int?
}

struct FoodItem {
    var name: String
    var price: Double
}

enum QuantumValue: Codable {
    
    var doubleValue: Double? {
           switch self {
           case .int(let value): return Double(value ?? 0)
           case .string(let value): return Double(value ?? "0")
           case .double(let value): return value
           default:
               return 0
           }
    }
    
    var stringValue: String? {
           switch self {
           case .int(let value): return "\(value ?? 0)"
           case .string(let value): return value
           case .double(let value): return String(value ?? 0)
           default:
               return "0"
           }
    }
   
    func encode(to encoder: Encoder) throws {
        
    }
    

    case bool(Bool), string(String), int(Int), double(Double)

    init(from decoder: Decoder) throws {
        if let bool = try? decoder.singleValueContainer().decode(Bool.self) {
            self = .bool(bool)
            return
        }

        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }
        
        if let double = try? decoder.singleValueContainer().decode(Double.self) {
            self = .double(double)
            return
        }
        
        throw QuantumError.missingValue
    }

    enum QuantumError:Error {
        case missingValue
    }
}

