import UIKit

protocol ControllerIndexDelegate {
    func getControllerIndex(index: Int)
}

var currentPageView = K_MENU_PAGE_VIEW

class PageViewController: UIPageViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }
    
    
    //MARK: Properties
    var pageControl: UIPageControl?
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    lazy var viewControllerList = [UIViewController]()
    var sb = UIStoryboard(name:"Main",bundle:nil)
    static var index_delegate: ControllerIndexDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        PageViewController.customDataSource = self
        if(currentPageView == K_MENU_PAGE_VIEW){
            viewControllerList = [sb.instantiateViewController(withIdentifier: "BreaFastViewController" ),
                    sb.instantiateViewController(withIdentifier: "LunchViewController" )]
        }else {
            viewControllerList = [sb.instantiateViewController(withIdentifier: "LocationViewController" ),
                    sb.instantiateViewController(withIdentifier: "LocationDetailVC" )]
        }
        
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController],direction: .forward,
                                    animated:false,completion:nil)
        }
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                
                subView.isScrollEnabled = !(currentPageView == K_MENU_PAGE_VIEW)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    func setFirstController() {
        setViewControllers([viewControllerList[0]], direction: .reverse, animated: true, completion: nil)
        PageViewController.index_delegate?.getControllerIndex(index: 0)
        self.pageControl?.currentPage = 0
    }
    
    func setSecondController() {
        setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)
        PageViewController.index_delegate?.getControllerIndex(index: 1)
        self.pageControl?.currentPage = 1
    }
    
   
    
    func setThirdController() {
        setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 2
    }
    
 
    //Assigning controller manually
    func nextController(index: Int, direction: UIPageViewController.NavigationDirection) {
        setViewControllers([viewControllerList[index]], direction: direction, animated: true, completion: nil)
        PageViewController.index_delegate?.getControllerIndex(index: index)
    }
    
    
    
    //Getting current controller index
    func currentControllerIndex(VC: UIViewController) {
        if let viewControllerIndex = viewControllerList.index(of: VC) {
            PageViewController.index_delegate?.getControllerIndex(index: viewControllerIndex)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
}

extension PageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
           
        return viewControllerList[nextIndex]
    }
}

extension PageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
       
//        if(viewControllerList.index(of: pageContentViewController)! == 1){
//            NotificationCenter.default.post(name: NSNotification.Name(N_LUNCH_TAB_COLOR), object: nil,userInfo: ["index": 1])
//        }else {
//               NotificationCenter.default.post(name: NSNotification.Name(N_BF_TAB_COLOR), object: nil,userInfo: ["index": 0])
//        }
    }
}


