//
//  SplashContentViewController.swift
//  FFK
//
//  Created by qw on 02/01/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class SplashContentViewController: UIViewController, PageContentIndexDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var pickupLocation: UILabel!
    
    var currentIndex = Int()
    var content = SplashContent()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SinglePageViewController.indexDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getContentIndex(index: currentIndex)
    }
    
    func getContentIndex(index: Int) {
        self.currentIndex = index
        self.pickupTime.text = self.convertTimestampToDate(content.pickupTime ?? 0, to: "h:mm a").lowercased()
        if let loc = content.address?.components(separatedBy: ","){
                   if(loc.count > 0){
                       let location = content.address?.replacingOccurrences(of: loc[0] + ", ", with: "")
                       self.pickupLocation.text = loc[0] + "\n" + (location ?? "")
                   }
               }
    }
    
    //MARK: IBAction
    @IBAction func orderDetailAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderReceivedViewController") as! OrderReceivedViewController
        myVC.orderId = Singleton.shared.activeOrderData[self.currentIndex].order_id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
