//
//  FeedbackViewController.swift
//  FFK
//
//  Created by qw on 03/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import StoreKit
import FaveButton


class FeedbackViewController: UIViewController,FaveButtonDelegate {
    //MARK: IBOutlets
    
    @IBOutlet weak var thumbView: View!
    @IBOutlet weak var rateusView: View!
    //  @IBOutlet weak var dislikeButton: UIButton!
    // @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var feedbackText: View!
    @IBOutlet weak var dislikeButton: FaveButton!
    @IBOutlet weak var likeButton: FaveButton!
    @IBOutlet weak var feedBack: UITextView!
    
    var orderId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.likeButton.delegate = self
        
        // self.likeButton.tintColor = primaryColor
        //self.likeButton.selectedColor = primaryColor
        //self.likeButton.normalColor = primaryColor
        self.dislikeButton.delegate = self
        self.dislikeButton.selectedColor = .lightGray
        self.dislikeButton.tintColor = .clear
        self.likeButton.isSelected = false
        self.thumbView.isHidden = false
        self.rateusView.isHidden = true
    }
    
    func callFeedbackAPI(val: Int){
        let param = [
            "order_id":self.orderId ,
            "feedback": val,
            "review": self.feedBack.text ?? "",
            ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ORDER_FEEDBACK, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ORDER_FEEDBACK, userToken: nil) { (response)  in
            self.thumbView.isHidden = true
            self.feedbackText.isHidden = true
            if (self.feedBack.text != ""){
                self.dismiss(animated: true) {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_RATED), object: nil)
                    }
                }
                return
            }
            if let rated = UserDefaults.standard.value(forKey: UD_APP_RATED) as? Bool{
                if(rated){
                    self.dismiss(animated: true) {
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_RATED), object: nil)
                        }
                    }
                }else{
                    self.rateusView.isHidden = false
                }
            }else {
                self.rateusView.isHidden = false
            }
            
            NavigationController.shared.getRecentOrder(nil)
            
        }
    }
    
    func faveButton(_ faveButton: FaveButton, didSelected selected: Bool) {
        print(selected)
    }
    
    //MARK: IBACtions
    @IBAction func submitFeedback(_ sender: Any) {
        if(feedBack.text!.isEmpty){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.heading = "Please give your feedback."
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }else {
            self.callFeedbackAPI(val: 2)
        }
        
    }
    
    @IBAction func dislike(_ sender: UIButton) {
        self.thumbView.isHidden = true
        self.rateusView.isHidden = true
        self.feedbackText.isHidden = false
    }
    
    
    @IBAction func rateApp(_ sender: Any) {
        
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        }else {
            let urlStr = "https://itunes.apple.com/app/id\(1486732896)"
            "https://itunes.apple.com/app/id\(1486732896)?action=write-review"
            
            guard let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) else { return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url) // openURL(_:) is deprecated from iOS 10.
            }
        }
        self.dismiss(animated: true) {
            DispatchQueue.main.async {
                UserDefaults.standard.setValue(true, forKey: UD_APP_RATED)
                NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_RATED), object: nil)
            }
        }
    }
    
    @IBAction func like(_ sender: UIButton) {
        self.callFeedbackAPI(val: 1)
    }
    
    @IBAction func notNow(_ sender: Any) {
        NavigationController.shared.getRecentOrder(nil)
        self.dismiss(animated: true) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.heading = "Thank you!"
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_RATED), object: nil)
            }
        }
        
    }
}



