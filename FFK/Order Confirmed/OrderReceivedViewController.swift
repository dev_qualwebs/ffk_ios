//
//  OrderReceivedViewController.swift
//  FFK
//
//  Created by AM on 14/10/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import TransitionButton
import Lottie

class OrderReceivedViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var linrOrederPrepared: UIView!
    @IBOutlet weak var lineOrderReceived: UIView!
    @IBOutlet weak var pickUpLocation: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    @IBOutlet weak var pointsEarned: UILabel!
    @IBOutlet weak var orderTable: ContentSizedTableView!
    
    @IBOutlet weak var labelReceived: UILabel!
    @IBOutlet weak var ImageReceived: UIImageView!
    @IBOutlet weak var labelReadyToPick: UILabel!
    @IBOutlet weak var imageReadyToPick: UIImageView!
    @IBOutlet weak var imagePickedup: UIImageView!
    @IBOutlet weak var labelPickedUp: UILabel!
    
    @IBOutlet weak var processMainView: UIView!
    @IBOutlet weak var processAmination: UIView!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var orderSubtotal: UILabel!
    @IBOutlet weak var orderTax: UILabel!
    @IBOutlet weak var finalTotal: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
    @IBOutlet weak var restaurentAddress: UILabel!
    
    @IBOutlet weak var viewForBack: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewForDiscount: UIView!
    @IBOutlet weak var discountLabel: DesignableUILabel!
    @IBOutlet weak var discountAmount: UILabel!
    @IBOutlet weak var viewForRewadPoint: UIView!
    @IBOutlet weak var orderid: UILabel!
    @IBOutlet weak var lottieView: UIView!
    
    
    var orderData: OrderResponse?
    var orderId = Int()
    var taxRate  = Double()
    var totalPrice = Double()
    var subTotalPrice = Double()
    var isNavigateFromPayment = false
    var initialTouchPoint = CGPoint(x: 0, y: 0)
    var scroll: UIPanGestureRecognizer?
    var isGuestLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.backAction(_:)))
        //swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        
        self.view.backgroundColor = primaryColor
      //  self.processMainView.isHidden = true
        if(self.orderId != 0){
            self.getOrderData()
        }
        if(self.isGuestLogin){
            self.viewForRewadPoint.isHidden = true
            self.manageScreenData()
        }else{
            self.viewForRewadPoint.isHidden = false
        }
        
        self.processAnimation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_CHANGE_ORDERCART_VIEW), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        var lottieAnimation = LottieAnimationView()
        lottieAnimation = LottieAnimationView(name: "113889-transfer-successful-tick")
        lottieAnimation.center = self.lottieView.center
        lottieAnimation.frame = self.lottieView.bounds
        lottieAnimation.contentMode = .scaleToFill
        lottieAnimation.loopMode = .playOnce
        lottieAnimation.animationSpeed = 1
        self.lottieView.addSubview(lottieAnimation)
        lottieAnimation.play()
    }
    
    @objc func orderCompleted(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CHANGE_ORDERCART_VIEW), object: nil)
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: PaymentViewController.self){
                return true
            } else {
                return false
            }
        })
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_CHANGE_ORDERCART_VIEW), object: nil)
    }
    
    func processAnimation() {
        let lay = CAReplicatorLayer()
        lay.frame.size = self.processAmination.frame.size
        let bar = CALayer()
        bar.frame = CGRect(x: 0,y: 0,width: 13,height: 13)
        bar.cornerRadius = bar.frame.width/2
        bar.backgroundColor = primaryColor.cgColor
        lay.addSublayer(bar)
        lay.instanceCount = 3
        lay.instanceTransform = CATransform3DMakeTranslation(20, 0, 0)
        let anim = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        anim.fromValue = 1.0
        anim.toValue = 0.2
        anim.duration = 1
        anim.repeatCount = .infinity
        anim.isRemovedOnCompletion = false
        bar.add(anim, forKey: nil)
        lay.instanceDelay = anim.duration / Double(lay.instanceCount)
        self.processAmination.layer.addSublayer(CALayer(layer: lay))
        self.processAmination.layer.addSublayer(lay)
        lay.bounds = self.processAmination.bounds
    }
    
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //        return true
    //    }
    //
    //    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
    //        let touchPoint = sender.location(in: self.view?.window)
    //
    //
    //        if sender.state == UIGestureRecognizer.State.began {
    //            initialTouchPoint = touchPoint
    //        }else if sender.state == UIGestureRecognizer.State.changed {
    //            UIView.animate(withDuration: 0.3, animations: {
    //                self.dismiss(animated: true, completion: nil)
    //            })
    //        }else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
    //            self.dismiss(animated: true, completion: nil)
    //        }
    //    }
    
    
    func getOrderData() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SIGLE_ORDER + "\(orderId)", method: .get, parameter: nil, objectClass: GetSingleOrder.self, requestCode: U_GET_SIGLE_ORDER, userToken: nil) { (response) in
            if(response.response.count > 0){
                self.orderData = response.response[0]
                for val in response.response{
                    if(val.status == 3 ){
                        if(val.feedback == nil){
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                            myVC.modalPresentationStyle = .overCurrentContext
                            myVC.orderId = val.order_id ?? 0
                            NotificationCenter.default.addObserver(self, selector: #selector(self.manageOrderView(_:)), name: NSNotification.Name(N_ORDER_RATED), object: nil)
                            self.present(myVC, animated: true, completion: nil)
                        }
                        break
                    }
                }
                self.manageScreenData()
            }
            ActivityIndicator.hide()
        }
    }
    
    @objc func manageOrderView(_ notif: Notification){
        NotificationCenter.default.addObserver(self, selector: #selector(self.manageOrderView(_:)), name: NSNotification.Name(N_ORDER_RATED), object: nil)
        self.dismiss(animated: true, completion: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.heading = "Thank you!"
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: false, completion: nil)
        self.backAction(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.manageOrderView(_:)), name: NSNotification.Name(N_ORDER_RATED), object: nil)
    }
    
    func manageScreenData() {
        self.contactNumber.text = self.orderData?.restaurant_contact_number
        self.restaurentAddress.text  = self.orderData?.restaurant_address
        self.orderid.text = "#\(self.orderData?.order_id ?? 0)"
//        if(self.orderData?.status == 1){
//            self.processMainView.isHidden = false
//        }else {
//            self.processMainView.isHidden = true
//        }
        
        self.pickupLocation.text = self.orderData?.restaurant_name ?? ""
        let date = Date(timeIntervalSince1970: TimeInterval(self.orderData?.pickup_time ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "America/Chicago")
        dateFormatter.dateFormat = "EEE, h:mm a"
        let a = dateFormatter.string(from: date)
        self.pickupTime.text = a.replacingOccurrences(of: "PM", with: "pm")
        self.pickupTime.text = a.replacingOccurrences(of: "AM", with: "am")
        self.pointsEarned.text =  "\(Int(self.orderData?.total_rewards ?? 0))" + " points earned"
        if(self.orderData?.coupon_type == 2){
            self.discountLabel.text = "   Free Dessert   "
        }else {
            self.discountLabel.text = "   Free Entree   "
        }
        if((self.orderData?.total_rewards ?? 0) == 0){
            self.pointsEarned.text = ""
        }
        self.calculateCartPrice()
        //var dateInt = self.convertDateToTimestamp(Singleton.shared.seletedTime!, to: "h:mm a")
        var endTime = Date(timeIntervalSince1970: TimeInterval(self.orderData?.pickup_time ?? 0))
        endTime = endTime.addingTimeInterval(TimeInterval(-15.0 * 60.0))
        if((self.orderData?.pickup_time ?? 0) > Int(Date().timeIntervalSince1970)){
            self.labelPickedUp.textColor = primaryColor
            self.imagePickedup.image = UIImage(named:"check-mark")
            self.labelReceived.textColor = .lightGray
            self.ImageReceived.image = UIImage(named:"check-mark-1")
            self.labelReadyToPick.textColor = .lightGray
            self.imageReadyToPick.image = UIImage(named:"check-mark-1")
        }else if(Int(endTime.timeIntervalSince1970) < Int(Date().timeIntervalSince1970)) {
            self.labelPickedUp.textColor = .lightGray
            self.imagePickedup.image = UIImage(named:"check-mark-1")
            self.labelReceived.textColor = .lightGray
            self.ImageReceived.image = UIImage(named:"check-mark-1")
            self.labelReadyToPick.textColor = primaryColor
            self.imageReadyToPick.image = UIImage(named:"check-mark")
        }else if(Int(endTime.timeIntervalSince1970) >
            Int(Date().timeIntervalSince1970)){
            self.labelPickedUp.textColor = .lightGray
            self.imagePickedup.image = UIImage(named:"check-mark-1")
            self.labelReceived.textColor = primaryColor
            self.ImageReceived.image = UIImage(named:"check-mark")
            self.labelReadyToPick.textColor = .lightGray
            self.imageReadyToPick.image = UIImage(named:"check-mark-1")
        }
        orderTable.reloadData()
    }
    
    func calculateCartPrice() {
        let mySubtotal = Double(self.orderData?.total_amount ?? "0")! - Double(self.orderData?.total_tax ?? "0")!
        self.orderSubtotal.text = "$" + "\(Double(round(100*mySubtotal)/100))"
        if(((self.orderData?.coupon_id ?? 0) != 0) || ((self.orderData?.bonus_id ?? 0) != 0)){
            if((self.orderData?.coupon_id ?? 0) != 0){
                self.discountLabel.text = "   Free Entree   "
                self.viewForDiscount.isHidden = false
            }else if ((self.orderData?.bonus_id ?? 0) != 0){
                self.viewForDiscount.isHidden = false
                self.discountLabel.text = "   Bonus   "
            }
        }else {
            self.viewForDiscount.isHidden = true
        }

        if(self.orderData?.discount_amount != "0.00" || self.orderData?.discount_amount != "0.0" || self.orderData?.discount_amount != "0" || self.orderData?.discount_amount != nil){
            var amount = Double(self.orderData?.order_total ?? "0")!   - Double(self.orderData?.discount_amount ?? "0")!
            amount = Double(round(100*amount)/100)
            self.orderSubtotal.text = "$" +  "\(String(format: "%.2f", (amount > 0 ? amount:0)))"
            self.discountAmount.text = "-$" + (self.orderData?.discount_amount ?? "")
        }
        
        if(self.orderData?.total_amount == "0" || self.orderData?.total_amount == "0.0" || self.orderData?.total_amount == "0.00"){
            self.viewForRewadPoint.isHidden = true
        }else{
            self.viewForRewadPoint.isHidden = false
        }
        self.orderTax.text = "$" + (self.orderData?.total_tax ?? "0")
        self.orderTotal.text = "$" +  (self.orderData?.order_total ?? "0")
        self.finalTotal.text = "$" +  (self.orderData?.total_amount ?? "0")
    }
    
    //MARK: IBACtion
    @IBAction func callAction(_ sender: Any) {
        let phone = self.orderData?.restaurant_contact_number?
            .replacingOccurrences(of: "-", with: "")
        self.callNumber(phoneNumber:phone ?? "")
    }
    
    @IBAction func backAction(_ sender: Any) {
        if(isNavigateFromPayment){
            UserDefaults.standard.setValue("true", forKey: UD_LAUNCH_FIRST_TIME)
            NavigationController.shared.pushMenuFromReward(controller: self, direction: 1)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension OrderReceivedViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.orderData?.order_details?.count ?? 0)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurentTableViewCell") as! RestaurentTableViewCell
        let data = self.orderData?.order_details?[indexPath.row]
        cell.nameLabel.text = data?.item_name ?? ""
        
        var selectedItem = String()
        var cartPrice = Double()
        for val in (data?.order_item)! {
            var price: Double = 0
            if let double = val.item_price?.doubleValue {
                 price = double
            }
            if let string = val.item_price?.stringValue{
                price = Double(string)!
            }

            if(price == 0.0){
                let string: String?
                if(!(val.item_count == 0)){
                    string = (val.item_name ?? "") + " (" +  "\(val.item_count ?? 1)" + ")"
                }else {
                    string = (val.item_name ?? "")
                }
                selectedItem =  selectedItem + (selectedItem == "" ? "": " \n") + string!
            }else {
                let itemCount = "\(val.item_count ?? 1)"
               // let price = ", $" + "\(price)" + ")"
                let string = (val.item_name ?? "") + " (" + itemCount + ")"
                selectedItem =  selectedItem + (selectedItem == "" ? "": " \n") + string
                cartPrice = cartPrice + Double(Double(val.item_count ?? 1)*(price))
            }
        }
        var mainPrice: Double = 0
        if let double = data?.item_price?.doubleValue {
             mainPrice = double
        }
        if let string = data?.item_price?.stringValue{
            mainPrice = Double(string)!
        }
        cartPrice  = mainPrice
        cell.orderLabel.text = selectedItem
        cell.foodImage!.sd_setImage(with: URL( string: U_CUSTOM_SIZE + (data?.item_image ?? "")), placeholderImage:nil)
        cell.orderLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
        cartPrice = (cartPrice * 100).rounded() / 100
        
        cell.rateLabel.text = "$" + "\(String(format: "%.2f", cartPrice))"
        return cell
    }
}
