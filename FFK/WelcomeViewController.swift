//
//  WelcomeViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit



class WelcomeViewController: UIViewController {    
    //IBOutlets
    @IBOutlet weak var animateImage: ImageView!
    @IBOutlet weak var animateView: UIView!
    @IBOutlet weak var welcomeTitle: UILabel!
    @IBOutlet weak var viewCreateAccount: UIView!
    @IBOutlet weak var viewOrderNow: UIView!
    //  @IBOutlet weak var pickupTime: UILabel!
    //  @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var viewCurrentorder: UIView!
    @IBOutlet weak var howRewardwork: UIButton!
    @IBOutlet weak var viewForCount: View!
    @IBOutlet weak var cartItemCount: UILabel!
    
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    var seconds = 2
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_CHANGE_WELCOME_VIEW), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.pickupLocation.text = Singleton.shared.selectedLocation.name ?? "Farmer's on Cullen"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.pickupLocation.text = Singleton.shared.selectedLocation.name ?? "Farmer's on Cullen"
        print( Singleton.shared.selectedLocation.name)
        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
            self.cartItemCount.text = "\(item)"
            if(item == 0){
                self.viewForCount.isHidden = true
            }else {
                self.viewForCount.isHidden = false
            }
        }else {
            self.viewForCount.isHidden = true
        }
        self.manageView()
    }
    
    @objc func orderCompleted(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CHANGE_WELCOME_VIEW), object: nil)
        DispatchQueue.main.async {
            self.viewDidAppear(false)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_CHANGE_WELCOME_VIEW), object: nil)
    }
    
    func manageView(){
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            if (Singleton.shared.activeOrderData.count > 0){
                self.welcomeTitle.text = "Your current order"
                self.viewCreateAccount.isHidden = true
                self.viewOrderNow.isHidden = true
                self.howRewardwork.isHidden = true
                self.viewCurrentorder.isHidden = false
                UIView.performWithoutAnimation {
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "SinglePageViewController") as! SinglePageViewController
                    controller.view.translatesAutoresizingMaskIntoConstraints = false
                    self.containerView.addSubview(controller.view)
                    self.containerView.reloadInputViews()
                    NSLayoutConstraint.activate([
                        controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
                        controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
                        controller.view.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
                        controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
                    ])
                    controller.didMove(toParent: self)
                }
            }else {
                self.welcomeTitle.text = "Hi " + (Singleton.shared.userDetail.first_name ?? "") + "!"
                self.welcomeTitle.font = UIFont(name: "Montserrat-Bold", size: 26)
                self.welcomeTitle.textColor = primaryColor
                self.viewCreateAccount.isHidden = true
                self.viewOrderNow.isHidden = false
                self.howRewardwork.isHidden = false
                self.viewCurrentorder.isHidden = true
            }
        }else {
            self.viewCreateAccount.isHidden = false
            self.viewOrderNow.isHidden = true
            self.welcomeTitle.text = "Earn rewards and skip the line!"
            self.howRewardwork.isHidden = false
            self.viewCurrentorder.isHidden = true
        }
    }
    
    @objc func updateTimer() {
        seconds -= 1
        UIView.animate(withDuration: 0.7, delay: 0, options: [.curveEaseOut], animations: {
            self.animateImage.transform = CGAffineTransform(translationX: 0, y: 5)
        }, completion:{
            (val) in
            UIView.animate(withDuration: 0.7, delay: 0, options: [.curveEaseOut], animations: {
                self.animateImage.transform = CGAffineTransform(translationX: 0, y: -5)
            },completion:nil )
        })
    }
    
    
    //IBAction
    @IBAction func locationAction(_ sender: Any){
        currentPageView = K_MAP_PAGE_VIEW
        isNavigationFromMenu = true
        NavigationController.shared.openLocationScreen(controller: self)
    }
    
    @IBAction func bagAction(_ sender: Any) {
        let pageControler = MainPageViewController.customDataSource as!  MainPageViewController
        pageControler.setThirdController()
    }
    
    
    @IBAction func openMenuAction(_ sender: Any)
    {
        let pageControler = MainPageViewController.customDataSource as!  MainPageViewController
        pageControler.setFirstController()
    }
    
    @IBAction func currentOrderdetails(_ sender: Any) {
        let pageController = MainPageViewController.customDataSource as! MainPageViewController
        pageController.setThirdController()
    }
    
    @IBAction func createAccount(_ sender: Any) {
        NavigationController.shared.openCreateAccount(controller: self)
    }
    
    @IBAction func signIn(_ sender: Any) {
        NavigationController.shared.openLoginScreen(controller: self)
    }
    
    @IBAction func menuController(_ sender: Any) {
        if (Singleton.shared.selectedLocation.id != nil){
            // let pageControl = WelcomePageViewController.customDataSource as! WelcomePageViewController
            //pageControl.setSecondController()
            isOrderNow = true
            NavigationController.shared.pushHome(controller: self)
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            currentPageView = K_MAP_PAGE_VIEW
            myVC.hideCrossView = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func rewardWork(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardWorkViewController") as! RewardWorkViewController
        //myVC.modalTransitionStyle = .
        // NavigationController.shared.navigateRewardWork(controller: self)
        //self.navigationController?.pushViewController(myVC, animated: true)
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
}
