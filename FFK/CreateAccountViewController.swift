//
//  CreateAccountViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FirebaseMessaging

class CreateAccountViewController: UIViewController {
    //IBOutlets
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var zipCode: SkyFloatingLabelTextField!
    @IBOutlet weak var dob: SkyFloatingLabelTextField!
    @IBOutlet weak var showPassImg: UIImageView!
    
    
    var popUpView = false
    var fName: String?
    var lName: String?
    var userEmail: String?
    var googleId: String?
    var fbId: String?
    var errorCount = 0
    var appleId: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstName.text = fName ?? ""
        self.lastName.text = lName ?? ""
        self.email.text = userEmail ?? ""
        self.showPassImg.image = #imageLiteral(resourceName: "visibility")
        self.mobileNumber.delegate = self
        self.zipCode.delegate = self
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.email.delegate = self
        self.password.delegate = self
        self.dob.delegate = self
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
      //  swipeRight.direction = .right
      //self.view!.addGestureRecognizer(swipeRight)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
      // self.navigationController?.addCustomTransitioning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      //  self.view.gestureRecognizers?.removeAll()
    }
    
    func clearFields() {
        self.firstName.text = ""
        self.lastName.text = ""
        self.email.text = ""
        self.password.text = ""
        self.mobileNumber.text = ""
        self.zipCode.text = ""
        self.dob.text = ""
    }
    
    
    //IBAction
    @IBAction func showPaaswordAction(_ sender: Any) {
        if(self.showPassImg.image == #imageLiteral(resourceName: "visibility")){
            self.showPassImg.image = #imageLiteral(resourceName: "password")
            self.password.isSecureTextEntry = false
        }else {
            self.showPassImg.image = #imageLiteral(resourceName: "visibility")
            self.password.isSecureTextEntry = true
        }
    }
    
    @IBAction func createAccount(_ sender: Any) {
        self.errorCount = 0
        if(firstName.text!.isEmpty){
            self.errorCount += 1
            firstName.errorMessage = "Enter First Name"
        }
        if(lastName.text!.isEmpty){
            self.errorCount += 1
            lastName.errorMessage = "Enter Last Name"
        }
        if(email.text!.isEmpty){
            self.errorCount += 1
            email.errorMessage = "Enter Email Address"
        }
        if !(self.isValidEmail(emailStr: email.text!.replacingOccurrences(of: " ", with: ""))){
            self.errorCount += 1
            email.errorMessage = "Enter valid Email Address"
        }
        if(password.text!.isEmpty){
            self.errorCount += 1
            password.errorMessage = "Enter Password"
        }
        if(!self.isPasswordValid(password.text!)){
            self.errorCount += 1
            password.errorMessage = "Password must be alphanumeric"
        }
        if(self.mobileNumber.text!.isEmpty){
            self.errorCount += 1
            mobileNumber.errorMessage  = "Enter mobile number"
        }
        
        
        if(self.errorCount > 0){
            return
        }else {
            ActivityIndicator.show(view: self.view)
            let myCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? ""
            let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) ?? ""
            let charactersToRemove = CharacterSet(charactersIn: "()+-")
            var telephone = self.mobileNumber.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            var userDob = String()
            if let bitrhDate = K_PICKER_DATE {
                userDob = self.convertTimestampToDate(Int(bitrhDate.timeIntervalSince1970), to: "MM-dd-yyyy")
                    
            }
            let param = [
                "first_name": self.firstName.text,
                "last_name": self.lastName.text,
                "email": (self.email.text ?? "").replacingOccurrences(of: " ", with: ""),
                "mobile": telephone,
                "password": self.password.text,
                "zip_code": self.zipCode.text,
                "date_of_birth": userDob,
                "cart_id": myCartId,
                "google_id":self.googleId ?? "",
                "facebook_id": self.fbId ?? "",
                "token":fcmToken,
                "apple_id": self.appleId ?? "",
                "platform": 2
                ] as? [String: Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REGISTER, method: .post, parameter: param, objectClass: SignUp.self, requestCode: U_REGISTER, userToken: nil) { (response) in
                self.clearFields()
                ActivityIndicator.hide()
                if(NavigationController.shared.hasLocationPermission()){
                  NavigationController.shared.emailNotificationAction(param: 1)
                }
                UserDefaults.standard.set(response.response.token, forKey: UD_TOKEN)
                let userData = try! JSONEncoder().encode(response.response)
                Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_LOGGEDIN_USER)
                UserDefaults.standard.set(userData, forKey:UD_USER_DETAIl)
                Singleton.shared.userDetail = response.response
                UserDefaults.standard.removeObject(forKey: UD_GUEST_LOGIN_DETAIL)
                let nAVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                nAVC.modalPresentationStyle = .overFullScreen
                nAVC.heading = response.message ?? ""
                //"You have successfully subscribed with Farmer's.\nGet rewarded with free entree now."
                self.navigationController?.present(nAVC, animated: false, completion: {
                    if (Singleton.shared.selectedLocation.id != nil){
                        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
                            if(item == 0){
                                NavigationController.shared.pushHome(controller: self)
                            }else {
                                NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
                            }
                        }else {
                            NavigationController.shared.pushHome(controller: self)
                        }
                    }else {
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                        currentPageView = K_MAP_PAGE_VIEW
                        myVC.hideCrossView = true
                        self.navigationController?.pushViewController(myVC, animated: true)
                    }
                })
            }
        }
        
    }
    
    @objc func handleDate() {
        if (K_PICKER_DATE != nil){
            
            self.dob.text = self.convertTimestampToDate(Int(K_PICKER_DATE!.timeIntervalSince1970), to: "MMM dd, yyyy")
        }
    }
    
    @IBAction func signIn(_ sender: Any) {
        NavigationController.shared.openLoginScreen(controller: self)
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: CreateAccountViewController.self){
                return true
            } else {
                return false
            }
        })
    }
    
    @IBAction func crossAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDate), name:Notification.Name(rawValue: N_SELECT_DATE), object: nil)
        self.navigationController?.present(myVC, animated: true)
    }
    
}


extension CreateAccountViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! SkyFloatingLabelTextField).errorMessage = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNumber){
            self.mobileNumber.text =  self.mobileNumber.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            let currentCharacterCount = self.mobileNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            
            return newLength <= 14
        }else if (textField == zipCode) {
            let currentCharacterCount = self.zipCode.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 5
        }else {
            return true
        }
    }
}
