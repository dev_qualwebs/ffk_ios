//
//  LoginViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GoogleSignIn
import FBSDKLoginKit
import AVFoundation
import AuthenticationServices
import Firebase
import FirebaseMessaging

class LoginViewController: UIViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    
    @IBOutlet weak var showPassImg: UIImageView!
    @IBOutlet weak var guestLoginButton: UIButton!
    
    
    var popUpView = false
    var popFromOrderPage = false
    var googleData = GIDGoogleUser()
    var fbData = [String:Any]()
    let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) ?? ""
    var errorCount = 0
    var appleData = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showPassImg.image = #imageLiteral(resourceName: "visibility")
      //  GIDSignIn.sharedInstance()?.presentingViewController = self
        self.email.delegate = self
        self.password.delegate = self
        self.password.isSecureTextEntry = true
     //   let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
     //   swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
       // self.navigationController?.addCustomTransitioning()
        self.guestLoginButton.isHidden = !popFromOrderPage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func getFacebookUserInfo(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            guard AccessToken.current != nil else {
                print("Failed to get access token")
                return
            }
            self.getFBUserData()
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    let myCartId = (UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? "")
                    
                    if let data = result as? [String:Any]{
                        let param = [
                            "facebook_id": data["id"] as! String,
                            "token":self.fcmToken,
                            "cart_id":myCartId,
                            "platform": 2
                            ] as? [String:Any]
                        self.fbData = data
                        
                        NotificationCenter.default.addObserver(self, selector: #selector(self.handleSocialNavigation(_:)), name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil)
                        ActivityIndicator.show(view: self.view)
                        SessionManager.shared.methodForApiCalling(url: U_BASE + U_FB_LOGIN, method: .post, parameter: param, objectClass: SignUp.self, requestCode: U_FB_LOGIN, userToken: nil) { (response) in
                            let userData = try! JSONEncoder().encode(response.response)
                            UserDefaults.standard.set(userData, forKey:UD_USER_DETAIl)
                            Singleton.shared.userDetail = response.response
                            UserDefaults.standard.set(response.response.token, forKey: UD_TOKEN)
                            ActivityIndicator.show(view: self.view)
                            self.handleNavigation(response: response)
                            
                        }
                    }
                }
            })
        }
    }
    
    @objc func handleSocialNavigation(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SOCIAL_LOGIN), object: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        if((notif.userInfo?["loginType"] as? String) == "facebook"){
            myVC.fName = fbData["first_name"] as? String
            myVC.lName = fbData["last_name"] as? String
            myVC.userEmail = fbData["email"] as? String
            myVC.fbId = fbData["id"] as? String
        }else if((notif.userInfo?["loginType"] as? String) == "apple"){
            myVC.fName = appleData["first_name"] as? String ?? ""
            myVC.lName = appleData["last_name"] as? String ?? ""
            myVC.userEmail = appleData["email"] as? String ?? ""
            myVC.appleId = appleData["id"] as? String
        }else {
            if let comp = self.googleData.profile{
             var components = comp.name.components(separatedBy: " ")
            if(components.count > 0){
                let firstName = components.removeFirst()
                let lastName = components.joined(separator: " ")
                myVC.fName = firstName
                myVC.lName = lastName
            }
        }
            myVC.googleId = self.googleData.userID
            myVC.userEmail = self.googleData.profile?.email
        }
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func handleNavigation(response: SignUp) {
        let userData = try! JSONEncoder().encode(response.response)
        UserDefaults.standard.set(userData, forKey:UD_USER_DETAIl)
        UserDefaults.standard.set(true, forKey:UD_LAUNCH_FIRST_TIME)
        UserDefaults.standard.set(response.response.token, forKey: UD_TOKEN)
        NavigationController.shared.getRecentOrder(1)
        NavigationController.shared.getCartData()
        UserDefaults.standard.removeObject(forKey: UD_GUEST_LOGIN_DETAIL)
        Singleton.shared.userDetail = response.response
        Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_LOGGEDIN_USER)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SOCIAL_LOGIN), object: nil)
        
        if(NavigationController.shared.hasLocationPermission()){
            NavigationController.shared.emailNotificationAction(param: 1)
        }
        if(self.popFromOrderPage){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            let myCartId = (UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? "") as? String
            myVC.orderCart = myCartId ?? ""
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            if(self.popUpView){
                self.navigationController?.popViewController(animated: true)
            }else {
                NavigationController.shared.pushHome(controller: self)
            }
        }
    }
    
    
    
    //IBAction
    @IBAction func showPaaswordAction(_ sender: Any) {
        if(self.showPassImg.image == #imageLiteral(resourceName: "visibility")){
            self.showPassImg.image = #imageLiteral(resourceName: "password")
            self.password.isSecureTextEntry = false
        }else {
            self.showPassImg.image = #imageLiteral(resourceName: "visibility")
            self.password.isSecureTextEntry = true
        }
    }
    
    
    @IBAction private func loginWithReadPermissions() {
        self.getFacebookUserInfo()
    }
    
    @IBAction private func logOut() {
        let loginManager = LoginManager()
        loginManager.logOut()
        
        let alertController = UIAlertController(nibName: "Logout", bundle: Bundle(path: "Logged out."))
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func googleAction(_ sender: Any) {
       
        let signInConfig = GIDConfiguration.init(clientID: K_GOOGLE_CLIENT_ID)
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) {  user, error in
            guard error == nil else { return }
            guard user != nil else {return}
           
            let myCartId = (UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? "")
            let param = [
                "google_id": user!.userID,
                "token":self.fcmToken,
                "cart_id": myCartId,
                "platform": 2
                ] as? [String:Any]
            self.googleData = user!
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.handleSocialNavigation(_:)), name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil)
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GOOGLE_LOGIN, method: .post, parameter: param, objectClass: SignUp.self, requestCode: U_GOOGLE_LOGIN, userToken: nil) { (response) in
                ActivityIndicator.show(view: self.view)
                self.handleNavigation(response: response)
            }
        }
    }
    
    @IBAction func appleAction(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        }
    }
    
    @IBAction func createAccount(_ sender: Any) {
        NavigationController.shared.openCreateAccount(controller: self)
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: LoginViewController.self){
                return true
            } else {
                return false
            }
        })
    }
    
    @IBAction func forgetPasswordAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassViewController") as! ForgotPassViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    
    @IBAction func signIn(_ sender: Any) {
        self.errorCount = 0
        if(email.text!.isEmpty){
            self.errorCount += 1
            email.errorMessage = "Enter Email Address"
        }
        if !(self.isValidEmail(emailStr: self.email.text!.replacingOccurrences(of: " ", with: ""))){
            self.errorCount += 1
            email.errorMessage = "Enter valid Email Address"
        }
        if(password.text!.isEmpty){
            self.errorCount += 1
            password.errorMessage = "Enter Password"
        }
        
        if(self.errorCount > 0){
            return
        }else {
            ActivityIndicator.show(view: self.view)
            let myCartId = (UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? "")
            let param = [
                "email": (self.email.text ?? "").replacingOccurrences(of: " ", with: ""),
                "password":self.password.text,
                "cart_id": myCartId,
                "token":fcmToken,
                "platform": 2
                ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN, method: .post, parameter: param, objectClass: SignUp.self, requestCode: U_LOGIN, userToken: nil) { (response) in
                self.handleNavigation(response: response)
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.heading = response.message ?? ""
                myVC.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }
        }
    }
    
    
    @IBAction func crossAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func guestAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "GuestLoginViewController") as! GuestLoginViewController
        myVC.popFromOrderPage = self.popFromOrderPage
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}

extension LoginViewController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
}

extension LoginViewController: ASAuthorizationControllerDelegate{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    // Authorization Succeeded
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Get user data with Apple ID credentitial
            let userId = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            let userEmail = appleIDCredential.email
            self.appleData["first_name"] = userFirstName
            self.appleData["last_name"] = userLastName
            self.appleData["email"] = userEmail
            self.appleData["id"] = userId
            NotificationCenter.default.addObserver(self, selector: #selector(self.handleSocialNavigation(_:)), name: NSNotification.Name(rawValue:N_SOCIAL_LOGIN), object: nil)
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_APPLE_LOGIN, method: .post, parameter: ["apple_id":userId], objectClass: SignUp.self, requestCode: U_APPLE_LOGIN, userToken: nil) { (response) in
                ActivityIndicator.show(view: self.view)
                self.handleNavigation(response: response)
            }
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Get user data using an existing iCloud Keychain credential
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            // Write your code here
        }
    }
}
