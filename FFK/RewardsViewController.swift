//
//  RewardsViewController.swift
//  FFK
//
//  Created by AM on 04/10/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit


class RewardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var rewardTable: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var pointsLeft: UILabel!
  //  @IBOutlet weak var sliderValue: CustomSlider!
    
    var rewardData = GetRewardResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGestureRight))
      //  swipeRight.direction = .right
        //self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
        self.noDataLabel.isHidden = true
      //  sliderValue.minimumValue = 0
     //   sliderValue.maximumValue = 3000
       
        if #available(iOS 15.0, *) {
            if(self.rewardTable != nil){
            self.rewardTable.sectionHeaderTopPadding = 0
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.rewardData = Singleton.shared.myRewards
        self.totalPoints.text = Int(self.rewardData.total_rewards?.points ?? "0")!.withCommas() ?? ""
        if(self.rewardData.list.count > 0){
            if(self.rewardData.list[0].rewards.count > 0){
                if(self.rewardData.list[0].rewards[0].type == 1 && ((self.rewardData.list[0].rewards[0].total_rewards ?? 0) > 0)){
                    self.pointsLeft.text = ((self.rewardData.list[0].rewards[0].total_rewards ?? 0).withCommas() ?? "") + " points from your last order, nice!"
                }else {
                    if(self.rewardData.list[0].rewards.count > 1){
                        if((self.rewardData.list[0].rewards[1].total_rewards ?? 0) > 0){
                            self.pointsLeft.text = ((self.rewardData.list[0].rewards[1].total_rewards ?? 0).withCommas() ?? "") + " points from your last order, nice!"
                        }
                    }
                }
            }
        }else {
            self.pointsLeft.text = ""
        }
        
       // self.sliderValue.value = Float(self.rewardData.total_rewards?.points ?? "0")!
        //        if(self.rewardData.list.count > 0){
        //            for val in 0..<self.rewardData.list.count {
        //                let al = self.rewardData.list[val].rewards.filter{
        //                    $0.type == 1
        //                }
        //               self.rewardData.list[val].rewards = al
        //            }
        //        }
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        rewardTable.estimatedRowHeight = 100.0
        rewardTable.rowHeight = UITableView.automaticDimension
        rewardTable.tableFooterView = UIView()
        self.rewardTable.reloadData()
    }
    
    @objc func handleGestureRight(gesture: UISwipeGestureRecognizer) -> Void {
        self.navigationController?.popViewController(animated:   true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(self.rewardData.list.count <= 0){
            noDataLabel.isHidden = false
            return 0
        }else{
            noDataLabel.isHidden = true
            return self.rewardData.list.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.rewardData.list[section].month_name
    }

    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.rewardData.list[section].rewards.count
    }
 
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as? UITableViewHeaderFooterView
        header?.textLabel?.font = UIFont(name: "Montserrat-Medium", size: 16)
        header?.tintColor = backgroundColor
        header?.layer.borderWidth = 0.5
        header?.layer.borderColor = barBackgroundColor.cgColor
        header?.textLabel?.textColor = UIColor.darkGray
        header?.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if(self.rewardData.list.count > section){
            return 30
        }else {
        return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = rewardTable.dequeueReusableCell(withIdentifier: "RewardCollectionViewCell") as! RewardCollectionViewCell
            
        if(self.rewardData.list[indexPath.section].rewards.count <= indexPath.row){
            return cell
        }else {
        let data = self.rewardData.list[indexPath.section].rewards[indexPath.row]
            cell.creditPoints2.text = "+" + "\((data.total_rewards ?? 0).withCommas() ?? "")".replacingOccurrences(of: "-", with: "")
            if(data.type == 1){
                cell.purchaseCreditLabel.text = "Purchase Credit"
                cell.viewForImage.isHidden = true
                cell.viewForPurcahse.isHidden = false
            }else if(data.type == 2){
            cell.viewForImage.isHidden = false
            cell.viewForPurcahse.isHidden = true
            cell.offerImage.image = #imageLiteral(resourceName: "3")
            cell.rewardPoint.text = "3,000 points redeemed"
            cell.rewardPoint.textColor = .darkGray
            cell.rewardPoint.font = UIFont(name: "Montserrat-Regular", size: 14)
        }else if(data.type == 3){
            cell.viewForImage.isHidden = false
            cell.viewForPurcahse.isHidden = true
            cell.offerImage.image = #imageLiteral(resourceName: "1")
            cell.rewardPoint.text = "Happy Birthday!"
            cell.rewardPoint.textColor = primaryColor
            cell.rewardPoint.font = UIFont(name: "Montserrat-SemiBold", size: 16)
        }else if(data.type == 4){
            cell.viewForImage.isHidden = false
            cell.viewForPurcahse.isHidden = true
            cell.offerImage.image = #imageLiteral(resourceName: "2")
            cell.rewardPoint.text = "Free Dish Reward"
            cell.rewardPoint.textColor = primaryColor
            cell.rewardPoint.font = UIFont(name: "Montserrat-SemiBold", size: 16)
        }else if(data.type == 5){
            cell.purchaseCreditLabel.text = "Refunded"
            cell.viewForImage.isHidden = true
            cell.viewForPurcahse.isHidden = false
            cell.creditPoints2.text = "-" + "\((data.total_rewards ?? 0).withCommas() ?? "")".replacingOccurrences(of: "-", with: "")
            cell.creditPoints2.textColor = .lightGray
        }else {
            cell.viewForImage.isHidden = true
            cell.viewForPurcahse.isHidden = false
        }
        
        cell.purchaseDate2.text = self.convertTimestampToDate(data.order_date!, to: "eee, M/d/yy")
        cell.rewardDate.text = self.convertTimestampToDate(data.order_date!, to: "eee, M/d/yy")
        
        return cell
        }
    }
}


class RewardCollectionViewCell: UITableViewCell {
    //MARK: IBOUTLETS
    @IBOutlet weak var purchaseDate2: UILabel!
    @IBOutlet weak var creditPoints2: UILabel!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var labelCreditDate: UILabel!
    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var viewForImage: UIView!
    @IBOutlet weak var rewardDate: UILabel!
    @IBOutlet weak var rewardPoint: UILabel!
    @IBOutlet weak var viewForPurcahse: UIView!
    @IBOutlet weak var purchaseCreditLabel: UILabel!
    
}
