//
//  NotificationViewController.swift
//  Farmers
//
//  Created by Sagar Pandit on 25/11/21.
//  Copyright © 2021 AM. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet weak var notificationTable: UITableView!
    
    var notificationData = [NotificationResponse]()
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getNotificationData()
        self.navigationController?.addCustomTransitioning()
        notificationTable.estimatedRowHeight = 100
        notificationTable.rowHeight = UITableView.automaticDimension
    }
    
    func getListFromServer(_ pageNumber: Int){
           self.isLoadingList = false
           self.notificationTable.reloadData()
    }
    
    func loadMoreItemsForList(){
          currentPage += 1
        getNotificationData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && (scrollView.contentOffset.y > 50) && !isLoadingList){
                self.isLoadingList = true
                self.loadMoreItemsForList()
            }
        }
    
    func getNotificationData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NOTIFICATIONS + "?page=\(self.currentPage)", method: .get, parameter: nil, objectClass: GetNotification.self, requestCode: U_GET_NOTIFICATIONS, userToken: nil) { response in
            for val in response.response{
            self.notificationData.append(val)
            }
            self.isLoadingList = false
            self.notificationTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBACtions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"RestaurentTableViewCell") as! RestaurentTableViewCell
        let val = notificationData[indexPath.row]
        cell.nameLabel.text = val.notification_title
        cell.orderLabel.text = val.notification_content
        cell.rateLabel.text = val.created_at
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
        let type = Int(self.notificationData[indexPath.row].type_id ?? "0")
                if (type == 4 || type == 3 || type == 5 || type == 5){
                    NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_COMPLETED), object: nil,userInfo: ["orderId": self.notificationData[indexPath.row].order_id ?? 0])
                    NavigationController.shared.getRecentOrder(self.notificationData[indexPath.row].order_id ?? 0)
                }else if(type == 12){
                    NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil,userInfo: nil)
                }else if(type == 7 || type == 8){
                    NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil,userInfo: nil)
                }else if(type == 6){
                    NotificationCenter.default.post(name: NSNotification.Name(N_NEW_REWARD), object: nil,userInfo: nil)
                }
    }
}
