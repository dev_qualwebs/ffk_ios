//
//  RealtimeFirebase.swift
//  Farmers
//
//  Created by Sagar Pandit on 07/12/21.
//  Copyright © 2021 AM. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class RealtimeFirebase{
    static var shared = RealtimeFirebase()
    var isFirstTime = true
    
    public func checkRestaurantStatus(completionHandler: @escaping (String) -> Void){
        ref.child("restaurant_2").child("settings").observe(.value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            Singleton.shared.breakfastTimeMessage = ""
            Singleton.shared.lunchTimeMessage = ""
            K_KITCHEN_ONLINE_TWO = value?["is_kitchen_online"] as? String ?? "1"
            completionHandler("test")
        })
        
        ref.child("restaurant_1").child("settings").observe(.value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            Singleton.shared.breakfastTimeMessage = ""
            Singleton.shared.lunchTimeMessage = ""
            K_KITCHEN_ONLINE_ONE = value?["is_kitchen_online"] as? String ?? "1"
            completionHandler("test")
        })
    }
    
    public func addMenuChangeObserver(completionHandler: @escaping (String) -> Void) {
        ref.child("restaurant_2").child("kioskUpdate").observe(.value, with: { (snapshot) in
            if(self.isFirstTime == false){
                self.isFirstTime = false
                if(Singleton.shared.restaurentLocation.count > 0 && Singleton.shared.selectedLocation.id == Singleton.shared.restaurentLocation[1].id){
                    completionHandler("test")
                }
            }else {
                self.isFirstTime = false
            }
        })
        
        ref.child("restaurant_1").child("kioskUpdate").observe(.value, with: { (snapshot) in
            if(self.isFirstTime == false){
                self.isFirstTime = false
                if(Singleton.shared.restaurentLocation.count > 0 && Singleton.shared.selectedLocation.id == Singleton.shared.restaurentLocation[0].id){
                    completionHandler("test")
                }
            }else {
                self.isFirstTime = false
            }
        })
        
    }
    
}
