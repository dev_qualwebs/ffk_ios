//
//  SideMenuViewController.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import StoreKit

class SideMenuViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var signoutButton: UIButton!
    @IBOutlet weak var loggedInScroll: UIScrollView!
    @IBOutlet weak var loogedOutScroll: UIView!
    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var pointsLeft: UILabel!
    @IBOutlet weak var sliderValue: CustomSlider!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var address2: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var viewRewardCount: View!
    @IBOutlet weak var viewBonusCount: View!
    @IBOutlet weak var rewardCount: UILabel!
    @IBOutlet weak var bonusCount: UILabel!
    @IBOutlet weak var rateAppButton: UIButton!
    
    
    var rewardData = GetRewardResponse()
    var isRewardPointCalled = false
    var isBonusCalled = false
    var isSavedPointCalled = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = primaryColor
        //        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        //        navigationController?.navigationBar.titleTextAttributes = textAttributes
        //
        //               if #available(iOS 13.0, *) {
        //                  let statusBar1 =  UIView()
        //                  statusBar1.frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame as! CGRect
        //                  statusBar1.backgroundColor = primaryColor
        //                  UIApplication.shared.keyWindow?.addSubview(statusBar1)
        //
        //               } else {
        //
        //                  let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        //                  statusBar1.backgroundColor = primaryColor
        //               }
        //sliderValue.transform = sliderValue.transform.scaledBy(x: 1, y: 8)
//        sliderValue.layer.cornerRadius = 8
//        sliderValue.clipsToBounds = true
        
        
       
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN) as? String {
                   DispatchQueue.main.async {
                       //if(Singleton.shared.sideMenuBonus.count == 0){
                         self.getBonus()
                      // }
                      // if(Singleton.shared.sideMenuReward.order_rewards_coupons.count == 0){
                        self.getSavedCoupen()
                      // }
                   }
               }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getBonusRewrd), name: NSNotification.Name(N_BONUS_REWARD_COUNT), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.progressView.progress = 0
        self.progressView.layer.cornerRadius = 8
        self.progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 8
        progressView.subviews[1].clipsToBounds = true
        if let rated = UserDefaults.standard.value(forKey: UD_APP_RATED) as? Bool{
                   if(rated){
                       self.rateAppButton.isHidden = true
                   }else{
                        self.rateAppButton.isHidden = false
                   }
               }
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN) as? String {
            self.loggedInScroll.isHidden = false
            self.loogedOutScroll.isHidden = true
            self.userName.text = "Hi \(Singleton.shared.userDetail.first_name ?? "")!"
            if let loc = Singleton.shared.selectedLocation.address?.components(separatedBy: ","){
                      if(loc.count > 0){
                          var location = Singleton.shared.selectedLocation.address?.replacingOccurrences(of: loc[0] + ", ", with: "")
                          self.address.text = loc[0] + "\n" + (location ?? "")
                      }
                  }
        }else {
            self.loggedInScroll.isHidden = true
            self.loogedOutScroll.isHidden = false
            if let loc = Singleton.shared.selectedLocation.address?.components(separatedBy: ","){
                      if(loc.count > 0){
                          var location = Singleton.shared.selectedLocation.address?.replacingOccurrences(of: loc[0] + ", ", with: "")
                          self.address2.text = loc[0] + "\n" + (location ?? "")
                      }
                  }
        }
        if(Singleton.shared.myRewards.list.count == 0){
            if(self.isRewardPointCalled == false){
               self.isRewardPointCalled = true
              getRewardData()
            }
        }else {
            self.rewardData = Singleton.shared.myRewards
            self.totalPoints.text = Int(self.rewardData.total_rewards?.points ?? "0")!.withCommas() ?? ""
            if(self.rewardData.list.count > 0){
                self.managePointLeft()
            }else {
                self.pointsLeft.text = "Order and earn points for FREE Farmer's Fresh Kitchen!"
            }
            //            self.pointsLeft.text = "\(self.rewardData.list[0].rewards[0].total_rewards ?? 0)" + " points from your last order, nice!"
            //            if((2000 - (Int(self.rewardData.total_rewards?.points ?? "0")!)) > 0){
            //                self.pointsLeft.text = "\(2000 - (Int(self.rewardData.total_rewards?.points ?? "0")!))" + " points from your last order, nice!"
            //            }else {
            //                getRewardData()
            //            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.manageSlider()
       
    }
    
    func manageSlider(){
        //self.sliderValue.value = 0
        self.progressView.progress = 0
          
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                self.progressView.setProgress(Float(self.rewardData.total_rewards?.points ?? "0")!/3000, animated: true)
                //self.progressView.progress =
                 // self.sliderValue.setValue(Float(self.rewardData.total_rewards?.points ?? "0")!, animated: true)
              }, completion: nil)

          // quarter done:
//          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//              print(self.progressView.currentPresentationValue)
//              // prints 0.272757, expected 0.25 with some tolerance
//          }
//
//          // half done:
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
//              print(self.progressView.currentPresentationValue)
//
//          }
//
//          // three quarters done:
//          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//              print(self.progressView.currentPresentationValue)
//          }
    }
    
    func managePointLeft(){
        if(self.rewardData.list[0].rewards.count > 0){
            if(self.rewardData.list[0].rewards[0].type == 1){
                self.pointsLeft.text = ((self.rewardData.list[0].rewards[0].total_rewards ?? 0).withCommas() ?? "") + " points from your last order, nice!"
            }else if(self.rewardData.list[0].rewards[0].type == 2){
                self.pointsLeft.text = "Order and earn points for FREE Farmer's Fresh Kitchen!"
            }else if(self.rewardData.list[0].rewards[0].type == 3){
               self.pointsLeft.text = ((self.rewardData.list[0].rewards[0].total_rewards ?? 0).withCommas() ?? "") + " bonus points received! #loved"
            }else if(self.rewardData.list[0].rewards[0].type == 4){
               self.pointsLeft.text = ((self.rewardData.list[0].rewards[0].total_rewards ?? 0).withCommas() ?? "") + " bonus points received! #loved"
            }else {
                if(self.rewardData.list[0].rewards.count > 1){
                    if(self.rewardData.list[0].rewards[1].type == 1){
                        self.pointsLeft.text = ((self.rewardData.list[0].rewards[1].total_rewards ?? 0).withCommas() ?? "") + " points from your last order, nice!"
                    }else{
                       if(self.rewardData.list[0].rewards.count > 2){
                        if(self.rewardData.list[0].rewards[2].type == 1){
                            self.pointsLeft.text = ((self.rewardData.list[0].rewards[2].total_rewards ?? 0).withCommas() ?? "") + " points from your last order, nice!"
                        }
                      }
                    }
                }
            }
        }
    }
    
   // override func viewDidAppear(_ animated: Bool) {
  //     DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
  //          let pageController = WelcomePageViewController.customDataSource as! WelcomePageViewController
 //           pageController.setSecondController2()
 //      })
 //    }
    
    @objc func getBonusRewrd(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_BONUS_REWARD_COUNT), object: nil)
        self.getBonus()
        self.getSavedCoupen()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getBonusRewrd), name: NSNotification.Name(N_BONUS_REWARD_COUNT), object: nil)
        
    }
    
    func getBonus(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_BONUS_LIST, method: .get, parameter: nil, objectClass: GetBonus.self, requestCode: U_BONUS_LIST, userToken: nil) { (response) in
            var bonus =  response.response.bonus
            bonus.append(contentsOf: response.response.offer)
            Singleton.shared.sideMenuBonus = bonus
            if(bonus.count > 0){
                self.viewBonusCount.isHidden = false
                self.bonusCount.text = "\(bonus.count)"
            }else {
                self.viewBonusCount.isHidden = true
            }
            ActivityIndicator.hide()
        }
    }
    
    func getSavedCoupen(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REWARD_COUPEN, method: .get, parameter: nil, objectClass: GetRewardCoupen.self, requestCode: U_GET_REWARD_COUPEN, userToken: nil) { (response) in
            var totalReward = Int()
            self.address.text = Singleton.shared.selectedLocation.address ?? ""
            Singleton.shared.sideMenuReward = response.response
            for val in response.response.admin_reward_coupon{
                if(val.status == 1){
                    totalReward += 1
                }
            }
            
            for val in response.response.birthday_coupons{
                if(val.status == 1){
                    totalReward += 1
                }
            }
            for val in response.response.order_rewards_coupons{
                if(val.status == 1){
                    totalReward += 1
                }
            }
            if(totalReward > 0){
                self.viewRewardCount.isHidden = false
                self.rewardCount.text = "\(totalReward)"
                
            }else {
                self.viewRewardCount.isHidden = true
            }
            ActivityIndicator.hide()
        }
    }
    
    
    func getRewardData() {
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REWARDS, method: .get, parameter: nil, objectClass: GetRewards.self, requestCode: U_GET_REWARDS, userToken: nil) { (response) in
                self.isRewardPointCalled = false
                Singleton.shared.myRewards = response.response
                self.rewardData = Singleton.shared.myRewards
                self.totalPoints.text = "\(Int(self.rewardData.total_rewards?.points ?? "0")?.withCommas() ?? "")"
                if(self.rewardData.list.count > 0){
                    self.managePointLeft()
                }else {
                    self.pointsLeft.text = ""
                }
                self.manageSlider()
            }
        }else {
            self.rewardData = Singleton.shared.myRewards
            self.totalPoints.text = "\(Int(self.rewardData.total_rewards?.points ?? "0")?.withCommas() ?? "")"
            self.pointsLeft.text = "\((3000 - (Int(self.rewardData.total_rewards?.points ?? "0")!)).withCommas() ?? "")" + " points from your last order, nice!"
            
            self.progressView.progress = Float(self.rewardData.total_rewards?.points ?? "0")!/3000
        }
        
    }
    
    //MARK: IBActions
    @IBAction func termsAction(_ sender: Any) {
        self.openUrl(urlStr: "https://farmersfreshkitchen.com/about-us/terms-and-conditions")
    }
    
    @IBAction func legalAction(_ sender: Any) {
        self.openUrl(urlStr: "https://farmersfreshkitchen.com/about-us/privacy-policy")
    }
    
    @IBAction func personalAction(_ sender: Any) {
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController") as! PersonalInfoViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            myVC.heading = "Please login to view profile"
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func locationAction(_ sender: Any)
    {
        currentPageView = K_MAP_PAGE_VIEW
        isNavigationFromMenu = true
        NavigationController.shared.openLocationScreen(controller: self)
    }
    
    @IBAction func bonusAction(_ sender: Any) {
       
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BonusViewController") as! BonusViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func paymentButtonAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        let pageController = MainPageViewController.customDataSource as! MainPageViewController
        pageController.setSecondController()
    }
    
    @IBAction func savedRewards(_ sender: Any) {
        if(rewardData.list.count == 0){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            myVC.heading = "No Rewards"
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func rateApp(_ sender: Any) {

           if #available(iOS 10.3, *) {
             SKStoreReviewController.requestReview()
           }else {
               let urlStr = "https://itunes.apple.com/app/id\(1486732896)"
               "https://itunes.apple.com/app/id\(1486732896)?action=write-review"

               guard let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) else { return
               }
               if #available(iOS 10.0, *) {
                   UIApplication.shared.open(url, options: [:], completionHandler: nil)
               } else {
                   UIApplication.shared.openURL(url) // openURL(_:) is deprecated from iOS 10.
               }
           }
             self.dismiss(animated: true) {
                 DispatchQueue.main.async {
                    UserDefaults.standard.set(true, forKey: UD_APP_RATED)
                   // self.rateAppButton.isHidden = true
//                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
//                           myVC.heading = "Thank you!"
//                           myVC.modalPresentationStyle = .overFullScreen
//                    self.navigationController?.present(myVC, animated: false, completion: nil)
                 }
             }
       }
    
    
    @IBAction func rewardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedRewardsViewController") as! SavedRewardsViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func rewardWork(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardWorkViewController") as! RewardWorkViewController
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: false, completion: nil)
        
    }
    
    @IBAction func createAccountAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        //myVC.popUpView = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PreferenceViewController") as! PreferenceViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    //    func showPopup(title: String, msg: String) {
    //        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    //        //alert.view.backgroundColor = lightPink
    //        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
    //            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
    //            UserDefaults.standard.removeObject(forKey: UD_TOKEN)
    //            self.signoutButton.setTitle("Sign In", for: .normal)
    //            self.navigationController?.pushViewController(myVC, animated: true)
    //        }
    //        let noBtn = UIAlertAction(title: "No", style: .default){
    //            (UIAlertAction) in
    //            self.dismiss(animated: true, completion: nil)
    //        }
    //        alert.addAction(yesBtn)
    //        alert.addAction(noBtn)
    //        present(alert, animated: true, completion: nil)
    //    }
}
