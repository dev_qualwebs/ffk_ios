//
//  AppDelegate.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import FirebaseMessaging
import UserNotifications
import SquareInAppPaymentsSDK
import FirebaseDatabase
import FirebaseRemoteConfig

var ref: DatabaseReference!
//let apiClient = BTAPIClient(authorization: "production_5rghjsmt_kkxt46y9f72c3ckg")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var storyBoard:UIStoryboard?
    var remoteConfig: RemoteConfig!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow:1) as Date)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        configureFirebase(application: application)
        
        SQIPInAppPaymentsSDK.squareApplicationID = K_SQUARE_APP_ID
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
        //          self.handleUserNotfication(info: ["":""])
        //        }
        // Override point for customization after application launch.
        return true
    }
    
    
    func initializeGoogleMap(){
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")
        
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                }
            } else {
            }
           
            
            if let googleKey = self.remoteConfig["google_api_key_app"].stringValue as? String {
                K_GOOGLE_API_KEY = googleKey
                GMSPlacesClient.provideAPIKey(K_GOOGLE_API_KEY)
                GMSServices.provideAPIKey(K_GOOGLE_API_KEY)
            }
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let url = userActivity.webpageURL {
            var view = url.lastPathComponent
            var parameters: [String: String] = [:]
            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                parameters[$0.name] = $0.value
            }
            self.continueUserActivity(webpageURL: url)
            //redirect(to: view, with: parameters)
        }
        return true
    }
    
    
    func continueUserActivity(webpageURL: URL?) {
        guard let urlString = webpageURL?.absoluteString else { return }
        guard let urlComponents = URLComponents(string: urlString) else { return }
        let lastPathComponent = webpageURL?.lastPathComponent
        
        if (urlString.contains("my-rewards")) {
            NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil,userInfo: nil)
        } else if (urlString.contains("menu")) {
            if let controller = self.window?.rootViewController{
                NavigationController.shared.pushHome(controller: controller)
            }
        } else if (urlString.contains("signup")) {
            NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_SIGNUP), object: nil,userInfo: nil)
        } else if (urlString.contains("rewards-points")) {
            NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_REWARD_POINTS), object: nil,userInfo: nil)
        }else if (urlString.contains("orders")) {
            NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_COMPLETED), object: nil,userInfo: ["orderId": Int(lastPathComponent ?? "0")])
        }else if (urlString.contains("bonuses")) {
            NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil,userInfo: nil)
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func configureFirebase(application: UIApplication){
        FirebaseApp.configure()
        ref = Database.database().reference()
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN){
            Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_LOGGEDIN_USER)
            Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_ALL_USER)
        }else {
            Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_ALL_USER)
        }
        let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in
            
        }
        application.registerForRemoteNotifications()
        initializeGoogleMap()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    //This function work when we tap on nitification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        self.handleUserNotfication(info: userInfo)
    }
    
    func handleUserNotfication(info:[AnyHashable:Any]){
        
        // if let requestData = info["data"] as? [String:Any]{
        //let a = "{\"notification_type\":4,\"order_id\":1589}"
        if  let data = info["data"] as? String{
            var notificationType = NotificationType()
            let stringToData = Data(data.utf8)
            let decoder = JSONDecoder()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                do {
                    let type = try decoder.decode(NotificationType.self, from: stringToData)
                    if (type.notification_type == 4 || type.notification_type == 3 || type.notification_type == 5 || type.notification_type == 5){
                        NotificationCenter.default.post(name: NSNotification.Name(N_ORDER_COMPLETED), object: nil,userInfo: ["orderId": type.order_id ?? 0])
                        NavigationController.shared.getRecentOrder(type.order_id ?? 0)
                    }else if(type.notification_type == 12){
                        NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_BONUS), object: nil,userInfo: info)
                    }else if(type.notification_type == 7 || type.notification_type == 8){
                        NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_REWARD), object: nil,userInfo: info)
                    }else if(type.notification_type == 6){
                        NotificationCenter.default.post(name: NSNotification.Name(N_NEW_REWARD), object: nil,userInfo: info)
                    }else {
                        if let controller = self.window?.rootViewController{
                            NavigationController.shared.pushHome(controller: controller)
                        }
                    }
                }catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
}
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcm token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: UD_FCM_TOKEN)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.handleUserNotfication(info: userInfo)
            let center = UNUserNotificationCenter.current()
            center.removeAllDeliveredNotifications()
        }
    }
}
