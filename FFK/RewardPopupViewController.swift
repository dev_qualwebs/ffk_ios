//
//  RewardPopupViewController.swift
//  FFK
//
//  Created by qw on 08/01/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

protocol RewardPopup {
    func showRewardPopup()
}

protocol Confirmation{
    func confirmationSelection()
}

class RewardPopupViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var viewForConfirmation: UIView!
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var viewForReward: View!
    @IBOutlet weak var secondText: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    
    var rewardDelegate: RewardPopup? = nil
    var confirmationDelegate: Confirmation? = nil
    var secondTxt = false
    var isItemStockValidation = false
    
    var isConfirmationViewHidden = true
    var confirmationText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isItemStockValidation){
            self.secondText.isHidden = false
            self.okButton.setTitle("CONTINUE", for: .normal)
            self.cancelButton.setTitle("GO To MENU", for: .normal)
            self.confirmationLabel.text = confirmationText
            self.secondText.isHidden = true
        }else if(secondTxt){
            self.secondText.isHidden = false
            self.okButton.setTitle("YES", for: .normal)
            self.cancelButton.setTitle("NO", for: .normal)
        }else {
            self.secondText.isHidden = true
        }
        self.viewForConfirmation.isHidden = isConfirmationViewHidden
        self.confirmationLabel.text = self.confirmationText
        self.viewForReward.isHidden = !isConfirmationViewHidden
    }
    
    //MARK: IBAction
    @IBAction func viewRewardAction(_ sender: Any) {
        self.rewardDelegate?.showRewardPopup()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        if(self.isItemStockValidation){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_HANDLE_ITEM_OUTOFSTOCK), object: nil, userInfo: ["cancel":"true"])
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func confirmOkAction(_ sender: Any) {
        self.confirmationDelegate?.confirmationSelection()
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
