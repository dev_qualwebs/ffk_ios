

import UIKit

var isFirstTime = true

class TabbarPageViewController: UIPageViewController,UIScrollViewDelegate {
    
    //MARK: Properties
    var pageControl: UIPageControl?
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    lazy var viewControllerList = [UIViewController]()
    var sb = UIStoryboard(name:"Main",bundle:nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        self.dataSource = self
        self.delegate = self
        TabbarPageViewController.customDataSource = self
        viewControllerList = [sb.instantiateViewController(withIdentifier: "HomeViewController" ),
                              sb.instantiateViewController(withIdentifier: "RecentOrderTableViewController" ),
                              sb.instantiateViewController(withIdentifier: "RecentOrderTableViewController" )]
//        if let active = UserDefaults.standard.value(forKey: UD_ACTIVE_ORDER_EXIST) {
//                if let firstViewController = viewControllerList.first {
//                    self.setViewControllers([viewControllerList[0]],direction: .forward,
//                                            animated:false,completion:nil)
//                }
//        }else{
            if let firstViewController = viewControllerList.first {
                self.setViewControllers([viewControllerList[0]],direction: .forward,
                                        animated:false,completion:nil)
            }
            
      //  }
        for view in self.view.subviews {
               // (view as! UIScrollView).delegate =  self
                (view as! UIScrollView).bounces = false
                (view as! UIScrollView).isScrollEnabled = false
        }
    }
    
    
    
    func setFirstController() {
        setViewControllers([viewControllerList[0]], direction: .reverse, animated: true, completion: nil)
        // self.pageControl?.currentPage = 0
    }
    
    func setSecondController() {
        // NotificationCenter.default.post(name: NSNotification.Name("recent-order-tab"), object: nil)
        if(currentTab == K_FAVOURITE_TAB){
            currentTab = K_RECENT_TAB
            setViewControllers([viewControllerList[1]], direction: .reverse, animated: true, completion: nil)
        }else {
            currentTab = K_RECENT_TAB
            setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)
        }
        
        //self.pageControl?.currentPage = 1
    }
    
    func setThirdController() {
        currentTab = K_FAVOURITE_TAB
        setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
        //self.pageControl?.currentPage = 2
    }
    
    func setLastController() {
        // if(tab == K_FAVOURITE_TAB){
        setViewControllers([viewControllerList[3]], direction: .forward, animated: false, completion: nil)
        // self.pageControl?.currentPage = 2
        //  }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
}

extension TabbarPageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
        else{return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
        else{return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
        return viewControllerList[nextIndex]
    }
}

extension TabbarPageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
    }
    
}



