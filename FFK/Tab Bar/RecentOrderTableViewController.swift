

import UIKit

var currentTab = K_RECENT_TAB


class RecentOrderTableViewController: UIViewController, Confirmation, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && (scrollView.contentOffset.y > 50) && !isLoadingList){
            self.isLoadingList = true
            self.loadMoreItemsForList()
        }
    }
    
    func confirmationSelection() {
        if(self.rewardPopupType == 1){
            let indexPath = self.selectedIndexPath
            NavigationController.shared.emptyMyCart()
            //bagItem = []
            self.selectedMenu = self.orderTableData[indexPath.row].menu_id ?? 0
            //bagItem.insert(self.orderTableData[indexPath.row].order_id ?? 0)
            self.recentOrderTable.reloadData()
            self.addToCart.animShow()
        }else if(self.rewardPopupType == 2){
            ActivityIndicator.show(view: self.view)
            let param = [
                "order_id": self.selectedOrderId.order_id ?? 0,
                "favorite_label_id": self.selectedOrderId.favorite_label_id
            ] as? [String: Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_MARK_FAVOURITE, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_MARK_FAVOURITE, userToken: nil) { (response) in
                Singleton.shared.recentOrderHistory = []
                Singleton.shared.activeOrderData = []
                self.orderHistoryData = []
                self.currentPage = 1
                self.callRecentOrderApi()
                ActivityIndicator.hide()
            }
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet var recentOrderTable: UITableView!
    @IBOutlet weak var viewforSigup: UIView!
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var viewForOrder: View!
    //    @IBOutlet weak var pickupTime: UILabel!
    //    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var addToCart: UIView!
    @IBOutlet weak var activeOrderTable: ContentSizedTableView!
    @IBOutlet weak var noLabelHeading: UILabel!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    
    
    var orderHistoryData = [OrderResponse]()
    var favouriteData = [OrderResponse]()
    var orderTableData = [OrderResponse]()
    var activeOrderData = [OrderResponse]()
    var showView = 1
    var noDataText:String = "Reorder without the wait! We'll save your recent orders here for you."
    var selectedMenu = Int()
    var selectedIndexPath = IndexPath()
    
    var currentTableIndex = [String:Any]()
    var contentOffset: CGPoint?
    var selectedOrderId = OrderResponse()
    var rewardPopupType = Int()
    var bagItem = Int()
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupNavigationBar()
        self.mainScrollView.delegate = self
        
        self.popupView.isHidden = true
        self.labelNoData.isHidden  = true
        self.noLabelHeading.isHidden = true
        //self.addToCart.backgroundColor = .clear
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CHANGE_RECENT_VIEW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_CHANGE_RECENT_VIEW), object: nil)
        if #available(iOS 15.0, *) {
            if(self.recentOrderTable != nil){
                self.recentOrderTable.sectionHeaderTopPadding = 0
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.recentOrderTable.estimatedRowHeight = 140
        self.recentOrderTable.rowHeight = UITableView.automaticDimension
        if(bagItem != 0){
            self.addToCart.isHidden = false
        }else {
            self.addToCart.isHidden = true
        }
        recentOrderTable.tableFooterView = UIView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            self.popupView.isHidden = true
            orderHistoryData = []
            self.currentPage = 1
            callRecentOrderApi()
        }else {
            if(currentTab == K_RECENT_TAB){
                self.labelNoData.text = noDataText
                self.noLabelHeading.text = "No Recent Orders"
            }else {
                self.labelNoData.text = "Reorder your favorites even faster! Just heart a recent order and you'll find it here."
                self.noLabelHeading.text = "No Saved Favorites"
            }
            
            if(showView == 1){
                self.viewforSigup.isHidden = false
            }
            self.labelNoData.isHidden = true
            self.noLabelHeading.isHidden = true
            self.popupView.isHidden = false
        }
    }
    
    func  handleRestaurentAvialable(mainMenuId: Int, menuName: String){
        if(Singleton.shared.mainMenuData.count > 0){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            for val in Singleton.shared.mainMenuData{
                if(val.menu_id == mainMenuId){
                    if(menuName == "Lunch"){
                        if(Singleton.shared.lunchTimeMessage != ""){
                            myVC.heading = Singleton.shared.lunchTimeMessage
                        }else {
                            let time = self.convertTimestampToDate((val.from ?? 0), to: "h:mm a").lowercased()
                            myVC.heading = "Lunch will be served tomorrow at \(time)."
                        }
                        myVC.modalPresentationStyle = .overFullScreen
                        self.navigationController?.present(myVC, animated: false, completion: nil)
                    }else if(menuName == "Breakfast") {
                        if(Singleton.shared.breakfastTimeMessage != ""){
                            myVC.heading = Singleton.shared.breakfastTimeMessage
                        }else {
                            let time = self.convertTimestampToDate((val.from ?? 0), to: "h:mm a").lowercased()
                            myVC.heading = "Breakfast will be served tomorrow at \(time) ."
                        }
                        myVC.modalPresentationStyle = .overFullScreen
                        self.navigationController?.present(myVC, animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    
    func getListFromServer(_ pageNumber: Int){
        self.isLoadingList = false
        self.recentOrderTable.reloadData()
    }
    
    func loadMoreItemsForList(){
        currentPage += 1
        callRecentOrderApi()
    }
    
    func  callRecentOrderApi(){
        self.favouriteData = []
        self.activeOrderData = []
        if let logged = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            if((Singleton.shared.recentOrderHistory.count == 0 && Singleton.shared.activeOrderData.count == 0) || self.isLoadingList){
                  ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ORDER_HISTORY + "/\(Singleton.shared.selectedLocation.id ?? 1)?page=\(self.currentPage)", method: .get, parameter: nil, objectClass: GetOrder.self, requestCode: U_GET_ORDER_HISTORY, userToken: nil) { (response) in
                    self.isLoadingList = false
                    if(response.response.past.count == 0 && self.currentPage != 1){
                        self.currentPage -= 1
                    }else if(self.currentPage == 1) {
                        let arrData = response.response.past.filter{
                            $0.order_details!.count >= 1
                        }
                        for val in arrData{
                            self.orderHistoryData.append(val)
                        }
                    }else if(self.currentPage != 1 && response.response.past.count > 0) {
                        let arrData = response.response.past.filter{
                            $0.order_details!.count >= 1
                        }
                        self.orderHistoryData = self.orderHistoryData + arrData
                    }
                    Singleton.shared.recentOrderHistory = self.orderHistoryData
                    self.activeOrderData = response.response.active
                    if(self.activeOrderData.count > 0){
                       UserDefaults.standard.set(try? JSONEncoder().encode(self.activeOrderData[0]), forKey: UD_ACTIVE_ORDER_EXIST)
                    }
                    Singleton.shared.activeOrderData = response.response.active
                    for val in self.orderHistoryData {
                        if(val.is_favorite == 1){
                            self.favouriteData.append(val)
                        }
                    }
                    if(currentTab == K_RECENT_TAB){
                        self.orderTableData = self.orderHistoryData
                        self.recentOrderTable.reloadData()
                        self.activeOrderTable.reloadData()
                        
                       // let pageControler = TabbarPageViewController.customDataSource as!  TabbarPageViewController
                       // pageControler.setSecondController()
                    }else{
                        self.orderTableData = self.favouriteData
                        self.activeOrderData = []
                        self.recentOrderTable.reloadData()
                        self.activeOrderTable.reloadData()
                      //  let pageControler = TabbarPageViewController.customDataSource as!  TabbarPageViewController
                      //  pageControler.setThirdController()
                    }
                    
                    if(self.orderTableData.count == 0 && self.activeOrderData.count == 0){
                        self.showView = 2
                        if(currentTab == K_RECENT_TAB){
                            self.noDataText = "Reorder without the wait! We'll save your recent orders here for you."
                            self.labelNoData.text = self.noDataText
                            self.noLabelHeading.text = "No Recent Orders"
                        }else {
                            self.noDataText = "Reorder your favorites even faster! Just heart a recent order and you'll find it here."
                            self.noLabelHeading.text = "No Saved Favorites"
                            self.labelNoData.text = self.noDataText
                        }
                        if(self.showView == 1){
                            self.viewforSigup.isHidden = false
                        }else {
                            self.labelNoData.isHidden = false
                            self.noLabelHeading.isHidden = false
                            self.viewforSigup.isHidden = true
                        }
                        self.popupView.isHidden = false
                    }else {
                        self.popupView.isHidden = true
                    }
                    ActivityIndicator.hide()
                }
            }else {
                self.isLoadingList = false
                self.activeOrderData = Singleton.shared.activeOrderData
                self.orderHistoryData  = Singleton.shared.recentOrderHistory
                for val in self.orderHistoryData {
                    if(val.is_favorite == 1){
                        self.favouriteData.append(val)
                    }
                }
                if(currentTab == K_RECENT_TAB){
                    self.orderTableData = self.orderHistoryData
                    self.recentOrderTable.reloadData()
                    self.activeOrderTable.reloadData()
                }else{
                    self.orderTableData = self.favouriteData
                    self.activeOrderData = []
                    self.activeOrderTable.reloadData()
                    self.recentOrderTable.reloadData()
                }
                if(self.orderTableData.count == 0 && self.activeOrderData.count == 0){
                    self.showView = 2
                    if(currentTab == K_RECENT_TAB){
                        self.noDataText = "Reorder without the wait! We'll save your recent orders here for you."
                        self.labelNoData.text = self.noDataText
                        self.noLabelHeading.text = "No Recent Orders"
                    }else {
                        self.noDataText = "Reorder your favorites even faster! Just heart a recent order and you'll find it here."
                        self.labelNoData.text = self.noDataText
                        self.noLabelHeading.text = "No Saved Favorites"
                    }
                    if(self.showView == 1){
                        self.viewforSigup.isHidden = false
                    }else {
                        self.labelNoData.isHidden = false
                        self.viewforSigup.isHidden = true
                    }
                    self.popupView.isHidden = false
                }else {
                    self.popupView.isHidden = true
//                    if(currentTab == K_RECENT_TAB){
//                        let pageControler = TabbarPageViewController.customDataSource as!  TabbarPageViewController
//                        pageControler.setSecondController()
//
//                    }else {
//                        let pageControler = TabbarPageViewController.customDataSource as!  TabbarPageViewController
//                        pageControler.setThirdController()
//                    }
                }
                ActivityIndicator.hide()
            }
        }
    }
    
    @objc func orderCompleted(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name:
                                                    NSNotification.Name(N_CHANGE_RECENT_VIEW), object: nil)
        self.orderHistoryData = []
        self.activeOrderData = []
        Singleton.shared.recentOrderHistory = []
        Singleton.shared.activeOrderData = []
        self.recentOrderTable.reloadData()
        self.activeOrderTable.reloadData()
        self.currentPage = 1
        self.callRecentOrderApi()
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCompleted(_:)), name: NSNotification.Name(N_CHANGE_RECENT_VIEW), object: nil)
    }
    
    
    
    // MARK: Actions
    @IBAction func currentOrderdetails(_ sender: Any) {
        let pageController = MainPageViewController.customDataSource as! MainPageViewController
        pageController.setThirdController()
    }
    
    @objc func refreshHandler() {
        Singleton.shared.recentOrderHistory = []
        Singleton.shared.activeOrderData = []
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: { [weak self] in
            if #available(iOS 10.0, *) {
                self?.recentOrderTable.refreshControl?.endRefreshing()
            }
            self?.orderHistoryData = []
            self?.currentPage = 1
            self?.callRecentOrderApi()
        })
    }
    
    @IBAction func createAccountAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        myVC.popUpView = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addBagAction(_ sender: Any) {
        if let id = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String{
            ActivityIndicator.show(view: self.view)
            let param = [
                "cart_id": id
            ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_EMPTY_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_EMPTY_CART, userToken: nil) { (response) in
                Singleton.shared.cardDetails = GetCart()
                UserDefaults.standard.removeObject(forKey: UD_ADD_TO_CART)
                UserDefaults.standard.removeObject(forKey: UD_CATEGORY_TYPE)
                UserDefaults.standard.removeObject(forKey: UD_CARTITEM_COUNT)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REORDER_RECENT, method: .post, parameter: ["order_id": self.bagItem], objectClass: SuccessResponse.self, requestCode: U_REORDER_RECENT, userToken: nil) { (response) in
                    self.bagItem = 0
                    Singleton.shared.cardDetails = GetCart()
                    UserDefaults.standard.set(self.selectedMenu, forKey: UD_CATEGORY_TYPE)
                    self.addToCart.isHidden = true
                    self.recentOrderTable.reloadData()
                    self.currentOrderdetails(self)
                    
                    ActivityIndicator.hide()
                }
            }
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REORDER_RECENT, method: .post, parameter: ["order_id": bagItem], objectClass: SuccessResponse.self, requestCode: U_REORDER_RECENT, userToken: nil) { (response) in
                self.bagItem = 0
                Singleton.shared.cardDetails = GetCart()
                UserDefaults.standard.set(self.selectedMenu, forKey: UD_CATEGORY_TYPE)
                self.addToCart.isHidden = true
                self.recentOrderTable.reloadData()
                self.currentOrderdetails(self)
                ActivityIndicator.hide()
            }
        }
    }
    
    
}

// MARK: - TableView

extension RecentOrderTableViewController: FavouriteMark,UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == activeOrderTable){
            return self.activeOrderData.count
        }else {
            return self.orderTableData.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(tableView == activeOrderTable){
            return "Active Orders"
        }else {
            if(currentTab == K_RECENT_TAB){
                if(self.orderTableData.count == 0){
                    return ""
                }else{
                    return "Recent Orders"
                }
                
            }else {
                if(self.orderTableData.count == 0){
                    return ""
                }else{
                    return "Favorites"
                }
            }
            if (self.orderTableData.count == 0){
                return ""
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if(tableView == activeOrderTable){
            if(self.activeOrderData.count > 0){
                return 50
            }else {
                return 0
            }
        }else {
            
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as? UITableViewHeaderFooterView
        header?.textLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        header?.tintColor = .white
        //header?.layer.borderWidth = 0.5
        // header?.layer.borderColor = barBackgroundColor.cgColor
        header?.textLabel?.textColor = UIColor.black
        header?.textLabel?.textAlignment = .center
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == activeOrderTable){
            let cell = activeOrderTable.dequeueReusableCell(withIdentifier: "ActiveOrderTableViewCell") as! ActiveOrderTableViewCell
            if(self.activeOrderData.count > indexPath.row){
                let val = self.activeOrderData[indexPath.row]
                cell.pickupTime.text = self.convertTimestampToDate(val.pickup_time ?? 0, to: "h:mm a").lowercased()
                if let loc = val.restaurant_address?.components(separatedBy: ","){
                    if(loc.count > 0){
                        var location = val.restaurant_address?.replacingOccurrences(of: loc[0] + ",", with: "")
                        cell.pickupLocation.text = loc[0] + "\n" + (location ?? "")
                    }
                }
                cell.orderDetail = {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderReceivedViewController") as! OrderReceivedViewController
                    myVC.orderId = val.order_id ?? 0
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
            return cell
        }else {
            
            if(currentTab == K_RECENT_TAB){
                let cell = recentOrderTable.dequeueReusableCell(withIdentifier: "RecentOrderCell1", for: indexPath) as!  RecentOrderCell
                if(self.orderTableData.count > indexPath.row){
                    let data = self.orderTableData[indexPath.row]
                    cell.orderDetail = data
                    cell.testOrderTable.reloadData()
                    cell.orderDate2.text = self.convertTimestampToDate(data.order_date ?? 0, to: "M/d/yy")
                    if(data.menu_name == "Lunch"){
                        cell.menuType.text = "LUNCH"
                        ///cell.menuImage.image = UIImage(named: "☼")
                    }else {
                        cell.menuType.text = "BREAKFAST"
                        // cell.menuImage.image = UIImage(named: "Group 1")
                    }
                    var selectedItem = String()
                    if((data.order_details?.count ?? 0)! <= 1){
                        cell.viewShowMore.isHidden = true
                        
                    }else {
                        cell.viewShowMore.isHidden = false
                    }
                    for i in 0..<(data.order_details?.count ?? 0) {
                        if(i == 0){
                            cell.orderTotal2.text = data.order_details?[i].item_name
                        }else {
                            cell.orderTotal2.text = (cell.orderTotal2.text ?? "") + ", " + (data.order_details?[i].item_name ?? "")
                        }
                    }
                    
                    if(bagItem == data.order_id){
                        cell.tickImage2.image = UIImage(named: "verified(3)")
                    }else {
                        cell.tickImage2.image = UIImage(named: "icons8-filled-circle-64 (1)")
                    }
                    if(data.is_favorite == 1){
                        cell.favouriteImage2.image = UIImage(named: "favorite-heart-button")
                    }else {
                        cell.favouriteImage2.image = UIImage(named: "like")
                    }
                    
                    cell.likeButton = {
                        if(cell.favouriteImage2.image == UIImage(named: "like")){
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CartHolderNameViewController") as! CartHolderNameViewController
                            myVC.nameDelegate = self
                            self.selectedOrderId = data
                            // myVC.modalPresentationStyle = .overCurrentContext
                            self.navigationController?.present(myVC, animated: true, completion: nil)
                        }else {
                            self.selectedOrderId = data
                            self.cardHolderName(val: GetFavouriteResponse(favorite_label_id: data.favorite_label_id, label_name: nil), customMarker: false)
                            
                        }
                    }
                    
                    cell.orderButton = {
                        
                        if(data.is_in_stock == 0){
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                            myVC.heading = "This item is out of stock."
                            myVC.modalPresentationStyle = .overFullScreen
                            self.navigationController?.present(myVC, animated: false, completion: nil)
                        }else if(data.menu_name == "Lunch" && Singleton.shared.lunchTimeMessage != ""){
                            self.handleRestaurentAvialable(mainMenuId: data.menu_id ?? 0, menuName: "Lunch")
                        }else if(data.menu_name == "Breakfast" && Singleton.shared.breakfastTimeMessage != ""){
                            self.handleRestaurentAvialable(mainMenuId: data.menu_id ?? 0, menuName: "Breakfast")
                        }else {
                            if(cell.tickImage2.image == #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")){
                                self.bagItem = data.order_id ?? 0
                                self.addToCart.animShow()
                                self.recentOrderTable.reloadData()
                            }else if(cell.tickImage2.image == #imageLiteral(resourceName: "verified(3)")){
                                self.bagItem = 0
                                self.recentOrderTable.reloadData()
                                self.addToCart.animHide()
                            }
                            
                            // let indexPath = IndexPath(row: indexPath.row, section: 0)
                            //self.recentOrderTable.reloadRows(at: [indexPath], with: .none)
                            
                        }
                    }
                }
                return cell
            }else {
                let cell = recentOrderTable.dequeueReusableCell(withIdentifier: "RecentOrderCell2", for: indexPath) as! RecentOrderCell
                if(self.orderTableData.count > indexPath.row){
                    let data = self.orderTableData[indexPath.row]
                    cell.orderDetail = data
                    cell.orderDate2.text = data.label_name
                    cell.orderTotal2.text = "$" + (data.total_amount ?? "0")
                    if(data.menu_name == "Lunch"){
                        cell.menuType.text = "LUNCH"
                        //  cell.menuImage.image = UIImage(named: "☼")
                    }else {
                        cell.menuType.text = "BREAKFAST"
                        // cell.menuImage.image = UIImage(named: "Group 1")
                    }
                    //   cell.favouriteLabel.isHidden = true
                    //  cell.favouriteLabel2.isHidden = true
                    if(bagItem == data.order_id){
                        cell.tickImage2.image = UIImage(named: "verified(3)")
                    }else {
                        cell.tickImage2.image = UIImage(named: "icons8-filled-circle-64 (1)")
                    }
                    
                    if(data.is_favorite == 1){
                        cell.favouriteImage2.image = UIImage(named: "favorite-heart-button")
                    }else {
                        cell.favouriteImage2.image = UIImage(named: "like")
                    }
                    
                    var selectedItem = String()
                    if((data.order_details?.count ?? 0) <= 1){
                        cell.viewShowMore.isHidden = true
                        
                    }else {
                        cell.viewShowMore.isHidden = false
                    }
                    for i in 0..<(data.order_details?.count ?? 0) {
                        if(i == 0){
                            cell.orderTotal2.text = data.order_details?[i].item_name
                        }else {
                            
                            cell.orderTotal2.text = (cell.orderTotal2.text ?? "") + ", " + (data.order_details?[i].item_name ?? "")
                        }
                    }
                    
                    cell.orderButton = {
                        if(data.is_in_stock == 0){
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                            myVC.heading = "This item is out of stock."
                            myVC.modalPresentationStyle = .overFullScreen
                            self.navigationController?.present(myVC, animated: false, completion: nil)
                        }else if(data.menu_name == "Lunch" && Singleton.shared.lunchTimeMessage != ""){
                            self.handleRestaurentAvialable(mainMenuId: data.menu_id ?? 0, menuName: "Lunch")
                        }else if(data.menu_name == "Breakfast" && Singleton.shared.breakfastTimeMessage != ""){
                            self.handleRestaurentAvialable(mainMenuId: data.menu_id ?? 0, menuName: "Breakfast")
                        }else {
                            if(cell.tickImage2.image == #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")){
                                self.bagItem = data.order_id ?? 0
                                self.addToCart.animShow()
                                self.recentOrderTable.reloadData()
                            }else if(cell.tickImage2.image == #imageLiteral(resourceName: "verified(3)")){
                                self.bagItem = 0
                                self.recentOrderTable.reloadData()
                                self.addToCart.animHide()
                            }
                        }
                    }
                    
                    cell.likeButton = {
                        if(cell.favouriteImage2.image == UIImage(named: "like")){
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CartHolderNameViewController") as! CartHolderNameViewController
                            myVC.nameDelegate = self
                            self.selectedOrderId = data
                            // myVC.modalPresentationStyle = .overCurrentContext
                            self.navigationController?.present(myVC, animated: true, completion: nil)
                        }else {
                            
                            self.selectedOrderId = data
                            self.cardHolderName(val: GetFavouriteResponse(favorite_label_id: data.favorite_label_id, label_name: nil), customMarker: false)
                        }
                    }
                }
                return cell
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == recentOrderTable){
            let cell = tableView.cellForRow(at: indexPath) as! RecentOrderCell
            if((self.orderTableData[indexPath.row].order_details?.count ?? 0)! > 1){
                if(cell.isShowTable){
                    cell.isShowTable = false
                    UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                        cell.showMoreImage.transform = cell.showMoreImage.transform.rotated(by: CGFloat.pi/1)
                        self.currentTableIndex["indexPath"] = indexPath
                        cell.orderDetail = self.orderTableData[indexPath.row]
                        UIView.transition(with: cell.testOrderTable, duration: 0.2, options: .transitionCrossDissolve, animations: {
                            cell.testOrderTable.beginUpdates()
                            cell.testOrderTable.endUpdates()
                        },completion: nil)
                        self.currentTableIndex["height"] = cell.testOrderTable.contentSize.height + 110
                        self.recentOrderTable.beginUpdates()
                        self.recentOrderTable.endUpdates()
                        self.recentOrderTable.layoutSubviews()
                    }, completion: nil)
                    
                }else {
                    cell.isShowTable = true
                    UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                        cell.showMoreImage.transform = cell.showMoreImage.transform.rotated(by: CGFloat.pi/1)
                        self.currentTableIndex["indexPath"] = indexPath
                        cell.orderDetail = self.orderTableData[indexPath.row]
                        UIView.transition(with: cell.testOrderTable, duration: 0.2, options: .curveEaseInOut, animations: {
                            cell.testOrderTable.reloadData()
                            // cell.testOrderTable.endUpdates()
                        },completion: nil)
                        self.currentTableIndex["height"] = cell.testOrderTable.contentSize.height + 110
                        self.recentOrderTable.beginUpdates()
                        self.recentOrderTable.endUpdates()
                        self.recentOrderTable.layoutSubviews()
                    }, completion: nil)
                }
            }
        }
    }
    
    func cardHolderName(val: GetFavouriteResponse,customMarker: Bool) {
        if(customMarker){
            ActivityIndicator.show(view: self.view)
            let param = [
                "order_id": self.selectedOrderId.order_id ?? 0,
                "label_name": val.label_name
            ] as? [String: Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_FAVOURITE_LABEL, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_FAVOURITE_LABEL, userToken: nil) { (response) in
                Singleton.shared.recentOrderHistory = []
                Singleton.shared.activeOrderData = []
                self.orderHistoryData = []
                self.currentPage = 1
                self.callRecentOrderApi()
                ActivityIndicator.hide()
            }
        }else {
            if(self.selectedOrderId.is_favorite == 1){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
                self.rewardPopupType = 2
                myVC.modalPresentationStyle = .overFullScreen
                myVC.confirmationText = "Remove \(self.selectedOrderId.label_name ?? "") from favorites?"
                myVC.isConfirmationViewHidden = false
                myVC.confirmationDelegate = self
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else {
                ActivityIndicator.show(view: self.view)
                let param = [
                    "order_id": self.selectedOrderId.order_id ?? 0,
                    "favorite_label_id": val.favorite_label_id
                ] as? [String: Any]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_MARK_FAVOURITE, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_MARK_FAVOURITE, userToken: nil) { (response) in
                    Singleton.shared.recentOrderHistory = []
                    Singleton.shared.activeOrderData = []
                    self.orderHistoryData = []
                    self.currentPage = 1
                    self.callRecentOrderApi()
                    ActivityIndicator.hide()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if((self.currentTableIndex["indexPath"] as? IndexPath) == indexPath){
            if let height = self.currentTableIndex["height"] as? Int{
              return CGFloat(height)
            }
        }
        return UITableView.automaticDimension
    }
    
}

