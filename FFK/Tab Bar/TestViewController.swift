////
////  TestViewController.swift
////  FFK
////
////  Created by qw on 23/12/19.
////  Copyright © 2019 AM. All rights reserved.
////
//
//import UIKit
//
//class TestViewController: UIViewController {
//    
//    
//    @IBOutlet weak var myTable: UITableView!
//    
//    var orderHistoryData = [OrderResponse]()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ORDER_HISTORY, method: .get, parameter: nil, objectClass: GetOrder.self, requestCode: U_GET_ORDER_HISTORY, userToken: nil) { (response) in
//            self.orderHistoryData = response.response.past
//            self.myTable.reloadData()
//        }
//
//    }
//}
//
//extension TestViewController : UITableViewDelegate, UITableViewDataSource {
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.orderHistoryData.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TestTableView") as! TestTableView
//        let val = self.orderHistoryData[indexPath.row]
//        cell.orderDetail = val
//       // cell.testCollection.reloadData()
//        cell.nameLabel.text = "total"
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = myTable.cellForRow(at: indexPath) as! RestaurentTableViewCell
//        cell.testTable.isHidden = false
//    }
//    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let cell = myTable.cellForRow(at: indexPath) as! RestaurentTableViewCell
//        cell.testTable.isHidden = true
//    }
//    
//}
//
//class TestTableView: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewFlowLayout{
//    //MARK: IBOutlets
//    @IBOutlet weak var restaurentCollection: UICollectionView!
//    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
//    
//    var orderDetail:OrderResponse?
//    override func awakeFromNib() {
//           super.awakeFromNib()
//    restaurentCollection.collectionViewLayout.invalidateLayout()
//    }
//    
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return orderDetail?.order_details.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        <#code#>
//    }
//    
//    
//}
//
//
//class testCollection: UICollectionViewCell {
//    @IBOutlet weak var orderName: UILabel!
//}
