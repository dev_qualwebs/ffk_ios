//
//  DemoTableViewCell.swift
//  FFK
//
//  Created by AM on 26/09/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit


class RecentOrderCell: UITableViewCell {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var testOrderTable: UITableView!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var orderTotal2: UILabel!
    @IBOutlet weak var favouriteImage2: ImageView!
    @IBOutlet weak var orderDate2: UILabel!
    @IBOutlet weak var tickImage2: ImageView!
    //  @IBOutlet weak var favouriteLabel: UIButton!
    @IBOutlet weak var favouriteLabel2: UIButton!
    @IBOutlet weak var viewShowMore: UIView!
    @IBOutlet weak var showMoreImage: ImageView!
    
    @IBOutlet weak var menuType: UILabel!
    
    
    var likeButton: (() -> Void)? = nil
    var orderButton: (() -> Void)? = nil
    var showMore: (() -> Void)? = nil
    var isShowTable = false
    var selectedHeight = [Int:CGFloat]()
    
    var orderDetail:OrderResponse?
    
    override func awakeFromNib() {
        testOrderTable.delegate = self
        testOrderTable.dataSource = self
        testOrderTable.estimatedRowHeight = UITableView.automaticDimension
        //CGFloat(125 * (orderDetail?.order_details.count ?? 1))
        testOrderTable.rowHeight = UITableView.automaticDimension
        super.awakeFromNib()
    }
    
    
    //    override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
    //        let durations = [0.26, 0.2, 0.2]
    //        return durations[itemIndex]
    //    }
    
    
    //MARK: IBActions
    @IBAction func buttonHandler(_: AnyObject) {
        if let likeButton = self.likeButton {
            likeButton()
        }
    }
    
    @IBAction func showMoreAction(_: AnyObject) {
        if let showMore = self.showMore {
            showMore()
        }
    }
    
    @IBAction func orderAction(_ : AnyObject) {
        if let orderButton = self.orderButton{
            orderButton()
        }
    }
    
}

extension RecentOrderCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if((orderDetail?.order_details?.count ?? 0)! > 0){
            // if(self.isShowTable) {
            return (orderDetail!.order_details?.count ?? 0)!
            // }else {
            //    return 1
            // }
        }else{
            return 0
        }
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            print("Deleted")
//
//            orderDetail!.order_details.remove(at: indexPath.row) //Remove element from your array
//            tableView.deleteRows(at: [indexPath], with: .automatic)
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentOrderTableViewCell") as! RecentOrderTableViewCell
        if((indexPath.row) > ((self.orderDetail?.order_details?.count ?? 0)!-1)){
            return cell
        }
        let data = orderDetail?.order_details?[indexPath.row]
        if((orderDetail?.order_details?.count ?? 0)! <= 1){
            cell.bottomViewColor.backgroundColor = .clear
        }
        cell.itemName.text = data?.item_name
        var selectedItem = String()
        for val in (data?.order_item!)! {
            let string = val.item_name ?? ""
            selectedItem =  selectedItem + (selectedItem == "" ? "": " \n") + string
        }
        cell.subItem.text = selectedItem
        self.selectedHeight[indexPath.row] = self.getLabelHeight(text: selectedItem, width: cell.subItem.frame.width, font: cell.subItem.font)
        if(self.isShowTable || orderDetail!.order_details?.count == 1){
            cell.subItemHeight.constant = self.selectedHeight[indexPath.row] ?? 0
        }else {
            cell.subItemHeight.constant = 0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let cell = tableView.cellForRow(at: indexPath) as? RecentOrderTableViewCell{
            if(self.isShowTable){
                cell.subItemHeight.constant = self.selectedHeight[indexPath.row] ?? 0
            }else {
                cell.subItemHeight.constant = 0
            }
            return UITableView.automaticDimension
        }else {
            return UITableView.automaticDimension
        }
    }
    
    
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            print("Deleted")
//
//            currentCart.remove(at: indexPath.row) //Remove element from your array
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
//        }
//    }
    
    func getLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let lbl = UILabel(frame: .zero)
        lbl.frame.size.width = width
        lbl.font = font
        lbl.numberOfLines = 0
        lbl.text = text
        lbl.sizeToFit()
        return lbl.frame.size.height
    }
}


class RecentOrderTableViewCell: UITableViewCell {
    //MARK: IBOUtlets
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    // @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var subItem: UILabel!
    @IBOutlet weak var subItemHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewColor: UIView!
    
}

class ActiveOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var pickupTime: UILabel!
    
    var orderDetail: (() -> Void)? = nil
    
    @IBAction func orderDetailAction(_ : AnyObject) {
        if let orderDetail = self.orderDetail{
            orderDetail()
        }
    }
}
