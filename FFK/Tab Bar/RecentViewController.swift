//
//  RecentViewController.swift
//  FFK
//
//  Created by AM on 24/09/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import FoldingCell

class RecentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK: IBOUtlets
    @IBOutlet weak var recentOrderTable: UITableView!
  
    enum Const {
        static let closeCellHeight: CGFloat = 60
        static let openCellHeight: CGFloat = 120
        static let rowsCount = 3
    }
    
    var cellHeights: [CGFloat] = [60,60,60]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    

    
    
    
    // MARK: Helpers
    private func setup() {
        cellHeights = Array(repeating: Const.closeCellHeight, count: Const.rowsCount)
        recentOrderTable.estimatedRowHeight = Const.closeCellHeight
        recentOrderTable.rowHeight = UITableView.automaticDimension
       // recentOrderTable.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        if #available(iOS 10.0, *) {
            recentOrderTable.refreshControl = UIRefreshControl()
            recentOrderTable.refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        }
    }
    
    // MARK: Actions
    @objc func refreshHandler() {
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: { [weak self] in
            if #available(iOS 10.0, *) {
                self?.recentOrderTable.refreshControl?.endRefreshing()
            }
            self?.recentOrderTable.reloadData()
        })
    }
    
     func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 3
    }
    
     func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as RecentOrderTableViewCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == Const.closeCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        
       // cell.number = indexPath.row
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentOrderTableViewCell", for: indexPath) as! RecentOrderTableViewCell
        let durations: [TimeInterval] = [0.26,0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        return cell
    }
    
     func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! RecentOrderTableViewCell
        cell.foregroundView.backgroundColor = .clear
        cell.containerView.backgroundColor = .clear
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == Const.closeCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = Const.openCellHeight
            cell.unfold(true, animated: true, completion: nil)
            cell.foregroundView.backgroundColor = .clear
            cell.containerView.backgroundColor = .clear
            duration = 0.2
        } else {
            cellHeights[indexPath.row] = Const.closeCellHeight
            cell.unfold(false, animated: true, completion: nil)
            cell.foregroundView.backgroundColor = .clear
            cell.containerView.backgroundColor = .clear
            duration = 0.2
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .transitionCurlDown, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
          
            if cell.frame.maxY > tableView.frame.maxY {
                tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }, completion: nil)
    }
}


class RecentOrderTableViewCell: FoldingCell {
    //MARK: IBOutlets
    
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderFor: UILabel!
    @IBOutlet weak var modifierName: UILabel!
    @IBOutlet weak var items: UILabel!
    @IBOutlet weak var heartImage: ImageView!
    
    var likeButton:(() -> Void)? = nil
    var tickButton:(() -> Void)? = nil
    
    //MARK: IBAction
    @IBAction func likeAction(_ sender: Any) {
        if let like = self.likeButton {
            like()
        }
    }
    
    @IBAction func tickAction(_ sender: Any) {
        if let tick = self.tickButton {
            tick()
        }
    }
    
    
}


