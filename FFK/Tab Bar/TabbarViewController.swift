//
//  TabbarViewController.swift
//  FFK
//
//  Created by AM on 24/09/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

var isNavigationFromWelcome = false
var isAnimationViewHidden = false

class TabbarViewController: UIViewController, CAAnimationDelegate{
    
    //MARK: IBOutlets
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var recentLabel: UILabel!
    @IBOutlet weak var favouriteCell: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var recentButton: UIButton!
    @IBOutlet weak var menubutton: UIButton!
    @IBOutlet weak var cartItemCount: UILabel!
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var viewMenulabel: UIView!
    @IBOutlet weak var viewForRecent: UIView!
    @IBOutlet weak var viewForFavourite: UIView!
    @IBOutlet weak var animationVIew: UIView!
    @IBOutlet weak var viewForCount: View!
    @IBOutlet weak var processAmination: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menuLabel.font = UIFont(name: "Montserrat-SemiBold", size: 20)
        self.recentLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
        self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
        self.viewMenulabel.isHidden = false
        self.viewForRecent.isHidden = true
        self.viewForFavourite.isHidden = true
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if (vc.isKind(of: LoginViewController.self) || vc.isKind(of: CreateAccountViewController.self)){
                return true
            } else {
                return false
            }
        })
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("recent-order-tab"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateItemCount), name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.recentordertab), name: NSNotification.Name("recent-order-tab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateRestaurantName), name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(isNavigationFromWelcome){
            isNavigationFromWelcome = false
            self.openMenuTab()
        }
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.layer.removeAllAnimations()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.pickupLocation.text = Singleton.shared.selectedLocation.name ?? ""
        if(self.pickupLocation.text == nil || self.pickupLocation.text == ""){
            self.pickupLocation.text = "Farmer's on Cullen"
        }
        self.manageBagCount()
    }
    
    @objc func updateRestaurantName(){
        self.pickupLocation.text = Singleton.shared.selectedLocation.name ?? ""
        NavigationController.shared.getMenuType(id: Singleton.shared.selectedLocation.id ?? 1)
        self.manageBagCount()
    }
    
    @objc func hidAnimationView(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("hide_animation_view"), object: nil)
        isAnimationViewHidden = true
        UIView.animate(withDuration: 0.75, delay: 0.1, options: .transitionCrossDissolve, animations: {
            self.animationVIew.alpha = 0.5
        }) { (val) in
            self.animationVIew.alpha = 0
            UIView.transition(with: self.animationVIew, duration: 0.1, options: .transitionCrossDissolve, animations: {
                self.animationVIew.isHidden = true
            })
        }
        self.manageBagCount()
    }
    
    func manageBagCount(){
        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
            self.cartItemCount.text = "\(item)"
            if(item == 0){
                self.viewForCount.isHidden = true
            }else {
                self.viewForCount.isHidden = false
            }
        }else {
            self.viewForCount.isHidden = true
        }
    }
    
    func processAnimation() {
        let lay = CAReplicatorLayer()
        lay.frame.size = self.processAmination.frame.size
        let bar = CALayer()
        bar.frame = CGRect(x: 0,y: 0,width: 8,height: 8)
        bar.cornerRadius = bar.frame.width/2
        bar.backgroundColor = primaryColor.cgColor
        lay.addSublayer(bar)
        lay.instanceCount = 3
        lay.instanceTransform = CATransform3DMakeTranslation(20, 0, 0)
        let anim = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        anim.fromValue = 1.0
        anim.toValue = 0.2
        anim.duration = 1
        anim.repeatCount = .infinity
        anim.isRemovedOnCompletion = false
        bar.add(anim, forKey: nil)
        lay.instanceDelay = anim.duration / Double(lay.instanceCount)
        self.processAmination.layer.addSublayer(CALayer(layer: lay))
        self.processAmination.layer.addSublayer(lay)
        lay.bounds = self.processAmination.bounds
    }
    
    func openMenuTab(){
        UIButton.animate(withDuration: 0.2,
                         animations: {
            let pageController = TabbarPageViewController.customDataSource as! TabbarPageViewController
            
            pageController.setFirstController()
            self.recentLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.viewMenulabel.isHidden = false
            self.viewForRecent.isHidden = true
            self.viewForFavourite.isHidden = true
        },
                         completion: { finish in
            UIButton.animate(withDuration: 0.0, animations: {
                self.menuLabel.font = UIFont(name: "Montserrat-SemiBold", size: 20)
                self.recentLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
                self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
            })
        })
        self.manageBagCount()
    }
    
    @objc func recentordertab(){
        NotificationCenter.default.removeObserver(self
                                                  , name: NSNotification.Name("recent-order-tab"), object: nil)
        UIButton.animate(withDuration: 0.2,
                         animations: {
            self.menuLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.viewMenulabel.isHidden = true
            self.viewForRecent.isHidden = false
            self.viewForFavourite.isHidden = true
        },
                         completion: { finish in
            UIButton.animate(withDuration: 0.0, animations: {
                self.recentLabel.font = UIFont(name: "Montserrat-SemiBold", size: 20)
                self.menuLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
                self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
            })
        })
        self.manageBagCount()
    }
    
    @objc func updateItemCount() {
        NotificationCenter.default.removeObserver(self
                                                  , name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
            self.cartItemCount.text = "\(item)"
            if(item == 0){
                self.viewForCount.isHidden = true
            }else {
                self.viewForCount.isHidden = false
            }
        }else {
            self.viewForCount.isHidden = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateItemCount), name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
    }
    
    //MARK: IBACtion
    @IBAction func locationAction(_ sender: Any){
        //        currentPageView = K_MAP_PAGE_VIEW
        //        isNavigationFromMenu = true
        //        NavigationController.shared.openLocationScreen(controller: self)
        
        let pageControl = WelcomePageViewController.customDataSource as! WelcomePageViewController
      //  pageControl.currentIndex = 0
      //  pageControl.pageControl?.currentPage = 0
        pageControl.setFirstController()
    }
    
    @IBAction func bagAction(_ sender: Any) {
        let pageControler = MainPageViewController.customDataSource as!  MainPageViewController
        pageControler.setThirdController()
    }
    
    
    @IBAction func openMenuAction(_ sender: Any)
    {
        let pageControler = MainPageViewController.customDataSource as!  MainPageViewController
        pageControler.setFirstController()
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        self.openMenuTab()
    }
    
    @IBAction func recentAction(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
            let pageController = TabbarPageViewController.customDataSource as! TabbarPageViewController
            pageController.setSecondController()
            self.menuLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.viewMenulabel.isHidden = true
            self.viewForRecent.isHidden = false
            self.viewForFavourite.isHidden = true
        },
                         completion: { finish in
            UIButton.animate(withDuration: 0.0, animations: {
                self.recentLabel.font = UIFont(name: "Montserrat-SemiBold", size: 20)
                self.menuLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
                self.favouriteCell.font = UIFont(name: "Montserrat-Medium", size: 15)
            })
        })
    }
    
    @IBAction func favouriteAction(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
            let pageController = TabbarPageViewController.customDataSource as! TabbarPageViewController
            pageController.setThirdController()
            self.recentLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
            self.viewMenulabel.isHidden = true
            self.viewForRecent.isHidden = true
            self.viewForFavourite.isHidden = false
            self.menuLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
        },
                         completion: { finish in
            UIButton.animate(withDuration: 0.0, animations: {
                self.favouriteCell.font = UIFont(name: "Montserrat-SemiBold", size: 20)
                self.recentLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
                self.menuLabel.font = UIFont(name: "Montserrat-Medium", size: 15)
            })
        })
    }
    
    
}
