//
//  MultiCardViewController.swift
//  Farmers
//
//  Created by qw on 15/02/20.
//  Copyright © 2020 AM. All rights reserved.

import UIKit

class MultiCardViewController: UIViewController, CardIndexDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var cardNumber: DesignableUILabel!
    @IBOutlet weak var cardholderName: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    
    @IBOutlet weak var visaImage: UIImageView!
    
    
    static var currentIndex:Int? = nil
    var content = GetCardResponse()
    var cardData = [GetCardResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cardData = Singleton.shared.paymentCards
        CardPageViewController.indexDelegate = self
    }
    
    func getCards(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_CARD, method: .get, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_NEW_CARD, userToken: nil) { (response) in
            self.cardData = response.response
            ActivityIndicator.hide()
        }
    }
    
    
    func getContentIndex(index: Int) {
        MultiCardViewController.currentIndex = index
        let val = self.cardData[index]
        var selectedCard = String()
        switch val.card_type {
        case "VISA_CREDIT":
            self.visaImage.image = UIImage(named: "visa")
            selectedCard = "with Visa"
            break
            case "MASTERCARD_CREDIT":
                self.visaImage.image = UIImage(named: "mastercard")
                selectedCard = "with Master Card"
            break
            
            case "JCB":
                self.visaImage.image = UIImage(named: "jcb")
                selectedCard = "with JCB"
            break
            case "DISCOVER":
                self.visaImage.image = UIImage(named: "discover")
                selectedCard = "with Discover"
            break
            case "UNKNOWN":
                self.visaImage.image = nil
                selectedCard = ""
             break
            case "AMEX":
                self.visaImage.image = UIImage(named: "american-express")
                selectedCard = "with American Express"
            break
        default:
            selectedCard = ""
            self.visaImage.image = nil
        }
       
        self.cardNumber.text = val.masked_card_number
        self.cardholderName.text = val.name?.uppercased()
        let year = String(val.expiry_year!.suffix(2))
        self.expiryDate.text = (val.expiry_month ?? "") + "/" + (year ?? "")
    }
    
}
