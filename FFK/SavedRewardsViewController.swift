//
//  SavedRewardsViewController.swift
//  FFK
//
//  Created by AM on 21/10/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class SavedRewardsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK: IBOutltes
    @IBOutlet weak var rewardTable: UITableView!
    @IBOutlet weak var bdayView: UIView!
    @IBOutlet weak var adminRewardTable: ContentSizedTableView!
    @IBOutlet weak var noRewardData: UIView!
    @IBOutlet weak var bdayRedeem: CustomButton!
    @IBOutlet weak var bdayDate: UILabel!
    @IBOutlet weak var adminDate: UILabel!
    
    
    
    var rewardData = RewardResponse()
    var savedRewardData = [RewardCoupenRespose]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGestureRight))
       // swipeRight.direction = .right
      //  self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
        rewardTable.estimatedRowHeight = UITableView.automaticDimension
        rewardTable.rowHeight = 200
        rewardTable.tableFooterView = UIView()
        getSavedCoupen()
        if #available(iOS 15.0, *) {
            self.rewardTable.sectionHeaderTopPadding = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @objc func handleGestureRight(gesture: UISwipeGestureRecognizer) -> Void {
       // NavigationController.shared.addTransition(direction: .fromRight, controller: self)
           self.navigationController?.popViewController(animated: true)
    }
    
    func getSavedCoupen(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REWARD_COUPEN, method: .get, parameter: nil, objectClass: GetRewardCoupen.self, requestCode: U_GET_REWARD_COUPEN, userToken: nil) { (response) in
            self.savedRewardData = response.response.order_rewards_coupons
            self.rewardData = response.response
            self.rewardTable.delegate = self
            self.rewardTable.dataSource = self
            self.rewardTable.reloadData()
            
            if(self.rewardData.birthday_coupons.count == 0){
                self.bdayView.isHidden = true
            }else {
                self.bdayView.isHidden = false
                if((self.rewardData.birthday_coupons[0].status) == 1){
                    self.bdayRedeem.backgroundColor = primaryColor
                    self.bdayRedeem.setTitle("Apply Reward", for: .normal)
                }else if(self.rewardData.birthday_coupons[0].status == 2){
                    self.bdayRedeem.backgroundColor = .lightGray
                    
                    self.bdayRedeem.setTitle("Redeemed", for: .normal)
                    self.bdayRedeem.isUserInteractionEnabled = false
                }else if(self.rewardData.birthday_coupons[0].status == 3){
                    self.bdayRedeem.backgroundColor = primaryColor
                    //self.bdayRedeem.setTitle("Applied", for: .normal)
                    self.bdayRedeem.setTitle("Remove from Bag", for: .normal)
                    self.bdayRedeem.isUserInteractionEnabled = true
                }
            }
            
            if(self.rewardData.order_rewards_coupons.count == 0){
                self.rewardTable.isHidden = true
            }else {
                self.rewardTable.isHidden = false
            }
            
            if(self.rewardData.admin_reward_coupon.count == 0){
                self.adminRewardTable.isHidden = true
            }else {
                self.adminRewardTable.isHidden = false
                self.adminRewardTable.delegate = self
                self.adminRewardTable.dataSource = self
                self.adminRewardTable.reloadData()
            }
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        // NavigationController.shared.addTransition(direction: .fromRight, controller: self)
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func addAnotherMenu(_ sender: Any) {
           let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
           self.navigationController?.pushViewController(myVC, animated: true)
           
       }
    
    
    @IBAction func applyRewardBday(_ sender: Any) {
        if(self.rewardData.birthday_coupons.count > 0){
            if(self.rewardData.birthday_coupons[0].status == 1){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_APPLY_REWARD + "\(self.rewardData.birthday_coupons[0].coupon_id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_APPLY_REWARD, userToken: nil) { (response) in
                               Singleton.shared.cardDetails = GetCart()
                               NavigationController.shared.pushMenuFromReward(controller: self, direction: 2)
                           }
            }else if(self.rewardData.birthday_coupons[0].status == 3){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_REWARD + "\(self.rewardData.birthday_coupons[0].coupon_id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_REWARD, userToken: nil) { (response) in
                    Singleton.shared.cardDetails = GetCart()
                    Singleton.shared.selectedRewardCoupenId = nil
                    ActivityIndicator.hide()
                    self.getSavedCoupen()
                }
            }
           
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == adminRewardTable){
            if(self.rewardData.admin_reward_coupon.count == 0 && self.rewardData.birthday_coupons.count == 0){
                self.noRewardData.isHidden = false
            }else {
                self.noRewardData.isHidden = true
            }
            return self.rewardData.admin_reward_coupon.count
        }else {
            if(self.rewardData.order_rewards_coupons.count == 0 && self.rewardData.birthday_coupons.count == 0){
                self.noRewardData.isHidden = false
            }else {
                self.noRewardData.isHidden = true
            }
            return self.rewardData.order_rewards_coupons.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == adminRewardTable){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SavedRewardCell2") as! SavedRewardCell
            var val = self.rewardData.admin_reward_coupon[indexPath.row]
            cell.expiryDate.text = "Expires: \(self.convertTimestampToDate(val.expiry!, to: "M/d/yy"))"
            cell.headingLabel.text =  val.name ?? "Free Entree"
              if(val.status == 1){
                          cell.redeemButton.backgroundColor = primaryColor
                          cell.redeemButton.setTitle("Apply Reward", for: .normal)
                          cell.redeemButton.isUserInteractionEnabled = true
                      }else if(val.status == 2){
                          cell.redeemButton.backgroundColor = .lightGray
                          cell.redeemButton.setTitle("Redeemed", for: .normal)
                          cell.redeemButton.isUserInteractionEnabled = false
                      }else if(val.status == 3){
                          cell.redeemButton.backgroundColor = primaryColor
                          cell.redeemButton.setTitle("Applied", for: .normal)
                         // cell.redeemButton.isUserInteractionEnabled = false
                          cell.redeemButton.setTitle("Remove from Bag", for: .normal)
                          cell.redeemButton.isUserInteractionEnabled = true
                      }
            cell.redeemButton.sizeToFit()
            cell.orderButton = {
                
                Singleton.shared.cardDetails = GetCart()
                if(cell.redeemButton.titleLabel?.text == "Remove from Bag"){
                    ActivityIndicator.show(view: self.view)
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_REWARD + "\(Singleton.shared.selectedRewardCoupenId ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_REWARD, userToken: nil) { (response) in
                        Singleton.shared.cardDetails = GetCart()
                        Singleton.shared.selectedRewardCoupenId = nil
                        ActivityIndicator.hide()
                        self.getSavedCoupen()
                        
                    }
                }else if(cell.redeemButton.titleLabel?.text == "Applied"){
                    ActivityIndicator.hide()
                    return
                }else {
                    ActivityIndicator.show(view: self.view)
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_APPLY_REWARD + "\(val.coupon_id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_APPLY_REWARD, userToken: nil) { (response) in
                        Singleton.shared.cardDetails = GetCart()
                        Singleton.shared.selectedRewardCoupenId = val.coupon_id ?? 0
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                               myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "We've added a free entree to your bag. Way to go!"
                        ActivityIndicator.hide()
                        NavigationController.shared.pushMenuFromReward(controller: self, direction: 2)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.navigationController?.present(myVC, animated: false, completion: nil)
                        })
                    }
                }
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SavedRewardCell") as! SavedRewardCell
            var val = self.savedRewardData[indexPath.row]
            cell.expiryDate.text = "Expires: \(self.convertTimestampToDate(val.expiry!, to: "M/d/yy"))"
            cell.headingLabel.text =  val.name ?? "Free Entree"
            cell.subheadingLabel.text = "Enjoy a FREE regular breakfast or lunch entree! Extras may be added at an additional cost."
            
            if(val.status == 1){
                cell.redeemButton.backgroundColor = primaryColor
                cell.redeemButton.setTitle("Apply Reward", for: .normal)
                cell.redeemButton.isUserInteractionEnabled = true
            }else if(val.status == 2){
                cell.redeemButton.backgroundColor = .lightGray
                cell.redeemButton.setTitle("Redeemed", for: .normal)
                cell.redeemButton.isUserInteractionEnabled = false
            }else if(val.status == 3){
                cell.redeemButton.backgroundColor = primaryColor
                cell.redeemButton.isUserInteractionEnabled = false
               // cell.redeemButton.setTitle("Applied", for: .normal)
                cell.redeemButton.setTitle("Remove from Bag", for: .normal)
                cell.redeemButton.isUserInteractionEnabled = true
            }
            cell.redeemButton.sizeToFit()
            cell.orderButton = {
                
                Singleton.shared.cardDetails = GetCart()
                if(cell.redeemButton.titleLabel?.text == "Remove from Bag"){
                    ActivityIndicator.show(view: self.view)
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_REWARD + "\(Singleton.shared.selectedRewardCoupenId ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_REWARD, userToken: nil) { (response) in
                        Singleton.shared.cardDetails = GetCart()
                        Singleton.shared.selectedRewardCoupenId = nil
                        ActivityIndicator.hide()
                        self.getSavedCoupen()
                        
                    }
                }else if(cell.redeemButton.titleLabel?.text == "Applied"){
                    ActivityIndicator.hide()
                    return
                }else {
                    ActivityIndicator.show(view: self.view)
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_APPLY_REWARD + "\(val.coupon_id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_APPLY_REWARD, userToken: nil) { (response) in
                        Singleton.shared.cardDetails = GetCart()
                        Singleton.shared.selectedRewardCoupenId = val.coupon_id ?? 0
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                               myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "We've added a free entree to your bag. Way to go!"
                        ActivityIndicator.hide()
                        NavigationController.shared.pushMenuFromReward(controller: self, direction: 2)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.navigationController?.present(myVC, animated: false, completion: nil)
                        })
                    }
                }
            }
            return cell
        }
    }
    
}

class SavedRewardCell: UITableViewCell{
    //MARK: IBOUtlets
    // @IBOutlet weak var dateOn: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var redeemButton: CustomButton!
    
    @IBOutlet weak var subheadingLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    
    var orderButton:(()-> Void)? = nil
    
    @IBAction func orderAction(_ sender: Any) {
        if let orderButton = self.orderButton {
            orderButton()
        }
    }
}
