//
//  SaveNicknameVC.swift
//  FFK
//
//  Created by Qualwebs on 22/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
//import iOSDropDown

protocol FavouriteMark {
    func cardHolderName(val: GetFavouriteResponse,customMarker: Bool)
}

class CartHolderNameViewController: UIViewController {
    
    //MARK: IBOUtlets
  //  @IBOutlet weak var userName: SkyFloatingLabelTextField!
    @IBOutlet weak var favouriteMark: SkyFloatingLabelTextField!
    @IBOutlet weak var favoriteLabelCount: UILabel!
    @IBOutlet weak var contentView: View!
    
    
    
    var nameDelegate: FavouriteMark? = nil
    var markData = [GetFavouriteResponse]()
    var selectedMark = GetFavouriteResponse()
    var currentIndex = 0
    var isCustomMark = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.keyboardAppearance = UIKeyboardAppearance(rawValue: 50)!
        self.favouriteMark.delegate = self
        if(Singleton.shared.favouriteLabel.count == 0){
            getFavouriteLabel()
            
        }else {
            self.markData = Singleton.shared.favouriteLabel
            self.favouriteMark.text = self.markData[0].label_name
            self.favoriteLabelCount.text = "\(20 - (self.favouriteMark.text?.count ?? 0))"
            self.selectedMark = self.markData[0]
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.previousNextDisplayMode = .default
    }
    
    func getFavouriteLabel(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_FAVOURITE_LABEL, method: .get, parameter: nil, objectClass: GetFavouriteLabel.self, requestCode: U_FAVOURITE_LABEL, userToken: nil) { (response) in
            self.markData  = response.response
            self.favouriteMark.text = self.markData[0].label_name
            self.favoriteLabelCount.text = "\(20 - (self.favouriteMark.text?.count ?? 0))"
            self.selectedMark = self.markData[0]
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func cancelBtnPressed(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if(self.favouriteMark.text!.isEmpty){
            self.favouriteMark.resignFirstResponder()
            self.favouriteMark.errorMessage = "Please add a label"
        }else {
            if(self.isCustomMark){
                self.nameDelegate?.cardHolderName(val:GetFavouriteResponse(favorite_label_id: 0, label_name: self.favouriteMark.text ?? "") ,customMarker: true)
            }else {
                self.nameDelegate?.cardHolderName(val: self.selectedMark,customMarker: false)
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        if(markData.count > 0){
            if(currentIndex == self.markData.count-1){
                currentIndex = 0
                self.favouriteMark.text = self.markData[currentIndex].label_name
                self.favoriteLabelCount.text = "\(20 - (self.favouriteMark.text?.count ?? 0))"
                self.selectedMark = self.markData[currentIndex]
                self.isCustomMark = false
            }else {

                self.isCustomMark = false
                currentIndex = currentIndex + 1
                self.favouriteMark.text = self.markData[currentIndex].label_name
                self.favoriteLabelCount.text = "\(20 - (self.favouriteMark.text?.count ?? 0))"
                self.selectedMark = self.markData[currentIndex]
            }
        }
    }
    
}

extension CartHolderNameViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.autocorrectionType = .no
        self.favouriteMark.errorMessage = ""
        self.favoriteLabelCount.text = "20"
        self.favouriteMark.text = ""
        self.isCustomMark = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.contentView.center = self.view.center
        if(self.favouriteMark.text?.lowercased() == self.selectedMark.label_name?.lowercased()){
            self.favouriteMark.text = self.selectedMark.label_name
            self.favoriteLabelCount.text = "\(20 - (self.favouriteMark.text?.count ?? 0))"
            self.isCustomMark = false
        }else if(self.favouriteMark.text!.isEmpty){
            self.favouriteMark.text = ""//self.selectedMark.label_name
            self.isCustomMark = true
        }else {
            self.isCustomMark = true
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if(textField == favouriteMark){
          let currentCharacterCount = self.favouriteMark.text?.count ?? 0
          if (range.length + range.location > currentCharacterCount){
              return false
          }
          let newLength = currentCharacterCount + string.count - range.length
          self.favoriteLabelCount.text = "\(20 - newLength)"
          return newLength <= 19
          }else {
            return true
        }
      }
}
