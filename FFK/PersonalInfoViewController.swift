//
//  PersonalInfoViewController.swift
//  FFK
//
//  Created by AM on 07/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class PersonalInfoViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var infoStack: UIStackView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var zipcode: SkyFloatingLabelTextField!
    @IBOutlet weak var birthday: SkyFloatingLabelTextField!
    @IBOutlet weak var phone: SkyFloatingLabelTextField!
    @IBOutlet weak var changePassImg: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    
    let isLoggedIn = UserDefaults.standard.value(forKey: UD_TOKEN)
    var userData = UserInfo()
    var selectedDOB:Int?
    var errorCount = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
        
      //  swipeRight.direction = .right
      //  self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
        infoStack.isUserInteractionEnabled = false
        self.changePassImg.isHidden = true
        self.saveButton.isHidden = true
        self.zipcode.delegate = self
        self.phone.delegate = self
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.email.delegate = self
        self.phone.delegate = self
        self.birthday.delegate = self
        self.zipcode.delegate = self
        birthday.isUserInteractionEnabled = false
        if(isLoggedIn != nil){
            self.getUserInfo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.editButton.isHidden = true
        self.saveButton.isHidden = false
        self.infoStack.isUserInteractionEnabled = true
        self.changePassImg.isHidden = false
        //  self.name.becomeFirstResponder()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        self.editInfo()
    }
    
    
    @objc func handleDate() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: N_SELECT_DATE), object: nil)
        if (K_PICKER_DATE != nil){
            self.birthday.text = self.convertTimestampToDate(Int(K_PICKER_DATE!.timeIntervalSince1970), to: "MMM dd, yyyy")
            self.selectedDOB = Int(K_PICKER_DATE!.timeIntervalSince1970)
            
        }
    }
    
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDate), name:Notification.Name(rawValue: N_SELECT_DATE), object: nil)
        self.navigationController?.present(myVC, animated: true)
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    func getUserInfo(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PROFILE, method: .get, parameter: nil, objectClass: GetUserInfo.self, requestCode: U_UPDATE_PROFILE, userToken: nil) { (response) in
            let data = response.response
            self.userData = response.response
            self.firstName.text  = (data.first_name ?? "")
            self.lastName.text  = (data.last_name ?? "")
            self.email.text = data.email
//            if((data.date_of_birth ?? 0) != 0){
//                self.birthday.text = self.convertTimestampToDate(data.date_of_birth ?? 0, to: "MMM d, yyyy")
//            }
//            self.selectedDOB = data.date_of_birth ?? 0
            if((data.date_of_birth ?? "") != ""){
                let timestamp = self.convertDateToTimestamp(data.date_of_birth ?? "", to: "MM-dd-yyyy")
                self.birthday.text = self.convertTimestampToDate(timestamp, to: "MMM d, yyyy")
            }
   //         self.selectedDOB = data.date_of_birth ?? 0
            self.phone.text = data.mobile
            self.phone.text =  self.phone.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            self.zipcode.text = data.zip_code
            
        }
    }
    
    func editInfo() {
        self.errorCount = 0
        if(firstName.text!.isEmpty){
            self.errorCount += 1
            firstName.errorMessage = "Enter First Name"
        }
        
        if(lastName.text!.isEmpty){
            self.errorCount += 1
            lastName.errorMessage = "Enter Last Name"
        }
        
        if(email.text!.isEmpty){
            self.errorCount += 1
            email.errorMessage = "Enter Email Address"
        }
        if !(self.isValidEmail(emailStr: self.email.text!.replacingOccurrences(of: " ", with: ""))){
            self.errorCount += 1
            email.errorMessage = "Please Enter Valid Email Address"
        }
        if(phone.text!.isEmpty){
            self.errorCount += 1
            phone.errorMessage = "Please Enter Mobile Number"
        }
        
        if(self.errorCount > 0){
            return
        }else {
            ActivityIndicator.show(view: self.view)
            self.infoStack.isUserInteractionEnabled = true
            self.changePassImg.isHidden = false
            
            var telephone = self.phone.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            let dob = self.convertTimestampToDate(self.selectedDOB ?? 0, to: "MM-dd-yyyy")
            let param = [
                "first_name":self.firstName.text ?? "",
                "last_name":self.lastName.text ?? "",
                "email": (self.email.text ?? "").replacingOccurrences(of: " ", with: ""),
                "mobile": telephone,
                "zip_code": self.zipcode.text ?? "",
                "date_of_birth":dob
                ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_PROFILE, method: .post, parameter: param, objectClass: ErrorResponse.self, requestCode: U_UPDATE_PROFILE, userToken: nil) { (response) in
                self.saveButton.isHidden = true
                self.editButton.isHidden = false
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.heading = "Your profile has been updated!"
                myVC.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(myVC, animated: false, completion: nil)
                self.infoStack.isUserInteractionEnabled = false
                self.changePassImg.isHidden = true
                self.editButton.setTitle("Edit", for: .normal)
                Singleton.shared.userDetail.first_name = self.firstName.text ?? ""
                Singleton.shared.userDetail.last_name = self.lastName.text ?? ""
                Singleton.shared.userDetail.email = self.email.text ?? ""
                Singleton.shared.userDetail.mobile = self.phone.text ?? ""
                Singleton.shared.userDetail.zip_code = self.zipcode.text ?? ""
                Singleton.shared.userDetail.date_of_birth = dob//self.selectedDOB
                let userData = try! JSONEncoder().encode(Singleton.shared.userDetail)
                UserDefaults.standard.set(userData, forKey:UD_USER_DETAIl)
                ActivityIndicator.hide()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == phone){
            self.phone.text =  self.phone.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            let currentCharacterCount = self.phone.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14
        }else if(textField == zipcode) {
            let currentCharacterCount = self.zipcode.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 5
        }else {
            return true
        }
    }
}
