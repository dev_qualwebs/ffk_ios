

import UIKit

var selectedCardId: Int?
var appleCardSelected = false

class PaymentVC: UIView {
    //MARK: IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var cardNumber: UITextField!
       @IBOutlet weak var cardmonth: UITextField!
       @IBOutlet weak var cvvNumber: UITextField!
       @IBOutlet weak var cardYear: UITextField!
       @IBOutlet weak var cardImage: UIImageView!
       
       @IBOutlet weak var cardNumberView: View!
       @IBOutlet weak var expiryMonthView: View!
       @IBOutlet weak var expiryYearView: View!
       @IBOutlet weak var cardCVVView: View!
    
    var isCardInvalid = false
    var guestLoginDetails = [String:Any]()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
       Bundle.main.loadNibNamed("MapView", owner: self, options: nil)
       contentView.fixInView(self)
        
        if let guestLogin = UserDefaults.standard.value(forKey: UD_GUEST_LOGIN_DETAIL) as? [String:Any]{
          self.guestLoginDetails = guestLogin
        }
        
            
    }
    
    
}




extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
