//
//  RestaurentTableViewCell.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class RestaurentTableViewCell: UITableViewCell  {
    //MARK: IBOutlets
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var lblNumberofItems: UILabel!
    @IBOutlet weak var viewForTick: UIView!
    @IBOutlet weak var quantityStack: UIView!
    @IBOutlet weak var btnForTick: UIButton!
    @IBOutlet weak var rewardType: UILabel!
    @IBOutlet weak var labelForError: UILabel!
    @IBOutlet weak var viewForError: View!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var viewForRewardType: UIView!
    @IBOutlet weak var backVIewCount: View!
    @IBOutlet weak var viewNoModifiers: UIView!
    
    var nVC:FooItemViewController?
    var selectItem: (() -> Void)? = nil
    var plusItem: (() -> Void)? = nil
    var minusItem: (() -> Void)? = nil
    var removeItem: (() -> Void)? = nil
    var editItem: (() -> Void)? = nil
    var duplicateItem: (() -> Void)? = nil
    var orderDetail:OrderResponse?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func selectAction(_ sender: Any) {
        if let selectItem = self.selectItem {
            selectItem()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func addItemsPressed(_ sender: Any)
    {
        if let plusItem = self.plusItem {
            plusItem()
        }
    }
    
    @IBAction func subtractItemsPressed(_ sender: Any)
    {
        if let minusItem = self.minusItem {
            minusItem()
        }
    }
    
    @IBAction func removeAction(_ sender: Any){
        if let removeItem = self.removeItem {
            removeItem()
        }
    }
    
    @IBAction func duplicateAction(_ sender: Any) {
        if let duplicate = self.duplicateItem{
            duplicate()
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        if let edit = self.editItem{
            edit()
        }
    }

}

