//
//  CardKnoxViewController.swift
//  Farmers
//
//  Created by Sagar Pandit on 16/07/23.
//  Copyright © 2023 AM. All rights reserved.
//

import UIKit


class CardKnoxViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let _notificationCenter = NotificationCenter.default
              _notificationCenter.addObserver(self,
                                              selector: #selector(transactionNotification(aNotification:)),
                                              name: Notification.Name(CardknoxSDK.transactionResultSubscription_NSNotificationCenterName()),
                                              object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CardknoxSDK.setPrincipalKey("qualwebsdev0ca39ee2fa384011907ea65b64f07982")
        CardknoxSDK.setxSoftwareName("FarmersFreshMeat", xSoftwareVersion: "1.0.0", xVersion: "4.5.9")
    }
    
    
    // Define a function that accepts a Notification.
    // This method will be invoked when the SDK sends transaction processing results
    @objc func transactionNotification(aNotification : Notification) {
        // Use the SDK's "response" object utility method to transform a Notification into a "response" object
        let response = PaymentTransactionResponse.unwrap(aNotification) as! PaymentTransactionResponse

        if response.isSuccess()
        {
            // Transaction successfully processed
            let errorMessage = response.errorMessage()!
            let refNum = response.xRefNum()!
            // ... other properties ...
        } else {
            // Transaction processing resulted in an error message which can be extracted from this property:
            let errorMessage = response.errorMessage()!
            let errorCode = response.xErrorCode()!
            let error = response.xError()!
        }
    }
    

}
