
import UIKit



class MainPageViewController: UIPageViewController, UIScrollViewDelegate {
    
    //MARK: Properties
    var pageControl: UIPageControl?
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    lazy var viewControllerList = [UIViewController]()
    var sb = UIStoryboard(name:"Main",bundle:nil)
    fileprivate var currentIndex = 1
    var isFirstTime = true
    static var accountDelegate: ResetData? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        MainPageViewController.customDataSource = self
        viewControllerList = [sb.instantiateViewController(withIdentifier: "SideMenuViewController" ),
                              sb.instantiateViewController(withIdentifier: "WelcomeNavigationViewController" ),
                              sb.instantiateViewController(withIdentifier: "OrderCartViewController")
        ]
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([viewControllerList[1]],direction: .forward,
                                    animated:false,completion:nil)
        }
        
        for view in view.subviews {
            if view is UIScrollView {
                (view as! UIScrollView).delegate =  self
                (view as! UIScrollView).bounces = true
                (view as! UIScrollView).isScrollEnabled = true
                break
            }
        }
    }
    
   
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(currentIndex == 0 && scrollView.contentOffset.x < self.view.frame.width){
            scrollView.contentOffset.x = self.view.frame.width
        }else if(currentIndex == 2 && scrollView.contentOffset.x > self.view.frame.width){
            scrollView.contentOffset.x = self.view.frame.width
        }
        if(currentIndex == 0 && isFirstTime){
            self.isFirstTime = false
            NotificationCenter.default.post(name: NSNotification.Name(N_BREAK_DATA), object: nil, userInfo: ["from_account": true])
              NotificationCenter.default.post(name: NSNotification.Name(N_LUNCH_DATA), object: nil, userInfo: ["from_account": true])
        }else if(currentIndex == 1){
            self.isFirstTime = true
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (currentIndex == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0);
        } else if (currentIndex == viewControllerList.count - 1 && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0);
        }
    }
    
    func setFirstController() {
        //currentIndex = 0
        setViewControllers([viewControllerList[0]], direction: .reverse, animated: true, completion: nil)
        // self.pageControl?.currentPage = 0
    }
    
    func setSecondController() {
        currentIndex = 1
        setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 1
    }
    
    
    func setHomeController() {
        currentIndex = 1
        setViewControllers([viewControllerList[1]], direction: .reverse, animated: true, completion: nil)
        //self.pageControl?.currentPage = 1
    }
    
    func setThirdController() {
        setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
}

extension MainPageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
        return viewControllerList[nextIndex]
    }
}

extension MainPageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
        
        if completed {
               // Get current index
               let pageContentViewController = pageViewController.viewControllers![0]
               currentIndex = viewControllerList.index(of: pageContentViewController)!
        }
    }
}


