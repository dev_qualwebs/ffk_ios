//
//  APIViewController.swift
//  FFK
//
//  Created by AM on 01/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import CoreLocation
class NavigationController: UIViewController {
    
    static var shared = NavigationController()
    var  locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func addTransition(direction: CATransitionSubtype,controller: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = direction
        
        UIApplication.shared.windows.first?.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        self.view.backgroundColor = .clear
        controller.navigationController?.view.layer.add(transition, forKey: nil)
    }
    
    
   
    
    func navigateSavedReward(controller: UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "SavedRewardsViewController") as! SavedRewardsViewController
        self.addTransition(direction:.fromLeft, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func navigateBonus(controller: UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "BonusViewController") as! BonusViewController
        self.addTransition(direction:.fromLeft, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func pushHome(controller: UIViewController) {
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.addTransition(direction:.fromTop, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if (vc.isKind(of: LoginViewController.self) || vc.isKind(of: CreateAccountViewController.self)){
                return true
            } else {
                return false
            }
        })
    }
    
    func pushStart(controller: UIViewController) {
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.addTransition(direction:.fromTop, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    func pushHome2(controller: UIViewController) {
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.addTransition(direction:.fromLeft, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func showAlertScreen(message: String){
        if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as? NotAvailableViewController{
            myVC.heading = message
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: false, completion: nil)
        }
    }
    
    
    //    func pushFromOrder(controller: UIViewController){
    //        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
    //        self.addTransition(direction:.fromLeft, controller: controller)
    //        controller.navigationController?.pushViewController(myVC, animated: false)
    //    }
    //    func popHome(controller: UIViewController){
    //    }
    
    //NAVIGATE MapScreen
    func openLocationScreen(controller: UIViewController) {
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        let transition:CATransition = CATransition()
        currentPageView = K_MAP_PAGE_VIEW
        self.addTransition(direction: .fromBottom, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    //NAVIGATE CreateAccount
    func openCreateAccount(controller:UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    //NAVIGATE LoginScreen
    func openLoginScreen(controller:UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    //NAVIGATE SIDEMENU
    func pushMenu(controller: UIViewController) {
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        self.addTransition(direction:.fromLeft, controller: controller)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
    
    //NAVIGATE FROM REWARD ITEM
    func pushMenuFromReward(controller: UIViewController,direction: Int) {
        if(direction == 1){
            let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            self.addTransition(direction:.fromLeft, controller: controller)
            isNavigationFromRewardItem = false
            controller.navigationController?.pushViewController(myVC, animated: true)
        }else if(direction == 3){
            let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            self.addTransition(direction:.fromRight, controller: controller)
            isNavigationFromRewardItem = false
            controller.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            self.addTransition(direction:.fromRight, controller: controller)
            isNavigationFromRewardItem = true
            controller.navigationController?.pushViewController(myVC, animated: true)
        }
        
    }
    
    func getCartData(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CART_DATA, method: .get, parameter: nil, objectClass: GetCart.self, requestCode: U_GET_CART_DATA, userToken: nil) { (response) in
            if(response.response.cart_data.count > 0){
                UserDefaults.standard.setValue(response.response.cart_details.cart_id ?? "", forKey: UD_ADD_TO_CART)
                UserDefaults.standard.set(response.response.cart_data.count, forKey: UD_CARTITEM_COUNT)
                NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
            }
        }
    }
    
    func emptyMyCart() {
        if let id = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String{
            let param = [
                "cart_id": id
            ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_EMPTY_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_EMPTY_CART, userToken: nil) { (response) in
                Singleton.shared.cardDetails = GetCart()
                UserDefaults.standard.removeObject(forKey: UD_ADD_TO_CART)
                UserDefaults.standard.removeObject(forKey: UD_CATEGORY_TYPE)
                UserDefaults.standard.removeObject(forKey: UD_CARTITEM_COUNT)
                NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
            }
        }
    }
    
    func getMenuType(id: Int){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU_TYPE +  "\(id)", method: .get, parameter: nil, objectClass: GetMenu.self, requestCode: U_GET_MENU_TYPE, userToken: nil) { (dataResponse) in
            if(dataResponse.response.count > 1){
                Singleton.shared.mainMenuData = dataResponse.response
                let data = try! JSONEncoder().encode(Singleton.shared.mainMenuData)
                UserDefaults.standard.set(data, forKey: UD_MENU_TYPE)
                K_BREAK_ID = dataResponse.response[0].menu_id ?? 0
                MENU_TYPE = K_BREAK_ID
                K_LUNCH_ID = dataResponse.response[1].menu_id ?? 0
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_BREAK_DATA)))
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_LUNCH_DATA)))
            }
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    func getPreferenceData() {
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PREFERENCE, method: .get, parameter: nil, objectClass: GetPReference.self, requestCode: U_GET_PREFERENCE, userToken: nil) { (response) in
                Singleton.shared.prefrenceData = response.response
                UserDefaults.standard.setValue(Singleton.shared.prefrenceData.restaurant_preference ?? 0, forKey: UD_SELECTED_LOCATION_PREF)
            }
        }
    }
    
    func emailNotificationAction(param:Int){
        if let id = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DEVICE_PREFER, method: .post, parameter: ["push_notification":param], objectClass: SuccessResponse.self, requestCode: U_DEVICE_PREFER, userToken: nil) { (response) in
                NavigationController.shared.getPreferenceData()
            }
        }
    }
    
    func getRecentOrder(_ orderId: Int?){
            if let logged = UserDefaults.standard.value(forKey: UD_TOKEN){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ORDER_HISTORY + "/\(Singleton.shared.selectedLocation.id ?? 1)", method: .get, parameter: nil, objectClass: GetOrder.self, requestCode: U_GET_ORDER_HISTORY, userToken: nil) { response in
                    Singleton.shared.recentOrderHistory = response.response.past
                    Singleton.shared.activeOrderData = response.response.active
                    Singleton.shared.singleArrayContent = []
                    if(response.response.active.count == 0){
                        UserDefaults.standard.removeObject(forKey: UD_ACTIVE_ORDER_EXIST)
                    }else{
                        let object = SplashContent(pickupTime: response.response.active[0].pickup_time, address: response.response.active[0].restaurant_address)
                        let data = try! JSONEncoder().encode(object)
                        UserDefaults.standard.set(data, forKey: UD_SINGLE_ACTIVE_ORDER)
                        UserDefaults.standard.set(try! JSONEncoder().encode(response.response.active[0]), forKey: UD_ACTIVE_ORDER_EXIST)
                        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_WELCOME_VIEW), object: nil)
                    }
                    for val in Singleton.shared.activeOrderData {
                        Singleton.shared.singleArrayContent.append(SplashContent(pickupTime: val.pickup_time, address: val.restaurant_address))
                    }
                    if(orderId != nil){
                        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_ORDERCART_VIEW),object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_RECENT_VIEW),object: nil)
                    }
                }
            }
        
    }
}
