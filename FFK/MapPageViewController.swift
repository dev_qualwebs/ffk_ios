//
//  MapPageViewController.swift
//  FFK
//
//  Created by Qualwebs on 22/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class MapPageViewController: UIPageViewController {
    
    //Mark:Properties
    var pageControl:UIPageControl?
    static var customDataSource:UIPageViewControllerDataSource?
    lazy var viewControllerList:[UIViewController] = {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        return [sb.instantiateViewController(withIdentifier: "LocationViewController")]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let firstViewController = viewControllerList.first {
         self.setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
        }
    
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
   
}
