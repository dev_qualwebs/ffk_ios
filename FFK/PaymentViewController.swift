//
//  PaymentViewController.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Foundation
import TransitionButton
import SkyFloatingLabelTextField
import PassKit
import UPCarouselFlowLayout
import SquareInAppPaymentsSDK


var menuId = Int()
var timer = Timer()
class PaymentViewController: UIViewController,SlideButtonDelegate,AddCard,Confirmation{
    func handleGuestCard(nonce: String?, zip: String?, street: String?) {
        self.guestNonceId = nonce ?? ""
        self.cardknoxStreet = street ?? ""
        self.cardKnoxZip = zip ?? ""
        self.callGuestOrderAPI()
    }
    
    func confirmationSelection() {
        if(self.isGuestLogin){
            self.callGuestOrderAPI()
        }else {
            self.callAPiForOrder()
        }
        
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var timeCollectionView: UICollectionView!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var paymentButton: TransitionButton!
    @IBOutlet weak var slidingPayView: MMSlidingButton!
    @IBOutlet weak var notAvailableView: UIView!
    @IBOutlet weak var notAvailableLabel: UILabel!
    @IBOutlet weak var slideStackView: UIStackView!
    @IBOutlet weak var cardStack: UIStackView!
    @IBOutlet weak var viewChangeTime: UIView!
    @IBOutlet weak var freeOrderText: UIView!
    @IBOutlet weak var currentTimeLabel: UILabel!
    
    @IBOutlet weak var cardNumber: UITextField!
    @IBOutlet weak var cardmonth: UITextField!
    @IBOutlet weak var cvvNumber: UITextField!
    @IBOutlet weak var cardYear: UITextField!
    @IBOutlet weak var cardImage: UIImageView!
    
    @IBOutlet weak var cardNumberView: View!
    @IBOutlet weak var expiryMonthView: View!
    @IBOutlet weak var expiryYearView: View!
    @IBOutlet weak var cardCVVView: View!
    @IBOutlet weak var cardDetailStack: UIView!
    
    @IBOutlet weak var postalCodeView: View!
    @IBOutlet weak var postalCode: UITextField!
    
    @IBOutlet weak var mainTimeView: UIView!
    @IBOutlet weak var bottomSlideSpace: NSLayoutConstraint!
    @IBOutlet weak var paymentCards: UICollectionView!
    @IBOutlet weak var applePayButton: UIView!
    @IBOutlet weak var guestAppleView: UIStackView!
    
    //    @IBOutlet weak var aboutView: View!
    //    @IBOutlet weak var downArrowImage: UIImageView!
    //    @IBOutlet weak var gradientView: GradientView!
    //    @IBOutlet weak var gradientBgView: UIImageView!
    @IBOutlet weak var descriptionLabel: DesignableUILabel!
    @IBOutlet weak var addCardPayLabel: UILabel!
    
    
    var currentTime:Int? = nil
    var previousTime:Int? = nil
    var orderCart: String?
    var activityIndicator = UIActivityIndicatorView()
    var timeArr = [String]()
    var transition: JTMaterialTransition?
    var shortTime:[String] = []
    var arrDateTime = [Date]()
    var dateTimeTable = Date()
    var isTimeSelected = false
    var isTimeSelectedBefore = false
    var isFreeOrder = false
    var timeFrom: Int?
    var timeTo: Int?
    var selectedCard: GetCardResponse?
    var isGuestLogin = false
    var guestLoginDetails = [String:Any]()
    var isCardInvalid = false
    var appplePaymentData = [String : Any]()
    let layout = UPCarouselFlowLayout()
    var indexNumber:Int = 0
    var myCards = Singleton.shared.paymentCards
    var bonus_id:Int?
    var guestNonceId = String()
    var guestCheckoutTime = Int()
    var cardknoxStreet = String()
    var cardKnoxZip = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Regular", size: 12), NSAttributedString.Key.foregroundColor : UIColor.lightGray]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Montserrat-Regular", size: 12), NSAttributedString.Key.foregroundColor : primaryColor]
        
        let attributedString1 = NSMutableAttributedString(string:"By placing this order, you consent to text messages and agree to the ", attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:"Terms of Service. ", attributes:attrs2)
        let attributedString3 = NSMutableAttributedString(string:"Est. 4 Msgs/Order. Reply STOP to cancel, HELP for help. Msg&data rates may apply.", attributes:attrs1)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        self.descriptionLabel.attributedText = attributedString1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let cardknoxSDKUI = CardknoxSDKUI.create() as? CardknoxSDKUI
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.paymentCards.isUserInteractionEnabled = true
        self.paymentCards.setContentOffset(CGPoint.zero, animated:false)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_ADD_NEW_CARD), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_ITEM_OUTOFSTOCK), object: nil)
    }
    
    func manageBottomSpace(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                // print("iPhone 5 or 5S or 5C")
                self.bottomSlideSpace.constant = 20
                break
            case 1334:
                // print("iPhone 6/6S/7/8")
                self.bottomSlideSpace.constant = 25
                break
            case 1920, 2208:
                // print("iPhone 6+/6S+/7+/8+")
                self.bottomSlideSpace.constant = 40
                break
            case 2436:
                //print("iPhone X/XS/11 Pro")
                self.bottomSlideSpace.constant = 50
                break
            case 2688:
                // print("iPhone XS Max/11 Pro Max")
                self.bottomSlideSpace.constant = 90
                break
            case 1792:
                // print("iPhone XR/ 11 ")
                self.bottomSlideSpace.constant = 80
                break
            default:
                print("Unknown")
            }
        }
    }
    
    
    func decimal(with string: String) -> NSDecimalNumber {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        return formatter.number(from: string) as? NSDecimalNumber ?? 0
    }
    
    func setTime(){
        var startTime = Date(timeIntervalSince1970: TimeInterval(timeFrom ?? 0))
        //    startTime = startTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
        var endTime = Date(timeIntervalSince1970: TimeInterval(timeTo ?? 0))
        
        // endTime = endTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
        self.handleTableTime(startTime: startTime, endTime: endTime)
        
        var timeStamp:String?
        self.timeArr = []
        var count = 0
        
        self.shortTime = []
        
        for i in 0...arrDateTime.count-1
        {
            if !(arrDateTime[i] > Calendar.current.date(byAdding: .minute, value: 15, to: Date())!){
                continue
            }
            let date = Int(arrDateTime[i].timeIntervalSince1970)
            if(date > Int(Date().timeIntervalSince1970)){
                count = count + 1
                timeStamp = self.convertTimestampToDate(date, to: "h:mm a")
                self.viewChangeTime.isHidden = true
                if(count == 0){
                    shortTime.append(timeStamp! + " (quickest)")
                }else if(count < 4){
                    shortTime.append(timeStamp!)
                }else if(count == 4){
                    shortTime.append(timeStamp!)
                }else if(count == 5){
                    shortTime.append("more options")
                }
                timeArr.append(timeStamp!)
            }
        }
        if(self.timeArr.count < 5){
            self.shortTime = self.timeArr
        }
        if(timeArr.count == 0){
            self.notAvailableView.isHidden = false
            self.timeCollectionView.isHidden = true
            self.viewChangeTime.isHidden = true
            self.mainTimeView.isHidden = true
        }else{
            self.mainTimeView.isHidden = false
            
            self.currentTimeLabel.text = timeArr[0]
        }
        
        self.timeCollectionView.reloadData()
    }
    
    func handleTableTime(startTime:Date, endTime:Date)
    {
        dateTimeTable = startTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
        arrDateTime.append(dateTimeTable)
        if(dateTimeTable < endTime){
            handleTableTime(startTime: dateTimeTable, endTime: endTime)
        }
        else{
            return
        }
    }
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        print("status")
        if(status == "Unlocked"){
            self.paymentButton.isHidden = false
            self.slidingPayView.isHidden = true
            self.payAction(self)
        }else{
            self.slidingPayView.reset()
        }
    }
    
    func checkItemInStock(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CHECK_ITEM_IN_STOCK, method: .post, parameter: ["cart_id":self.orderCart,"restaurant_id": Singleton.shared.selectedLocation.id ?? 0], objectClass: SuccessResponse.self, requestCode: U_CHECK_ITEM_IN_STOCK, userToken: nil) { response in
            self.callAPiForOrder()
            ActivityIndicator.hide()
        }
    }
    
    func showPopup(title: String, msg: String) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.confirmationText = msg
        myVC.isItemStockValidation = true
        myVC.isConfirmationViewHidden = false
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
    }
    
    
    func callAPiForOrder(){
        self.paymentButton.startAnimation()
        var param = [String : Any]()
        var time = Int()
        let date = self.convertTimestampToDate(Int(Date().timeIntervalSince1970), to: "d/M/yyyy")
        if((Singleton.shared.seletedTime ?? "").contains("(quickest)")){
            Singleton.shared.seletedTime = Singleton.shared.seletedTime!.replacingOccurrences(of: "(quickest)", with: "")
            time = self.convertDateToTimestamp("\(date)," + " " + Singleton.shared.seletedTime!.uppercased(), to: "d/M/yyyy, h:mm a")
        }else if(Singleton.shared.seletedTime!.contains("more options")){
            Singleton.shared.seletedTime = Singleton.shared.seletedTime!.replacingOccurrences(of: "(quickest)", with: "")
            time = self.convertDateToTimestamp("\(date)," + " " + Singleton.shared.seletedTime!.uppercased(), to: "d/M/yyyy, h:mm a")
        }else {
            time = self.convertDateToTimestamp("\(date)," + " " + Singleton.shared.seletedTime!.uppercased(), to: "d/M/yyyy, h:mm a")
        }
        let currentTime = Date().addingTimeInterval(15*60)
        if(time < Int(currentTime.timeIntervalSince1970)){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            Singleton.shared.seletedTime = nil
            self.setTime()
            slidingPayView.reset()
            self.paymentCards.isUserInteractionEnabled = true
            self.paymentButton.isHidden = true
            self.slidingPayView.isHidden = false
            self.slidingPayView.layer.borderWidth = 1
            myVC.heading = "Minimum pickuptime should be 15 minutes."
            self.navigationController?.present(myVC, animated: false, completion: nil)
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(30)) {
            self.slidingPayView.reset()
            ActivityIndicator.hide()
            self.paymentCards.isUserInteractionEnabled = true
            self.paymentButton.isHidden = true
            if(selectedCardId == nil && !(self.isGuestLogin)){
                self.applePayButton.isHidden = false
                self.slidingPayView.isHidden = true
            }else {
                self.applePayButton.isHidden = true
                self.slidingPayView.isHidden = false
            }
            self.slidingPayView.isHidden = false
            self.slidingPayView.layer.borderWidth = 1
        }
        if(self.isGuestLogin){
            self.guestCheckoutTime = time
            if(self.guestNonceId != ""){
                self.callGuestOrderAPI()
            }else if !(self.appplePaymentData.isEmpty){
                self.callGuestOrderAPI()
            }else {
                if(Singleton.shared.selectedLocation.payment_gateway == "cardknox"){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                    myVC.cardDelegate = self
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else {
                    let controller = self.makeCardEntryViewController()
                    controller.collectPostalCode = true
                    self.guestNonceId = ""
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }else {
            if(self.slidingPayView.buttonText == "Slide to Pay"){
                if(self.appplePaymentData.isEmpty){
                    param = [
                        "restaurant_id":Singleton.shared.selectedLocation.id ?? 0,
                        "selected_card":selectedCardId ?? 0,
                        "pickup_time":time,
                        "cart_id": self.orderCart ?? "",
                        "bonus_id":Int(self.bonus_id ?? 0),
                        "postalCode":"12345",
                        "platform":2,
                        "nonceType": Singleton.shared.selectedLocation.payment_gateway ?? ""
                    ]
                }else {
                    param = [
                        "restaurant_id":Singleton.shared.selectedLocation.id,
                        "selected_card":selectedCardId,
                        "pickup_time":time,
                        "cart_id": self.orderCart,
                        "payment_data":self.appplePaymentData,
                        "bonus_id":self.bonus_id ?? 0,
                        "postalCode":"12345",
                        "platform":2,
                        "nonceType": Singleton.shared.selectedLocation.payment_gateway ?? ""
                    ]
                }
            }else {
                param = [
                    "restaurant_id":Singleton.shared.selectedLocation.id,
                    "pickup_time":time,
                    "cart_id": self.orderCart,
                    "bonus_id":self.bonus_id ?? 0,
                    "postalCode":"12345",
                    "platform":2,
                    "nonceType": Singleton.shared.selectedLocation.payment_gateway ?? ""
                ]
                
            }
            
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            backgroundQueue.async(execute: {
                NotificationCenter.default.addObserver(self, selector: #selector(self.resetPaymentButton), name: NSNotification.Name(N_RESET_PAY_BUTTON), object: nil)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_CONFIRM_ORDER_BRAINTREE, method: .post, parameter: param, objectClass: OrderConfiremed.self, requestCode: U_CONFIRM_ORDER_BRAINTREE, userToken: nil) { (response) in
                    NavigationController.shared.getRecentOrder(nil)
                    UserDefaults.standard.removeObject(forKey: UD_CARTITEM_COUNT)
                    UserDefaults.standard.removeObject(forKey: UD_ACTIVE_ORDER_EXIST)
                    self.appplePaymentData = [String : Any]()
                    ActivityIndicator.hide()
                    self.paymentCards.isUserInteractionEnabled = true
                    Singleton.shared.recentOrderHistory = []
                    Singleton.shared.activeOrderData = []
                    UserDefaults.standard.removeObject(forKey: UD_CATEGORY_TYPE)
                    selectedCardId = nil
                    Singleton.shared.sideMenuBonus = [GetBonusResponse]()
                    Singleton.shared.sideMenuReward = RewardResponse()
                    Singleton.shared.myRewards = GetRewardResponse()
                    Singleton.shared.seletedTime = nil
                    menuId = 0
                    Singleton.shared.orderTotal = String()
                    // Singleton.shared.selectedBonus = GetBonusResponse()
                    if(Singleton.shared.selectedLocationPref == 2){
                        let selectedLoc = try! JSONEncoder().encode(Singleton.shared.selectedLocation)
                        UserDefaults.standard.setValue(selectedLoc, forKey: UD_RESTAURANT_SELECTED)
                    }
                    DispatchQueue.global(qos: .background).async {
                        self.emptyMyCart(id: self.orderCart!, type: 2)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(N_BONUS_REWARD_COUNT), object: nil)
                    self.paymentButton.stopAnimation(animationStyle: .expand, completion: {
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderReceivedViewController") as! OrderReceivedViewController
                        
                        myVC.orderId = response.response?.order_id ?? 0
                        myVC.isNavigateFromPayment = true
                        NavigationController.shared.addTransition(direction:.fromTop, controller: self)
                        self.navigationController?.pushViewController(myVC, animated: true)
                    })
                }
            })
        }
        
    }
    
    func stringGetCreditCardType(string:String) -> String{
        let regVisa = NSPredicate(format:"SELF MATCHES %@", "^4[0-9]{12}(?:[0-9]{3})?$")
        let regMaster = NSPredicate(format:"SELF MATCHES %@", "^5[1-5][0-9]{14}$")
        let regExpress = NSPredicate(format:"SELF MATCHES %@", "^3[47][0-9]{13}$")
        let regDiners = NSPredicate(format:"SELF MATCHES %@", "^3(?:0[0-5]|[68][0-9])[0-9]{11}$")
        let regDiscover = NSPredicate(format:"SELF MATCHES %@", "^6(?:011|5[0-9]{2})[0-9]{12}$")
        let regJCB = NSPredicate(format:"SELF MATCHES %@", "^(?:2131|1800|35\\d{3})\\d{11}$")
        
        if (regVisa.evaluate(with: string)){
            self.cardImage.image = UIImage(named: "visa")
            self.cardImage.isHidden = false
            isCardInvalid = false
            return "VISA"
        }else if (regMaster.evaluate(with: string)){
            
            self.cardImage.image = UIImage(named: "mastercard")
            
            self.cardImage.isHidden = false
            isCardInvalid = false
            return "MASTER"
        }else  if (regExpress.evaluate(with: string)){
            
            self.cardImage.image = #imageLiteral(resourceName: "american-express")
            
            self.cardImage.isHidden = false
            isCardInvalid = false
            return "AEXPRESS"
        }else if (regDiners.evaluate(with: string)){
            
            self.cardImage.image = #imageLiteral(resourceName: "dinners-club")
            
            self.cardImage.isHidden = false
            isCardInvalid = false
            return "DINERS"
        }else if (regDiscover.evaluate(with: string)){
            
            self.cardImage.image = #imageLiteral(resourceName: "discover")
            
            self.cardImage.isHidden = false
            isCardInvalid = false
            return "DISCOVERS"
        }else if (regJCB.evaluate(with: string)){
            
            self.cardImage.image = #imageLiteral(resourceName: "jcb")
            
            self.cardImage.isHidden = false
            isCardInvalid = false
            return "JCB"
        }else{
            //self.cardTypeButton.setTitle("", for: .normal)
            self.cardImage.image = nil
            
            self.cardImage.isHidden = true
            isCardInvalid = true
            return "invalid"
        }
    }
    
    //MARK: IBActions
    @IBAction func initialiseApplePay(_ sender: Any) {
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.modalPresentationStyle = .overFullScreen
        if(Singleton.shared.selectedLocation.id == nil){
            slidingPayView.reset()
            
            myVC.heading = "Looks like you missed Pickup Location."
            self.navigationController?.present(myVC, animated: false, completion: nil)
            
        }else if(Singleton.shared.seletedTime == nil || Singleton.shared.seletedTime == ""){
            
            myVC.heading = "Looks like you missed Pickup Time."
            
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }else if(self.orderCart == nil){
            myVC.heading = "Your cart id is invalid"
            self.navigationController?.present(myVC, animated: false, completion: nil)
            
        }else {
            self.requestApplePayAuthorization()
        }
        
    }
    
    @objc func addNewCardAction() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_ADD_NEW_CARD), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addNewCardAction), name: NSNotification.Name(N_ADD_NEW_CARD), object: nil)
        if(Singleton.shared.selectedLocation.payment_gateway == "cardknox"){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            myVC.cardDelegate = self
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let controller = self.makeCardEntryViewController()
            controller.collectPostalCode = true
            self.guestNonceId = ""
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func payAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.modalPresentationStyle = .overFullScreen
        if(self.isGuestLogin){
            self.guestLoginDetails = UserDefaults.standard.value(forKey: UD_GUEST_LOGIN_DETAIL) as! [String:Any]
        }
        if(self.slidingPayView.buttonText == "Slide to Pay"){
            if(Singleton.shared.selectedLocation.id == nil){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Looks like you missed Pickup Location."
                self.navigationController?.present(myVC, animated: false, completion: nil)
                
            }else if(Singleton.shared.seletedTime == nil || Singleton.shared.seletedTime == ""){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Looks like you missed Pickup Time."
                
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else if(self.orderCart == nil){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Your cart id is invalid"
                self.navigationController?.present(myVC, animated: false, completion: nil)
                
            }else if(Singleton.shared.paymentCards.count == 0 && self.isGuestLogin == false && appleCardSelected == false){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Add card to pay"
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else if(selectedCardId == nil && self.isGuestLogin == false && appleCardSelected == false){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Looks like you missed Payment Method."
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else {
                self.paymentCards.isUserInteractionEnabled = false
                self.appplePaymentData = [String : Any]()
                if let button = sender as? UIButton {
                    if(button.tag == 1){
                        self.guestNonceId = ""
                    }
                }
                self.checkItemInStock()
            }
        }else {
            if(Singleton.shared.selectedLocation.id == nil){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Looks like you missed Pickup Location."
                self.navigationController?.present(myVC, animated: false, completion: nil)
                
            }else if(Singleton.shared.seletedTime == nil || Singleton.shared.seletedTime == ""){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Looks like you missed Pickup Time."
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else if(self.orderCart == nil){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                myVC.heading = "Your cart id is invalid"
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else if((self.cardNumber.text!.isEmpty) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.cardNumberView.layer.borderColor = primaryColor.cgColor
            }else if((self.cardmonth.text!.isEmpty) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.expiryMonthView.layer.borderColor = primaryColor.cgColor
            }else if((self.cardYear.text!.isEmpty) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.expiryYearView.layer.borderColor = primaryColor.cgColor
            }else if((self.cardYear.text!.count < 2) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.expiryYearView.layer.borderColor = primaryColor.cgColor
            }else if((self.cvvNumber.text!.isEmpty) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.cardCVVView.layer.borderColor = primaryColor.cgColor
            }else if((self.cvvNumber.text!.count < 3) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.cardCVVView.layer.borderColor = primaryColor.cgColor
            }else if((self.postalCode.text!.isEmpty) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.postalCodeView.layer.borderColor = primaryColor.cgColor
            }else if((self.postalCode.text!.count < 6) && self.isGuestLogin){
                slidingPayView.reset()
                self.paymentButton.isHidden = true
                self.slidingPayView.isHidden = false
                self.slidingPayView.layer.borderWidth = 1
                self.postalCodeView.layer.borderColor = primaryColor.cgColor
            }else {
                self.paymentCards.isUserInteractionEnabled = false
                self.appplePaymentData = [String : Any]()
                if let button = sender as? UIButton {
                    if(button.tag == 1){
                        self.guestNonceId = ""
                    }
                }
                self.checkItemInStock()
            }
        }
        
    }
    
    @IBAction func myBagAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        NavigationController.shared.pushMenuFromReward(controller: self,direction:1)
    }
    
    func emptyMyCart(id: String, type:Int) {
        let param = [
            "cart_id": id
        ] as? [String:Any]
        var url = String()
        if(type == 1){
            url = U_EMPTY_GUEST_CART
        }else if(type == 2){
            url = U_EMPTY_CART
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode:  url, userToken: nil) { (response) in
            
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        }
        
    }
    
    
    
    @IBAction func openMap(_ sender: Any) {
        currentPageView = K_MAP_PAGE_VIEW
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func changeTimeAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeViewController") as! TimeViewController
        self.setTime()
        if(self.notAvailableView.isHidden == false){
            return
        }
        self.viewChangeTime.isHidden = false
        self.timeCollectionView.isHidden = true
        self.notAvailableView.isHidden = true
        self.mainTimeView.isHidden = false
        myVC.timeData  = Array(self.timeArr)
        if(self.currentTime != nil){
            myVC.previousTime = self.currentTime!
        }
        myVC.timeDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.present(myVC, animated: false, completion: nil)
    }
    
    
    @IBAction func termsAction(_ sender: Any) {
        self.openUrl(urlStr: "https://farmersfreshkitchen.com/about-us/terms-and-conditions")
    }
    
}

extension PaymentViewController {
    @objc func handleOutOfStock(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_HANDLE_ITEM_OUTOFSTOCK), object: nil)
        self.resetPaymentButton()
        if let cancel = notif.userInfo?["cancel"] as? String{
            self.navigationController?.popViewController(animated: true)
        }else {
            if let message = notif.userInfo?["msg"] as? String{
                self.showPopup(title: "", msg: message)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleOutOfStock(_:)), name: NSNotification.Name(N_HANDLE_ITEM_OUTOFSTOCK), object: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == timeCollectionView){
            return
        }
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let indexNumbers = paymentCards.indexPathForItem(at: center) {
            indexNumber = indexNumbers.row
        }
        
        if(indexNumber == (myCards.count-1)){
            if let guestLogin = UserDefaults.standard.value(forKey: UD_GUEST_LOGIN_DETAIL) as? [String:Any]{
                self.slidingPayView.isHidden = false
                self.paymentButton.isHidden = true
                self.applePayButton.isHidden = true
                self.cardDetailStack.isHidden = true
                self.guestAppleView.isHidden = true
            }else {
                self.slidingPayView.isHidden = true
                self.paymentButton.isHidden = true
                self.applePayButton.isHidden = true
                self.cardDetailStack.isHidden = true
                self.guestAppleView.isHidden = true
            }
        }else if(indexNumber == (myCards.count-2)){
            self.slidingPayView.isHidden = true
            self.paymentButton.isHidden = true
            self.applePayButton.isHidden = false
            self.cardDetailStack.isHidden = true
            self.guestAppleView.isHidden = true
        }else {
            selectedCardId = self.myCards[indexNumber].id ?? 0
            self.slidingPayView.isHidden = false
            self.paymentButton.isHidden = true
            self.applePayButton.isHidden = true
            self.cardDetailStack.isHidden = true
            self.guestAppleView.isHidden = true
        }
    }
    
    @objc func addCard() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_CHANGE_SELECTED_CARD), object: nil)
        
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_CARD, method: .get, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_NEW_CARD, userToken: nil) { (response) in
            Singleton.shared.paymentCards = response.response
            self.myCards = []
            self.myCards = response.response
            if(response.response.count > 0){
                self.slidingPayView.isHidden = false
                self.paymentButton.isHidden = true
                self.applePayButton.isHidden = true
                self.cardDetailStack.isHidden = true
                self.guestAppleView.isHidden = true
            }else if(response.response.count == 0){
                if(self.freeOrderText.isHidden){
                    self.slidingPayView.isHidden = true
                    self.paymentButton.isHidden = true
                    self.applePayButton.isHidden = false
                    self.cardDetailStack.isHidden = true
                    self.guestAppleView.isHidden = true
                }
            }
            self.myCards.append(GetCardResponse(id: nil, user_id: nil, token: nil, type: nil, name: nil, expiry_month: nil, expiry_year: nil, issue_number: nil, start_month: nil, start_year: nil, card_type: nil, masked_card_number: nil, card_scheme_type: nil, card_scheme_name: nil, card_issuer: nil, country_code: nil, card_class: nil))
            self.myCards.append(GetCardResponse(id: nil, user_id: nil, token: nil, type: nil, name: nil, expiry_month: nil, expiry_year: nil, issue_number: nil, start_month: nil, start_year: nil, card_type: nil, masked_card_number: nil, card_scheme_type: nil, card_scheme_name: nil, card_issuer: nil, country_code: nil, card_class: nil))
            self.paymentCards.reloadData()
            ActivityIndicator.hide()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.addCard), name: NSNotification.Name(N_CHANGE_SELECTED_CARD), object: nil)
    }
    
    @objc func resetPaymentButton(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            self.slidingPayView.reset()
            if(selectedCardId == nil && !(self.isGuestLogin)){
                self.applePayButton.isHidden = false
                self.slidingPayView.isHidden = true
            }else {
                self.applePayButton.isHidden = true
                self.slidingPayView.isHidden = false
            }
            self.paymentButton.isHidden = true
            self.paymentCards.isUserInteractionEnabled = true
            self.slidingPayView.layer.borderWidth = 1
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_RESET_PAY_BUTTON), object: nil)
        }
    }
    
    func initializeView(){
        self.cardNumber.delegate = self
        self.cardmonth.delegate = self
        self.cardYear.delegate = self
        self.cvvNumber.delegate = self
        self.postalCode.delegate = self
        layout.layoutAttributesForElements(in: self.paymentCards.frame)
        layout.sideItemAlpha = 1
        layout.sideItemScale = 0.9
        layout.itemSize = CGSize(width: self.view.frame.width*0.8, height: 180)
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing:15)
        layout.scrollDirection  = .horizontal
        self.paymentCards.collectionViewLayout = layout
        self.view.backgroundColor = primaryColor
        slidingPayView.delegate = self
        
        slidingPayView.layer.borderColor = primaryColor.cgColor
        slidingPayView.layer.borderWidth = 1
        self.orderTotal.text = "Total " + (Singleton.shared.orderTotal)
        if let guestLogin = UserDefaults.standard.value(forKey: UD_GUEST_LOGIN_DETAIL) as? [String:Any]{
            self.isGuestLogin = true
            self.paymentCards.isHidden = true
            self.cardDetailStack.isHidden = false
            self.slideStackView.isHidden = true
            self.guestAppleView.isHidden = false
            self.guestLoginDetails = guestLogin
        }else {
            self.paymentCards.isHidden = false
            self.cardDetailStack.isHidden = true
            self.guestAppleView.isHidden = true
            if(Singleton.shared.paymentCards.count == 0){
                self.addCard()
            }else {
                
                self.myCards = []
                self.myCards = Singleton.shared.paymentCards
                self.myCards.append(GetCardResponse(id: nil, user_id: nil, token: nil, type: nil, name: nil, expiry_month: nil, expiry_year: nil, issue_number: nil, start_month: nil, start_year: nil, card_type: nil, masked_card_number: nil, card_scheme_type: nil, card_scheme_name: nil, card_issuer: nil, country_code: nil, card_class: nil))
                self.myCards.append(GetCardResponse(id: nil, user_id: nil, token: nil, type: nil, name: nil, expiry_month: nil, expiry_year: nil, issue_number: nil, start_month: nil, start_year: nil, card_type: nil, masked_card_number: nil, card_scheme_type: nil, card_scheme_name: nil, card_issuer: nil, country_code: nil, card_class: nil))
                self.paymentCards.reloadData()
            }
        }
        
        if (Singleton.shared.orderTotal == "$0" || Singleton.shared.orderTotal == "$0.0" || Singleton.shared.orderTotal == "$0.00" || Singleton.shared.orderTotal == nil){
            self.cardStack.isHidden = true
            self.slidingPayView.buttonText = "Confirm Order"
            self.slidingPayView.isHidden = false
            self.paymentButton.isHidden = true
            self.applePayButton.isHidden = true
            self.freeOrderText.isHidden = false
        }else {
            self.slidingPayView.buttonText = "Slide to Pay"
            self.slidingPayView.isHidden = false
            self.paymentButton.isHidden = true
            self.applePayButton.isHidden = true
            self.cardStack.isHidden = false
            self.freeOrderText.isHidden = true
        }
        
        self.transition = JTMaterialTransition(animatedView: self.paymentButton)
        
        
        if let loc = Singleton.shared.selectedLocation.address?.components(separatedBy: ","){
            if(loc.count > 0){
                var location = Singleton.shared.selectedLocation.address?.replacingOccurrences(of: loc[0] + ", ", with: "")
                self.pickupLocation.text = loc[0] + "\n" + (location ?? "")
            }
        }
        
        if(Singleton.shared.mainMenuData.count > 0){
            if(menuId == (Singleton.shared.mainMenuData[0].menu_id ?? 0)){
                timeFrom = Singleton.shared.mainMenuData[0].from
                timeTo = Singleton.shared.mainMenuData[0].to
                self.notAvailableLabel.text = Singleton.shared.breakfastTimeMessage
            }else {
                timeFrom = Singleton.shared.mainMenuData[1].from
                timeTo = Singleton.shared.mainMenuData[1].to
                self.notAvailableLabel.text = Singleton.shared.lunchTimeMessage
            }
            
            
            if(Singleton.shared.seletedTime != nil &&   Singleton.shared.seletedTime != ""){
                self.currentTimeLabel.text = Singleton.shared.seletedTime
                isTimeSelectedBefore = true
                self.timeCollectionView.isHidden = true
                self.notAvailableView.isHidden = true
                self.viewChangeTime.isHidden = false
                
                var startTime = Date(timeIntervalSince1970: TimeInterval(timeFrom ?? 0))
                var endTime = Date(timeIntervalSince1970: TimeInterval(timeTo ?? 0))
                // endTime = endTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
                self.handleTableTime(startTime: startTime, endTime: endTime)
                var timeStamp:String?
                timeArr = []
                for i in 0...arrDateTime.count-1
                {
                    let date = Int(arrDateTime[i].timeIntervalSince1970)
                    if(date > Int(Date().timeIntervalSince1970)){
                        timeStamp = self.convertTimestampToDate(date, to: "h:mm a")
                        timeArr.append(timeStamp!)
                    }
                }
            }else {
                self.setTime()
            }
        }else {
            if(Singleton.shared.breakfastTimeMessage != ""){
                self.notAvailableLabel.text = Singleton.shared.breakfastTimeMessage
            }else if(Singleton.shared.lunchTimeMessage != "") {
                self.notAvailableLabel.text = Singleton.shared.lunchTimeMessage
            }
        }
        if(self.isGuestLogin){
            self.addCardPayLabel.text = "Enter card details & pay"
        }else {
            self.addCardPayLabel.text = "Add card & pay"
        }
        
        
        // self.manageBottomSpace()
        NotificationCenter.default.addObserver(self, selector: #selector(self.addCard), name: NSNotification.Name(N_CHANGE_SELECTED_CARD), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addNewCardAction), name: NSNotification.Name(N_ADD_NEW_CARD), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleOutOfStock), name: NSNotification.Name(N_HANDLE_ITEM_OUTOFSTOCK), object: nil)
    }
}

extension PaymentViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == cardNumber){
            cardNumberView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cardmonth){
            expiryMonthView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cardYear){
            expiryYearView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cvvNumber){
            cardCVVView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == postalCode){
            postalCodeView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == cardNumber){
            self.stringGetCreditCardType(string: self.cardNumber.text!.replacingOccurrences(of: " ", with: ""))
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "####  ####  ####  ####", replacmentCharacter: "#")
            self.guestLoginDetails["card_number"] = self.cardNumber.text?.replacingOccurrences(of: "  ", with: "")
            return newLength <= 22
        }else if(textField == cardmonth) {
            let currentCharacterCount = self.cardmonth.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 2){
                self.cardmonth.text! += string
                self.guestLoginDetails["expiry_month"] = self.cardmonth.text ?? ""
                
                self.cardYear.becomeFirstResponder()
            }
            return newLength <= 2
        }else if(textField == cardYear) {
            let currentCharacterCount = self.cardYear.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 4){
                self.cardYear.text! += string
                self.guestLoginDetails["expiry_year"] = Int(self.cardYear.text ?? "0")!
                self.cvvNumber.becomeFirstResponder()
            }
            return newLength <= 4
        }else if(textField == cvvNumber) {
            let currentCharacterCount = self.cvvNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.guestLoginDetails["cvc"] = (Int(self.cvvNumber.text ?? "0") ?? 0)
            return newLength <= 3
        }else if(textField == postalCode) {
            let currentCharacterCount = self.postalCode.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.guestLoginDetails["postalCode"] = (Int(self.postalCode.text ?? "0") ?? 0)
            return newLength <= 6
        }else {
            return true
        }
    }
    
}

extension PaymentViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TimeProtocol {
    func selectPickupTime(current: Int, previous: Int, selectedTime: String) {
        if(self.isTimeSelectedBefore == false){
            self.shortTime[3] = selectedTime
            //Singleton.shared.seletedTime = selectedTime
            self.timeCollectionView.reloadData()
        }else {
            self.currentTimeLabel.text = Singleton.shared.seletedTime
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if(collectionView == paymentCards){
            return myCards.count
        }else {
            if(self.viewChangeTime.isHidden){
                if(self.shortTime.count == 0){
                    self.notAvailableView.isHidden = false
                    self.viewChangeTime.isHidden = true
                    self.timeCollectionView.isHidden = true
                }else {
                    self.notAvailableView.isHidden = true
                    self.timeCollectionView.isHidden = false
                }
            }
            return self.shortTime.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == paymentCards){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! CarouselCollectionViewCell
            let data = myCards[indexPath.row]
            if !(self.isGuestLogin){
                cell.addNewCardButton = {
                    self.addNewCardAction()
                }
                cell.addNewCardText.text = "+ Add new card"
            }else {
                cell.addNewCardText.text = "Enter Card Details Below"
            }
            
            
            if((indexPath.row) == (self.myCards.count-1)){
                if let guestLogin = UserDefaults.standard.value(forKey: UD_GUEST_LOGIN_DETAIL) as? [String:Any]{
                    // cell.cardDetailStack.isHidden = true
                    cell.cardView.isHidden = true
                    cell.appleView.isHidden = true
                    cell.addNewCard.isHidden = false
                }else{
                    // cell.cardDetailStack.isHidden = true
                    cell.cardView.isHidden = true
                    cell.appleView.isHidden = true
                    cell.addNewCard.isHidden = false
                }
            }else if((indexPath.row) == (self.myCards.count-2)){
                // cell.cardDetailStack.isHidden = true
                cell.cardView.isHidden = true
                cell.appleView.isHidden = false
                cell.addNewCard.isHidden = true
            }else {
                // cell.cardDetailStack.isHidden = borderc
                
                cell.cardView.isHidden = false
                cell.appleView.isHidden = true
                cell.addNewCard.isHidden = true
                //                if(data.is_default == 1){
                //                    selectedCardId = data.id ?? 0
                //                }
                if((indexPath.row) == 0){
                    selectedCardId = self.myCards[0].id ?? 0
                }
                cell.cardNum.text = data.masked_card_number
                cell.userName.text = data.name?.uppercased()
                if let expYear = data.expiry_year {
                    let year = String(expYear.suffix(2))
                    cell.expiryYear.text = (data.expiry_month ?? "") + "/" + (year ?? "")
                }
                switch data.card_type?.lowercased() {
                case "Visa".lowercased():
                    cell.visaImage.image = UIImage(named: "visa")
                    self.selectedCard = data
                    break
                    
                case "Mastercard".lowercased():
                    cell.visaImage.image = UIImage(named: "mastercard")
                    self.selectedCard = data
                    break
                    
                case "JCB".lowercased():
                    cell.visaImage.image = UIImage(named: "jcb")
                    self.selectedCard = data
                    break
                case "Discover".lowercased():
                    cell.visaImage.image = UIImage(named: "discover")
                    self.selectedCard = data
                    break
                case "UNKNOWN".lowercased():
                    cell.visaImage.image = nil
                    self.selectedCard = data
                    break
                case "Amex".lowercased():
                    cell.visaImage.image = UIImage(named: "american-express")
                    self.selectedCard = data
                    break
                default:
                    self.selectedCard = data
                    cell.visaImage.image = nil
                }
            }
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell", for: indexPath) as! TimeCollectionViewCell
            cell.timeLabel.text = self.shortTime[indexPath.row].lowercased()
            if(cell.timeLabel.text == Singleton.shared.seletedTime){
                cell.timeView.backgroundColor = primaryColor
                Singleton.shared.seletedTime = cell.timeLabel.text
                cell.timeView.layer.borderWidth = 0
                cell.timeLabel.textColor = .white
                self.isTimeSelected = true
                self.previousTime = self.currentTime ?? indexPath.row
                self.currentTime = indexPath.row
                //  collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
            }else {
                cell.timeView.backgroundColor = .white
                cell.timeView.layer.borderWidth = 1
                cell.timeLabel.textColor = primaryColor
            }
            
            cell.time = {
                if((cell.timeLabel.text == "more options")){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeViewController") as! TimeViewController
                    myVC.timeData  = Array(self.timeArr[4..<self.timeArr.count])
                    
                    if(self.currentTime != nil){
                        myVC.previousTime = self.currentTime!
                    }
                    myVC.currentTime = indexPath.row
                    myVC.timeDelegate = self
                    myVC.modalPresentationStyle = .overFullScreen
                    self.present(myVC, animated: false, completion: nil)
                    return
                }
                if(cell.timeView.backgroundColor == primaryColor){
                    cell.timeView.backgroundColor = .white
                }else {
                    cell.timeView.backgroundColor = primaryColor
                    Singleton.shared.seletedTime = cell.timeLabel.text
                    self.previousTime = self.currentTime
                    self.currentTime = indexPath.row
                }
                if(self.previousTime != nil){
                    self.timeCollectionView.reloadItems(at: [IndexPath(row: self.previousTime!, section: 0)])
                }
                self.timeCollectionView.reloadItems(at: [IndexPath(row: self.currentTime!, section: 0)])
            }
            if(indexPath.row == 3){
                if !(isTimeSelected){
                    if(Singleton.shared.seletedTime != nil){
                        cell.timeView.backgroundColor = primaryColor
                        Singleton.shared.seletedTime = cell.timeLabel.text
                        cell.timeView.layer.borderWidth = 0
                        cell.timeLabel.textColor = .white
                        self.isTimeSelected = true
                        self.previousTime = self.currentTime ?? indexPath.row
                        self.currentTime = indexPath.row
                    }
                }
            }
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == timeCollectionView){
            
            let label = UILabel(frame: CGRect.zero)
            label.text = self.shortTime[indexPath.row]
            label.sizeToFit()
            return CGSize(width: label.frame.width + 30, height: 50)
            
        }else {
            return CGSize(width: self.view.frame.width*0.8, height: 180)
        }
    }
}

extension PaymentViewController: UINavigationControllerDelegate, SQIPCardEntryViewControllerDelegate{
    func navigationControllerSupportedInterfaceOrientations(
        _: UINavigationController
    ) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    func makeCardEntryViewController() -> SQIPCardEntryViewController {
        let theme = SQIPTheme()
        theme.saveButtonTitle = self.isGuestLogin ? "Checkout":"Save"
        let cardEntry = SQIPCardEntryViewController(theme: theme)
        cardEntry.collectPostalCode = false
        cardEntry.delegate = self
        return cardEntry
    }
    
    func cardEntryViewController(
        _: SQIPCardEntryViewController,
        didCompleteWith _: SQIPCardEntryCompletionStatus
    ) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func cardEntryViewController(_ val: SQIPCardEntryViewController,
                                 didObtain cardDetail: SQIPCardDetails,
                                 completionHandler: @escaping (Error?) -> Void) {
        self.dismiss(animated: true, completion: nil)
        if(self.isGuestLogin){
            self.appplePaymentData = [String:Any]()
            self.guestNonceId = cardDetail.nonce
            self.checkItemInStock()
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CARD, method: .post, parameter: ["nonce":cardDetail.nonce], objectClass: SuccessResponse.self, requestCode: U_ADD_CARD, userToken: nil) { response in
                self.addCard()
                ActivityIndicator.hide()
            }
        }
        
        completionHandler(nil)
    }
    
    func callGuestOrderAPI(){
        ActivityIndicator.show(view: self.view)
        let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
        var param = [String:Any]()
        param = self.guestLoginDetails
        param["cart_id"] = self.orderCart
        param["restaurant_id"] = Singleton.shared.selectedLocation.id
        param["pickup_time"] = self.guestCheckoutTime
        if !(self.appplePaymentData.isEmpty){
            param["payment_data"] = self.appplePaymentData
        }
        param["firebase_token"] = fcmToken
        param["nonce"] = self.guestNonceId
        param["nonceType"] = Singleton.shared.selectedLocation.payment_gateway ?? ""
        param["cardholderName"] = (param["user_first_name"] as! String) + (param["user_last_name"] as! String)
        if(Singleton.shared.selectedLocation.payment_gateway == "cardknox"){
            param["xZip"] = self.cardKnoxZip
            param["xStreet"] = self.cardknoxStreet
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetPaymentButton), name: NSNotification.Name(N_RESET_PAY_BUTTON), object: nil)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GUEST_CHECKOUT_BRAINTREE, method: .post, parameter: param, objectClass: GuestCheckoutResponse.self, requestCode: U_GUEST_CHECKOUT_BRAINTREE, userToken: nil) { (response) in
            NavigationController.shared.getRecentOrder(nil)
            self.cardKnoxZip = ""
            self.cardknoxStreet = ""
            self.guestNonceId = ""
            UserDefaults.standard.removeObject(forKey: UD_CARTITEM_COUNT)
            self.guestLoginDetails = [String:Any]()
            UserDefaults.standard.setValue(nil,forKey: UD_GUEST_LOGIN_DETAIL)
            Singleton.shared.recentOrderHistory = []
            Singleton.shared.activeOrderData = []
            Singleton.shared.orderSubTotal = ""
            UserDefaults.standard.removeObject(forKey: UD_CATEGORY_TYPE)
            selectedCardId = nil
            self.paymentCards.isUserInteractionEnabled = true
            Singleton.shared.myRewards = GetRewardResponse()
            Singleton.shared.seletedTime = nil
            menuId = 0
            Singleton.shared.orderTotal = String()
            // Singleton.shared.selectedBonus = GetBonusResponse()
            DispatchQueue.global(qos: .background).async {
                self.emptyMyCart(id: self.orderCart!, type: 1)
            }
            self.isGuestLogin = false
            ActivityIndicator.show(view: self.view)
            self.paymentButton.stopAnimation(animationStyle: .expand, completion: {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderReceivedViewController") as! OrderReceivedViewController
                myVC.orderData = response.response!
                myVC.isGuestLogin = true
                myVC.isNavigateFromPayment = true
                NavigationController.shared.addTransition(direction:.fromTop, controller: self)
                self.navigationController?.pushViewController(myVC, animated: true)
            })
        }
    }
}

extension PaymentViewController:PKPaymentAuthorizationViewControllerDelegate{
    
    func requestApplePayAuthorization() {
        if(Singleton.shared.selectedLocation.payment_gateway == "cardknox"){
            let supportedNetworks: [PKPaymentNetwork] = [.visa, .masterCard, .amex]
            
            if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: supportedNetworks) {
                let request = PKPaymentRequest()
                request.merchantIdentifier = K_APPLE_MERCHANT_ID_CARDKNOX
                request.supportedNetworks = supportedNetworks
                request.merchantCapabilities = .capability3DS
                request.countryCode = "US"
                request.currencyCode = "USD"
                let paymentItem = PKPaymentSummaryItem(label: "Product Name", amount: NSDecimalNumber(string: "10.00"))
                request.paymentSummaryItems = [paymentItem]
                
                let paymentAuthorizationViewController =
                PKPaymentAuthorizationViewController(paymentRequest: request)
                
                paymentAuthorizationViewController!.delegate = self
                
                present(paymentAuthorizationViewController!, animated: true, completion: nil)
            }
        }else {
            guard SQIPInAppPaymentsSDK.canUseApplePay else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.heading = "Error paying with apple."
                self.navigationController?.present(myVC, animated: false, completion: nil)
                return
            }
            let paymentRequest = PKPaymentRequest.squarePaymentRequest(
                // Set to youran Apple merchant ID
                merchantIdentifier: K_APPLE_MERCHANT_ID_SQUARE,
                countryCode: "US",
                currencyCode: "USD"
            )
            
            // Payment summary information will be displayed on the Apple Pay sheet.
            paymentRequest.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "Farmer's Fresh Kitchen", amount: self.decimal(with: Singleton.shared.orderTotal.replacingOccurrences(of: "$", with: ""))),
                
            ]
            
            let paymentAuthorizationViewController =
            PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
            
            paymentAuthorizationViewController!.delegate = self
            
            present(paymentAuthorizationViewController!, animated: true, completion: nil)
        }
    }
    
    func paymentAuthorizationViewControllerDidFinish(
        _: PKPaymentAuthorizationViewController
    ) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func paymentAuthorizationViewController(
        _: PKPaymentAuthorizationViewController,
        didAuthorizePayment payment: PKPayment,
        handler completion: @escaping (
            PKPaymentAuthorizationResult) -> Void
    ) {
        if(Singleton.shared.selectedLocation.payment_gateway == "cardknox"){
             let base64String = payment.token.paymentData.base64EncodedString()
                self.appplePaymentData["token"] = base64String
        }else {
            // Exchange the authorized PKPayment for a nonce.
            let nonceRequest = SQIPApplePayNonceRequest(payment: payment)
            nonceRequest.perform { cardDetails, error in
                if let cardDetails = cardDetails {
                    if error != nil {
                        ActivityIndicator.hide()
                        print(error)
                        return
                    }
                    ActivityIndicator.show(view: self.view)
                    self.guestNonceId = ""
                    self.appplePaymentData["token"] = cardDetails.nonce
                    selectedCardId = nil
                    self.cardmonth.text = ""
                    self.cardYear.text = ""
                    self.cardNumber.text = ""
                    self.cvvNumber.text = ""
                    self.paymentCards.isUserInteractionEnabled = false
                    self.dismiss(animated: true, completion: nil)
                    self.callAPiForOrder()
                } else if let error = error {
                    print(error)
                    completion(PKPaymentAuthorizationResult(status: .failure, errors: [error]))
                }
            }
        }
    }
}


class CarouselCollectionViewCell: UICollectionViewCell {
    //MARK:IBOutlets
    @IBOutlet weak var visaImage: UIImageView!
    @IBOutlet weak var cardNum: DesignableUILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var expiryYear: UILabel!
    @IBOutlet weak var cardView: View!
    @IBOutlet weak var appleView: View!
    @IBOutlet weak var addNewCard: View!
    @IBOutlet weak var addNewCardText: UILabel!
    
    static let identifier = "CarouselCollectionViewCell"
    var addNewCardButton:(()-> Void)? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    @IBAction func addNewCardAction(_ sender: Any) {
        if let addNewCardButton = self.addNewCardButton{
            addNewCardButton()
        }
    }
    
}


extension StringProtocol {
    var data: Data { Data(utf8) }
    var base64Encoded: Data { data.base64EncodedData() }
    var base64Decoded: Data? { Data(base64Encoded: string) }
}

extension LosslessStringConvertible {
    var string: String { .init(self) }
}

extension Sequence where Element == UInt8 {
    var data: Data { .init(self) }
    var base64Decoded: Data? { Data(base64Encoded: data) }
    var string: String? { String(bytes: self, encoding: .utf8) }
}
