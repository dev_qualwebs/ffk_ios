

import UIKit

var isOrderNow = false
class WelcomeNavigationViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let pageControl = WelcomePageViewController.customDataSource as! WelcomePageViewController
        if (isOrderNow){
            isOrderNow = false
            pageControl.setSecondController()
            pageControl.currentIndex = 1
        }
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.welocmeGesture))
                  swipeUp.direction = .up
                  self.view!.addGestureRecognizer(swipeUp)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.welocmeGesture))
        swipeDown.direction = .down
        self.view!.addGestureRecognizer(swipeDown)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.gestureRecognizers?.removeAll()
    }
    
    @objc func welocmeGesture(gesture: UISwipeGestureRecognizer) -> Void {
        let pageControl = WelcomePageViewController.customDataSource as! WelcomePageViewController
            if gesture.direction == UISwipeGestureRecognizer.Direction.up {
            if(pageControl.currentIndex == 0){
                pageControl.setSecondController()
                pageControl.currentIndex = 1
            }
           }else if gesture.direction == UISwipeGestureRecognizer.Direction.down {
              if(pageControl.currentIndex == 1){
                 pageControl.setFirstController()
                pageControl.currentIndex = 0
               }
           }
       }
}
