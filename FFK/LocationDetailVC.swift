//
//  LocationDetailViewController.swift
//  FFK
//
//  Created by Qualwebs on 22/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

var locationId = Int()
var restaurantDistance = String()

class LocationDetailVC: UIViewController, Confirmation, RestaurantSelected{
    func setRestaurant() {
        self.selectRestaurant()
    }
    
    
    func confirmationSelection() {
        self.changeuserPreference()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var locationDetailTable: UITableView!
    
    
    var restaurentDetail = LocationResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MapViewController.restaurantDelegate = self
        // NotificationCenter.default.addObserver(self, selector: #selector(self.selectRestaurant), name: NSNotification.Name(N_SELECT_RESTAURANT_BUTTON), object: nil)
        locationDetailTable.estimatedRowHeight = UITableView.automaticDimension
        locationDetailTable.rowHeight = 150
        
        //self.view.backgroundColor = primaryColor
        self.view.layoutIfNeeded()
        if #available(iOS 15.0, *) {
            self.locationDetailTable.sectionHeaderTopPadding = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name("page_controller"), object: nil, userInfo: ["id":1])
        
        if(locationId == nil || locationId == 0){
            locationId = Singleton.shared.selectedLocation.id ?? 0
            getResstaurentDetail()
        }else {
            getResstaurentDetail()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SELECT_RESTAURANT_BUTTON), object: nil)
    }
    
    func getResstaurentDetail(){
        if(locationId == 0){
            return
        }
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_RESTAURENT_DETAIL + "\(locationId)", method: .get, parameter: nil, objectClass: GetLocationDetail.self, requestCode: U_GET_RESTAURENT_DETAIL, userToken: nil) { (response) in
            self.restaurentDetail = response.response
            self.locationDetailTable.delegate = self
            self.locationDetailTable.dataSource = self
            self.locationDetailTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func likeClicked(_ sender: Any) {
        
    }
    
    
    func calllFavouriteApi() {
        ActivityIndicator.show(view: self.view)
        let param = [
            "restaurant_id": self.restaurentDetail.id ?? 0
        ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_RESTAURENT_FAVOURITE, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_RESTAURENT_FAVOURITE, userToken: nil) { (response) in
            if(Singleton.shared.selectedLocationPref == 3){
                let selectedLoc = try! JSONEncoder().encode(self.restaurentDetail)
                UserDefaults.standard.set(selectedLoc, forKey: UD_RESTAURANT_SELECTED)
            }
            ActivityIndicator.hide()
        }
    }
    
    @objc func selectRestaurant() {
        // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SELECT_RESTAURANT_BUTTON), object: nil)
        let id = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int
        if(id != nil && id != 0){
            if(Singleton.shared.selectedLocation.id == self.restaurentDetail.id){
                Singleton.shared.selectedLocation = self.restaurentDetail
                currentPageView = K_MENU_PAGE_VIEW
                Singleton.shared.selectedTimeArr = []
                Singleton.shared.mainMenuData = []
                UserDefaults.standard.removeObject(forKey: UD_MENU_TYPE)
                Singleton.shared.breakfastTimeMessage = String()
                Singleton.shared.lunchTimeMessage = String()
                Singleton.shared.lunchMenu = []
                Singleton.shared.breakfastMenu = []
                NavigationController.shared.pushStart(controller: self)
            }else {
                if(self.restaurentDetail.id != nil){
                    self.showPopup(title: "Are you sure?", msg: "Changing location will remove all item(s) from the bag.")
                }
            }
        }else {
            if(self.restaurentDetail.id == nil){
                let page = PageViewController.customDataSource as! PageViewController
                page.setFirstController()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.heading = "Select a restaurant"
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else {
                Singleton.shared.selectedLocation = self.restaurentDetail
                if let selectedLoc = try? JSONEncoder().encode(self.restaurentDetail){
                  UserDefaults.standard.set(selectedLoc, forKey: UD_RESTAURANT_SELECTED)
                }
                currentPageView = K_MENU_PAGE_VIEW
                NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_REST_LOCATION), object: nil)
            }
        }
        //   NotificationCenter.default.addObserver(self, selector: #selector(self.selectRestaurant), name: NSNotification.Name(N_SELECT_RESTAURANT_BUTTON), object: nil)
    }
    
    func showPopup(title: String, msg: String) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.confirmationText = msg
        myVC.isConfirmationViewHidden = false
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
    }
    
    func changeuserPreference(){
        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
            if(item > 0){
                NavigationController.shared.emptyMyCart()
            }
        }
        if let loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN) {
            if(self.restaurentDetail.id != nil){
                let param = [
                    "restaurant_id": self.restaurentDetail.id!
                ]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_SELECT_PREFERENCE, method: .post, parameter: param, objectClass: ErrorResponse.self, requestCode: U_SELECT_PREFERENCE, userToken: nil) { (response) in
                    
                    Singleton.shared.selectedLocation = self.restaurentDetail
                    currentPageView = K_MENU_PAGE_VIEW
                    NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_REST_LOCATION), object: nil)
                }
            }
        }else{
            Singleton.shared.selectedLocation = self.restaurentDetail
            currentPageView = K_MENU_PAGE_VIEW
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_REST_LOCATION), object: nil)
        }
    }
}

extension LocationDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationDetailCell") as! LocationDetailCell
        if let loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN) {
            cell.viewForFavorite.isHidden = false
        }else {
            cell.viewForFavorite.isHidden = true
        }
        cell.restaurentDistance.text = restaurantDistance
        cell.restaurentName.text = self.restaurentDetail.name
        cell.restaurentAddress.text = self.restaurentDetail.address
        cell.additionalInfo.text = "" //self.restaurentDetail.additional_info
        if(cell.additionalInfo.text == "" || cell.additionalInfo.text == nil){
            cell.additionalInfo.isHidden = true
        }else {
            cell.additionalInfo.isHidden = false
        }
        cell.restaurentTime.text = self.restaurentDetail.additional_info
        //(self.restaurentDetail.is_opened == 1 ? "OPEN":"CLOSED")
        if((self.restaurentDetail.favorite?.is_favorite ?? 0) == 0 ){
            cell.likeImage.image = UIImage(named: "like")
        }else {
            cell.likeImage.image = UIImage(named: "favorite-heart-button")
        }
        cell.restaurentNumber.text = "\(self.restaurentDetail.contact_number!)"
        cell.likeBtn = {
            if let loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN) {
                if(cell.likeImage.image == UIImage(named: "favorite-heart-button")){
                    cell.likeImage.image = UIImage(named: "like")
                }else {
                    cell.likeImage.image = UIImage(named: "favorite-heart-button")
                }
                self.calllFavouriteApi()
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.heading = "Please Login to mark restaurant as favorite"
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }
        }
        
        cell.callButton = {
            let phone = cell.restaurentNumber.text?.replacingOccurrences(of: "-", with: "")
            self.callNumber(phoneNumber:phone ?? "")
        }
        return cell
    }
}


class LocationDetailCell: UITableViewCell{
    //MARK:OUtlets
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var restaurentName: UILabel!
    @IBOutlet weak var restaurentAddress: UILabel!
    @IBOutlet weak var restaurentNumber: UILabel!
    @IBOutlet weak var restaurentTime: UILabel!
    @IBOutlet weak var restaurentClose: UILabel!
    @IBOutlet weak var restaurentDistance: UILabel!
    @IBOutlet weak var additionalInfo: UILabel!
    @IBOutlet weak var viewForFavorite: UIView!
    
    
    var callButton: (()-> Void)? = nil
    var likeBtn: (()-> Void)? = nil
    
    @IBAction func likeAction(_ sender: Any) {
        if let likeBtn = self.likeBtn {
            likeBtn(    )
        }
    }
    
    
    @IBAction func callAction(_ sender: Any) {
        if let callButton = self.callButton {
            callButton()
        }
    }
    
}
