//
//  BonusItemViewController.swift
//  Farmers
//
//  Created by qw on 03/11/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class BonusItemViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var bonusItemTable: UITableView!
    
    var bonusItemData = [BonusItemResponse]()
    var selectedItem = Int()
    var bonus = GetBonusResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     bonusItemTable.tableFooterView = UIView()
        self.navigationController?.addCustomTransitioning()
        self.bonusItemTable.estimatedRowHeight = 90
        self.bonusItemTable.rowHeight = UITableView.automaticDimension
        self.bonusItemTable.isScrollEnabled = true
        self.getBonusItem()
        if #available(iOS 15.0, *) {
            self.bonusItemTable.sectionHeaderTopPadding = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    @objc func handleGestureRight(gesture: UISwipeGestureRecognizer) -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getBonusItem(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_WELCOME_ITEMS + "\(Singleton.shared.selectedLocation.id ?? 2)", method: .get, parameter: nil, objectClass: GetBonusItem.self, requestCode: U_WELCOME_ITEMS, userToken: nil) { (response) in
            self.bonusItemData = response.response
            self.bonusItemTable.delegate = self
            self.bonusItemTable.dataSource = self
            
            self.bonusItemTable.reloadData()
            ActivityIndicator.hide()
            self.bonusItemTable.isUserInteractionEnabled = true
            self.bonusItemTable.isScrollEnabled = true
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: false, completion: nil)
        }
    }
    
}

extension BonusItemViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bonusItemData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurentTableViewCell") as! RestaurentTableViewCell
        let val = self.bonusItemData[indexPath.row]
        cell.foodImage.sd_setImage(with: URL( string: U_CUSTOM_SIZE + (val.item_image ?? "")), placeholderImage:nil)
        cell.nameLabel.text = val.item_name
        cell.orderLabel.text = val.item_description
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedItem = self.bonusItemData[indexPath.row].item_id ?? 0
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FeaturedRecepieViewController") as! FeaturedRecepieViewController
        myVC.isNavigationFromBonus = true
        let val = self.bonusItemData[indexPath.row]
        myVC.menuData = getMenusList(item_id: val.item_id, category_id: 0, item_name: val.item_name, item_price: 0, item_image: val.item_image, modifiers: val.modifier_group_id, thumbnail: val.item_image, is_common: 0, item_description: val.item_description, reward_item: 0)
        myVC.bonusUrl = U_BASE + U_APPLY_BONUS + "\(self.bonus.bonus_id ?? 0)/\(self.selectedItem)"
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
}
