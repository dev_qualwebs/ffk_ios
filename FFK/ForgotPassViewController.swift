//
//  ForgotPassViewController.swift
//  FFK
//
//  Created by qw on 14/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPassViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: IBOUtlets
    @IBOutlet weak var viewForEmail: UIView!
    @IBOutlet weak var viewForOtp: UIView!
    @IBOutlet weak var emailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var enteredEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
      //  swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
        self.viewForEmail.isHidden = false
        self.viewForOtp.isHidden = true
        //self.emailAddress.delegate = self
    }
    
    //MARK: IBActions
    
    @IBAction func sendAtion(_ sender: Any) {
        if(emailAddress.text!.isEmpty){
            self.emailAddress.errorMessage = "Enter email address"
        }else if !(self.isValidEmail(emailStr: self.emailAddress.text!.replacingOccurrences(of: " ", with: ""))) {
            self.emailAddress.errorMessage = "Enter valid email address"
        }else {
            self.callOTPApi()
        }
    }
    
    func callOTPApi() {
        ActivityIndicator.show(view: self.view)
        let param = [
            "email": (self.emailAddress.text ?? "").replacingOccurrences(of: " ", with: "")
            ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEND_OTP, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_SEND_OTP, userToken: nil) { (response) in
            self.enteredEmail.text =  self.emailAddress.text
            self.viewForEmail.isHidden = true
            self.viewForOtp.isHidden = false
            ActivityIndicator.hide()
        }
        
    }
    
    @IBAction func verifyOtpAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
}
