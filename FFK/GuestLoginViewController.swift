//
//  GuestLoginViewController.swift
//  Farmers
//
//  Created by qw on 26/03/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FirebaseMessaging

class GuestLoginViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var tickImage: ImageView!
    @IBOutlet weak var dob: SkyFloatingLabelTextField!
    @IBOutlet weak var showPassImg: UIImageView!
    @IBOutlet weak var createAccountView: UIStackView!
    @IBOutlet weak var rewardLabel: DesignableUILabel!
    
    @IBOutlet weak var zipCode: SkyFloatingLabelTextField!
    
    var popFromOrderPage = false
    var errorCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tickImage.image = UIImage(named: "Rectangle 1")
        self.lastName.delegate = self
        self.mobileNumber.delegate = self
        self.firstName.delegate = self
        self.email.delegate = self
        self.password.delegate = self
        self.dob.delegate = self
        self.zipCode.delegate = self
        let points = Double(Singleton.shared.orderSubTotal.replacingOccurrences(of: "$", with: ""))
        self.showPassImg.image = #imageLiteral(resourceName: "visibility")
        self.rewardLabel.text = "Don't miss out on your free meal! Check this box to create an account and get \(Int((points ?? 0) * 10)) reward points."
        self.view.backgroundColor = primaryColor
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
       // swipeRight.direction = .right
        //self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
    }
    
    @objc func handleDate() {
        if (K_PICKER_DATE != nil){
            self.dob.text = self.convertTimestampToDate(Int(K_PICKER_DATE!.timeIntervalSince1970), to: "MMM dd, yyyy")
        }
    }
    
    func clearFields() {
        self.firstName.text = ""
        self.lastName.text = ""
        self.email.text = ""
        self.password.text = ""
        self.mobileNumber.text = ""
        self.dob.text = ""
    }
    
    func callGuestLoginApi(){
        ActivityIndicator.show(view: self.view)
        let myCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? ""
        let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) ?? ""
        let charactersToRemove = CharacterSet(charactersIn: "()+-")
        var telephone = self.mobileNumber.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        let param = [
            "user_first_name": self.firstName.text ?? "",
            "user_last_name": self.lastName.text ?? "",
            "user_email": self.email.text ?? "",
            "user_number": telephone,
            "token":fcmToken,
            "platform": 2
            ] as? [String: Any]
        UserDefaults.standard.set(param, forKey: UD_GUEST_LOGIN_DETAIL)
            if(self.popFromOrderPage){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                let myCartId = (UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? "") as? String
                myVC.orderCart = myCartId ?? ""
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                if (Singleton.shared.selectedLocation.id != nil){
                    if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
                        if(item == 0){
                            NavigationController.shared.pushMenu(controller: self)
                        }else {
                            NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
                        }
                    }else {
                        NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
                    }
                }else {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                    currentPageView = K_MAP_PAGE_VIEW
                    myVC.hideCrossView = true
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
    }
    
    //MARK: IBAction
    @IBAction func tickAction(_ sender: Any) {
        if(tickImage.image == #imageLiteral(resourceName: "Rectangle 1")){
            self.createAccountView.isHidden = false
            tickImage.image = #imageLiteral(resourceName: "check-square")
        }else {
            self.createAccountView.isHidden = true
            tickImage.image = #imageLiteral(resourceName: "Rectangle 1")
        }
    }
    
     @IBAction func showPaaswordAction(_ sender: Any) {
               if(self.showPassImg.image == #imageLiteral(resourceName: "visibility")){
                   self.showPassImg.image = #imageLiteral(resourceName: "password")
                   self.password.isSecureTextEntry = false
               }else {
                   self.showPassImg.image = #imageLiteral(resourceName: "visibility")
                   self.password.isSecureTextEntry = true
               }
           }
    
    @IBAction func crossAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDate), name:Notification.Name(rawValue: N_SELECT_DATE), object: nil)
        self.navigationController?.present(myVC, animated: true)
    }
    
    @IBAction func createAccount(_ sender: Any) {
        self.errorCount = 0
        if(firstName.text!.isEmpty){
            self.errorCount += 1
            firstName.errorMessage = "Enter First Name"
        }
        if(lastName.text!.isEmpty){
            self.errorCount += 1
            lastName.errorMessage = "Enter Last Name"
        }
        if(email.text!.isEmpty){
            self.errorCount += 1
            email.errorMessage = "Enter Email Address"
        }
        if !(self.isValidEmail(emailStr: email.text!.replacingOccurrences(of: " ", with: ""))){
            self.errorCount += 1
            email.errorMessage = "Enter valid Email Address"
        }
        if(self.mobileNumber.text!.isEmpty){
            self.errorCount += 1
            mobileNumber.errorMessage  = "Enter mobile number"
        }
        if(tickImage.image == UIImage(named: "Rectangle 1")){
            if(self.errorCount > 0){
             return
            }else {
              self.callGuestLoginApi()
              return
            }
        }
        
        if(password.text!.isEmpty){
            self.errorCount += 1
            password.errorMessage = "Enter Password"
        }
        if(!self.isPasswordValid(password.text!)){
            self.errorCount += 1
            password.errorMessage = "Password must be alphanumeric"
        }
        if(dob.text!.isEmpty){
            self.errorCount += 1
            dob.errorMessage = "Enter Date of Birth"
        }
        if(self.errorCount > 0){
            return
        }else {
            ActivityIndicator.show(view: self.view)
            let myCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? ""
            let fcmToken = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) ?? ""
            let charactersToRemove = CharacterSet(charactersIn: "()+-")
            var telephone = self.mobileNumber.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            
            let dob = self.convertTimestampToDate(Int(K_PICKER_DATE!.timeIntervalSince1970), to: "MM-dd-yyyy")
            
            let param = [
                "first_name": self.firstName.text ?? "",
                "last_name": self.lastName.text ?? "",
                "email": (self.email.text ?? "").replacingOccurrences(of: " ", with: ""),
                "mobile": telephone,
                "password": self.password.text ?? "",
                "zip_code": self.zipCode.text,
                "date_of_birth":dob ,
                "cart_id": myCartId,
                "google_id":"",
                "facebook_id":"",
                "token":fcmToken,
                "platform": 2
                ] as? [String: Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REGISTER, method: .post, parameter: param, objectClass: SignUp.self, requestCode: U_REGISTER, userToken: nil) { (response) in
                self.clearFields()
                ActivityIndicator.hide()
                UserDefaults.standard.set(response.response.token, forKey: UD_TOKEN)
                let userData = try! JSONEncoder().encode(response.response)
                UserDefaults.standard.set(userData, forKey:UD_USER_DETAIl)
                Singleton.shared.userDetail = response.response
                Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_LOGGEDIN_USER)
                let nAVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                nAVC.modalPresentationStyle = .overFullScreen
                nAVC.heading = "You have successfully subscribed with Farmer's. Get rewarded with free entree now."
                self.navigationController?.present(nAVC, animated: false, completion: {
                   if(self.popFromOrderPage){
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                        let myCartId = (UserDefaults.standard.value(forKey: UD_ADD_TO_CART) ?? "") as? String
                        myVC.orderCart = myCartId ?? ""
                        self.navigationController?.pushViewController(myVC, animated: true)
                    }else {
                        if (Singleton.shared.selectedLocation.id != nil){
                            if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
                                if(item == 0){
                                    NavigationController.shared.pushMenu(controller: self)
                                }else {
                                    NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
                                }
                            }else {
                                NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
                            }
                        }else {
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                            currentPageView = K_MAP_PAGE_VIEW
                            myVC.hideCrossView = true
                            self.navigationController?.pushViewController(myVC, animated: true)
                        }
                    }
                })
            }
        }
    }
}

extension GuestLoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! SkyFloatingLabelTextField).errorMessage = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNumber){
            self.mobileNumber.text =  self.mobileNumber.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            let currentCharacterCount = self.mobileNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            
            return newLength <= 14
        }else if (textField == zipCode) {
            let currentCharacterCount = self.zipCode.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 5
        }else {
            return true
        }
    }
}
