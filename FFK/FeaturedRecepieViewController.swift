//
//  FeaturedRecepieViewController.swift
//  Farmers
//
//  Created by qw on 27/02/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class FeaturedRecepieViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var restaurentsTable: UITableView!
    @IBOutlet weak var cartItemCount: UILabel!
    // @IBOutlet weak var viewForCount: View!
    @IBOutlet weak var addToButton: CustomButton!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    
    var menuData = getMenusList()
    var menuId = Int()
    var itemId = Int()
    var heading = String()
    var isCommon = Int()
    var isNavigationFromBonus = false
    var bonusUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headingLabel.text = heading
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
       // swipeRight.direction = .right
        //  self.view!.addGestureRecognizer(swipeRight)
        //self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
        self.addToButton.backgroundColor = primaryColor
        self.initialiseView()
        if #available(iOS 15.0, *) {
            if(self.restaurentsTable != nil){
              self.restaurentsTable.sectionHeaderTopPadding = 0
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
        //            self.cartItemCount.text = "\(item)"
        //            if(item == 0){
        //                self.viewForCount.isHidden = true
        //            }else {
        //                self.viewForCount.isHidden = false
        //            }
        //        }else {
        //            self.viewForCount.isHidden = true
        //        }
    }
    
    func initialiseView(){
        self.itemImage.sd_setImage(with: URL( string: U_IMAGE_BASE + (menuData.item_image ?? "")), placeholderImage:nil)
        self.itemName.text = self.menuData.item_name
        self.itemDescription.text = self.menuData.item_description
        if(self.isNavigationFromBonus){
            self.itemPrice.isHidden = true
        }else {
            self.itemPrice.text = "$\(self.menuData.item_price ?? 0)"
        }
    }
    
    func addToCart(menuId:Int, itemId: Int) {
        ActivityIndicator.show(view: self.view)
        if(self.isNavigationFromBonus){
            SessionManager.shared.methodForApiCalling(url: self.bonusUrl, method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_APPLY_BONUS, userToken: nil) { (response) in
                ActivityIndicator.hide()
                NavigationController.shared.pushMenuFromReward(controller: self, direction: 3)
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                               myVC.modalPresentationStyle = .overFullScreen
                               myVC.heading = "Bonus item added successfully to your cart."
                               
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }
            
        }else {
            var param = [String:Any]()
            if let id =  UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String {
                param = [
                    "menu_id":menuId,
                    "menu": [],
                    "cart_id": id,
                    "item_id":itemId,
                    "restaurant": Singleton.shared.selectedLocation.id,
                ]
            }else {
                param = [
                    "menu_id":menuId,
                    "menu": [],
                    "item_id":itemId,
                    "restaurant": Singleton.shared.selectedLocation.id,
                ]
            }
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART, method: .post, parameter: param, objectClass: AddToCartResponse.self, requestCode: U_ADD_TO_CART, userToken: nil) { (response) in
                UserDefaults.standard.set(response.response, forKey: UD_ADD_TO_CART)
                ActivityIndicator.hide()
                let count = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int
                if(self.isCommon != 1 || count == nil || count == 0){
                    UserDefaults.standard.set(menuId, forKey: UD_CATEGORY_TYPE)
                }
                Singleton.shared.cardDetails = GetCart()
                ActivityIndicator.hide()
                NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func addToBagAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        self.addToCart(menuId: self.menuId, itemId: self.itemId)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
