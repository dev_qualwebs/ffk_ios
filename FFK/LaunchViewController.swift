//
//  LaunchViewController.swift
//  Farmers
//
//  Created by qw on 02/06/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.transition(with: self.view, duration: 2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: false, completion: nil)
            })
        }
    }
    

}
