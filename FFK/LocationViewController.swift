//
//  LocationViewController.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import GooglePlaces


class LocationViewController: UIViewController, CLLocationManagerDelegate {
    //MARK: IBOutlets
    
    @IBOutlet weak var locationtable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var locationsData = [LocationResponse]()
    var searchData = [LocationResponse]()
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var locationManager = CLLocationManager()
    var currentTable = 1
    let token = GMSAutocompleteSessionToken.init()
    let filter = GMSAutocompleteFilter()
    let placesClient = GMSPlacesClient()
    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filter.type = .address
        filter.country = "USA"
        
        searchBar.delegate = self
        self.tableView.keyboardDismissMode = .onDrag
        locationManager.delegate = self
        searchBar.showsCancelButton = false
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        self.locationtable.isScrollEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.enableTableScroll), name: NSNotification.Name(N_SCROLL_TABLE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationData), name: NSNotification.Name(N_LOCATION_DATA), object: nil)
        locationtable.tableFooterView = UIView()
        self.searchBar.searchBarStyle = .minimal
        self.hideKeyboardWhenTappedAround()
        if #available(iOS 15.0, *) {
            self.locationtable.sectionHeaderTopPadding = 0
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name("page_controller"), object: nil, userInfo: ["id":0])
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        
        if let nav = self.navigationController {
            nav.view.endEditing(true)
        }
    }
    
    @objc func locationData(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_LOCATION_DATA), object: nil)
        self.locationsData = []
        if let data = notification.userInfo!["data"] as? [LocationResponse]{
            for val in data {
                var loc = val
                loc.isSelected = false
                self.locationsData.append(loc)
            }
            self.locationtable.reloadData()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationData), name: NSNotification.Name(N_LOCATION_DATA), object: nil)
    }
    
    
    @objc func enableTableScroll(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SCROLL_TABLE), object: nil)
        let data = notification.userInfo!["scrollEnable"] as! Bool
        if(data){
            self.locationtable.isScrollEnabled = true
        }else {
            self.locationtable.isScrollEnabled = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.enableTableScroll), name: NSNotification.Name(N_SCROLL_TABLE), object: nil)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    
    //MARK IBAction
    @IBAction func addressButtonAction(_ sender: Any) {
        self.searchBar.resignFirstResponder()
        self.latitude = 0
        self.longitude = 0
        self.currentTable = 1
        self.searchBar.text = ""
        NotificationCenter.default.post(name: NSNotification.Name(N_LOCATION_CONTAINER), object: nil, userInfo: ["increaseHeight": "false"])
        self.locationtable.reloadData()
    }
    
}

extension LocationViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(currentTable == 1){
            self.locationtable.isScrollEnabled = false
            if(self.hasLocationPermission() && self.latitude == 0){
                if let loc = locationManager.location {
                    self.latitude = loc.coordinate.latitude
                    self.longitude = loc.coordinate.longitude
                }
            }
            var newLocation = [LocationResponse]()
            for val in self.locationsData {
                var loc = val
                loc.distance = Double(self.calculateDistanceMeter(cor1: CLLocation(latitude: self.latitude, longitude: self.longitude), cor2: CLLocation(latitude: CLLocationDegrees(Float(val.latitude ?? "0")!), longitude: CLLocationDegrees(Float(val.longitude ?? "0")!))))
                newLocation.append(loc)
            }
            self.locationsData = []
            self.locationsData = newLocation.sorted(by: {
                ($0.distance ?? 0) < ($1.distance ?? 0)
            })
            if(self.locationsData.count > 0){
                NotificationCenter.default.post(name: NSNotification.Name(N_SELECTED_RESTAURENT_MAP), object: nil,userInfo: ["lat": self.locationsData[0].latitude,"lng":self.locationsData[0].longitude])
            }
            return self.locationsData.count
        }else {
            self.locationtable.isScrollEnabled = true
            return self.searchData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
        var val = LocationResponse()
        if(self.currentTable == 1){
            cell.mapImage.isHidden = true
            val = self.locationsData[indexPath.row]
            cell.cityName.isHidden = false
            cell.cityName.text = val.name
            cell.cityName.textColor = primaryColor
            cell.address.text = val.address
            cell.address.textColor  = .black
            if(self.hasLocationPermission() && self.latitude == 0){
                if let loc = locationManager.location {
                    self.latitude = loc.coordinate.latitude
                    self.longitude = loc.coordinate.longitude
                }
            }
            if(self.latitude != 0){
                cell.distance.text = self.calculateDistance(cor1: CLLocation(latitude: self.latitude, longitude: self.longitude), cor2: CLLocation(latitude: CLLocationDegrees(Float(val.latitude ?? "0")!), longitude: CLLocationDegrees(Float(val.longitude ?? "0")!)))
            }else {
                cell.distance.text = ""
            }
        }else if(currentTable == 2){
            cell.mapImage.isHidden = true
            if((self.searchData.count-1) < (indexPath.row)){
                return cell
            }
            val = self.searchData[indexPath.row]
            if let loc = val.name?.components(separatedBy: ","){
                if(loc.count > 0){
                    var location = val.name?.replacingOccurrences(of: loc[0] + ", ", with: "")
                    cell.cityName.text = loc[0]
                    cell.cityName.textColor = .black
                    cell.distance.text = ""
                    cell.address.text = (location ?? "")
                    cell.address.textColor  = .darkGray
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(self.currentTable == 1){
            let val = self.locationsData[indexPath.row]
            Singleton.shared.cityName = val.name ?? ""
            Singleton.shared.address = val.address ?? ""
            locationId = val.id ?? 0
            if(self.hasLocationPermission()){
                restaurantDistance = self.calculateDistance(cor1: CLLocation(latitude: self.latitude, longitude: self.longitude), cor2: CLLocation(latitude: CLLocationDegrees(Float(val.latitude ?? "0")!), longitude: CLLocationDegrees(Float(val.longitude ?? "0")!)))
            }
            NotificationCenter.default.post(name: NSNotification.Name(N_SELECTED_RESTAURENT_MAP), object: nil,userInfo: ["lat": self.locationsData[indexPath.row].latitude,"lng":self.locationsData[indexPath.row].longitude])
            let page = PageViewController.customDataSource as! PageViewController
            page.setSecondController()
        }else if(self.currentTable == 2){
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                
                UInt(GMSPlaceField.coordinate.rawValue) |
                GMSPlaceField.addressComponents.rawValue |
                                                        GMSPlaceField.formattedAddress.rawValue)
            if(self.searchData.count > 0){
                placesClient.fetchPlace(fromPlaceID:  self.searchData[indexPath.row].placeId ?? "", placeFields: fields, sessionToken: nil) { (place: GMSPlace?, error) in
                    if let result = place {
                        self.searchBar.text = result.name
                        self.latitude = result.coordinate.latitude
                        self.longitude = result.coordinate.longitude
                        self.currentTable = 1
                        self.searchBar.resignFirstResponder()
                        self.searchData = []
                        self.locationtable.reloadData()
                        NotificationCenter.default.post(name: NSNotification.Name(N_LOCATION_CONTAINER), object: nil, userInfo: ["increaseHeight": "false"])
                    }
                }
            }
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y < -100){
            NotificationCenter.default.post(name: NSNotification.Name(N_CONTAINER_SIZE), object: nil, userInfo: nil)
            self.locationtable.isScrollEnabled = false
        }
    }
}


extension LocationViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.currentTable = 2
        self.locationtable.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(N_LOCATION_CONTAINER), object: nil, userInfo: ["increaseHeight": "true"])
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if (self.searchBar.text == ""){
            self.currentTable = 1
            self.latitude = 0
            self.longitude = 0
            self.locationtable.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        placesClient.findAutocompletePredictions(fromQuery: searchText, filter: filter, sessionToken: token) { (results, error) in
            if let error = error {
                print("Autocomplete error: \(error)")
                return
            }
            if let results = results {
                self.searchData = []
                self.currentTable = 2
                for result in results {
                    var a = LocationResponse()
                    a.placeId = result.placeID
                    a.name = result.attributedFullText.string
                    self.searchData.append(a)
                    self.locationtable.reloadData()
                    
                }
            }
        }
    }
}


extension LocationViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.searchBar.text = place.name
        // self.address.text = place.name
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        
        self.locationtable.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("clicked")
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func calculateDistance(cor1: CLLocation,cor2: CLLocation) -> String {
        var distanceInMeters =  cor1.distance(from: cor2)
        if(distanceInMeters < 1609){
            return "\(Int(distanceInMeters)) meters"
        }else {
            distanceInMeters = distanceInMeters/1609
            return "\(Int(distanceInMeters)) miles"
        }
    }
    
    func calculateDistanceMeter(cor1: CLLocation,cor2: CLLocation) -> Int {
        var distanceInMeters =  cor1.distance(from: cor2)
        return Int(distanceInMeters)
    }
    
}
