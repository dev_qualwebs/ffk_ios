//
//  ViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseMessaging
import FirebaseRemoteConfig

class ViewController: UIViewController,CLLocationManagerDelegate, Confirmation {
    func confirmationSelection() {
        if (Singleton.shared.restaurentLocation.count > 0) {
            if (Singleton.shared.restaurentLocation[0].id == restaurantId){
                NavigationController.shared.getMenuType(id: Singleton.shared.restaurentLocation[1].id ?? 0)
                Singleton.shared.selectedLocation = Singleton.shared.restaurentLocation[1]
                let selectedLoc = try! JSONEncoder().encode(Singleton.shared.selectedLocation)
                UserDefaults.standard.set(selectedLoc, forKey: UD_RESTAURANT_SELECTED)
                NavigationController.shared.getRecentOrder(0)
                NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
                
            }else {
                NavigationController.shared.getMenuType(id: Singleton.shared.restaurentLocation[0].id ?? 0)
                Singleton.shared.selectedLocation =  Singleton.shared.restaurentLocation[0]
                let selectedLoc = try! JSONEncoder().encode(Singleton.shared.selectedLocation)
                UserDefaults.standard.set(selectedLoc, forKey: UD_RESTAURANT_SELECTED)
                NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
            }
            NavigationController.shared.emptyMyCart()
        }
    }
    
    lazy var locationManager = CLLocationManager()
    var restaurantId = Int()
    var remoteConfig: RemoteConfig!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let activeOrder = UserDefaults.standard.data(forKey: UD_SINGLE_ACTIVE_ORDER) as? Data{
            do{
                let object = try? JSONDecoder().decode(SplashContent.self, from: activeOrder)
                Singleton.shared.activeOrderData.append(OrderResponse())
                Singleton.shared.activeOrderData[0].pickup_time = object?.pickupTime
                Singleton.shared.activeOrderData[0].address = object?.address
                Singleton.shared.singleArrayContent.append(object ?? SplashContent())
            NotificationCenter.default.post(name: NSNotification.Name(N_CHANGE_WELCOME_VIEW), object: nil)
            }catch {
                print(error)
            }
        }
        self.handleBaseUrl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func handleViewIntialization(){
        Singleton.shared.selectedLocation = Singleton.getSelectedRestaurant()
        self.handleRestaurantPreference()
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            NavigationController.shared.getRecentOrder(nil)
        }
        
        DispatchQueue.global(qos: .background).async {
            if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
                self.getCards()
            }
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.functionCalled(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserLogout), name: NSNotification.Name(N_LOGOUT_USER), object: nil)
    }
    
    func handleBaseUrl(){
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")
        
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                }
            } else {
            }
            if let url = self.remoteConfig["force_update_base_url"].stringValue as? String {
              //  U_BASE = url != "" ? url:U_BASE
                self.handleViewIntialization()
            }
            
            if let image_url = self.remoteConfig["main_image_base_url"].stringValue as? String {
                U_IMAGE_BASE = image_url
            }
            
            if let square_image_url = self.remoteConfig["square_image_base_url"].stringValue as? String {
                U_CUSTOM_SIZE = square_image_url
            }
            
            if let googleKey = self.remoteConfig["google_api_key_app"].stringValue as? String {
                K_GOOGLE_API_KEY = googleKey
            }
        }
    }
    
    @objc func handleUserLogout(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_LOGOUT_USER), object: nil)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGOUT, method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_LOGOUT, userToken: nil) { (response) in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            NavigationController.shared.emptyMyCart()
            UserDefaults.standard.removeObject(forKey: UD_ACTIVE_ORDER_EXIST)
            Messaging.messaging().unsubscribe(fromTopic: K_TOPIC_FOR_LOGGEDIN_USER)
            Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_ALL_USER)
            Singleton.shared.initialiseValues()
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserLogout), name: NSNotification.Name(N_LOGOUT_USER), object: nil)
    }
    
    func handleRestaurantPreference(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways,.authorizedWhenInUse:
            locationManager.delegate = self
            if let loc = locationManager.location {
                self.getPreferredRestaurant(loc: loc)
            }else {
                getLocations()
            }
        case .notDetermined:
            getLocations()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
        case .denied:
            getLocations()
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        case .restricted:
            // Nothing you can do, app cannot use location services
            break
        }
    }
    
    func getPreferredRestaurant(loc:CLLocation){
        let preference = Singleton.shared.selectedLocationPref
        var selectedRestaurant = Singleton.shared.selectedLocation
        if(selectedRestaurant.id == nil){
            if(Singleton.shared.restaurentLocation.count > 0){
                selectedRestaurant = Singleton.shared.restaurentLocation[0]
                Singleton.shared.selectedLocation = Singleton.shared.restaurentLocation[0]
                self.getLocations()
            }else {
                self.getLocations()
                return
            }
        }else {
            self.getLocations()
        }
        if (preference == 1){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PREFERRED_RESTAURANT, method: .post, parameter: ["latitude":loc.coordinate.latitude,"longitude":loc.coordinate.longitude], objectClass: GetLocationDetail.self, requestCode: U_GET_PREFERRED_RESTAURANT, userToken: nil) { (response) in
                if(selectedRestaurant.id != response.response.id){
                    self.restaurantId = selectedRestaurant.id ?? 0
                    self.handleLocationPopup(loc: response.response)
                }else{
                    NavigationController.shared.getMenuType(id: response.response.id ?? 0)
                    Singleton.shared.selectedLocation = response.response
                    NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
                    DispatchQueue.global(qos: .background).async {
                        self.updateUserLocation(coordinate: loc.coordinate)
                    }
                }
            }
        }else if(preference == 2){
            NavigationController.shared.getMenuType(id: selectedRestaurant.id ?? 0)
            Singleton.shared.selectedLocation = selectedRestaurant
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
            DispatchQueue.global(qos: .background).async {
                self.updateUserLocation(coordinate: loc.coordinate)
            }
        }else if(preference == 3){
            NavigationController.shared.getMenuType(id: selectedRestaurant.id ?? 0)
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
            DispatchQueue.global(qos: .background).async {
                self.updateUserLocation(coordinate: loc.coordinate)
            }
        }
    }
    
    @objc func functionCalled(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        if let val = notif.userInfo?["msg"] as? String{
            if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as? NotAvailableViewController{
                myVC.heading = val
                myVC.modalPresentationStyle = .overFullScreen
                self.present(myVC, animated: false, completion: nil)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.functionCalled(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
    }
    
    func getCards(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_CARD, method: .get, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_NEW_CARD, userToken: nil) { (response) in
            Singleton.shared.paymentCards = response.response
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
        }
    }
    
    
    func updateUserLocation(coordinate: CLLocationCoordinate2D){
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            let param = [
                "latitude":coordinate.latitude,
                "longitude":coordinate.longitude
            ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SET_USER_ADDRESS, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_SET_USER_ADDRESS, userToken: nil) { (response) in
                
            }
        }
    }
    
    func getLocations() {
        let locations = Singleton.shared.restaurentLocation
        if (locations.count > 0){
            Singleton.shared.selectedLocation = locations[0]
            NavigationController.shared.getMenuType(id: locations[0].id ?? 0)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_RESTAURENTS, method: .get, parameter: nil, objectClass: GetLocation.self, requestCode: U_GET_RESTAURENTS, userToken: nil) { (response) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: N_LOCATION_DATA), object: nil,userInfo: ["data": response.response])
                Singleton.shared.restaurentLocation = response.response
                if(response.response.count > 0){
                    Singleton.shared.selectedLocation = response.response[0]
                    let selectedLoc = try! JSONEncoder().encode(response.response)
                    let selectedLoc1 = try! JSONEncoder().encode(response.response[0])
                    UserDefaults.standard.set(selectedLoc, forKey: UD_FFK_RESTAURENT)
                    UserDefaults.standard.set(selectedLoc1, forKey: UD_RESTAURANT_SELECTED)
                }
            }
        }else {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_RESTAURENTS, method: .get, parameter: nil, objectClass: GetLocation.self, requestCode: U_GET_RESTAURENTS, userToken: nil) { (response) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: N_LOCATION_DATA), object: nil,userInfo: ["data": response.response])
                Singleton.shared.restaurentLocation = response.response
                if(Singleton.shared.selectedLocation.id == nil){
                    Singleton.shared.selectedLocation = response.response[0]
                }
                if(response.response.count > 0){
                    let selectedLoc = try! JSONEncoder().encode(response.response)
                    let selectedLoc1 = try! JSONEncoder().encode(response.response[0])
                    UserDefaults.standard.set(selectedLoc, forKey: UD_FFK_RESTAURENT)
                    UserDefaults.standard.set(selectedLoc1, forKey: UD_RESTAURANT_SELECTED)
                    NavigationController.shared.getMenuType(id: response.response[0].id!)
                }
            }
        }
    }
    
    func handleLocationPopup(loc:LocationResponse){
        if(NavigationController.shared.hasLocationPermission()){
            if(loc.id != Singleton.shared.selectedLocation.id){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.confirmationText = "You are closer to \(loc.name ?? "").\nDo you want to change your current restaurant to \(loc.name ?? "")?"
                myVC.secondTxt = true
                myVC.isConfirmationViewHidden = false
                myVC.confirmationDelegate = self
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }
            else {
                self.confirmationSelection()
            }
        }
    }
}

