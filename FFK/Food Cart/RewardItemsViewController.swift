//
//  MyCartViewController.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SDWebImage

protocol RewardItemAdded {
  func refreshCartTable()
}

class RewardItemsViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var myCartTable: UITableView!
    @IBOutlet weak var noItemsFound: UILabel!
    
   // var rewardItems = RewardItems()
    var rewardItemDelegate: RewardItemAdded? = nil
    var rewardData = [RewardItemsRespose]()
    //var sectionHeading = ["Free Entree", "Dessert(Birthday Reward)","Free Entree By Admin"]
    var itemFor = 1
    var navigatedFromSavedReward = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = primaryColor
        myCartTable.estimatedRowHeight = 90.0
        myCartTable.rowHeight = UITableView.automaticDimension
         myCartTable.tableFooterView = UIView()
        getCartData()
        if #available(iOS 15.0, *) {
            self.myCartTable.sectionHeaderTopPadding = 0
        }
    }
    
    func getCartData() {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REWARD_ITEMS + "/\(itemFor)", method: .get, parameter: nil, objectClass: GetRewardItems.self, requestCode: U_GET_REWARD_ITEMS, userToken: nil) { (response) in
                self.rewardData = response.response
                self.rewardData = self.rewardData.filter{$0.item != nil}
              //  self.rewardItems = response.response
                self.myCartTable.reloadData()
                ActivityIndicator.hide()
            }
    }
    
    func addToCart(id: Int){
        ActivityIndicator.show(view: self.view)
        var param = [String:Any?]()
        if let cart =  UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String {
            param = [
                "menu_id":id,
                "menu": nil,
                "cart_id": cart,
                "flag": self.itemFor,
                "restaurant": Singleton.shared.selectedLocation.id,
                ]
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART, method: .post, parameter: param, objectClass: AddToCartResponse.self, requestCode: U_ADD_TO_CART, userToken: nil) { (response) in
            UserDefaults.standard.set(response.response, forKey: UD_ADD_TO_CART)
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                     myVC.modalPresentationStyle = .overFullScreen
            myVC.heading = response.message
                                    self.navigationController?.present(myVC, animated: false, completion: nil)
           
            ActivityIndicator.hide()
            if(self.navigatedFromSavedReward){
                NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
            }else {
                self.rewardItemDelegate?.refreshCartTable()
                self.dismiss(animated: true, completion:nil)
            }
        }
    }
    
    
    ///IBACtions
    
    @IBAction func backAction(_ sender: Any) {
        if(navigatedFromSavedReward){
          self.navigationController?.popViewController(animated: true)
        }else {
            self.dismiss(animated: true, completion:nil)
        }
    }
}

extension RewardItemsViewController : UITableViewDelegate,UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 0
//    }
//
//
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return self.sectionHeading[section]
//    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.rewardData.count == 0){
            self.noItemsFound.isHidden = true
        }else {
            self.noItemsFound.isHidden = true
        }
        return self.rewardData.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurentTableViewCell") as! RestaurentTableViewCell
            let data = self.rewardData[indexPath.row].item
        cell.nameLabel.text = data?.item_name ?? ""
            cell.orderLabel.text = data?.item_description ?? ""
            cell.foodImage!.sd_setImage(with: URL( string: U_IMAGE_BASE +  (data?.item_image ?? "")), placeholderImage:nil)
            return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(itemFor == 1){
            self.addToCart(id: self.rewardData[indexPath.row].item_id!)
        }else if(itemFor == 2){
             self.addToCart(id: self.rewardData[indexPath.row].item_id!)
        }else  if(itemFor == 3){
            self.addToCart(id: self.rewardData[indexPath.row].item_id!)
        }
    }
}
