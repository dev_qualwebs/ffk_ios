//
//  EditCartViewController.swift
//  FFK
//
//  Created by AM on 27/09/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit


protocol EditCart {
    func updateCartTable()
}

class EditCartViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var restaurentsTable: UITableView!
    @IBOutlet weak var noData: UILabel!
    @IBOutlet weak var subheading: UILabel!
    @IBOutlet weak var cartItemCount: UILabel!
    
    //Mark: Properties
    var mainMenuId = Int()
    var navTitle = String()
    var navSubtitle = String()
    var submenuData = [SubMenuResponse]()
    var cartItem:CardItems?
    var dictionary:[SubMenuResponse] = []
    var totalSelectedItem =  Int()
    var ruleType = Int()
    var editDelegate: EditCart? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
       // swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        noData.isHidden = true
        self.view.backgroundColor = primaryColor
        restaurentsTable.estimatedRowHeight = UITableView.automaticDimension
        //restaurentsTable.rowHeight = UITableView.automaticDimension
        restaurentsTable.tableFooterView = UIView()
        self.subheading.text = navSubtitle
        getSubmenu()
        
        if #available(iOS 15.0, *) {
            self.restaurentsTable.sectionHeaderTopPadding = 0
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let item = UserDefaults.standard.value(forKey: UD_CARTITEM_COUNT) as? Int {
            self.cartItemCount.text = "\(item)"
        }
    }
    
    ///IBACtions
    @IBAction func addToBag(_ sender: Any) {
        handleSelectedValue()
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func myBagAction(_ sender: Any) {
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderCartViewController") as! OrderCartViewController
        myVC.popBack = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func getSubmenu(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_EDIT_CART_MODIFIER + "\(self.mainMenuId)", method: .get, parameter: nil, objectClass: GetSubmenu.self, requestCode: U_GET_SUBCATEGORIES, userToken: nil) { (response) in
            self.submenuData = response.response
            
            for val in 0..<self.submenuData.count {
                self.submenuData[val].sectionItemCount = 0
                for check in 0..<self.submenuData[val].meals.count {
                    self.submenuData[val].meals[check].itemCount = self.submenuData[val].meals[check].count ?? 0
                    if(self.submenuData[val].meals[check].itemCount! > 0){
                        self.submenuData[val].sectionItemCount = (self.submenuData[val].sectionItemCount ?? 0) + 1
                        self.submenuData[val].meals[check].isSelected = 1
                    }else {
                        self.submenuData[val].meals[check].isSelected = 0
                    }
                }
            }
            if(self.submenuData.count == 0){
                self.noData.isHidden = false
            }else {
                self.noData.isHidden = true
            }
            self.restaurentsTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func handleSelectedValue(){
        
        var selectedItems = [AddToCart]()
        for val in self.submenuData{
            switch val.is_rule {
            case 1:
                if (val.sectionItemCount! != val.item_exactly!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select \(val.item_exactly!) from \(val.modifier_group_name ?? "")"
                    self.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id!)", modifier_item_id: "\(data.id!)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                            //selectedItems = items
                        }
                    }
                }
                break
            case 2:
                if !(val.sectionItemCount! >= val.item_range_from! && val.sectionItemCount! <= val.item_range_to!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select \(val.item_range_from!) from \(val.modifier_group_name ?? "")"
                    self.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id!)", modifier_item_id: "\(data.id!)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                            //selectedItems = items
                        }
                    }
                }
                break
            case 3:
                if (val.sectionItemCount! != val.item_maximum!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select \(val.item_maximum!) from \(val.modifier_group_name ?? "")"
                    self.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id!)", modifier_item_id: "\(data.id!)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                            // selectedItems = items
                        }
                    }
                }
                break
            default:
                break
            }
        }
        
        if(selectedItems.count  != 0 ){
            do {
                let jsonEncoder = JSONEncoder()
                let jsonData = try! jsonEncoder.encode(selectedItems)
                if let jsonArray = try JSONSerialization.jsonObject(with: jsonData, options : .allowFragments) as? [Dictionary<String,Any>]
                {
                    var param = [String:Any]()
                    param = [
                        "cart_item_id":self.mainMenuId,
                        "menu": jsonArray
                    ]
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_MY_CART, method: .post, parameter: param, objectClass: ErrorResponse.self, requestCode: U_UPDATE_MY_CART, userToken: nil) { (response) in
                        self.editDelegate?.updateCartTable()
                        
                        self.dismiss(animated: false, completion: nil)
                        //   self.navigationController?.popToRootViewController(animated: true)
                    }
                } else {
                    print("bad json")
                }
            } catch { print(error) }
        }
        
    }
}

extension EditCartViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int{
        return self.submenuData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.submenuData[section].meals.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.submenuData[section].modifier_group_name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var val = self.submenuData[indexPath.section].meals[indexPath.row]
        var sectionData = self.submenuData[indexPath.section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurentTableViewCell") as! RestaurentTableViewCell
        cell.viewForError.alpha = 0
        cell.foodImage.sd_setImage(with: URL(string: U_CUSTOM_SIZE + (sectionData.meals[indexPath.row].item_image ?? "")), placeholderImage:nil)
        cell.nameLabel.text = val.item_name
        if((sectionData.single_item_maximum ?? 0) == 1 || (sectionData.single_item_maximum ?? 0) == 0 ){
            cell.quantityStack.isHidden = true
            cell.viewForTick.isHidden = false
            cell.btnForTick.isHidden = false
        }else {
            cell.quantityStack.isHidden = false
            cell.viewForTick.isHidden = true
            cell.btnForTick.isHidden = true
        }
        
        
        cell.lblNumberofItems.text = "\(val.itemCount ?? 0)"
        if(cell.lblNumberofItems.text == "0"){
            cell.backVIewCount.backgroundColor = backgroundColor
        }else {
            cell.backVIewCount.backgroundColor = primaryColor
        }
        
        if(val.isSelected == 1){
            cell.checkImage.image = UIImage(named: "verified(3)")
        }else{
            cell.checkImage.image = UIImage(named: "")
        }
        cell.selectItem = {
            switch (sectionData.is_rule!) {
            case 1:
                if(cell.checkImage.image == UIImage(named: "")){
                    if(((self.submenuData[indexPath.section].sectionItemCount ?? 0) < self.submenuData[indexPath.section].item_exactly ?? 0) || self.submenuData[indexPath.section].meals.count == 2){
                        cell.checkImage.image = UIImage(named: "verified(3)")
                        if(self.submenuData[indexPath.section].meals.count == 2){
                            self.submenuData[indexPath.section].sectionItemCount = 1
                        }else {
                            self.submenuData[indexPath.section].sectionItemCount = (self.submenuData[indexPath.section].sectionItemCount ?? 0) + 1
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 1
                        
                        if(self.submenuData[indexPath.section].meals.count == 2){
                            if(indexPath.row == 0){
                                self.submenuData[indexPath.section].meals[1].isSelected = 0
                                self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 1, section: indexPath.section) as IndexPath)], with: .none)
                            }else {
                                self.submenuData[indexPath.section].meals[0].isSelected = 0
                                self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 0, section: indexPath.section) as IndexPath)], with: .none)
                            }
                        }
                        self.restaurentsTable.reloadRows(at: [indexPath], with: .none)
                    }else {
                        if(self.submenuData[indexPath.section].item_exactly == 1 && self.submenuData.count == 2){
                            return
                        }
                        if(self.submenuData[indexPath.section].item_exactly == 1 && self.submenuData.count > 2){
                            var meal = self.submenuData[indexPath.section].meals
                            for val in 0..<self.submenuData[indexPath.section].meals.count{
                                meal[val].isSelected = 0
                                meal[val].itemCount = 0
                            }
                         self.submenuData[indexPath.section].meals = meal
                         self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 1
                         self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                         self.restaurentsTable.reloadSections([indexPath.section], with: .none)
                         return
                        }
                        
                        cell.labelForError.text = "Can't add more than \(self.submenuData[indexPath.section].item_exactly ?? 0) item(s)"
                        cell.viewForError.alpha = 1
                        ////cell.labelForError.numberOfLines = 2
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                    }
                }else {
                    if(self.submenuData[indexPath.section].meals.count == 2){
                        if(indexPath.row == 0){
                            self.submenuData[indexPath.section].meals[1].isSelected = 1
                            self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 1, section: indexPath.section) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[indexPath.section].meals[0].isSelected = 1
                            self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 0, section: indexPath.section) as IndexPath)], with: .none)
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                    }else{
                        cell.checkImage.image = UIImage(named: "")
                        if (self.submenuData[indexPath.section].sectionItemCount! > 0){
                            self.submenuData[indexPath.section].sectionItemCount = ( self.submenuData[indexPath.section].sectionItemCount ?? 0) - 1
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 0
                    }
                    
                    self.restaurentsTable.reloadRows(at: [indexPath], with: .none)
                }
                break
            case 2:
                if(cell.checkImage.image == UIImage(named: "")){
                    if((((self.submenuData[indexPath.section].sectionItemCount ?? 0) >= (sectionData.item_range_from ?? 0)) && ((self.submenuData[indexPath.section].sectionItemCount ?? 0) < (sectionData.item_range_to ?? 0)) || self.submenuData[indexPath.section].sectionItemCount == 0) || self.submenuData[indexPath.section].meals.count == 2){
                        cell.checkImage.image = UIImage(named: "verified(3)")
                        if(self.submenuData[indexPath.section].meals.count == 2){
                            self.submenuData[indexPath.section].sectionItemCount = 1
                        }else {
                            self.submenuData[indexPath.section].sectionItemCount = (self.submenuData[indexPath.section].sectionItemCount ?? 0) + 1
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 1
                        
                        if(self.submenuData[indexPath.section].meals.count == 2){
                            if(indexPath.row == 0){
                                self.submenuData[indexPath.section].meals[1].isSelected = 0
                                self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 1, section: indexPath.section) as IndexPath)], with: .none)
                            }else {
                                self.submenuData[indexPath.section].meals[0].isSelected = 0
                                self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 0, section: indexPath.section) as IndexPath)], with: .none)
                            }
                        }
                        self.restaurentsTable.reloadRows(at: [indexPath], with: .none)
                        
                    }else {
                        if(self.submenuData[indexPath.section].item_range_to == 1 && self.submenuData.count == 2){
                            return
                        }
                        
                        if(self.submenuData[indexPath.section].item_range_to == 1 && self.submenuData.count > 2){
                            var meal = self.submenuData[indexPath.section].meals
                            for val in 0..<self.submenuData[indexPath.section].meals.count{
                                meal[val].isSelected = 0
                                meal[val].itemCount = 0
                            }
                         self.submenuData[indexPath.section].meals = meal
                         self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 1
                         self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                         self.restaurentsTable.reloadSections([indexPath.section], with: .none)
                         return
                        }
                        
                        cell.labelForError.text = "Can't add more than \(sectionData.item_range_to ?? 0) item(s)"
                        cell.viewForError.alpha = 1
                        ////cell.labelForError.numberOfLines = 2
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                    }
                }else {
                    if(self.submenuData[indexPath.section].meals.count == 2){
                        if(indexPath.row == 0){
                            self.submenuData[indexPath.section].meals[1].isSelected = 1
                            self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 1, section: indexPath.section) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[indexPath.section].meals[0].isSelected = 1
                            self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 0, section: indexPath.section) as IndexPath)], with: .none)
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                    }else{
                        cell.checkImage.image = UIImage(named: "")
                        if (self.submenuData[indexPath.section].sectionItemCount! > 0){
                            self.submenuData[indexPath.section].sectionItemCount = ( self.submenuData[indexPath.section].sectionItemCount ?? 0) - 1
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 0
                    }
                    
                    self.restaurentsTable.reloadRows(at: [indexPath], with: .none)
                }
                break
            case 3:
                if(cell.checkImage.image == UIImage(named: "")){
                    if(((self.submenuData[indexPath.section].sectionItemCount ?? 0) < sectionData.item_maximum ?? 0) || self.submenuData[indexPath.section].meals.count == 2){
                        cell.checkImage.image = UIImage(named: "verified(3)")
                        if(self.submenuData[indexPath.section].meals.count == 2){
                            self.submenuData[indexPath.section].sectionItemCount = 1
                        }else {
                            self.submenuData[indexPath.section].sectionItemCount = (self.submenuData[indexPath.section].sectionItemCount ?? 0) + 1
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 1
                        
                        if(self.submenuData[indexPath.section].meals.count == 2){
                            if(indexPath.row == 0){
                                self.submenuData[indexPath.section].meals[1].isSelected = 0
                                self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 1, section: indexPath.section) as IndexPath)], with: .none)
                            }else {
                                self.submenuData[indexPath.section].meals[0].isSelected = 0
                                self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 0, section: indexPath.section) as IndexPath)], with: .none)
                            }
                            
                        }
                        self.restaurentsTable.reloadRows(at: [indexPath], with: .none)
                    }else {
                        if(self.submenuData[indexPath.section].item_maximum == 1 && self.submenuData.count == 2){
                            return
                        }
                        
                        if(self.submenuData[indexPath.section].item_maximum == 1 && self.submenuData.count > 2){
                            var meal = self.submenuData[indexPath.section].meals
                            for val in 0..<self.submenuData[indexPath.section].meals.count{
                                meal[val].isSelected = 0
                                meal[val].itemCount = 0
                            }
                         self.submenuData[indexPath.section].meals = meal
                         self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 1
                         self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                         self.restaurentsTable.reloadSections([indexPath.section], with: .none)
                         return
                        }
                        cell.labelForError.text = "Can't add more than \(sectionData.item_maximum ?? 0) item(s)"
                        cell.viewForError.alpha = 1
                        ////cell.labelForError.numberOfLines = 2
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                    }
                }else {
                    if(self.submenuData[indexPath.section].meals.count == 2){
                        if(indexPath.row == 0){
                            self.submenuData[indexPath.section].meals[1].isSelected = 1
                            self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 1, section: indexPath.section) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[indexPath.section].meals[0].isSelected = 1
                            self.restaurentsTable.reloadRows(at: [(NSIndexPath(row: 0, section: indexPath.section) as IndexPath)], with: .none)
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                    }else{
                        cell.checkImage.image = UIImage(named: "")
                        if (self.submenuData[indexPath.section].sectionItemCount! > 0){
                            self.submenuData[indexPath.section].sectionItemCount = ( self.submenuData[indexPath.section].sectionItemCount ?? 0) - 1
                        }
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = 0
                    }
                    
                    self.restaurentsTable.reloadRows(at: [indexPath], with: .none)
                }
                
                break
            default:
                break
            }
        }
        
        cell.minusItem = {
            switch (sectionData.is_rule!) {
            case 1:
                if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) > 0){
                    val.itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) - 1
                    self.submenuData[indexPath.section].meals[indexPath.row].itemCount = ( (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) - 1)
                    self.submenuData[indexPath.section].sectionItemCount = self.submenuData[indexPath.section].sectionItemCount! - 1
                    if(self.submenuData[indexPath.section].meals[indexPath.row].itemCount == 0){
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                    }
                    cell.lblNumberofItems.text = "\((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0))"
                    
                }
                break
            case 2:
                if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) > 0){
                    val.itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) - 1
                    
                    self.submenuData[indexPath.section].meals[indexPath.row].itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) - 1
                    self.submenuData[indexPath.section].sectionItemCount = self.submenuData[indexPath.section].sectionItemCount! - 1
                    if(self.submenuData[indexPath.section].meals[indexPath.row].itemCount == 0){
                        
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                    }
                    cell.lblNumberofItems.text = "\((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0))"
                    
                }
                break
            case 3:
                if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) > 0){
                    val.itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) - 1
                    
                    self.submenuData[indexPath.section].meals[indexPath.row].itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) - 1
                    self.submenuData[indexPath.section].sectionItemCount = self.submenuData[indexPath.section].sectionItemCount! - 1
                    if(self.submenuData[indexPath.section].meals[indexPath.row].itemCount == 0){
                        
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 0
                    }
                    cell.lblNumberofItems.text = "\((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0))"
                }
                break
            default:
                break
            }
            if(cell.lblNumberofItems.text == "0"){
                cell.backVIewCount.backgroundColor = backgroundColor
            }else {
                cell.backVIewCount.backgroundColor = primaryColor
            }
        }
        
        cell.plusItem = {
            switch (sectionData.is_rule!) {
            case 1:
                if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) < sectionData.item_exactly!){
                    if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) < sectionData.single_item_maximum!){
                        val.itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) + 1
                        
                        (self.submenuData[indexPath.section].meals[indexPath.row].itemCount) = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) + 1
                        //if(self.submenuData[indexPath.section].meals[indexPath.row].itemCount == 1){
                        self.submenuData[indexPath.section].sectionItemCount = self.submenuData[indexPath.section].sectionItemCount! + 1
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                        // }
                        cell.lblNumberofItems.text = "\(self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0)"
                    }else {
                        cell.labelForError.text = "Can't add more item(s)"
                        cell.viewForError.alpha = 1
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                        
                    }
                    
                }else {
                    cell.labelForError.text = "Can't add more than \(sectionData.item_exactly ?? 0) item(s)"
                    cell.viewForError.alpha = 1
                    cell.labelForError.sizeToFit()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        cell.viewForError.alpha = 0
                    }
                    
                }
                break
            case 2:
                if(((self.submenuData[indexPath.section].sectionItemCount ?? 0) < sectionData.item_range_to!) || (self.submenuData[indexPath.section].sectionItemCount == 0) || (((self.submenuData[indexPath.section].sectionItemCount ?? 0) == sectionData.item_range_to!) && ((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) < sectionData.single_item_maximum!) && ((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) > 0))){
                    if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) < sectionData.single_item_maximum!){
                        val.itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) + 1
                        
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) + 1
                        //if(self.submenuData[indexPath.section].meals[indexPath.row].itemCount == 1){
                        self.submenuData[indexPath.section].sectionItemCount = self.submenuData[indexPath.section].sectionItemCount! + 1
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                        // }
                        cell.lblNumberofItems.text = "\((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0))"
                    }else {
                        cell.labelForError.text = "Can't add more item(s)"
                        cell.viewForError.alpha = 1
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                        
                    }
                }else {
                    if("\(sectionData.single_item_maximum ?? 0 )" == cell.lblNumberofItems.text){
                        cell.labelForError.text = "Can't add more than \(sectionData.single_item_maximum ?? 0) item(s)"
                        cell.viewForError.alpha = 1
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                        
                    }else {
                        cell.labelForError.text = "Can't add more than \(sectionData.item_range_to ?? 0) item(s)"
                        cell.viewForError.alpha = 1
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                    }
                }
                break
            case 3:
                if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) < sectionData.item_maximum!){
                    if((self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) < sectionData.single_item_maximum!){
                        val.itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) + 1
                        
                        self.submenuData[indexPath.section].meals[indexPath.row].itemCount = (self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0) + 1
                        //if(self.submenuData[indexPath.section].meals[indexPath.row].itemCount == 1){
                        self.submenuData[indexPath.section].sectionItemCount = self.submenuData[indexPath.section].sectionItemCount! + 1
                        self.submenuData[indexPath.section].meals[indexPath.row].isSelected = 1
                        // }
                        cell.lblNumberofItems.text = "\(self.submenuData[indexPath.section].meals[indexPath.row].itemCount ?? 0)"
                    }else {
                        cell.labelForError.text = "Can't add more item(s)"
                        cell.viewForError.alpha = 1
                        cell.labelForError.sizeToFit()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            cell.viewForError.alpha = 0
                        }
                    }
                }else {
                    cell.labelForError.text = "Can't add more than \(sectionData.item_maximum!) item(s)"
                    cell.viewForError.alpha = 1
                    cell.labelForError.sizeToFit()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        cell.viewForError.alpha = 0
                    }
                    
                }
                break
            default:
                break
            }
            if(cell.lblNumberofItems.text == "0"){
                cell.backVIewCount.backgroundColor = backgroundColor
            }else {
                cell.backVIewCount.backgroundColor = primaryColor
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        let attrs1 = [NSAttributedString.Key.font :  UIFont(name: "Montserrat-Regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.gray]
        
        let attrs2 = [NSAttributedString.Key.font :  UIFont(name: "Montserrat-Medium", size: 16), NSAttributedString.Key.foregroundColor : UIColor.black]
        var myLable = UILabel()
        
        
        if(self.submenuData[section].is_rule == 1){
            if((self.submenuData[section].item_exactly ?? 0) == 1){
                let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:self.submenuData[section].modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                header.textLabel?.attributedText = attributedString2
                
            }else {
                let attributedString1 = NSMutableAttributedString(string:" (choose upto \(self.submenuData[section].item_exactly ?? 0))", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:self.submenuData[section].modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                header.textLabel?.attributedText = attributedString2
            }
        }else if(self.submenuData[section].is_rule == 2){
            if((self.submenuData[section].item_range_to ?? 0) == 1){
                let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:self.submenuData[section].modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                header.textLabel?.attributedText = attributedString2
            }else {
                let attributedString1 = NSMutableAttributedString(string:" (choose upto \(self.submenuData[section].item_range_to ?? 0))", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:self.submenuData[section].modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                header.textLabel?.attributedText = attributedString2
                
            }
        }else{
            if((self.submenuData[section].item_maximum ?? 0) == 1){
                let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:self.submenuData[section].modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                header.textLabel?.attributedText = attributedString2
            }else {
                let attributedString1 = NSMutableAttributedString(string:" (choose upto \(self.submenuData[section].single_item_maximum ?? 0))", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:self.submenuData[section].modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                header.textLabel?.attributedText = attributedString2
            }
        }
        header.tintColor = backgroundColor
        header.layer.borderWidth = 0.5
        header.textLabel?.textAlignment = .right
        header.layer.borderColor = barBackgroundColor.cgColor
        //header.textLabel?.textColor = UIColor.darkGray
        header.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
