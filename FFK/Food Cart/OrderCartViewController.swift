//
//  OrderCartViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import GoogleSignIn

class OrderCartViewController: UIViewController,RewardItemAdded,UIGestureRecognizerDelegate, Confirmation {
    
    func confirmationSelection() {
        if(self.currentPopup == 1){
            self.callAPIForRemoveCart(id:self.selectedCartId)
        }else if(self.currentPopup == 3){
            self.callAPIForDulicateCart(cartId: self.selectedCartId)
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var orderCollection: UICollectionView!
    @IBOutlet weak var orderTable: UITableView!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var orderSubtotal: UILabel!
    @IBOutlet weak var orderTax: UILabel!
    @IBOutlet weak var finalTotal: UILabel!
    @IBOutlet weak var cartItemCount: UILabel!
    @IBOutlet weak var mealTotal: UILabel!
    @IBOutlet weak var completeMealStack: UIStackView!
    @IBOutlet weak var addItemButton: CustomButton!
    @IBOutlet weak var viewForRewardButton: UIView!
    @IBOutlet weak var viewForBirthdayButton: UIView!
    @IBOutlet weak var viewForCount: View!
    @IBOutlet weak var rewardItemImage: UIImageView!
    @IBOutlet weak var rewardItemName: UILabel!
    @IBOutlet weak var rewardItemPrice: UILabel!
    @IBOutlet weak var rewardItemDesc: UILabel!
    
    @IBOutlet weak var bdayItemImg: UIImageView!
    @IBOutlet weak var bdayItemPrice: UILabel!
    
    @IBOutlet weak var bdayItemDesc: UILabel!
    @IBOutlet weak var bdayItemName: UILabel!
    
    @IBOutlet weak var viewForOrderTotal: UIView!
    @IBOutlet weak var noBagItem: UIView!
    
    @IBOutlet weak var confirmDetailButton: CustomButton!
    @IBOutlet weak var viewForMeal: UIStackView!
    @IBOutlet weak var yourOrder: UIView!
    @IBOutlet weak var viewForAdminReward: UIView!
    @IBOutlet weak var noItemLabel: UILabel!
    @IBOutlet weak var orderStackView: UIStackView!
    @IBOutlet weak var orderContainerView: UIView!
    @IBOutlet weak var bonusButton: UIView!
    @IBOutlet weak var viewForPromotion: UIView!
    @IBOutlet weak var promotionText: UILabel!
    @IBOutlet weak var promotionAmount: UILabel!
    @IBOutlet weak var viewAddFreeEntree: UIView!
    @IBOutlet weak var mealPriceMainView: UIView!
    @IBOutlet weak var completeYourMealLabel: UILabel!
    @IBOutlet weak var noItemBagImage: UIImageView!
    @IBOutlet weak var noBagItemDetail: UILabel!
    @IBOutlet weak var noMealLabel: UILabel!
    
    
    var cartData = [GetCartResponse]()
    var cardDetails = CartDetailResponse()
    var taxRate  = Double()
    var totalPrice = Double()
    var subTotalPrice = Double()
    var mealsData = [GetMealsResponse]()
    var mealPrice = Double()
    var popBack = false
    var cartId = String()
    var isBirthday = Int()
    var isReward = Int()
    var isAdminReward = Int()
    var isBonusAvailable = Int()
    var isRewardAvailable:Int?
    var currentRewardId:Int?
    var mealPreviousTick:IndexPath?
    var mealCurrentTick:IndexPath?
    var currentPopup: Int?
    var selectedCartId = Int()
    var isCartAPICalled = false
    // var initialTouchPoint = CGPoint(x: 0, y: 0)
    // var scroll: UIPanGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //scroll?.delegate = self
        //scroll = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        self.noBagItemDetail.text = "Almost there!\nJust make sure your order matches the reward, and you are good to go!"
        self.noBagItem.isHidden = false
        Singleton.shared.cardDetails = GetCart()
        orderTable.estimatedRowHeight = 40.0
        self.view.backgroundColor = primaryColor
        self.orderContainerView.isHidden = true
        self.viewForMeal.isHidden = true
        orderTable.rowHeight = UITableView.automaticDimension
        self.completeMealStack.isHidden  = false
        if #available(iOS 15.0, *) {
            self.orderTable.sectionHeaderTopPadding = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // if(Singleton.shared.cardDetails.response.cart_data.count == 0){
        if(self.isCartAPICalled == false){
            self.isCartAPICalled = true
            getCartData()
        }
    }
    
    func handleBonusSection(){
        if UserDefaults.standard.value(forKey: UD_TOKEN) != nil{
            if(self.cartData.count > 0){
                let myTax = Double(round(100*Double( self.cardDetails.total_tax ?? "0")!)/100)
                let myTotal = Double(round(100*Double(self.cardDetails.total_amount ?? "0")!)/100)
                self.orderSubtotal.text = "$" +  "\(String(format: "%.2f", (Double(round(100*(myTotal-myTax > 0 ? myTotal-myTax:0))/100))))"
                self.finalTotal.text = "$" +  "\(String(format: "%.2f", (Double(round(100*Double(self.cardDetails.total_amount ?? "0")!)/100))))"
                if(isBonusAvailable == 0 && self.cardDetails.bonus_id == nil){
                    self.bonusButton.isHidden = true
                    self.viewForPromotion.isHidden = true
                }else {
                    if(self.cardDetails.bonus_id != nil && self.cardDetails.bonus_id != 0){
                        self.promotionText.text = "   Bonus   x   "
                        self.bonusButton.isHidden = true
                        if((self.cardDetails.discount_amount == "0.00") || (self.cardDetails.discount_amount == "0.0") || (self.cardDetails.discount_amount == "0")){
                            self.promotionAmount.text = ""
                        }else {
                            self.promotionAmount.text = "-$" + ( self.cardDetails.discount_amount ?? "0")
                        }
                        self.viewForPromotion.isHidden = false
                    }else {
                        self.bonusButton.isHidden = false
                        self.viewForPromotion.isHidden = true
                    }
                }
                //  }
            }
        }
    }
    
    func showMealPrice(){
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
            self.completeYourMealLabel.frame.origin.x = 20
            self.mealTotal.frame.origin.x = self.mealPriceMainView.frame.maxX - 90
            self.completeYourMealLabel.translatesAutoresizingMaskIntoConstraints = true
            self.mealTotal.translatesAutoresizingMaskIntoConstraints = true
        }, completion: nil)
    }
    
    func hideMealPrice(){
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
            self.completeYourMealLabel.center.x = self.view.center.x
            self.mealTotal.frame.origin.x = self.mealPriceMainView.frame.maxX
            self.completeYourMealLabel.translatesAutoresizingMaskIntoConstraints = true
            self.mealTotal.translatesAutoresizingMaskIntoConstraints = true
        }, completion: nil)
    }
    
    func manageRewardItemButton() {
        if(self.isRewardAvailable != nil){
            
            self.viewForBirthdayButton.isHidden = true
            self.viewForRewardButton.isHidden = true
            self.viewForAdminReward.isHidden = true
            self.viewAddFreeEntree.isHidden = true
            self.bonusButton.isHidden = true
            if(self.isBonusAvailable == 1 && ((self.cardDetails.bonus_id ?? 0) == 0)){
                self.bonusButton.isHidden = false
            }else{
             self.bonusButton.isHidden = true
            }
        }else {
            if(self.cartData.count == 0){
                if(isBirthday == 0){
                    self.viewForBirthdayButton.isHidden = true
                }else {
                    self.viewForRewardButton.isHidden = false
                }
                if(isReward == 0){
                    self.viewForRewardButton.isHidden = true
                }else {
                    self.viewForRewardButton.isHidden = false
                }
                if(isAdminReward == 0){
                    self.viewForAdminReward.isHidden = true
                }else {
                    self.viewForRewardButton.isHidden = false
                }
            }else {
                if(isBirthday == 1 || isReward == 1 || isAdminReward == 1){
                    self.viewAddFreeEntree.isHidden = false
                }else{
                    self.viewAddFreeEntree.isHidden = true
                }
            }
        }
        
        if(self.viewAddFreeEntree.isHidden == false && self.viewForRewardButton.isHidden == false && self.cartData.count != 0){
            self.viewAddFreeEntree.isHidden = false
            self.viewForRewardButton.isHidden = true
        }
    }
    
    func refreshCartTable() {
        getCartData()
    }
    
    func getCartData() {
        // ActivityIndicator.show(view: self.view)
        let currentCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String
        var baseUrl = String()
        if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            baseUrl = U_BASE + U_GET_CART_DATA
        }else{
            if(currentCartId == nil || currentCartId == ""){
                return
            }else{
                baseUrl = U_BASE + U_GET_CART_DATA + (currentCartId ?? "")
            }
        }
        SessionManager.shared.methodForApiCalling(url: baseUrl, method: .get, parameter: nil, objectClass: GetCart.self, requestCode: U_GET_CART_DATA, userToken: nil) { (response) in
            self.isCartAPICalled = false
            if(response.response.cart_data.count > 0){
                UserDefaults.standard.setValue(response.response.cart_details.cart_id ?? "", forKey: UD_ADD_TO_CART)
            }
            //  Singleton.shared.cardDetails = response
            self.initializeView(response: response)
        }
    }
    
    func initializeView(response: GetCart){
        let currentCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String
        self.cartData = []
        self.totalPrice = 0
        self.taxRate = 0
        self.subTotalPrice = 0
        self.cartId = currentCartId ?? ""
        self.cartData = response.response.cart_data
        self.cardDetails = response.response.cart_details
        
        if(self.cartData.count == 0){
            self.mealsData = []
            UserDefaults.standard.removeObject(forKey: UD_CATEGORY_TYPE)
        }else {
            if let id = UserDefaults.standard.value(forKey: UD_CATEGORY_TYPE) as? Int {
                self.getMenuMeals(menuId: id,cartId: self.cartId)
            }else if((self.cardDetails.bonus_id ?? 0) != 0){
                if(self.cartData.count == 1){
                    self.getMenuMeals(menuId: self.cartData[0].menu_id ?? 0,cartId: self.cartId)
                }
            }
        }
        self.manageView(dataCount: self.cartData.count)
        self.cartItemCount.text = "\(response.response.cart_data.count)"
        self.isRewardAvailable = self.cardDetails.coupon_id
        self.isBonusAvailable = self.cardDetails.is_bonus ?? 0
        Singleton.shared.selectedRewardCoupenId = self.isRewardAvailable
        self.noItemLabel.textColor = .darkGray
        self.noItemLabel.font = UIFont(name: "Montserrat-Regular", size: 18)
        if(self.isRewardAvailable != nil){
            if(self.cardDetails.coupon_type == 2){
                // self.promotionText.text = "   Free Dessert    "
                self.promotionText.text = "    Free Dessert    X   "
            }else {
                // self.promotionText.text = "    Free Entree    "
                self.promotionText.text = "    Free Entree    X   "
            }
            
            if(self.cardDetails.discount_amount != "0.00" || self.cardDetails.discount_amount != "0.0" || self.cardDetails.discount_amount != "0" || self.cardDetails.discount_amount != nil){
                self.promotionAmount.text = "-$" + ( self.cardDetails.discount_amount ?? "0")
                self.viewAddFreeEntree.isHidden = false
                self.bonusButton.isHidden = true
            }else{
                self.viewAddFreeEntree.isHidden = true
            }
            
            self.currentRewardId =  self.cardDetails.coupon_id
            self.noItemBagImage.image = UIImage(named: "ic_award")
            self.noBagItemDetail.isHidden = false
            if(self.cardDetails.coupon_type == 2){
                self.noItemLabel.text = "Free Dessert"
            }else {
                self.noItemLabel.text = "Free Entree"
            }
            self.noItemLabel.textColor = primaryColor
            self.noItemLabel.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        }else {
            self.noBagItemDetail.isHidden = true
            self.noItemBagImage.image = UIImage(named: "emptyCart")
            self.noItemLabel.text = "Your bag is empty :-("
            self.currentRewardId = nil
        }
        self.orderTotal.text = "$" +  "\(Double(round(100*Double(self.cardDetails.order_total ?? "0")!)/100))"
        let myTotal = Double(round(100*Double(self.cardDetails.total_amount ?? "0.00")!)/100)
        let myTax = Double(round(100*Double(self.cardDetails.total_tax ?? "0.00")!)/100)
        self.orderSubtotal.text = "$" +  "\(String(format: "%.2f", Double(round(100*(myTotal-myTax > 0 ? myTotal-myTax:0))/100)))"
        self.orderTax.text = "$" +  "\(String(format: "%.2f", myTax))"
        
        self.finalTotal.text = "$" +  "\((String(format: "%.2f", myTotal)))"
        
        self.isBirthday =  self.cardDetails.is_birthday ?? 0
        self.isReward =  self.cardDetails.is_reward_coupon ?? 0
        self.isAdminReward =  self.cardDetails.is_admin_reward ?? 0
        
        
        self.handleBonusSection()
        if((self.cardDetails.bonus_id ?? 0) != 0){
            self.viewAddFreeEntree.isHidden = true
            self.bonusButton.isHidden = true
            self.viewForBirthdayButton.isHidden = true
            self.viewForRewardButton.isHidden = true
            self.viewForAdminReward.isHidden = true
            self.viewForPromotion.isHidden = false
            //self.noBagItemDetail.isHidden = false
            //self.noItemBagImage.image = UIImage(named: "trophy")
            //self.noItemLabel.text = "Bonus Applied"
        }else if ((self.cardDetails.coupon_id ?? 0) != 0){
            if(self.cardDetails.discount_amount == "0.00" || self.cardDetails.discount_amount == "0.0" || self.cardDetails.discount_amount == "0" || self.cardDetails.discount_amount == nil){
                self.promotionAmount.text = "Missing Item"
                self.promotionAmount.textColor = primaryColor
            }else {
                self.promotionAmount.textColor = .black
            }
            self.viewAddFreeEntree.isHidden = true
            self.bonusButton.isHidden = true
            self.viewForBirthdayButton.isHidden = true
            self.viewForRewardButton.isHidden = true
            self.viewForAdminReward.isHidden = true
            self.viewForPromotion.isHidden = false
        }else {
            if(self.isRewardAvailable == nil){
                self.noBagItemDetail.isHidden = true
                self.noItemBagImage.image = UIImage(named: "emptyCart")
                self.noItemLabel.text = "Your bag is empty :-("
            }
            self.viewForPromotion.isHidden = true
            self.manageRewardItemButton()
        }
        
        UserDefaults.standard.set(response.response.cart_data.count, forKey: UD_CARTITEM_COUNT)
        NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        ActivityIndicator.hide()
        UIView.transition(with: orderTable, duration: 1.0, options: .curveEaseInOut, animations: {self.orderTable.reloadData()}, completion: nil)
    }
    
    func getMenuMeals(menuId:Int, cartId: String){
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU_MEALS + "\(menuId)/\(Singleton.shared.selectedLocation.id ?? 0)?cart_id=\(cartId)", method: .get, parameter: nil, objectClass: GetMeals.self, requestCode: U_GET_MENU_MEALS, userToken: nil) { (response) in
            self.mealsData = []
            self.mealTotal.text = ""
            self.mealPrice = 0
            
            
            for val in response.response {
                if(val.meal != nil){
                    var addTick = val
                    if(val.cart_item_id != nil){
                        addTick.meal?.isSelected = 1
                        self.mealPrice = (self.mealPrice ?? 0) + (val.meal?.item_price ?? 0)
                        // self.mealTotal.text = "$" + "\(round(self.mealPrice*100)/100)"
                    }else{
                        addTick.meal?.isSelected = 0
                    }
                    self.mealsData.append(addTick)
                }
                if(val.cart_item_id != nil){
                    
                }
            }
            
            if(self.mealTotal.text == "$0" || self.mealTotal.text == ""){
                self.mealTotal.text = ""
                //self.hideMealPrice()
            }else{
                // self.showMealPrice()
            }
            
            if(self.mealsData.count > 0){
                self.completeMealStack.isHidden  = false
            }else {
                self.completeMealStack.isHidden  = false
            }
            self.mealTotal.translatesAutoresizingMaskIntoConstraints = false
            if(self.mealsData.count == 0){
                self.noMealLabel.isHidden = false
            }else {
                self.noMealLabel.isHidden = true
            }
            self.orderCollection.reloadData()
        }
    }
    
    func manageView(dataCount:Int){
        if(dataCount == 0){
            self.viewForCount.isHidden = true
            self.viewForOrderTotal.isHidden = true
            self.noBagItem.isHidden = false
            
            self.confirmDetailButton.isHidden = true
            self.viewForMeal.isHidden = true
            self.yourOrder.isHidden = true
            if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
                if(Singleton.shared.activeOrderData.count > 0){
                    self.orderContainerView.isHidden = false
                    
                }else {
                    self.orderContainerView.isHidden = true
                }
            }
        }else {
            self.viewForCount.isHidden = false
            self.viewForOrderTotal.isHidden = false
            self.noBagItem.isHidden = true
            self.confirmDetailButton.isHidden = false
            self.viewForMeal.isHidden = false
            self.yourOrder.isHidden = false
            self.orderContainerView.isHidden = true
        }
        //self.orderContainerView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func addToCart(menuId:Int, itemId: Int) {
        
        var param = [String:Any]()
        if let id =  UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String {
            param = [
                "menu_id":menuId,
                "menu": [],
                "cart_id": id,
                "item_id":itemId,
                "item_flag":4,
                "restaurant": Singleton.shared.selectedLocation.id,
            ]
        }else {
            param = [
                "menu_id":menuId,
                "menu": [],
                "item_id":itemId,
                "item_flag":4,
                "restaurant": Singleton.shared.selectedLocation.id,
            ]
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART, method: .post, parameter: param, objectClass: AddToCartResponse.self, requestCode: U_ADD_TO_CART, userToken: nil) { (response) in
            UserDefaults.standard.set(response.response, forKey: UD_ADD_TO_CART)
            UserDefaults.standard.set(menuId, forKey: UD_CATEGORY_TYPE)
            self.orderCollection.isUserInteractionEnabled = true
            self.getCartData()
            self.mealCurrentTick = nil
            self.mealPreviousTick = nil
        }
    }
    
    
    //MARK: IBACtions
    
    @IBAction func backAction(_ sender: Any) {
        let pageController = MainPageViewController.customDataSource as! MainPageViewController
        pageController.setHomeController()
    }
    
    
    @IBAction func confirmDetailAction(_ sender: Any) {
        if(self.cartData.count == 0){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            myVC.heading = "Add some item to cart."
            self.navigationController?.present(myVC, animated: false, completion: nil)
            
        }else{
            menuId = self.cartData[0].menu_id ?? 0
            Singleton.shared.orderTotal = self.finalTotal.text ?? "$0"
            if let log = UserDefaults.standard.value(forKey: UD_TOKEN){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                myVC.orderCart = self.cartId
                myVC.bonus_id = self.cardDetails.bonus_id
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                if let guestLogin = UserDefaults.standard.value(forKey: UD_GUEST_LOGIN_DETAIL) as? [String:Any]{
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                    myVC.orderCart = self.cartId
                    myVC.bonus_id = self.cardDetails.bonus_id
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else{
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    Singleton.shared.orderSubTotal = self.orderSubtotal.text ?? "0"
                    myVC.popFromOrderPage = true
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
        }
    }
    
    @IBAction func bonusButtonAction(_ sender: Any) {
        if(self.isRewardAvailable != nil){
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_REWARD + "\(self.currentRewardId ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_REWARD, userToken: nil) { (response) in
                Singleton.shared.selectedRewardCoupenId = nil
                // Singleton.shared.selectedBonus = GetBonusResponse()
                self.getCartData()
            }
        }else {
            if(self.cardDetails.bonus_id != nil && self.cardDetails.bonus_id != 0){
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_BONUS + "\(self.cardDetails.bonus_id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_REWARD, userToken: nil) { (response) in
                    //Singleton.shared.selectedRewardCoupenId = nil
                    // Singleton.shared.selectedBonus = GetBonusResponse()
                    self.getCartData()
                }
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BonusViewController") as! BonusViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func addAothermenuAction(_ sender: Any) {
        let pageController = MainPageViewController.customDataSource as! MainPageViewController
        pageController.setHomeController()
    }
    
    @IBAction func addAnotherMenu(_ sender: Any) {
        isOrderNow = true
        NavigationController.shared.pushHome2(controller: self)
    }
    
    @IBAction func freeEntreeAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedRewardsViewController") as! SavedRewardsViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension OrderCartViewController : UITableViewDelegate,UITableViewDataSource,EditCart {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurentTableViewCell") as! RestaurentTableViewCell
        let data = self.cartData[indexPath.row]
        
        cell.nameLabel.text = data.item_name ?? ""
        var selectedItem = String()
        
        var cartPrice = Double()
        for val in data.details {
            var price: Double = 0
            if let double = val.item_price as? Double{
                price = double
            }
            if let string = val.item_price as? String{
                price = Double(string)!
            }
            
            if  (price == 0.0){
                let string: String?
                if(!(val.item_count == 0)){
                    string = (val.item_name ?? "") + " (" +  "\(val.item_count ?? 1)" + ")"
                }else {
                    string = (val.item_name ?? "")
                }
                selectedItem =  selectedItem + (selectedItem == "" ? "": " \n") + string!
            }else {
                let itemCount = "\(val.item_count ?? 1)"
                let prc = ", $" + "\(price)" + ")"
                let string = (val.item_name ?? "") + " (" + itemCount + prc
                selectedItem =  selectedItem + (selectedItem == "" ? "": " \n") + string
                cartPrice = cartPrice + Double(Double(val.item_count ?? 1)*(price))
            }
        }
        
        
        cell.orderLabel.text = selectedItem  == "" ? data.item_description:selectedItem
        cell.orderLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
        cell.foodImage!.sd_setImage(with: URL( string: U_CUSTOM_SIZE + (data.item_image ?? "")), placeholderImage:nil)
        if(data.item_flag == 0 || data.item_flag == 4){
            let price = Double(cartPrice) + (data.item_price ?? 0)
            cell.rateLabel.text = "$" + "\(((price*100).rounded()/100) ?? 0)"
            if(data.details.count == 0){
                cell.buttonView.isHidden = true
                cell.viewNoModifiers.isHidden = false
                cell.viewForRewardType.isHidden = true
            }else {
                cell.viewNoModifiers.isHidden = true
                cell.buttonView.isHidden = false
                cell.viewForRewardType.isHidden = true
            }
            cell.rewardType.text = "Free Entree"
        }else if(data.item_flag == 1){
            cell.rateLabel.text = ""
            cell.buttonView.isHidden = true
            cell.viewForRewardType.isHidden = false
            cell.rewardType.text = "Free Entree"
        }else if(data.item_flag == 2){
            cell.rateLabel.text = ""
            cell.buttonView.isHidden = true
            cell.viewForRewardType.isHidden = false
            cell.rewardType.text = "Birthday Reward"
        }else if(data.item_flag == 5){
            cell.rateLabel.text = "$" + "\((((data.item_price ?? 0)*100).rounded()/100) ?? 0)"
            cell.buttonView.isHidden = true
            cell.viewNoModifiers.isHidden = true
            cell.rewardType.text = ""
            cell.viewForRewardType.isHidden = true
        }else {
            cell.rateLabel.text = ""
            cell.buttonView.isHidden = true
            cell.viewForRewardType.isHidden = false
            cell.rewardType.text = "Free Entree"
        }
        
        cell.removeItem = {
            self.currentPopup = 1
            self.selectedCartId = data.cart_item_id ?? 0
            self.showPopup(title: "Remove Item", msg: "Are you sure?", alertType: 1, cartId: data.cart_item_id)
        }
        
        cell.editItem = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EditCartViewController") as! EditCartViewController
            myVC.editDelegate = self
            myVC.mainMenuId = data.cart_item_id!
            myVC.modalPresentationStyle = .fullScreen
            self.present(myVC, animated: false, completion: nil)
            //self.navigationController?.pushViewController(myVC, animated: true)
        }
        cell.duplicateItem = {
            self.currentPopup = 3
            self.selectedCartId = data.cart_item_id ?? 0
            self.showPopup(title: "Duplicate Cart", msg: "Are you sure?", alertType: 3,cartId: data.cart_item_id)
        }
        return cell
    }
    
    func updateCartTable() {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.heading = "Successfully updated cart"
        self.present(myVC, animated: false, completion: nil)
        self.getCartData()
    }
    
    
    
    func callAPIForDulicateCart(cartId: Int?){
        ActivityIndicator.show(view: self.view)
        let param = [
            "cart_item_id": cartId
            ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DUPLICATE_CART, method: .post, parameter: param, objectClass: ErrorResponse.self, requestCode: U_DUPLICATE_CART, userToken: nil) { (response) in
            self.getCartData()
        }
    }
    
    func callAPIForRemoveCart(id: Int){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_CART + "\(id)", method: .get, parameter: nil, objectClass: ErrorResponse.self, requestCode: U_REMOVE_CART, userToken: nil) { (response) in
            self.orderCollection.isUserInteractionEnabled = true
            self.getCartData()
        }
    }
    
    func showPopup(title: String, msg: String,alertType:Int, cartId: Int?) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        if(self.currentPopup == 1){
            myVC.confirmationText = "Remove from bag?"
        }else if(currentPopup == 3){
            myVC.confirmationText = "Duplicate cart item?"
        }
        
        myVC.isConfirmationViewHidden = false
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        //alert.view.backgroundColor = lightPink
        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            if(alertType == 1){
                self.callAPIForRemoveCart(id:cartId!)
            }else if(alertType == 2){
                
            }else if(alertType == 3){
                self.callAPIForDulicateCart(cartId: cartId!)
            }
            self.dismiss(animated: true, completion: nil)
        }
        let noBtn = UIAlertAction(title: "No", style: .default){
            (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FooItemViewController") as! FooItemViewController
        //        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension OrderCartViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mealsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrederCollectionViewCell", for: indexPath) as! OrederCollectionViewCell
        var data = self.mealsData[indexPath.row].meal
        
        let spinner = UIActivityIndicatorView(style: .whiteLarge)
        spinner.color = primaryColor
        spinner.frame = cell.indicatorButton.frame
        spinner.startAnimating()
        spinner.alpha = 1.0
        cell.indicatorButton.addSubview(spinner)
        if(data?.isSelected == 1){
            cell.checkImage.isHidden = false
            cell.indicatorButton.isHidden = true
        }else {
            cell.checkImage.isHidden = true
            cell.indicatorButton.isHidden = true
            
        }
        cell.dishName.text = data?.item_name ?? ""
        cell.dishPrice.text = "$" + "\(data?.item_price ?? 0)"
        cell.dishImage!.sd_setImage(with: URL(string:U_IMAGE_BASE + (data?.item_image ?? "")), placeholderImage:nil)
        
        cell.isTick = {
            if(data?.isSelected == 1){
                self.orderCollection.isUserInteractionEnabled = false
                self.mealsData[indexPath.row].meal?.isSelected = 0
                self.mealPrice = (self.mealPrice ?? 0) - (self.mealsData[indexPath.row].meal?.item_price ?? 0)
                // self.mealTotal.text = "$" + "\(round(self.mealPrice*100)/100)"
                cell.checkImage.isHidden = true
                cell.indicatorButton.isHidden = false
                data?.isSelected = 0
                ActivityIndicator.showPosition(view: self.view, center: cell.indicatorButton.center)
                self.callAPIForRemoveCart(id:self.mealsData[indexPath.row].cart_item_id ?? 0)
                if(self.mealPrice == 0){
                    self.mealTotal.text = ""
                    // self.hideMealPrice()
                }
            }else {
                self.orderCollection.isUserInteractionEnabled = false
                if(self.mealPrice == 0){
                    // self.showMealPrice()
                }
                self.mealPrice = (self.mealPrice ?? 0) + (self.mealsData[indexPath.row].meal?.item_price ?? 0)
                // self.mealTotal.text = "$" + "\(round(self.mealPrice*100)/100)"
                self.mealsData[indexPath.row].meal?.isSelected = 1
                data?.isSelected = 1
                cell.indicatorButton.isHidden = false
                cell.checkImage.isHidden = true
                
                self.addToCart(menuId: self.mealsData[indexPath.row].menu_id ?? 0, itemId: self.mealsData[indexPath.row].item_id ?? 0)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 175)
    }
    
}

