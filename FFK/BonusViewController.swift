//
//  BonusViewController.swift
//  FFK
//
//  Created by qw on 04/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class BonusViewController: UIViewController, Confirmation {
    func confirmationSelection() {
        self.bonusTable.reloadData()
    }
    
    
    //MARK: IBOUtlets
    @IBOutlet weak var bonusTable: ContentSizedTableView!
    @IBOutlet weak var noBonus: UIView!
    @IBOutlet weak var descriptionView: UIView!
    
    var bonusData = [GetBonusResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGestureRight))
       // swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
        self.bonusTable.estimatedRowHeight = 200
        self.bonusTable.rowHeight = UITableView.automaticDimension
        bonusTable.tableFooterView = UIView()
        self.getBonus()
        bonusTable.tableFooterView = UIView()
    }
    
    @objc func handleGestureRight(gesture: UISwipeGestureRecognizer) -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getBonus(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_BONUS_LIST, method: .get, parameter: nil, objectClass: GetBonus.self, requestCode: U_BONUS_LIST, userToken: nil) { (response) in
            var bonus =  response.response.bonus
            bonus.append(contentsOf: response.response.offer)
            self.bonusData = bonus
            self.bonusTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func callBonusAPI(bonus:GetBonusResponse){
        if(bonus.bonus_type == 3){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "BonusItemViewController") as! BonusItemViewController
        myVC.bonus = bonus
        self.navigationController?.pushViewController(myVC, animated: true)
        }else {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_APPLY_BONUS + "\(bonus.bonus_id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_APPLY_BONUS, userToken: nil) { (response) in

            NavigationController.shared.pushMenuFromReward(controller: self,direction: 2)
        }
        }
    }
    
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
       // NavigationController.shared.addTransition(direction: .fromRight, controller: self)
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func addAnotherMenu(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
}

extension BonusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.bonusData.count == 0){
            self.noBonus.isHidden = false
            self.descriptionView.isHidden = true
        }else {
            self.noBonus.isHidden = true
            self.descriptionView.isHidden = false
        }
        return self.bonusData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BonusTableCell") as! BonusTableCell
        let val = self.bonusData[indexPath.row]
        cell.bonusName.text = val.bonus_name
        cell.expiryDate.text = "Offer Expires: \(self.convertTimestampToDate(Int(val.bonus_expiry_unix ?? "0")!, to: "M/d/yy"))"
        
        cell.bonusDescription.text = val.description ?? "Enjoy a FREE regular breakfast or lunch entree! Extras may be added at an additional"
        cell.moreInformation.text = val.term_and_condition
        let line = cell.moreInformation.calculateMaxLines()
        let line2 = cell.bonusDescription.calculateMaxLines()
        if (line2 > 3){
            cell.moreView.isHidden = false
        }else {
            if(cell.moreInformation.text == "" || cell.moreInformation.text == nil || line <= 2){
                cell.moreView.isHidden = true
            }else {
                cell.moreView.isHidden = false
            }
        }
        
//        if(Singleton.shared.selectedBonus.bonus_id == val.bonus_id){
//            cell.selectButton.setTitle("Redeemed", for: .normal)
//            cell.mainView.shadowOpacity = 0.4
//            cell.mainView.borderColor = primaryColor
//            cell.mainView.borderWidth = 2
//            cell.mainView.alpha = 1
//
//        }else {
//            cell.selectButton.setTitle("Redeem", for: .normal)
//            cell.mainView.shadowOpacity = 0.1
//            cell.mainView.borderColor = .lightGray
//            cell.mainView.borderWidth = 0.5
//            cell.mainView.alpha = 0.8
      //  }
    //    if(Singleton.shared.selectedBonus.bonus_id == nil){
            cell.selectButton.setTitle("Redeem", for: .normal)
            cell.mainView.shadowOpacity = 0.4
            cell.mainView.borderColor = .lightGray
            cell.mainView.borderWidth = 0.5
            cell.mainView.alpha = 1
       // }
        cell.applyButton = {
            Singleton.shared.cardDetails = GetCart()
//            if(Singleton.shared.selectedBonus.bonus_id == val.bonus_id){
//               // self.showPopup(title: "Are you sure?", msg: "This will remove the current selection.")
//            }else {
               // if(Singleton.shared.selectedBonus.bonus_id == nil){
                    self.callBonusAPI(bonus:val)
               // }
           // }
        }
        
        cell.moreBtn = {
            if cell.isLabelAtMaxHeight {
                //  cell.moreButton.setTitle("Show More", for: .normal)
                cell.isLabelAtMaxHeight = false
                cell.moreInfoStack.isHidden = true
                //cell.moreInformation.numberOfLines = 3
                cell.moreInfoHeight.constant = 60
            }else {
                //  cell.moreButton.setTitle("Show Less", for: .normal)
                cell.isLabelAtMaxHeight = true
                cell.moreInfoStack.isHidden = false
               // cell.moreInformation.numberOfLines = 0
              //  cell.bonusDescription.numberOfLines = 0
               
                cell.moreInfoHeight.constant = cell.getLabelHeight(text: cell.bonusDescription.text ?? "", width: cell.bonusDescription.frame.width, font: cell.bonusDescription.font) + cell.getLabelHeight(text: cell.moreInformation.text ?? "", width: cell.moreInformation.frame.width, font: cell.moreInformation.font)
            }
           
            tableView.beginUpdates()
            tableView.endUpdates()
        }
        
        return cell
    }
    
    func showPopup(title: String, msg: String) {
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.confirmationText = "Are you sure?\nThis will remove the current selection."
        myVC.isConfirmationViewHidden = false
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
    }
}


class BonusTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var bonusName: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var bonusDescription: UILabel!
    @IBOutlet weak var mainView: View!
    @IBOutlet weak var moreView: UIView!
    @IBOutlet weak var moreInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var moreInfoStack: UIStackView!
    @IBOutlet weak var moreInformation: UILabel!
    @IBOutlet weak var selectButton: CustomButton!
    
    
    var applyButton: (()-> Void)? = nil
    var moreBtn: (()-> Void)? = nil
    var isLabelAtMaxHeight = false
    
    //MARK: IBAction
    @IBAction func applyBonus(_ sender: Any) {
        if let applyButton = applyButton {
            applyButton()
        }
    }
    
    
    @IBAction func moreAction(_ sender: Any) {
        if let moreBtn = moreBtn {
            moreBtn()
        }
    }
    
    func getLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let lbl = UILabel(frame: .zero)
        lbl.frame.size.width = width
        lbl.font = font
        lbl.numberOfLines = 0
        lbl.text = text
        lbl.sizeToFit()
        return lbl.frame.size.height + 10
    }
}
