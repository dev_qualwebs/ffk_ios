//
//  AddCardViewController.swift
//  Farmers
//
//  Created by qw on 12/02/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import Foundation
import SkyFloatingLabelTextField


protocol AddCard {
    func addCard()
    func handleGuestCard(nonce: String?, zip: String?, street: String?)
}

class AddCardViewController: UIViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var cardName: SkyFloatingLabelTextField!
    @IBOutlet weak var cardNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var expiryMonth: SkyFloatingLabelTextField!
    @IBOutlet weak var cvvText: SkyFloatingLabelTextField!
    @IBOutlet weak var expiryYear: SkyFloatingLabelTextField!
    @IBOutlet weak var postalCodeView: View!
    @IBOutlet weak var postalCode: UITextField!
    
    @IBOutlet weak var cardNumberView: View!
    @IBOutlet weak var cardNameView: View!
    @IBOutlet weak var expiryMonthView: View!
    @IBOutlet weak var expiryYearView: View!
    @IBOutlet weak var cardCVVView: View!
    @IBOutlet weak var addressView: View!
    @IBOutlet weak var addressField: UITextField!
    
    
    @IBOutlet weak var visaImage: UIImageView!
    @IBOutlet weak var visaTickImage: UIImageView!
    @IBOutlet weak var cardTypeButton: UIButton!
    
    @IBOutlet weak var tickImage: ImageView!
    
    var cardDelegate: AddCard? = nil
    var isCardInvalid = false
    var errorCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = primaryColor
        self.cardName.becomeFirstResponder()
        self.cardNumber.delegate = self
        self.cardName.delegate = self
        self.expiryMonth.delegate = self
        self.expiryYear.delegate = self
        self.postalCode.delegate = self
        self.cvvText.delegate = self
        self.addressField.delegate = self
        //        self.cardNumberView.layer.borderWidth = 1
        //        self.cardNameView.layer.borderWidth = 1
        //        self.expiryMonthView.layer.borderWidth = 1
        //        self.expiryYearView.layer.borderWidth = 1
        //        self.cardCVVView.layer.borderWidth = 1
        self.tickImage.image = #imageLiteral(resourceName: "check-square")
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
      //  swipeRight.direction = .right
        // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        
    }
    
    //MARK: IBActions
    @IBAction func addCard(_ sender: Any) {
        //        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeViewController") as! TimeViewController
        //        myVC.modalPresentationStyle = .overFullScreen
        //        myVC.timeDelegate = self
        //        myVC.isCardInfo = true
        //        myVC.timeData = ["Visa", "Master Card"]
        //
        //    self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        self.errorCount = 0
        if(self.cardNumber.text!.isEmpty){
            self.errorCount += 1
            self.cardNumberView.layer.borderColor = primaryColor.cgColor
        }
        if(self.isCardInvalid){
            self.errorCount += 1
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            self.cardNumberView.layer.borderColor = primaryColor.cgColor
            myVC.heading = "Entered card details are invalid"
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }
        if(self.cardName.text!.isEmpty){
            self.errorCount += 1
            self.cardNameView.layer.borderColor = primaryColor.cgColor
        }
        if(self.expiryMonth.text!.isEmpty){
            self.errorCount += 1
            self.expiryMonthView.layer.borderColor = primaryColor.cgColor
        }
        if(self.expiryYear.text!.isEmpty){
            self.errorCount += 1
            self.expiryYearView.layer.borderColor = primaryColor.cgColor
        }
        if(self.expiryYear.text!.count < 2){
            self.errorCount += 1
            self.expiryYearView.layer.borderColor = primaryColor.cgColor
        }
        if(self.cvvText.text!.isEmpty){
            self.errorCount += 1
            self.cardCVVView.layer.borderColor = primaryColor.cgColor
        }
        if(self.cvvText.text!.count < 3){
            self.errorCount += 1
            self.cardCVVView.layer.borderColor = primaryColor.cgColor
        }
        if(self.postalCode.text!.isEmpty){
            self.errorCount += 1
            self.postalCodeView.layer.borderColor = primaryColor.cgColor
        }
        if(self.postalCode.text!.count < 3){
            self.errorCount += 1
            self.postalCodeView.layer.borderColor = primaryColor.cgColor
        }
        
        if(self.addressField.text!.isEmpty){
            self.errorCount += 1
            self.addressView.layer.borderColor = primaryColor.cgColor
        }
        
        if(self.errorCount > 0){
            return
        }else {
            ActivityIndicator.show(view: self.view)
            let cardNumber = self.cardNumber.text?.replacingOccurrences(of: "  ", with: "")
            let year = (self.expiryYear.text ?? "").suffix(2)
           
            let param = [
                "xKey": U_CARDKNOX_KEY,
                 "xVersion": U_CARDKNOX_VERSION,
                 "xSoftwareName": U_CARDKNOX_SOFTWARE_NAME,
                 "xSoftwareVersion": U_CARDKNOX_SOFTWARE_VERSION,
                 "xCommand": "cc:Save",
                "xCardNum": cardNumber,
                "xCVV": self.cvvText.text ?? "",
                 "xExp": "\(self.expiryMonth.text ?? "")\(year)",
                "xZip": self.postalCode.text ?? "",
                "xStreet": self.addressField.text ?? ""
                ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_CARDKNOX_URL, method: .post, parameter: param, objectClass: CarKnoxResponse.self, requestCode: U_CARDKNOX_URL, userToken: nil) { (response) in
                if(response.xToken ?? "" == ""){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    self.cardNumberView.layer.borderColor = primaryColor.cgColor
                    myVC.heading = response.xError ?? "Permission denied"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }else if(Singleton.shared.userDetail.id == nil && (Singleton.shared.selectedLocation.payment_gateway == "cardknox")){
                    self.cardDelegate?.handleGuestCard(nonce: response.xToken ?? "", zip: self.postalCode.text ?? "", street: self.addressField.text ?? "")
                }else {
                    self.callAddCardAPI(token: response.xToken ?? "", xCardType: response.xCardType ?? "")
                }
            }
        }
    }
    
    func callAddCardAPI(token: String, xCardType: String) {
        let param = [
            "token": token,
            "expiry_month": self.expiryMonth.text ?? "",
            "expiry_year": (self.expiryYear.text ?? ""),
            "masked_card_number": (self.cardNumber.text ?? "").suffix(4),
            "card_type":xCardType,
            "zip_code": self.postalCode.text ?? "",
            "street_address": self.addressField.text ?? ""
            ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CARDKNOX_ADD_CARD, method: .post, parameter: param, objectClass: CarKnoxResponse.self, requestCode: U_CARDKNOX_ADD_CARD, userToken: nil) { (response) in
            self.cardDelegate?.addCard()
            ActivityIndicator.hide()
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.heading = "Card Added Successfully"
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == cardNumber){
            self.cardNumberView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cardName){
            self.cardNameView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == expiryMonth){
            self.expiryMonthView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == expiryYear){
            self.expiryYearView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == cvvText){
            self.cardCVVView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == postalCode){
            self.postalCodeView.layer.borderColor = UIColor.lightGray.cgColor
        }else if(textField == addressField){
            self.addressView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == cardNumber){
            self.stringGetCreditCardType(string: self.cardNumber.text!.replacingOccurrences(of: " ", with: ""))
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "####  ####  ####  ####", replacmentCharacter: "#")
            return newLength <= 22
        }else if(textField == expiryMonth) {
            let currentCharacterCount = self.expiryMonth.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 2){
                self.expiryMonth.text! += string
                self.expiryYear.becomeFirstResponder()
            }
            return newLength <= 2
        }else if(textField == expiryYear) {
            let currentCharacterCount = self.expiryYear.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 4){
                self.expiryYear.text! += string
                self.cvvText.becomeFirstResponder()
            }
            return newLength <= 4
        }else if(textField == cvvText) {
            let currentCharacterCount = self.cvvText.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 3
        }else if(textField == postalCode) {
            let currentCharacterCount = self.postalCode.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 6
        }else {
            return true
        }
    }
    
    func stringGetCreditCardType(string:String) -> String{
        let regVisa = NSPredicate(format:"SELF MATCHES %@", "^4[0-9]{12}(?:[0-9]{3})?$")
        let regMaster = NSPredicate(format:"SELF MATCHES %@", "^5[1-5][0-9]{14}$")
        let regExpress = NSPredicate(format:"SELF MATCHES %@", "^3[47][0-9]{13}$")
        let regDiners = NSPredicate(format:"SELF MATCHES %@", "^3(?:0[0-5]|[68][0-9])[0-9]{11}$")
        let regDiscover = NSPredicate(format:"SELF MATCHES %@", "^6(?:011|5[0-9]{2})[0-9]{12}$")
        let regJCB = NSPredicate(format:"SELF MATCHES %@", "^(?:2131|1800|35\\d{3})\\d{11}$")
        
        if (regVisa.evaluate(with: string)){
            self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = UIImage(named: "visa")
            self.visaTickImage.isHidden = false
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "VISA"
        }else if (regMaster.evaluate(with: string)){
            self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = UIImage(named: "mastercard")
            self.visaTickImage.isHidden = false
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "MASTER"
        }else  if (regExpress.evaluate(with: string)){
            self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = #imageLiteral(resourceName: "american-express")
            self.visaTickImage.isHidden = false
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "AEXPRESS"
        }else if (regDiners.evaluate(with: string)){
            self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = #imageLiteral(resourceName: "dinners-club")
            self.visaTickImage.isHidden = false
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "DINERS"
        }else if (regDiscover.evaluate(with: string)){
            self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = #imageLiteral(resourceName: "discover")
            self.visaTickImage.isHidden = false
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "DISCOVERS"
        }else if (regJCB.evaluate(with: string)){
            self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = #imageLiteral(resourceName: "jcb")
            self.visaTickImage.isHidden = false
            self.visaImage.isHidden = false
            isCardInvalid = false
            return "JCB"
        }else{
            //self.cardTypeButton.setTitle("", for: .normal)
            self.visaImage.image = nil
            self.visaTickImage.isHidden = true
            self.visaImage.isHidden = true
            isCardInvalid = true
            return "invalid"
        }
    }
    
    //MARK: IBActions
    @IBAction func tickAction(_ sender: Any) {
        if(tickImage.image == #imageLiteral(resourceName: "Rectangle 1")){
            tickImage.image = #imageLiteral(resourceName: "check-square")
        }else {
            tickImage.image = #imageLiteral(resourceName: "Rectangle 1")
        }
    }
}
