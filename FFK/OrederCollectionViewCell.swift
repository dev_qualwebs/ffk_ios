//
//  OrederCollectionViewCell.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import TransitionButton

class OrederCollectionViewCell: UICollectionViewCell {
    //MARK: IBOutlets
    
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var dishPrice: UILabel!
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var checkImage: ImageView!
    @IBOutlet weak var indicatorButton: TransitionButton!
    
    var isTick:(() -> Void)? = nil
    
    
    @IBAction func tickAction(_ sender: Any) {
        if let isTick = isTick {
            isTick()
        }
    }
    
    
}
