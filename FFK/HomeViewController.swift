//
//  HomeViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Alamofire

protocol ResetData{
    func refreshMenuData()
}

class HomeViewController: UIViewController, BreakfastData, LunchData {
    func breakfastDataDelegate(message: String, menu: String) {
        // self.timeObserver(message: message, menu: menu)
    }
    
    func lunchDataDelegate(message: String, menu: String) {
        // self.timeObserver(message: message, menu: menu)
    }
    
    
    //Mark:IBOutlets
    @IBOutlet weak var lblBreakfast: UILabel!
    @IBOutlet weak var lblLunch: UILabel!
    @IBOutlet weak var lblBreakfastTime: UILabel!
    @IBOutlet weak var lblLunchTime: UILabel!
    @IBOutlet weak var viewBreakfast: UIView!
    @IBOutlet weak var viewLunch: UIView!
    @IBOutlet weak var timeCollectionView: UICollectionView!
    @IBOutlet weak var breakfastImg: UIImageView!
    @IBOutlet weak var notAvailableLabel: UILabel!
    @IBOutlet weak var timeCollectionMainView: UIView!
    
    @IBOutlet weak var notAvailable: UIView!
    @IBOutlet weak var lunchImg: UIImageView!
    
    //MARK:Properties
    var breakFastTime:[String] = []
    var lunchTime:[String] = []
    var tableData: [String] = []
    var shortBreakFastTime:[String] = []
    var shortLunchTime:[String] = []
    var timeInterval = "breakfast"
    var currentTime:Int? = nil
    var previousTime:Int? = nil
    var items = [GetMenuResponse]()
    var arrDateBreakfast = [Date]()
    var arrDateLunch = [Date]()
    var dateBreakfast = Date()
    var dateLunch = Date()
    var menuApiCalled = false
    var timeMessage = ""
    var isMenuLoadingFirstTime = true
    var isFirstTime = true
    static var menuDelegate: ResetData? = nil
    
    //    var initialTouchPoint = CGPoint(x: 0, y: 0)
    //    var swipe: UIPanGestureRecognizer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BreaFastViewController.breakfastDelegate = self
        LunchViewController.lunchDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeBackColor(_:)), name: NSNotification.Name(N_LUNCH_TAB_COLOR), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeBackColor(_:)), name: NSNotification.Name(N_BF_TAB_COLOR), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTimer(_:)), name: NSNotification.Name(N_TIME_OBSERVER), object: nil)
        RealtimeFirebase.shared.checkRestaurantStatus { val in
            self.getMenuTimings()
        }
    }
    
    override func viewDidAppear(_ animated: Bool){
        currentPageView = K_MENU_PAGE_VIEW
        if(Singleton.shared.seletedTime == "" || Singleton.shared.seletedTime == nil){
            self.timeCollectionView.reloadData()
        }
        if(Singleton.shared.mainMenuData.count > 0){
            self.items = Singleton.shared.mainMenuData
            self.setHeading()
            if(self.isFirstTime){
                self.isFirstTime = false
                self.getMenuType()
            }
        }else {
            if(self.menuApiCalled == false){
                self.menuApiCalled = true
                getMenuType()
            }
        }
    }
    
    
    @objc func updateTimer(_ notif: Notification){
        self.timeObserver(message: Singleton.shared.breakfastTimeMessage, menu: "break")
        self.timeObserver(message: Singleton.shared.lunchTimeMessage, menu: "lunch")
    }
    
    func getMenuType(){
        //ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU_TYPE +  "\(Singleton.shared.selectedLocation.id ?? 1)", method: .get, parameter: nil, objectClass: GetMenu.self, requestCode: U_GET_MENU_TYPE, userToken: nil) { (dataResponse) in
            self.items = dataResponse.response
            self.menuApiCalled = false
            Singleton.shared.mainMenuData = dataResponse.response
            let data = try! JSONEncoder().encode(Singleton.shared.mainMenuData)
            UserDefaults.standard.set(data, forKey: UD_MENU_TYPE)
            if(self.items.count > 1){
                K_BREAK_ID = self.items[0].menu_id ?? 0
                MENU_TYPE = K_BREAK_ID
                K_LUNCH_ID = self.items[1].menu_id ?? 0
                HomeViewController.menuDelegate?.refreshMenuData()
                // NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_BREAK_DATA)))
                // NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_LUNCH_DATA)))
                self.setHeading()
            }
            
            // ActivityIndicator.hide()
            // self.breakfastAction(self)
        }
        
    }
    
    func setHeading()
    {
        if self.items.count > 0{
            lblBreakfast.text = self.items[0].menu_name
            lblLunch.text = self.items[1].menu_name
            lblBreakfastTime.text = "\(self.convertTimestampToDate(self.items[0].from ?? 0, to: "h:mm a").lowercased())" + " - " + "\(self.convertTimestampToDate(self.items[0].to ?? 0, to: "h:mm a").lowercased())"
            lblLunchTime.text = "\(self.convertTimestampToDate(self.items[1].from ?? 0, to: "h:mm a").lowercased())" + " - " + "\(self.convertTimestampToDate(self.items[1].to ?? 0, to: "h:mm a").lowercased())"
            
            breakFastTime.append("\(self.convertTimestampToDate(self.items[0].from ?? 0, to: "h:mm a").lowercased())")
            lunchTime.append("\(self.convertTimestampToDate(self.items[1].from ?? 0, to: "h:mm a").lowercased())")
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                if(MENU_TYPE == K_BREAK_ID){
                    if((Singleton.shared.breakfastTimeMessage != "") && ((self.items[1].to ?? 0) > Int(Date().timeIntervalSince1970)) && self.isMenuLoadingFirstTime){
                        self.isMenuLoadingFirstTime = false
                        self.lunchAction(self)
                    }else {
                        self.breakfastAction(self)
                    }
                }
            }
        }else{
            if(MENU_TYPE != 0){
                self.getMenuTimings()
            }
            // showAlert(title: "", message: "Data Not Found", action1Name: "OK", action2Name: "")
        }
    }
    
    func getMenuTimings(){
        Singleton.shared.breakfastTimeMessage = ""
        Singleton.shared.lunchTimeMessage = ""
        
        self.view.isUserInteractionEnabled = true
        if(MENU_TYPE == 0 || Singleton.shared.restaurentLocation.count == 0){
            return
        }
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_VALIDATE_MENU_TIMING  + "\(MENU_TYPE)/\(Singleton.shared.selectedLocation.id ?? Singleton.shared.restaurentLocation[0].id ?? 0)", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_VALIDATE_MENU_TIMING, userToken: nil) { response in
            self.view.isUserInteractionEnabled = true
            if(Singleton.shared.restaurentLocation.count > 0){
                if(Singleton.shared.selectedLocation.id == Singleton.shared.restaurentLocation[0].id){
                    if(K_KITCHEN_ONLINE_ONE == "1"){
                        if(MENU_TYPE == K_BREAK_ID){
                            self.timeObserver(message: Singleton.shared.breakfastTimeMessage, menu: "break")
                            
                        }else {
                                Singleton.shared.lunchTimeMessage = Singleton.shared.lunchTimeMessage
                                self.timeObserver(message: Singleton.shared.lunchTimeMessage, menu: "lunch")
                        }
                    }else {
                        self.timeObserver(message: Singleton.shared.lunchTimeMessage, menu: "lunch")
                        self.timeObserver(message: Singleton.shared.breakfastTimeMessage, menu: "break")
                    }
                }else {
                    if(K_KITCHEN_ONLINE_TWO == "1"){
                        if(MENU_TYPE == K_BREAK_ID){
                            self.timeObserver(message: Singleton.shared.breakfastTimeMessage, menu: "break")
                        }else {
                                self.timeObserver(message: Singleton.shared.lunchTimeMessage, menu: "lunch")
                        }
                    }else {
                        self.timeObserver(message: Singleton.shared.lunchTimeMessage, menu: "lunch")
                        self.timeObserver(message: Singleton.shared.breakfastTimeMessage, menu: "break")
                    }
                }
                
            }
        }
    }
    
    func breakfastTimeInterval(){
        if(self.items.count > 0){
            let startTime = Date(timeIntervalSince1970: TimeInterval(self.items[0].from ?? 0))
            let endTime = Date(timeIntervalSince1970: TimeInterval(self.items[0].to ?? 0))
            // endTime = endTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
            self.arrDateBreakfast = []
            self.handleBreakfastTime(startTime: startTime, endTime: endTime)
            var timeStamp:String?
            self.breakFastTime = []
            var count = 0
            self.tableData = []
            self.shortBreakFastTime = []
            for i in 0...arrDateBreakfast.count-1
            {
                if !(arrDateBreakfast[i] > Calendar.current.date(byAdding: .minute, value: 15, to: Date())!){
                    continue
                }
                let date = Int(arrDateBreakfast[i].timeIntervalSince1970)
                if(date > Int(Date().timeIntervalSince1970)){
                    count = count + 1
                    timeStamp = self.convertTimestampToDate(date, to: "h:mm a")
                    if(count == 1){
                        shortBreakFastTime.append(timeStamp ?? "" + " (quickest)")
                    }else if(count <= 4){
                        shortBreakFastTime.append(timeStamp ?? "")
                    }else if(count == 5){
                        shortBreakFastTime.append("more options")
                    }
                    breakFastTime.append(timeStamp ?? "")
                }
            }
            if(self.breakFastTime.count < 5){
                self.tableData = self.breakFastTime
                
            }else {
                self.tableData = self.shortBreakFastTime
            }
            Singleton.shared.selectedTimeArr = self.tableData
            
            self.timeCollectionView.reloadData()
        }
    }
    
    func timeObserver(message: String , menu: String){
        // self.notAvailableLabel.text = ""
        // if let msg = notif.object as? [String:Any]{
        if(message != ""){
            if((menu == "break") && (MENU_TYPE == K_BREAK_ID)){
                self.notAvailableLabel.text = message
                self.timeCollectionMainView.isHidden = true
                self.notAvailable.isHidden = false
            }else if((menu == "lunch") && (MENU_TYPE == K_LUNCH_ID)){
                self.notAvailableLabel.text = message
                self.timeCollectionMainView.isHidden = true
                self.notAvailable.isHidden = false
            }else if(Singleton.shared.lunchTimeMessage != "" && Singleton.shared.breakfastTimeMessage != ""){
                if(MENU_TYPE == K_LUNCH_ID){
                    self.notAvailableLabel.text = Singleton.shared.lunchTimeMessage
                    self.timeCollectionMainView.isHidden = true
                    self.notAvailable.isHidden = false
                }else if (MENU_TYPE == K_BREAK_ID) {
                    self.notAvailableLabel.text = Singleton.shared.breakfastTimeMessage
                    self.timeCollectionMainView.isHidden = true
                    self.notAvailable.isHidden = false
                }
            }
        }else {
            self.notAvailable.isHidden = true
            self.timeCollectionMainView.isHidden = false
            if(MENU_TYPE == K_BREAK_ID){
                if(self.items.count > 0){
                    if(((self.items[0].to ?? 0) < Int(Date().timeIntervalSince1970)) && ((self.items[1].to ?? 0) > Int(Date().timeIntervalSince1970)) && self.isMenuLoadingFirstTime){
                        self.lunchAction(self)
                        self.isMenuLoadingFirstTime = false
                    }else{
                        self.breakfastTimeInterval()
                    }
                }else {
                    self.breakfastTimeInterval()
                }
            }else {
                self.lunchTimeInterval()
            }
        }
        //    }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            if(MENU_TYPE == K_BREAK_ID){
                self.breakfastImg.image = #imageLiteral(resourceName: "Group 1 (1)")
                self.lunchImg.image = #imageLiteral(resourceName: "☼")
                self.viewBreakfast.backgroundColor = primaryColor
                self.viewLunch.backgroundColor = .white
                self.lblBreakfast.textColor = .white
                self.lblLunch.textColor = primaryColor
                self.lblLunchTime.textColor = primaryColor
                self.lblBreakfastTime.textColor = .white
            }else{
                self.breakfastImg.image = #imageLiteral(resourceName: "Group 1")
                self.lunchImg.image = #imageLiteral(resourceName: "☼ (1)")
                self.viewBreakfast.backgroundColor = .white
                self.viewLunch.backgroundColor = primaryColor
                self.lblBreakfast.textColor = primaryColor
                self.lblLunch.textColor = .white
                self.lblBreakfastTime.textColor = primaryColor
                self.lblLunchTime.textColor = .white
            }
        }
    }
    
    func handleBreakfastTime(startTime:Date, endTime:Date)
    {
        dateBreakfast = startTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
        arrDateBreakfast.append(dateBreakfast)
        if(dateBreakfast < endTime){
            handleBreakfastTime(startTime: dateBreakfast, endTime: endTime)
        }
        else{
            return
        }
    }
    
    func handleLunchTime(startTime:Date, endTime:Date){
        dateLunch = startTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
        arrDateLunch.append(dateLunch)
        if(dateLunch < endTime){
            handleLunchTime(startTime: dateLunch, endTime: endTime)
        }else{
            return
        }
    }
    
    func lunchTimeInterval(){
        if(self.items.count > 0){
            let startTime = Date(timeIntervalSince1970: TimeInterval(self.items[1].from ?? 0))
            let endTime = Date(timeIntervalSince1970: TimeInterval(self.items[1].to ?? 0))
            //   endTime = endTime.addingTimeInterval(TimeInterval(15.0 * 60.0))
            self.arrDateLunch = []
            self.handleLunchTime(startTime: startTime,endTime:endTime)
            var timeStamp:String?
            self.lunchTime = []
            self.tableData = []
            self.shortLunchTime = []
            var count = 0
            for i in 0...arrDateLunch.count-1
            {
                if !(arrDateLunch[i] > Calendar.current.date(byAdding: .minute, value: 15, to: Date())!){
                    continue
                }
                let date = Int(arrDateLunch[i].timeIntervalSince1970)
                if(date > Int(Date().timeIntervalSince1970)){
                    count = count + 1
                    timeStamp = self.convertTimestampToDate(date, to: "h:mm a")
                    if(count == 1){
                        shortLunchTime.append(timeStamp ?? "" + " (quickest)")
                    }else if(count <= 4){
                        shortLunchTime.append(timeStamp ?? "")
                    }else if(count == 5){
                        shortLunchTime.append("more options")
                    }
                    lunchTime.append(timeStamp ?? "")
                }
            }
            if(self.lunchTime.count < 5){
                self.tableData = self.lunchTime
            }else {
                self.tableData = self.shortLunchTime
            }
            Singleton.shared.selectedTimeArr = self.tableData
            self.timeCollectionView.reloadData()
        }
        //        if(Singleton.shared.selectedTimeArr.count == 0 && (Singleton.shared.lunchTimeMessage == "")){
        //            Singleton.shared.lunchMenu = []
        //            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_LUNCH_DATA)))
        //        }
    }
    
    @objc func changeBackColor(_ notification:NSNotification) {
        let index = notification.userInfo?["index"] as! Int
        if(index == 1){
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_LUNCH_TAB_COLOR), object: nil)
            self.viewBreakfast.backgroundColor = .white
            self.viewLunch.backgroundColor = backgroundColor
            lunchTimeInterval()
            timeInterval = "lunch"
            self.timeCollectionView.reloadData()
            NotificationCenter.default.addObserver(self, selector: #selector(self.changeBackColor(_:)), name: NSNotification.Name(N_LUNCH_TAB_COLOR), object: nil)
        }else {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_BF_TAB_COLOR), object: nil)
            self.viewBreakfast.backgroundColor = backgroundColor
            self.viewLunch.backgroundColor = .white
            breakfastTimeInterval()
            timeInterval = "breakfast"
            self.timeCollectionView.reloadData()
            NotificationCenter.default.addObserver(self, selector: #selector(self.changeBackColor(_:)), name: NSNotification.Name(N_BF_TAB_COLOR), object: nil)
        }
    }
    
    @IBAction func breakfastAction(_ sender: Any) {
        if(self.items.count == 0){
            self.getMenuType()
        }else {
            self.breakfastImg.image = #imageLiteral(resourceName: "Group 1 (1)")
            self.lunchImg.image = #imageLiteral(resourceName: "☼")
            self.viewBreakfast.backgroundColor = primaryColor
            self.viewLunch.backgroundColor = .white
            lblBreakfast.textColor = .white
            lblLunch.textColor = primaryColor
            lblLunchTime.textColor = primaryColor
            lblBreakfastTime.textColor = .white
            
            let pageViewController = PageViewController.customDataSource as! PageViewController
            MENU_TYPE = K_BREAK_ID//self.items[1].menu_id!
            pageViewController.setFirstController()
            // breakfastTimeInterval()
            self.getMenuTimings()
            timeInterval = "breakfast"
            self.timeCollectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
    }
    
    @IBAction func lunchAction(_ sender: Any) {
        if(self.items.count == 0){
            self.getMenuType()
        }else {
            self.breakfastImg.image = #imageLiteral(resourceName: "Group 1")
            self.lunchImg.image = #imageLiteral(resourceName: "☼ (1)")
            self.viewBreakfast.backgroundColor = .white
            self.viewLunch.backgroundColor = primaryColor
            lblBreakfast.textColor = primaryColor
            lblLunch.textColor = .white
            lblBreakfastTime.textColor = primaryColor
            lblLunchTime.textColor = .white
            let pageViewController = PageViewController.customDataSource as! PageViewController
            MENU_TYPE = K_LUNCH_ID//self.items[0].menu_id!
            pageViewController.setSecondController()
            //lunchTimeInterval()
            self.getMenuTimings()
            timeInterval = "lunch"
            self.timeCollectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
    }
}

extension HomeViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TimeProtocol {
    func selectPickupTime(current: Int, previous: Int, selectedTime: String) {
        self.currentTime = 3
        if(self.timeInterval == "lunch"){
            self.shortLunchTime[3] = selectedTime
            self.tableData = self.shortLunchTime
        }else {
            self.shortBreakFastTime[3] = selectedTime
            self.tableData = self.shortBreakFastTime
        }
        
        self.timeCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        // if(timeMessage == ""){
        if(self.tableData.count == 0){
            if(self.notAvailableLabel.text != ""){
                self.notAvailable.isHidden = false
                self.timeCollectionMainView.isHidden = true
            }else {
                self.notAvailable.isHidden = true
                self.timeCollectionMainView.isHidden = false
            }
        }else if(self.notAvailableLabel.text != ""){
            if(K_LUNCH_ID == MENU_TYPE && Singleton.shared.lunchTimeMessage == ""){
                self.notAvailable.isHidden = true
                self.timeCollectionMainView.isHidden = false
            }else if(K_BREAK_ID == MENU_TYPE && Singleton.shared.breakfastTimeMessage == ""){
                self.notAvailable.isHidden = true
                self.timeCollectionMainView.isHidden = false
            }else {
                self.notAvailable.isHidden = false
                self.timeCollectionMainView.isHidden = true
            }
        }else{
            self.notAvailable.isHidden = true
            self.timeCollectionMainView.isHidden = false
        }
        //        }else {
        //           self.notAvailableLabel.text = self.timeMessage
        //           self.notAvailable.isHidden = false
        //           self.timeCollectionView.isHidden = true
        //        }
        return tableData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCollectionViewCell", for: indexPath) as! TimeCollectionViewCell
        cell.timeLabel.text = self.tableData[indexPath.row].lowercased()
        if(cell.timeLabel.text == Singleton.shared.seletedTime || (self.currentTime == indexPath.row)){
            cell.timeView.backgroundColor = primaryColor
            cell.timeLabel.textColor = .white
            self.currentTime = indexPath.row
            Singleton.shared.seletedTime = cell.timeLabel.text
        }else{
            cell.timeView.backgroundColor = .white
            cell.timeLabel.textColor = primaryColor
        }
        
        cell.time = {
            if(cell.timeLabel.text == "more options"){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeViewController") as! TimeViewController
                if(self.timeInterval == "lunch"){
                    if(self.lunchTime.count > 5){
                        myVC.timeData = Array(self.lunchTime[4..<self.lunchTime.count])
                    }else {
                        myVC.timeData = Array(self.lunchTime)
                    }
                    
                }else {
                    if(self.breakFastTime.count > 5){
                        myVC.timeData = Array(self.breakFastTime[4..<self.breakFastTime.count])
                    }else {
                        myVC.timeData = Array(self.breakFastTime)
                    }
                }
                if(self.currentTime != nil){
                    myVC.previousTime = self.currentTime!
                }
                myVC.currentTime = indexPath.row
                myVC.timeDelegate = self
                myVC.modalPresentationStyle = .overFullScreen
                self.present(myVC, animated: false, completion: nil)
                return
            }
            if(cell.timeView.backgroundColor == primaryColor){
                cell.timeView.backgroundColor = .white
                cell.timeLabel.textColor = primaryColor
            }else {
                cell.timeLabel.textColor = .white
                cell.timeView.backgroundColor = primaryColor
                Singleton.shared.seletedTime = cell.timeLabel.text
                self.previousTime = self.currentTime
                self.currentTime = indexPath.row
            }
            if(self.previousTime != nil){
                self.timeCollectionView.reloadItems(at: [IndexPath(row: self.previousTime!, section: 0)])
            }
            self.timeCollectionView.reloadItems(at: [IndexPath(row: self.currentTime!, section: 0)])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let label = UILabel(frame: CGRect.zero)
        label.text = self.tableData[indexPath.item]
        label.sizeToFit()
        
        return CGSize(width:label.frame.width + 30, height: 50)
    }
}

