
import UIKit

var isNavigateToWelcomScreen = true

class WelcomePageViewController: UIPageViewController, UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//         scrollView.isScrollEnabled = true
//         if(currentIndex == 1 && scrollView.contentOffset.y > self.view.frame.height-6){
//             scrollView.contentOffset.y = self.view.frame.height
//         }
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//         if(currentIndex == 0 && scrollView.contentOffset.y <= self.view.frame.size.height){
//             UIView.performWithoutAnimation {
//                 scrollView.contentOffset.y = self.view.frame.size.height
//             }
//         }else if(currentIndex == 1 && scrollView.contentOffset.y > self.view.frame.size.height){
//
//             UIView.performWithoutAnimation {
//                 scrollView.contentOffset.y = self.view.frame.size.height
//             }
//         }

       // if(currentIndex == 1){
         //   NotificationCenter.default.post(name: NSNotification.Name("handle_scroll"), object: nil, userInfo: ["yPosition": CGFloat(200)])
          //  NotificationCenter.default.post(name: NSNotification.Name("handle_lunch_scroll"), object: nil, userInfo: ["yPosition": CGFloat(200)])
       // }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//         if(currentIndex == 0 && scrollView.contentOffset.y >= self.view.frame.size.height){
//             UIView.performWithoutAnimation {
//                 scrollView.contentOffset.y = self.view.frame.size.height
//             }
//         }else if(currentIndex == 1 && scrollView.contentOffset.y < self.view.frame.size.height){
//             UIView.performWithoutAnimation {
//                 scrollView.contentOffset.y = self.view.frame.size.height
//             }
//         }
    }

    //MARK: Properties
    var pageControl: UIPageControl?
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    lazy var viewControllerList = [UIViewController]()
    var sb = UIStoryboard(name:"Main",bundle:nil)
    var currentIndex = 0
    private var isFirstTimeScroll = true



    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        WelcomePageViewController.customDataSource = self
        viewControllerList = [                              sb.instantiateViewController(withIdentifier: "WelcomeViewController" ),
                                                            sb.instantiateViewController(withIdentifier: "TabbarViewController")]
        if let firstViewController = viewControllerList.first {
            if let val =  UserDefaults.standard.value(forKey: UD_LAUNCH_FIRST_TIME) as? String{
                if let activeOrder = UserDefaults.standard.value(forKey: UD_ACTIVE_ORDER_EXIST) as? Data, isNavigateToWelcomScreen{
                    let object = try? JSONDecoder().decode([OrderResponse].self, from: activeOrder)
                    UserDefaults.standard.setValue("false", forKey: UD_LAUNCH_FIRST_TIME)
                    self.setViewControllers([viewControllerList[0]],direction: .forward,
                                            animated:false,completion:nil)
                    isNavigateToWelcomScreen = false
                    currentIndex = 0
                    return
                }else {
                    if(val == "true"){
                        UserDefaults.standard.setValue("false", forKey: UD_LAUNCH_FIRST_TIME)
                        self.setViewControllers([viewControllerList[0]],direction: .forward,
                                                animated:false,completion:nil)
                        isNavigateToWelcomScreen = false
                        currentIndex = 0
                        return
                    }else{
                        UserDefaults.standard.setValue("false", forKey: UD_LAUNCH_FIRST_TIME)
                        self.setViewControllers([viewControllerList[1]],direction: .forward,
                                                animated:false,completion:nil)
                        isNavigateToWelcomScreen = false

                    }
                    currentIndex = 1
                }
            }else {

                UserDefaults.standard.setValue("false", forKey: UD_LAUNCH_FIRST_TIME)
                self.setViewControllers([viewControllerList[0]],direction: .forward,
                                        animated:false,completion:nil)
                self.currentIndex = 0
            }
        }
        for view in view.subviews {
            if view is UIScrollView {
                (view as! UIScrollView).isScrollEnabled = false
                // (view as! UIScrollView).bouncesZoom = false
                view.translatesAutoresizingMaskIntoConstraints = false
                (view as! UIScrollView).delegate =  self
            }
        }

    }


    func setFirstController() {
        setViewControllers([viewControllerList[0]], direction: .reverse, animated: true) { val in
            self.currentIndex = 0
            self.pageControl?.currentPage = 0
        }
    }

    func setSecondController() {
        //self.currentIndex = 1
        setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)

        //self.pageControl?.currentPage = 1
    }

    func setSecondController2() {
        self.currentIndex = 1
        setViewControllers([viewControllerList[1]], direction: .forward, animated: false, completion: nil)
        self.pageControl?.currentPage = 1

    }


    func setHomeController() {
        setViewControllers([viewControllerList[1]], direction: .reverse, animated: true, completion: nil)
        //self.pageControl?.currentPage = 1
    }

    func setThirdController() {
        setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
        //self.pageControl?.currentPage = 2
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .vertical, options: nil)
    }
}

extension WelcomePageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
        else{return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        return viewControllerList[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
        else{return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
        return viewControllerList[nextIndex]
    }
}

extension WelcomePageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        pageContentViewController.modalTransitionStyle = .crossDissolve
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
        if(viewControllerList.index(of: pageContentViewController)! == 1){
            NotificationCenter.default.post(name: NSNotification.Name(N_LUNCH_DATA), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_BREAK_DATA), object: nil)
        }
        if completed {
            let pageContentViewController = pageViewController.viewControllers![0]
            currentIndex = viewControllerList.index(of: pageContentViewController)!
        }
    }



    override func beginAppearanceTransition(_ isAppearing: Bool, animated: Bool) {

    }


    override func transition(from fromViewController: UIViewController, to toViewController: UIViewController, duration: TimeInterval, options: UIView.AnimationOptions = [], animations: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
        fromViewController.view.alpha = 0.5
        toViewController.view.alpha = 1
    }
}


