//
//  TimeCollectionViewCell.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class TimeCollectionViewCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeView: View!
    
    
    var time:(() -> Void)? = nil
    
    @IBAction func selectTime(_ sender: Any) {
        if let time = time {
            time()
        }
    }
    
    
}
