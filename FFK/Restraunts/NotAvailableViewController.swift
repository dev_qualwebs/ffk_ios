//
//  NotAvailableViewController.swift
//  FFK
//
//  Created by qw on 13/01/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class NotAvailableViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var titleText: UILabel!
    
    var heading = "Oops. Lunch and breakfast can't be added to the same order."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText.text = heading
        NotificationCenter.default.post(name: NSNotification.Name(N_RESET_PAY_BUTTON), object: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
          self.dismiss(animated: false, completion: nil)
        }
        
    }
    //MARK: IBActions
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
