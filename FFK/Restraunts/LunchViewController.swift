//
//  LunchViewController.swift
//  FFK
//
//  Created by AM on 28/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SDWebImage

protocol LunchData{
    func lunchDataDelegate(message: String, menu: String)
}

class LunchViewController: UIViewController, Confirmation, ResetData, UIScrollViewDelegate{
    func refreshMenuData() {
           MENU_TYPE = K_LUNCH_ID
                      if(Singleton.shared.lunchMenu.count == 0  && MENU_TYPE != 0){
                          getMenu()
                      }else{
                          if(self.lunchMenu.count == 0 && MENU_TYPE != 0){
                              self.lunchMenu = Singleton.shared.lunchMenu
                              self.restaurentsTable.reloadData()
                              self.restaurentsTable.scrollsToTop = true
                          }
                      }
    }
    
    func confirmationSelection() {
        let indexPath = self.selectedIndexPath
        NavigationController.shared.emptyMyCart()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FooItemViewController") as! FooItemViewController
        myVC.timeMessage = self.timeMessage
        myVC.navTitle = self.lunchMenu[indexPath.section].menus[indexPath.row].item_name ?? ""
        myVC.isCommon = self.lunchMenu[indexPath.section].menus[indexPath.row].is_common ?? 0
        myVC.itemId = self.lunchMenu[indexPath.section].menus[indexPath.row].item_id ?? 0
        myVC.mainMenuId = self.lunchMenu[indexPath.section].menu_id!
        if(self.lunchMenu[indexPath.section].menus[indexPath.row].modifiers == 0){
            if(Singleton.shared.selectedTimeArr.count == 0 || self.timeMessage != ""){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                let time = self.convertTimestampToDate((Singleton.shared.mainMenuData[1].from ?? 0), to: "h:mm a").lowercased()
                myVC.heading = self.timeMessage != "" ? self.timeMessage: Singleton.shared.lunchTimeMessage
                myVC.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else {
                let fVC = self.storyboard?.instantiateViewController(withIdentifier: "FeaturedRecepieViewController") as! FeaturedRecepieViewController
                fVC.menuId =  myVC.mainMenuId
                fVC.isCommon = self.lunchMenu[indexPath.section].menus[indexPath.row].is_common ?? 0
                fVC.itemId = myVC.itemId
                fVC.heading = self.lunchMenu[indexPath.section].menus[indexPath.row].item_name ?? ""
                fVC.menuData = self.lunchMenu[indexPath.section].menus[indexPath.row]
                self.navigationController?.pushViewController(fVC, animated: true)
            }
        }else {
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var restaurentsTable: UITableView!
    @IBOutlet weak var labelNodata: UILabel!
    
    //MARK: Properties
    var lunchMenu = [GetBreakfastLunch]()
    var timeMessage = ""
    var selectedIndexPath = IndexPath()
    static var lunchDelegate:LunchData? = nil

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        HomeViewController.menuDelegate = self
        self.labelNodata.isHidden = true
        restaurentsTable.delegate = self
        restaurentsTable.estimatedRowHeight = 90.0
        restaurentsTable.rowHeight = UITableView.automaticDimension
        restaurentsTable.tableFooterView = UIView()
        self.lunchMenu = Singleton.shared.lunchMenu
        self.restaurentsTable.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadLunchTable(_:)), name: NSNotification.Name(N_LUNCH_DATA), object: nil)
        let menuType = Singleton.shared.mainMenuData
        if(menuType.count > 0){
            K_LUNCH_ID = menuType[1].menu_id ?? 0
            if(MENU_TYPE == 0){
               MENU_TYPE = K_LUNCH_ID
            }
       //     self.getMenu()
        }
        if #available(iOS 15.0, *) {
            self.restaurentsTable.sectionHeaderTopPadding = 0
        }
        
        RealtimeFirebase.shared.addMenuChangeObserver { val in
            self.timeMessage = ""
       //     self.getMenu()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleScroll(_:)), name: NSNotification.Name("handle_lunch_scroll"), object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        MENU_TYPE = K_LUNCH_ID
        if(Singleton.shared.lunchMenu.count == 0 && MENU_TYPE != 0){
            self.getMenu()
        }else {
            self.timeMessage = Singleton.shared.lunchTimeMessage
            LunchViewController.lunchDelegate?.lunchDataDelegate(message: self.timeMessage, menu: "lunch")
            NotificationCenter.default.post(name: NSNotification.Name("hide_animation_view"), object: nil)
            if(self.lunchMenu.count == 0 && MENU_TYPE != 0){
                self.lunchMenu = Singleton.shared.lunchMenu
                self.restaurentsTable.reloadData()
            }
        }
    
        DispatchQueue.main.async {
            self.restaurentsTable.isScrollEnabled = true
            self.restaurentsTable.isUserInteractionEnabled = true
            self.restaurentsTable.scrollsToTop = true
            self.view.isUserInteractionEnabled = true
        }

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.restaurentsTable.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.restaurentsTable.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    @objc func handleScroll(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("handle_lunch_scroll"), object: nil)
        if let ypos = notif.userInfo?["yPosition"] as? CGFloat {
            self.restaurentsTable.setContentOffset(CGPoint(x: 0, y: self.restaurentsTable.contentOffset.y +  ypos), animated: true)
        }
       // NotificationCenter.default.addObserver(self, selector: #selector(self.handleScroll(_:)), name: NSNotification.Name("handle_lunch_scroll"), object: nil)
    }
    
    @objc func reloadLunchTable(_ notif:Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_LUNCH_DATA), object: nil)
        if let fromAccount = notif.userInfo?["from_account"] as? Bool {
            self.restaurentsTable.setContentOffset(CGPoint.zero, animated:false)
        }else {
            UIView.performWithoutAnimation {
                self.restaurentsTable.isScrollEnabled = true
                self.restaurentsTable.isUserInteractionEnabled = true
                self.restaurentsTable.scrollsToTop = true
                self.view.isUserInteractionEnabled = true
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadLunchTable), name: NSNotification.Name(N_LUNCH_DATA), object: nil)
    }
    
    func getMenu() {
        if(MENU_TYPE == 0){
            return
        }
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU + "\(K_LUNCH_ID)" + "/\(Singleton.shared.selectedLocation.id ?? 1)", method: .get, parameter: nil, objectClass: GetBreakLunchResonse.self, requestCode: "\(MENU_TYPE)", userToken: nil) { (dataResponse) in
            if(dataResponse.message != ""){
              //  Singleton.shared.lunchTimeMessage = dataResponse.message ?? ""
                self.timeMessage = Singleton.shared.lunchTimeMessage 
                if let id = UserDefaults.standard.value(forKey: UD_CATEGORY_TYPE) as? Int {
                    if(self.lunchMenu.count > 0){
                        if(id ==  self.lunchMenu[0].menu_id!){
                            NavigationController.shared.emptyMyCart()
                        }
                    }
                }
                self.view.isUserInteractionEnabled = true
            }
            LunchViewController.lunchDelegate?.lunchDataDelegate(message: dataResponse.message ?? "", menu: "lunch")
            Singleton.shared.lunchMenu = dataResponse.response
            let data = try! JSONEncoder().encode(Singleton.shared.lunchMenu)
            UserDefaults.standard.set(data, forKey: UD_LUNCH_ITEMS)
            self.lunchMenu = Singleton.shared.lunchMenu
            if(self.lunchMenu.count == 0){
                self.labelNodata.isHidden = false
            }else {
                self.labelNodata.isHidden = true
            }
            self.restaurentsTable.reloadData()
            self.restaurentsTable.scrollsToTop = true
            ActivityIndicator.hide()
            
        }
    }
}

extension LunchViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.lunchMenu.count
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.lunchMenu[section].menus.count == 0){
            labelNodata.isHidden = false
        }else {
            
            labelNodata.isHidden = true
        }
        return self.lunchMenu[section].menus.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.lunchMenu[section].category_name
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurentTableViewCell") as! RestaurentTableViewCell
        let data = self.lunchMenu[indexPath.section].menus[indexPath.row]
        cell.nameLabel.text = data.item_name ?? ""
        cell.orderLabel.text = data.item_description
        cell.orderLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
        cell.foodImage.sd_setImage(with: URL(string:U_CUSTOM_SIZE + (data.item_image ?? "")), placeholderImage:nil)
        //if(data.reward_item == 0){
        cell.rateLabel.text = "$" + "\(data.item_price ?? 0)"
        //  }else {
        //    cell.rateLabel.text = ""
        //  }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let type = UserDefaults.standard.value(forKey: UD_CATEGORY_TYPE) as? Int{
            if(type != (self.lunchMenu[0].menu_id ?? 0) && self.lunchMenu[indexPath.section].menus[indexPath.row].is_common == 2){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.confirmationText = "Oops, lunch and breakfast can’t be added to the same order.\nWant to proceed and empty your bag?"
                myVC.isConfirmationViewHidden = false
                myVC.confirmationDelegate = self
                self.selectedIndexPath = indexPath
                self.navigationController?.present(myVC, animated: false, completion: nil)
                return
                
            }else if(type == (self.lunchMenu[0].menu_id ?? 0) || self.lunchMenu[indexPath.section].menus[indexPath.row].is_common == 1) {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FooItemViewController") as! FooItemViewController
                myVC.timeMessage = self.timeMessage
                myVC.isCommon = self.lunchMenu[indexPath.section].menus[indexPath.row].is_common ?? 0
                myVC.navTitle = self.lunchMenu[indexPath.section].menus[indexPath.row].item_name ?? ""
                myVC.itemId = self.lunchMenu[indexPath.section].menus[indexPath.row].item_id ?? 0
                myVC.mainMenuId = self.lunchMenu[indexPath.section].menu_id!
                if(self.lunchMenu[indexPath.section].menus[indexPath.row].modifiers == 0){
                    if(Singleton.shared.selectedTimeArr.count == 0 || self.timeMessage != ""){
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                        let time = self.convertTimestampToDate((Singleton.shared.mainMenuData[1].from ?? 0), to: "h:mm a").lowercased()
                        myVC.heading = self.timeMessage != "" ? self.timeMessage: Singleton.shared.lunchTimeMessage
                        myVC.modalPresentationStyle = .overFullScreen
                        self.navigationController?.present(myVC, animated: false, completion: nil)
                    }else {
                        let fVC = self.storyboard?.instantiateViewController(withIdentifier: "FeaturedRecepieViewController") as! FeaturedRecepieViewController
                        fVC.menuId =  myVC.mainMenuId
                        fVC.isCommon = self.lunchMenu[indexPath.section].menus[indexPath.row].is_common ?? 0
                        fVC.heading = self.lunchMenu[indexPath.section].menus[indexPath.row].item_name ?? ""
                        fVC.itemId = myVC.itemId
                        fVC.menuData = self.lunchMenu[indexPath.section].menus[indexPath.row]
                        self.navigationController?.pushViewController(fVC, animated: true)
                    }
                }else {
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FooItemViewController") as! FooItemViewController
            myVC.timeMessage = self.timeMessage
            myVC.isCommon = self.lunchMenu[indexPath.section].menus[indexPath.row].is_common ?? 0
            myVC.navTitle = self.lunchMenu[indexPath.section].menus[indexPath.row].item_name ?? ""
            myVC.itemId = self.lunchMenu[indexPath.section].menus[indexPath.row].item_id ?? 0
            myVC.mainMenuId = self.lunchMenu[indexPath.section].menu_id!
            if(self.lunchMenu[indexPath.section].menus[indexPath.row].modifiers == 0){
                if(Singleton.shared.selectedTimeArr.count == 0 || self.timeMessage != ""){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    let time = self.convertTimestampToDate((Singleton.shared.mainMenuData[1].from ?? 0), to: "h:mm a").lowercased()
                    myVC.heading = self.timeMessage != "" ? self.timeMessage: Singleton.shared.lunchTimeMessage
                    myVC.modalPresentationStyle = .overFullScreen
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }else {
                    let fVC = self.storyboard?.instantiateViewController(withIdentifier: "FeaturedRecepieViewController") as! FeaturedRecepieViewController
                    fVC.menuId =  myVC.mainMenuId
                    fVC.isCommon = self.lunchMenu[indexPath.section].menus[indexPath.row].is_common ?? 0
                    fVC.heading = self.lunchMenu[indexPath.section].menus[indexPath.row].item_name ?? ""
                    fVC.itemId = myVC.itemId
                    fVC.menuData = self.lunchMenu[indexPath.section].menus[indexPath.row]
                    self.navigationController?.pushViewController(fVC, animated: true)
                }
            }else {
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as? UITableViewHeaderFooterView
        header?.textLabel?.font = UIFont(name: "Montserrat-Medium", size: 16)
        header?.tintColor = backgroundColor
        header?.layer.borderWidth = 0.5
        header?.layer.borderColor = barBackgroundColor.cgColor
        header?.textLabel?.textColor = UIColor.darkGray
        header?.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 30
    }
    
}
