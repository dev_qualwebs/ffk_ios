//
//  SubscriptionViewController.swift
//  FFK
//
//  Created by AM on 17/10/19.
//  Copyright © 2019 AM. All rights reserved.~
//

import UIKit

class SubscriptionViewController: UIViewController, Confirmation {
    func confirmationSelection() {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ACCOUNT, method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_DELETE_ACCOUNT, userToken: nil) { (response) in
            
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            NavigationController.shared.emptyMyCart()
            NavigationController.shared.getPreferenceData()
            Singleton.shared.initialiseValues()
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    //MARK: IBOUTLETS
    @IBOutlet weak var numberSwitch: UISwitch!
    @IBOutlet weak var emailSwitch: UISwitch!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userNumber: UILabel!
    
    @IBOutlet weak var heading: UILabel!
    
    var preferenceData = GetPreferenceResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
       // swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        let data = (Singleton.shared.userDetail.created_at ?? "2020").prefix(4)
        self.heading.text =  "You've been a Rewards Member since \(data)!"
        self.preferenceData = Singleton.shared.prefrenceData
        self.userEmail.text = Singleton.shared.userDetail.email
        self.userNumber.text = Singleton.shared.userDetail.mobile
        self.userNumber.text = self.userNumber.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
        self.view.backgroundColor = primaryColor
        emailSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        numberSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        if(self.preferenceData.subscription_preference?.email_subscription == 1){
            self.emailSwitch.isOn = true
        }else {
            self.emailSwitch.isOn = false
        }
        
        if(self.preferenceData.subscription_preference?.phone_number_subscription == 1){
            self.numberSwitch.isOn = true
        }else {
            self.numberSwitch.isOn = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func showPopup(title: String, msg: String) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.confirmationText = "Are you sure you want to miss out on special bonuses and free rewards offers?"
        myVC.isConfirmationViewHidden = false
        
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        self.showPopup(title: "Delete account", msg: "Are you sure?")
    }
    
    @IBAction func emailAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        var param = Int()
        if(emailSwitch.isOn){
            param = 1
        }else {
            param = 0
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBS_PREFERENCE, method: .post, parameter: ["email_subscription": param], objectClass: SuccessResponse.self, requestCode: U_SUBS_PREFERENCE, userToken: nil) { (response) in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.heading = response.message ?? ""
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
            NavigationController.shared.getPreferenceData()
            ActivityIndicator.hide() 
        }
        
    }
}
