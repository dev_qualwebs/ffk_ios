//
//  ChangePasswordViewController.swift
//  FFK
//
//  Created by qw on 30/11/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: UIViewController {
    //MARK: IBOUtlets
    
    @IBOutlet weak var currentPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var showPassImg: UIImageView!
     @IBOutlet weak var confirmPassImg: UIImageView!
    
    var errorCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
        //          swipeRight.direction = .right
        self.confirmPassImg.image = #imageLiteral(resourceName: "visibility")
        self.showPassImg.image = #imageLiteral(resourceName: "visibility")
               //   self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.view.backgroundColor = primaryColor
    }
        
    //MARK: IBAction
    @IBAction func showPassAction(_ sender: Any) {
           
           if(self.showPassImg.image == #imageLiteral(resourceName: "visibility")){
               self.showPassImg.image = #imageLiteral(resourceName: "password")
               self.currentPassword.isSecureTextEntry = false
           }else {
               self.showPassImg.image = #imageLiteral(resourceName: "visibility")
               self.currentPassword.isSecureTextEntry = true
           }
           
       }
       
       @IBAction func showConfirmPassAction(_ sender: Any) {
           if(self.confirmPassImg.image == #imageLiteral(resourceName: "visibility")){
               self.confirmPassImg.image = #imageLiteral(resourceName: "password")
               self.newPassword.isSecureTextEntry = false
           }else {
               self.confirmPassImg.image = #imageLiteral(resourceName: "visibility")
               self.newPassword.isSecureTextEntry = true
           }
       }
       
    
    @IBAction func chagePassword(_ sender: Any) {
        self.errorCount = 0
        if(currentPassword.text!.isEmpty){
            self.errorCount += 1
            currentPassword.errorMessage = "Enter current password"
        }
        if(newPassword.text!.isEmpty){
            self.errorCount += 1
            newPassword.errorMessage = "Enter new password"
        }
        if !(self.isPasswordValid(self.newPassword.text ?? "")){
            self.errorCount += 1
            newPassword.errorMessage = "Must be 8 characters alphanumeric"
        }
        if(self.errorCount > 0){
          return 
        }else {
            ActivityIndicator.show(view: self.view)
            let param = [
                "old_password":self.currentPassword.text,
                "new_password":self.newPassword.text
            ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_CHANGE_PASSWORD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_CHANGE_PASSWORD, userToken: nil) { (response) in
                 let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                           myVC.heading = "Successfully changed password"
                           myVC.modalPresentationStyle = .overFullScreen
                           self.navigationController?.present(myVC, animated: false, completion: nil)
                ActivityIndicator.hide()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
