//
//  PreferenceViewController.swift
//  FFK
//
//  Created by AM on 17/10/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseMessaging

class PreferenceViewController: UIViewController,CLLocationManagerDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate, Confirmation {
    
    func confirmationSelection() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGOUT, method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_LOGOUT, userToken: nil) { (response) in
            print(response)
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        NavigationController.shared.emptyMyCart()
        Messaging.messaging().unsubscribe(fromTopic: K_TOPIC_FOR_LOGGEDIN_USER)
        Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_ALL_USER)
        Singleton.shared.initialiseValues()
        self.navigationController?.pushViewController(myVC, animated: true)
        ActivityIndicator.hide()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var selectedRestaurent: UILabel!
    @IBOutlet weak var numberSubscription: UIImageView!
    @IBOutlet weak var emailSubscription: UIImageView!
    @IBOutlet weak var phoneNotification: UIImageView!
    @IBOutlet weak var locationSharing: UIImageView!
    
    var preferenceData = GetPreferenceResponse()
    var  locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = primaryColor
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
        //   swipeRight.direction = .right
          // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.getPreferenceData()
    }

    
    func getPreferenceData() {
           if let  loggedIn = UserDefaults.standard.value(forKey: UD_TOKEN){
            ActivityIndicator.show(view: self.view)
               SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PREFERENCE, method: .get, parameter: nil, objectClass: GetPReference.self, requestCode: U_GET_PREFERENCE, userToken: nil) { (response) in
                   Singleton.shared.prefrenceData = response.response
                 self.preferenceData = Singleton.shared.prefrenceData
                UserDefaults.standard.setValue(Singleton.shared.prefrenceData.restaurant_preference ?? 0, forKey: UD_SELECTED_LOCATION_PREF)
                self.handleView()
                ActivityIndicator.hide()
               }
           }
       }
    
    func handleView(){
        switch self.preferenceData.restaurant_preference {
        case 0:
            self.selectedRestaurent.text = "Order from my most recent location."
        case 1:
            self.selectedRestaurent.text = "Order from my most recent location."
        case 2:
            self.selectedRestaurent.text = "Order from the same location as my last order."
        case 3:
            self.selectedRestaurent.text = "Order from the location marked Favorite."
        default:
            break
        }
        
        if(self.preferenceData.subscription_preference?.email_subscription == 1){
            self.emailSubscription.image = #imageLiteral(resourceName: "success(6)")
            
        }else {
            self.emailSubscription.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        }
        
        if(self.preferenceData.subscription_preference?.phone_number_subscription == 1){
            self.numberSubscription.image = #imageLiteral(resourceName: "success(6)")
        }else {
            self.numberSubscription.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
            
        }
        
        if(self.preferenceData.device_preference?.push_notification == 1){
            self.phoneNotification.image = #imageLiteral(resourceName: "success(6)")
        }else {
            self.phoneNotification.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        }
        if(self.hasLocationPermission()){
            self.locationSharing.image = #imageLiteral(resourceName: "success(6)")
        }else {
            self.locationSharing.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    //MARK: IBACtion
    @IBAction func preferenceAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPrefViewController") as! RestaurantPrefViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func subscriptionAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func deviceAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DeviceSettingsViewController") as! DeviceSettingsViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id == nil){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            NavigationController.shared.emptyMyCart()
            Messaging.messaging().unsubscribe(fromTopic: K_TOPIC_FOR_LOGGEDIN_USER)
            Messaging.messaging().subscribe(toTopic: K_TOPIC_FOR_ALL_USER)
            Singleton.shared.initialiseValues()
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            self.showPopup(title: "Logout", msg: "Are your sure?")
        }
        
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func showPopup(title: String, msg: String) {
         let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RewardPopupViewController") as! RewardPopupViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.confirmationText = "Are you sure?"
        myVC.isConfirmationViewHidden = false
        myVC.confirmationDelegate = self
        self.navigationController?.present(myVC, animated: false, completion: nil)
    }
 
}
