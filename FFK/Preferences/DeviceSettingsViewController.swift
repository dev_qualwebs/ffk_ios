//
//  DeviceSettingsViewController.swift
//  FFK
//
//  Created by AM on 17/10/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import CoreLocation

class DeviceSettingsViewController: UIViewController,CLLocationManagerDelegate {
    
    //MARK: IBOUtlets
    @IBOutlet weak var locationSwitch: UISwitch!
    @IBOutlet weak var emailSwitch: UISwitch!
    
    var  locationManager = CLLocationManager()
    var preferenceData = GetPreferenceResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
       // swipeRight.direction = .right
       // self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.preferenceData = Singleton.shared.prefrenceData
        self.view.backgroundColor = primaryColor
        emailSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        locationSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        self.locationSwitch.isOn = self.hasLocationPermission()
        if(self.preferenceData.device_preference?.push_notification == 1){
            self.emailSwitch.isOn = true
        }else {
            self.emailSwitch.isOn = false
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    func updateUserLocation(coordinate: CLLocationCoordinate2D){
        let param = [
            "latitude":coordinate.latitude,
            "longitude":coordinate.longitude
            ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SET_USER_ADDRESS, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_SET_USER_ADDRESS, userToken: nil) { (response) in
            NavigationController.shared.getPreferenceData()
        }
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func locationAction(_ sender: Any) {
        if(self.locationSwitch.isOn){
            self.locationSwitch.isOn = true
            if let loc = locationManager.location {
                self.updateUserLocation(coordinate: loc.coordinate)
            }
            locationManager.startUpdatingLocation()
        }else {
            self.locationSwitch.isOn = false
            locationManager.stopUpdatingLocation()
        }
        UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
    }
    
    @IBAction func emailNotificationAction(_ sender: Any){
        var param = Int()
        ActivityIndicator.show(view: self.view)
        if(emailSwitch.isOn){
            self.emailSwitch.isOn = true
            param = 1
        }else {
            self.emailSwitch.isOn = false
            param = 0
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_DEVICE_PREFER, method: .post, parameter: ["push_notification":param], objectClass: SuccessResponse.self, requestCode: U_DEVICE_PREFER, userToken: nil) { (response) in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.heading = response.message ?? ""
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
            NavigationController.shared.getPreferenceData()
            ActivityIndicator.hide()
        }
    }
}
