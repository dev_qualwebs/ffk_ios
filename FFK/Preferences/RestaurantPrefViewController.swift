//
//  RestaurantPrefViewController.swift
//  FFK
//
//  Created by AM on 17/10/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class RestaurantPrefViewController: UIViewController {
    
    //MARK: IBACtion
    @IBOutlet weak var firstImage: ImageView!
    @IBOutlet weak var secondimage: ImageView!
    @IBOutlet weak var thirdImage: ImageView!
    
    var preferenceData = GetPreferenceResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture))
      //  swipeRight.direction = .right
      //  self.view!.addGestureRecognizer(swipeRight)
        self.navigationController?.addCustomTransitioning()
        self.preferenceData = Singleton.shared.prefrenceData
        self.view.backgroundColor = primaryColor
        switch self.preferenceData.restaurant_preference {
        case 0:
            self.firstImage.image = #imageLiteral(resourceName: "success(6)")
            self.secondimage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
            self.thirdImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        case 1:
            self.firstImage.image = #imageLiteral(resourceName: "success(6)")
            self.secondimage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
            self.thirdImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        case 2:
            self.secondimage.image = #imageLiteral(resourceName: "success(6)")
            self.firstImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
            self.thirdImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        case 3:
            self.thirdImage.image = #imageLiteral(resourceName: "success(6)")
            self.secondimage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
            self.firstImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        default:
            self.firstImage.image = #imageLiteral(resourceName: "success(6)")
            self.secondimage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
            self.thirdImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
        }
    }
    
    func callPreferenceAPI(pref: Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_RESTAURENT_PREF, method: .post, parameter: ["restaurant_preference":pref], objectClass: GetLocationDetail.self, requestCode: U_RESTAURENT_PREF, userToken: nil) { (response) in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            
            if(response.response.id == nil && pref == 1){
                myVC.heading = "Not able to set Restaurent Preference."
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else if(response.response.id == nil && pref == 2){
                myVC.heading = "Place your first order to select this option."
                self.navigationController?.present(myVC, animated: false, completion: nil)
            }else if(response.response.id == nil && pref == 3){
                myVC.heading = "Sorry, You haven't marked any restaurent as Favorite."
                self.navigationController?.present(myVC, animated: false, completion: nil)
                
            }else {
                if(pref == 1){
                    self.firstImage.image = #imageLiteral(resourceName: "success(6)")
                    self.secondimage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
                    self.thirdImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
                }else if(pref == 2){
                    self.secondimage.image = #imageLiteral(resourceName: "success(6)")
                    self.firstImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
                    self.thirdImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
                }else if(pref == 3){
                    self.thirdImage.image = #imageLiteral(resourceName: "success(6)")
                    self.secondimage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
                    self.firstImage.image = #imageLiteral(resourceName: "icons8-filled-circle-64 (1)")
                }
                if(response.response.id != Singleton.shared.selectedLocation.id){
                    NavigationController.shared.getMenuType(id: response.response.id ?? 0)
                    NavigationController.shared.emptyMyCart()
                }
                Singleton.shared.selectedLocation = response.response
                let selectedLoc = try! JSONEncoder().encode(Singleton.shared.selectedLocation)
                UserDefaults.standard.set(pref, forKey: UD_SELECTED_LOCATION_PREF)
                  NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_RESTAURANT_TAB), object: nil)
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.heading = "Your preference has been saved."
                myVC.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(myVC, animated: false, completion: nil)
                NavigationController.shared.getPreferenceData()
            }
            ActivityIndicator.hide()
        }
    }
    
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closestlocation(_ sender: Any) {
        self.callPreferenceAPI(pref: 1)
    }
    
    @IBAction func lastOrderedRestaurent(_ sender: Any) {
        self.callPreferenceAPI(pref: 2)
    }
    
    @IBAction func favouriteRestaurent(_ sender: Any) {
        
        self.callPreferenceAPI(pref: 3)
    }
    
}
