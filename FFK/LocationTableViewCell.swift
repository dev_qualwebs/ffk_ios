//
//  LocationTableViewCell.swift
//  FFK
//
//  Created by AM on 31/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var mapImage: UIView!
    
    var selectItem: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func selectAction(_ sender: Any) {
        if let selectItem = self.selectItem {
            selectItem()
        }
    }
}
