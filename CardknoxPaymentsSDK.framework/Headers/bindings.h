#include "embeddinator.h"
#import <Foundation/Foundation.h>


#if !__has_feature(objc_arc)
#error Embeddinator code must be built with ARC.
#endif

#ifdef __cplusplus
extern "C" {
#endif
void xamarin_embeddinator_initialize();
// forward declarations


NS_ASSUME_NONNULL_BEGIN

NS_ASSUME_NONNULL_END

#ifdef __cplusplus
} /* extern "C" */
#endif
