#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#pragma clang diagnostic ignored "-Wunguarded-availability-new"
#include <stdarg.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#include <objc/message.h>
#import <Foundation/Foundation.h>
#import <Speech/Speech.h>
#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>
#import <ContactsUI/ContactsUI.h>
#import <StoreKit/StoreKit.h>
#import <Photos/Photos.h>
#import <MessageUI/MessageUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import <GLKit/GLKit.h>
#import <WebKit/WebKit.h>
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTSubscriber.h>
#import <CoreTelephony/CTSubscriberInfo.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <QuartzCore/QuartzCore.h>
#import <Contacts/Contacts.h>
#import <AuthenticationServices/AuthenticationServices.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <CoreGraphics/CoreGraphics.h>
#import <EventKit/EventKit.h>
#import <AVFoundation/AVFoundation.h>

@class CardknoxPaymentsSDK_NSObjectBase;
@class TransactionParameters;
@class PaymentTransactionResponse;
@class CardknoxCardReaderCallback;
@class CardknoxCardReaderCallbackType;
@class CardknoxSDKCustomUI;
@class CardknoxSDKCustomUIScanCompleted;
@class CardknoxSDKCustomUIScannedDevice;
@class CardknoxSDKDirect;
@class CardknoxSDKUI;
@class PaymentTransactionRequest;
@class PaymentTransactionRequestUI;
@class CardknoxSDKVersion;
@class PaymentTransactionRequestDirect;
@class CardknoxSDK;
@class MessageUI_Mono_MFMailComposeViewControllerDelegate;
@class Foundation_NSDispatcher;
@class __MonoMac_NSActionDispatcher;
@class __MonoMac_NSSynchronizationContextDispatcher;
@class __Xamarin_NSTimerActionDispatcher;
@class Foundation_NSAsyncDispatcher;
@class __MonoMac_NSAsyncActionDispatcher;
@class __MonoMac_NSAsyncSynchronizationContextDispatcher;
@class Foundation_InternalNSNotificationHandler;
@class NSURLSessionDataDelegate;
@class UIKit_UIControlEventProxy;
@class System_Net_Http_NSUrlSessionHandler_WrappedNSInputStream;
@class __NSObject_Disposer;
@class __XamarinObjectObserver;
@class UIKit_UIBarButtonItem_Callback;
@class __UIGestureRecognizerToken;
@class __UIGestureRecognizerParameterlessToken;
@class UIKit_UISearchController___Xamarin_UISearchResultsUpdating;
@class System_Net_Http_NSUrlSessionHandler_NSUrlSessionHandlerDelegate;
@class Xamarin_Forms_Platform_iOS_iOS7ButtonContainer;
@class Xamarin_Forms_Platform_iOS_GlobalCloseContextGestureRecognizer;
@class Xamarin_Forms_Platform_iOS_PlatformRenderer;
@class Xamarin_Forms_Platform_iOS_VisualElementRenderer_1;
@class Xamarin_Forms_Platform_iOS_ViewRenderer_2;
@class Xamarin_Forms_Platform_iOS_ViewRenderer;
@class Xamarin_Forms_Platform_iOS_CellTableViewCell;
@class Xamarin_Forms_Platform_iOS_ActivityIndicatorRenderer;
@class Xamarin_Forms_Platform_iOS_BoxRenderer;
@class Xamarin_Forms_Platform_iOS_ButtonRenderer;
@class Xamarin_Forms_Platform_iOS_NoCaretField;
@class Xamarin_Forms_Platform_iOS_DatePickerRendererBase_1;
@class Xamarin_Forms_Platform_iOS_DatePickerRenderer;
@class Xamarin_Forms_Platform_iOS_EditorRendererBase_1;
@class Xamarin_Forms_Platform_iOS_EditorRenderer;
@class Xamarin_Forms_Platform_iOS_EntryRendererBase_1;
@class Xamarin_Forms_Platform_iOS_EntryRenderer;
@class Xamarin_Forms_Platform_iOS_HeaderWrapperView;
@class Xamarin_Forms_Platform_iOS_FormsRefreshControl;
@class Xamarin_Forms_Platform_iOS_ReadOnlyField;
@class Xamarin_Forms_Platform_iOS_PickerRendererBase_1;
@class Xamarin_Forms_Platform_iOS_PickerRenderer;
@class Xamarin_Forms_Platform_iOS_ProgressBarRenderer;
@class Xamarin_Forms_Platform_iOS_ScrollViewRenderer;
@class Xamarin_Forms_Platform_iOS_SearchBarRenderer;
@class Xamarin_Forms_Platform_iOS_SliderRenderer;
@class Xamarin_Forms_Platform_iOS_StepperRenderer;
@class Xamarin_Forms_Platform_iOS_SwitchRenderer;
@class Xamarin_Forms_Platform_iOS_TableViewModelRenderer;
@class Xamarin_Forms_Platform_iOS_UnEvenTableViewModelRenderer;
@class Xamarin_Forms_Platform_iOS_TableViewRenderer;
@class Xamarin_Forms_Platform_iOS_TimePickerRendererBase_1;
@class Xamarin_Forms_Platform_iOS_TimePickerRenderer;
@class Xamarin_Forms_Platform_iOS_ItemsViewDelegator_2;
@class Xamarin_Forms_Platform_iOS_CarouselViewDelegator;
@class Xamarin_Forms_Platform_iOS_ItemsViewRenderer_2;
@class Xamarin_Forms_Platform_iOS_CarouselViewRenderer;
@class Xamarin_Forms_Platform_iOS_StructuredItemsViewRenderer_2;
@class Xamarin_Forms_Platform_iOS_SelectableItemsViewRenderer_2;
@class Xamarin_Forms_Platform_iOS_GroupableItemsViewRenderer_2;
@class Xamarin_Forms_Platform_iOS_CollectionViewRenderer;
@class Xamarin_Forms_Platform_iOS_ItemsViewController_1;
@class Xamarin_Forms_Platform_iOS_StructuredItemsViewController_1;
@class Xamarin_Forms_Platform_iOS_SelectableItemsViewController_1;
@class Xamarin_Forms_Platform_iOS_GroupableItemsViewController_1;
@class Xamarin_Forms_Platform_iOS_SelectableItemsViewDelegator_2;
@class Xamarin_Forms_Platform_iOS_GroupableItemsViewDelegator_2;
@class Xamarin_Forms_Platform_iOS_ItemsViewCell;
@class Xamarin_Forms_Platform_iOS_TemplatedCell;
@class Xamarin_Forms_Platform_iOS_HeightConstrainedTemplatedCell;
@class Xamarin_Forms_Platform_iOS_HorizontalCell;
@class Xamarin_Forms_Platform_iOS_DefaultCell;
@class Xamarin_Forms_Platform_iOS_HorizontalDefaultSupplementalView;
@class Xamarin_Forms_Platform_iOS_HorizontalSupplementaryView;
@class Xamarin_Forms_Platform_iOS_HorizontalDefaultCell;
@class Xamarin_Forms_Platform_iOS_WidthConstrainedTemplatedCell;
@class Xamarin_Forms_Platform_iOS_VerticalCell;
@class Xamarin_Forms_Platform_iOS_VerticalDefaultCell;
@class Xamarin_Forms_Platform_iOS_ItemsViewLayout;
@class Xamarin_Forms_Platform_iOS_GridViewLayout;
@class Xamarin_Forms_Platform_iOS_ListViewLayout;
@class Xamarin_Forms_Platform_iOS_VerticalDefaultSupplementalView;
@class Xamarin_Forms_Platform_iOS_VerticalSupplementaryView;
@class Xamarin_Forms_Platform_iOS_FormsCAKeyFrameAnimation;
@class Xamarin_Forms_Platform_iOS_FormsCheckBox;
@class Xamarin_Forms_Platform_iOS_FormsUIImageView;
@class Xamarin_Forms_Platform_iOS_NativeViewWrapperRenderer;
@class Xamarin_Forms_Platform_iOS_PageContainer;
@class Xamarin_Forms_Platform_iOS_CheckBoxRendererBase_1;
@class Xamarin_Forms_Platform_iOS_PhoneFlyoutPageRenderer;
@class Xamarin_Forms_Platform_iOS_PhoneMasterDetailRenderer;
@class Xamarin_Forms_Platform_iOS_ChildViewController;
@class Xamarin_Forms_Platform_iOS_TabletFlyoutPageRenderer;
@class Xamarin_Forms_Platform_iOS_TabletMasterDetailRenderer;
@class Xamarin_Forms_Platform_iOS_ShellItemRenderer;
@class Xamarin_Forms_Platform_iOS_ShellSearchResultsRenderer;
@class Xamarin_Forms_Platform_iOS_ShellTableViewController;
@class Xamarin_Forms_Platform_iOS_UIContainerCell;
@class Xamarin_Forms_Platform_iOS_UIContainerView;
@class Xamarin_Forms_Platform_iOS_NativeViewPropertyListener;
@class Xamarin_Forms_Platform_iOS_CheckBoxRenderer;
@class Xamarin_Forms_Platform_iOS_CarouselViewLayout;
@class Xamarin_Forms_Platform_iOS_CarouselViewController;
@class Xamarin_Forms_Platform_iOS_CarouselTemplatedCell;
@class Xamarin_Forms_Platform_iOS_RefreshViewRenderer;
@class Xamarin_Forms_Platform_iOS_IndicatorViewRenderer;
@class Xamarin_Forms_Platform_iOS_FormsPageControl;
@class Xamarin_Forms_Platform_iOS_ShapeRenderer_2;
@class Xamarin_Forms_Platform_iOS_ShapeView;
@class Xamarin_Forms_Platform_iOS_ShapeLayer;
@class Xamarin_Forms_Platform_iOS_PathRenderer;
@class Xamarin_Forms_Platform_iOS_PathView;
@class Xamarin_Forms_Platform_iOS_EllipseRenderer;
@class Xamarin_Forms_Platform_iOS_EllipseView;
@class Xamarin_Forms_Platform_iOS_LineRenderer;
@class Xamarin_Forms_Platform_iOS_LineView;
@class Xamarin_Forms_Platform_iOS_PolygonRenderer;
@class Xamarin_Forms_Platform_iOS_PolygonView;
@class Xamarin_Forms_Platform_iOS_PolylineRenderer;
@class Xamarin_Forms_Platform_iOS_PolylineView;
@class Xamarin_Forms_Platform_iOS_RectangleRenderer;
@class Xamarin_Forms_Platform_iOS_RectangleView;
@class Xamarin_Forms_Platform_iOS_ShellFlyoutHeaderContainer;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell_SelectGestureRecognizer;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell_MoreActionSheetController;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell_MoreActionSheetDelegate;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell;
@class Xamarin_Forms_Platform_iOS_ContextScrollViewDelegate;
@class Xamarin_Forms_Platform_iOS_Platform_DefaultRenderer;
@class Xamarin_Forms_Platform_iOS_EntryCellRenderer_EntryCellTableViewCell;
@class Xamarin_Forms_Platform_iOS_ViewCellRenderer_ViewTableCell;
@class Xamarin_Forms_Platform_iOS_CarouselPageRenderer_CarouselPageContainer;
@class Xamarin_Forms_Platform_iOS_CarouselPageRenderer;
@class Xamarin_Forms_Platform_iOS_EditorRendererBase_1_FormsUITextView;
@class Xamarin_Forms_Platform_iOS_FrameRenderer_FrameView;
@class Xamarin_Forms_Platform_iOS_FrameRenderer;
@class Xamarin_Forms_Platform_iOS_ImageRenderer;
@class Xamarin_Forms_Platform_iOS_LabelRenderer_FormsLabel;
@class Xamarin_Forms_Platform_iOS_LabelRenderer;
@class Xamarin_Forms_Platform_iOS_ListViewRenderer_ListViewDataSource;
@class Xamarin_Forms_Platform_iOS_ListViewRenderer_UnevenListViewDataSource;
@class Xamarin_Forms_Platform_iOS_ListViewRenderer;
@class Xamarin_Forms_Platform_iOS_FormsUITableViewController;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer_FormsNavigationBar;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer_Container;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer;
@class Xamarin_Forms_Platform_iOS_OpenGLViewRenderer_Delegate;
@class Xamarin_Forms_Platform_iOS_OpenGLViewRenderer;
@class Xamarin_Forms_Platform_iOS_PageRenderer;
@class Xamarin_Forms_Platform_iOS_PickerRendererBase_1_PickerSource;
@class Xamarin_Forms_Platform_iOS_TabbedRenderer;
@class Xamarin_Forms_Platform_iOS_DragAndDropDelegate_CustomLocalStateData;
@class Xamarin_Forms_Platform_iOS_DragAndDropDelegate;
@class Xamarin_Forms_Platform_iOS_ModalWrapper;
@class Xamarin_Forms_Platform_iOS_PhoneFlyoutPageRenderer_ChildViewController;
@class Xamarin_Forms_Platform_iOS_EventedViewController_FlyoutView;
@class Xamarin_Forms_Platform_iOS_EventedViewController;
@class Xamarin_Forms_Platform_iOS_TabletFlyoutPageRenderer_InnerDelegate;
@class Xamarin_Forms_Platform_iOS_WkWebViewRenderer;
@class Xamarin_Forms_Platform_iOS_ShellFlyoutContentRenderer;
@class Xamarin_Forms_Platform_iOS_ShellFlyoutRenderer;
@class Xamarin_Forms_Platform_iOS_ShellPageRendererTracker_TitleViewContainer;
@class Xamarin_Forms_Platform_iOS_ShellRenderer;
@class Xamarin_Forms_Platform_iOS_ShellSectionRootHeader_ShellSectionHeaderCell;
@class Xamarin_Forms_Platform_iOS_ShellSectionRootHeader;
@class Xamarin_Forms_Platform_iOS_ShellSectionRootRenderer;
@class Xamarin_Forms_Platform_iOS_ShellSectionRenderer_GestureDelegate;
@class Xamarin_Forms_Platform_iOS_ShellSectionRenderer_NavDelegate;
@class Xamarin_Forms_Platform_iOS_ShellSectionRenderer;
@class Xamarin_Forms_Platform_iOS_ShellTableViewSource_SeparatorView;
@class Xamarin_Forms_Platform_iOS_ShellTableViewSource;
@class Xamarin_Forms_Platform_iOS_ImageButtonRenderer;
@class Xamarin_Forms_Platform_iOS_SwipeViewRenderer;
@class Xamarin_Forms_Platform_iOS_ToolbarItemExtensions_PrimaryToolbarItem;
@class Xamarin_Forms_Platform_iOS_ToolbarItemExtensions_SecondaryToolbarItem_SecondaryToolbarItemContent;
@class Xamarin_Forms_Platform_iOS_ToolbarItemExtensions_SecondaryToolbarItem;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer_SecondaryToolbar;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer_ParentingViewController;
@class Xamarin_Forms_Platform_iOS_WkWebViewRenderer_CustomWebViewNavigationDelegate;
@class Xamarin_Forms_Platform_iOS_WkWebViewRenderer_CustomWebViewUIDelegate;
@class Xamarin_Essentials_ShareActivityItemSource;
@class Xamarin_Essentials_AuthManager;
@class Xamarin_Essentials_SingleLocationListener;
@class Xamarin_Essentials_Contacts_ContactPickerDelegate;
@class Xamarin_Essentials_FilePicker_PickerDelegate;
@class Xamarin_Essentials_MediaPicker_PhotoPickerDelegate;
@class Xamarin_Essentials_Platform_UIPresentationControllerDelegate;
@class Xamarin_Essentials_WebAuthenticator_NativeSFSafariViewControllerDelegate;
@class Xamarin_Essentials_WebAuthenticator_ContextProvider;
@class Xamarin_Essentials_Permissions_LocationWhenInUse_ManagerDelegate;
@class OpenTK_Platform_iPhoneOS_CADisplayLinkTimeSource;
@class OpenTK_Platform_iPhoneOS_iPhoneOSGameView;
@class TTG_TTGSnackbar;
@class AIDatePickerController;
@class BigTed_ProgressHUD;
@class SignaturePad_Forms_SignaturePadCanvasRenderer;
@class SignaturePadView;
@class Xamarin_Controls_InkPresenter;
@class SignaturePadCanvasView;
@class CardKnox_Mobile_iOS_SDK_BindablePopupPageRenderer;
@class CardKnox_Mobile_iOS_SDK_CKBoxViewRenderer;
@class CardKnox_Mobile_iOS_SDK_CKExpDatePickerRenderer;
@class CardKnox_Mobile_iOS_SDK_CKExpDatePickerViewModel;
@class CardKnox_Mobile_iOS_SDK_CKEditorRenderer;
@class CardKnox_Mobile_iOS_SDK_CKFrameRenderer;
@class CardKnox_Mobile_iOS_SDK_CKImageRenderer;
@class CardKnox_Mobile_iOS_SDK_CKScrollViewRenderer;
@class CardKnox_Mobile_iOS_SDK_CollectionViewRenderer;
@class CardKnox_Mobile_iOS_SDK_CustomItemsViewController;
@class CardKnox_Mobile_iOS_SDK_CustomItemsViewDelegator;
@class CardKnox_Mobile_iOS_SDK_GrossRevenueChartRenderer;
@class CardKnox_Mobile_iOS_SDK_CKSpinnerRenderer;
@class CardKnox_Mobile_iOS_SDK_CKGregorianAndHebrewDatePickerRenderer;
@class CardKnox_Mobile_iOS_SDK_GregorianAndHebrewDatePickerViewModel;
@class CardKnox_Mobile_iOS_SDK_CKAmountEditorRenderer;
@class CardKnox_Mobile_iOS_SDK_AutoFontSizeLabelRenderer;
@class CardKnox_Mobile_iOS_SDK_CKPickerRenderer;
@class CardKnox_Mobile_iOS_SDK_CKPickerViewModel;
@class CardKnox_Mobile_iOS_SDK_BindablePageRenderer;
@class CardKnox_Mobile_iOS_SDK_CKEntryRenderer;
@class CardKnox_Mobile_iOS_SDK_CKLabelRenderer;
@class CardKnox_Mobile_iOS_SDK_CKSignaturePadRenderer;
@class CardKnox_Mobile_iOS_SDK_CoreNavigationPageRenderer;
@class CardKnox_Mobile_iOS_SDK_TabBarBackgroundRenderer;
@class CardKnox_Mobile_iOS_SDK_CKPieChartRenderer;
@class CardKnox_Mobile_iOS_SDK_SpinnerViewModel;
@class CardKnox_Mobile_iOS_SDK_iOSVP3300BluetoothScanner_CustomUICustomCBCentralDelegate;
@class CardKnox_Mobile_iOS_SDK_VP3300ScanService_CustomCBCentralDelegate;
@protocol TBDIDTDeviceCallback;
@class TBDIDTDeviceCallback;
@class CardKnox_Mobile_iOS_SDK_VP3300ScanService;
@class CardKnox_Mobile_iOS_SDK_VP3300DirectService_CustomCBCentralDelegate;
@protocol TBDIDTDeviceCallback2;
@class TBDIDTDeviceCallback2;
@class CardKnox_Mobile_iOS_SDK_VP3300DirectService;
@class CardKnox_Mobile_iOS_SDK_iOSFirstDeviceBondService_CustomUICustomCBCentralDelegate;
@class CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer4_CKObservablePickerViewModel;
@class CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer4;
@class CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer_CKObservablePickerViewModel;
@class CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer;
@class TBDBaseConnectionParams;
@class TBDAudioConnectionParams;
@class TBDBaseCommand;
@class TBDBaseConnection;
@class TBDBatteryStatusCommand;
@class TBDBleDevice;
@class TBDBleMgr;
@class TBDBluetoothConnection;
@class TBDBluetoothConnectionParams;
@class TBDConfigData;
@protocol TBDConnectionCallback;
@class TBDConnectionCallback;
@class TBDDevice;
@class TBDDeviceInformation;
@class TBDEmvData;
@class TBDIDTDeviceController;
@class TBDIDTDeviceController2;
@class TBDMsrData;
@class TBDSettings;
@class TBDSettings2;
@class TBDStartTransactionParams;

@interface CardknoxPaymentsSDK_NSObjectBase : NSObject {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSObject *) self;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface TransactionParameters : CardknoxPaymentsSDK_NSObjectBase {
}
	@property (nonatomic, assign) NSString * xMaskedCardNumber;
	@property (nonatomic, assign) NSString * xToken;
	@property (nonatomic, assign) NSString * xCommand;
	@property (nonatomic, assign) NSString * xCardNum;
	@property (nonatomic, assign) int VP3300TransactionTimeout;
	@property (nonatomic, assign) NSString * xExp;
	@property (nonatomic, assign) NSString * xRefNum;
	@property (nonatomic, assign) double xAmount;
	@property (nonatomic, assign) NSString * xInvoice;
	@property (nonatomic, assign) NSString * xPoNum;
	@property (nonatomic, assign) NSString * xDescription;
	@property (nonatomic, assign) NSString * xName;
	@property (nonatomic, assign) NSString * xBillFirstName;
	@property (nonatomic, assign) NSString * xBillLastName;
	@property (nonatomic, assign) NSString * xBillCompany;
	@property (nonatomic, assign) NSString * xBillStreet;
	@property (nonatomic, assign) NSString * xBillStreet2;
	@property (nonatomic, assign) NSString * xBillCity;
	@property (nonatomic, assign) NSString * xBillState;
	@property (nonatomic, assign) NSString * xBillZip;
	@property (nonatomic, assign) NSString * xBillCountry;
	@property (nonatomic, assign) NSString * xBillPhone;
	@property (nonatomic, assign) NSString * xShipFirstName;
	@property (nonatomic, assign) NSString * xShipLastName;
	@property (nonatomic, assign) NSString * xShipCompany;
	@property (nonatomic, assign) NSString * xShipStreet;
	@property (nonatomic, assign) NSString * xShipStreet2;
	@property (nonatomic, assign) NSString * xShipCity;
	@property (nonatomic, assign) NSString * xShipState;
	@property (nonatomic, assign) NSString * xShipZip;
	@property (nonatomic, assign) NSString * xShipCountry;
	@property (nonatomic, assign) NSString * xShipPhone;
	@property (nonatomic, assign) NSString * xAuthCode;
	@property (nonatomic, assign) BOOL xAllowDuplicate;
	@property (nonatomic, assign) BOOL xCustReceipt;
	@property (nonatomic, assign) NSString * xEmail;
	@property (nonatomic, assign) NSString * xCustom01;
	@property (nonatomic, assign) NSString * xCustom02;
	@property (nonatomic, assign) NSString * xCustom03;
	@property (nonatomic, assign) NSString * xCustom04;
	@property (nonatomic, assign) NSString * xCustom05;
	@property (nonatomic, assign) NSString * xCustom06;
	@property (nonatomic, assign) NSString * xCustom07;
	@property (nonatomic, assign) NSString * xCustom08;
	@property (nonatomic, assign) NSString * xCustom09;
	@property (nonatomic, assign) NSString * xCustom10;
	@property (nonatomic, assign) NSString * xCustom11;
	@property (nonatomic, assign) NSString * xCustom12;
	@property (nonatomic, assign) NSString * xCustom13;
	@property (nonatomic, assign) NSString * xCustom14;
	@property (nonatomic, assign) NSString * xCustom15;
	@property (nonatomic, assign) NSString * xCustom16;
	@property (nonatomic, assign) NSString * xCustom17;
	@property (nonatomic, assign) NSString * xCustom18;
	@property (nonatomic, assign) NSString * xCustom19;
	@property (nonatomic, assign) NSString * xCustom20;
	-(NSString *) xMaskedCardNumber;
	-(void) setXMaskedCardNumber:(NSString *)p0;
	-(NSString *) xToken;
	-(void) setXToken:(NSString *)p0;
	-(NSString *) xCommand;
	-(void) setXCommand:(NSString *)p0;
	-(NSString *) xCardNum;
	-(void) setXCardNum:(NSString *)p0;
	-(int) VP3300TransactionTimeout;
	-(void) setVP3300TransactionTimeout:(int)p0;
	-(NSString *) xExp;
	-(void) setXExp:(NSString *)p0;
	-(NSString *) xRefNum;
	-(void) setXRefNum:(NSString *)p0;
	-(double) xAmount;
	-(void) setXAmount:(double)p0;
	-(NSString *) xInvoice;
	-(void) setXInvoice:(NSString *)p0;
	-(NSString *) xPoNum;
	-(void) setXPoNum:(NSString *)p0;
	-(NSString *) xDescription;
	-(void) setXDescription:(NSString *)p0;
	-(NSString *) xName;
	-(void) setXName:(NSString *)p0;
	-(NSString *) xBillFirstName;
	-(void) setXBillFirstName:(NSString *)p0;
	-(NSString *) xBillLastName;
	-(void) setXBillLastName:(NSString *)p0;
	-(NSString *) xBillCompany;
	-(void) setXBillCompany:(NSString *)p0;
	-(NSString *) xBillStreet;
	-(void) setXBillStreet:(NSString *)p0;
	-(NSString *) xBillStreet2;
	-(void) setXBillStreet2:(NSString *)p0;
	-(NSString *) xBillCity;
	-(void) setXBillCity:(NSString *)p0;
	-(NSString *) xBillState;
	-(void) setXBillState:(NSString *)p0;
	-(NSString *) xBillZip;
	-(void) setXBillZip:(NSString *)p0;
	-(NSString *) xBillCountry;
	-(void) setXBillCountry:(NSString *)p0;
	-(NSString *) xBillPhone;
	-(void) setXBillPhone:(NSString *)p0;
	-(NSString *) xShipFirstName;
	-(void) setXShipFirstName:(NSString *)p0;
	-(NSString *) xShipLastName;
	-(void) setXShipLastName:(NSString *)p0;
	-(NSString *) xShipCompany;
	-(void) setXShipCompany:(NSString *)p0;
	-(NSString *) xShipStreet;
	-(void) setXShipStreet:(NSString *)p0;
	-(NSString *) xShipStreet2;
	-(void) setXShipStreet2:(NSString *)p0;
	-(NSString *) xShipCity;
	-(void) setXShipCity:(NSString *)p0;
	-(NSString *) xShipState;
	-(void) setXShipState:(NSString *)p0;
	-(NSString *) xShipZip;
	-(void) setXShipZip:(NSString *)p0;
	-(NSString *) xShipCountry;
	-(void) setXShipCountry:(NSString *)p0;
	-(NSString *) xShipPhone;
	-(void) setXShipPhone:(NSString *)p0;
	-(NSString *) xAuthCode;
	-(void) setXAuthCode:(NSString *)p0;
	-(BOOL) xAllowDuplicate;
	-(void) setXAllowDuplicate:(BOOL)p0;
	-(BOOL) xCustReceipt;
	-(void) setXCustReceipt:(BOOL)p0;
	-(NSString *) xEmail;
	-(void) setXEmail:(NSString *)p0;
	-(NSString *) xCustom01;
	-(void) setXCustom01:(NSString *)p0;
	-(NSString *) xCustom02;
	-(void) setXCustom02:(NSString *)p0;
	-(NSString *) xCustom03;
	-(void) setXCustom03:(NSString *)p0;
	-(NSString *) xCustom04;
	-(void) setXCustom04:(NSString *)p0;
	-(NSString *) xCustom05;
	-(void) setXCustom05:(NSString *)p0;
	-(NSString *) xCustom06;
	-(void) setXCustom06:(NSString *)p0;
	-(NSString *) xCustom07;
	-(void) setXCustom07:(NSString *)p0;
	-(NSString *) xCustom08;
	-(void) setXCustom08:(NSString *)p0;
	-(NSString *) xCustom09;
	-(void) setXCustom09:(NSString *)p0;
	-(NSString *) xCustom10;
	-(void) setXCustom10:(NSString *)p0;
	-(NSString *) xCustom11;
	-(void) setXCustom11:(NSString *)p0;
	-(NSString *) xCustom12;
	-(void) setXCustom12:(NSString *)p0;
	-(NSString *) xCustom13;
	-(void) setXCustom13:(NSString *)p0;
	-(NSString *) xCustom14;
	-(void) setXCustom14:(NSString *)p0;
	-(NSString *) xCustom15;
	-(void) setXCustom15:(NSString *)p0;
	-(NSString *) xCustom16;
	-(void) setXCustom16:(NSString *)p0;
	-(NSString *) xCustom17;
	-(void) setXCustom17:(NSString *)p0;
	-(NSString *) xCustom18;
	-(void) setXCustom18:(NSString *)p0;
	-(NSString *) xCustom19;
	-(void) setXCustom19:(NSString *)p0;
	-(NSString *) xCustom20;
	-(void) setXCustom20:(NSString *)p0;
	-(NSString *) description;
	-(id) init;
@end

@interface PaymentTransactionResponse : CardknoxPaymentsSDK_NSObjectBase {
}
	+(id) unwrap:(NSNotification *)p0;
	-(BOOL) isSuccess;
	-(NSString *) errorMessage;
	-(NSString *) xResult;
	-(NSString *) xStatus;
	-(NSString *) xError;
	-(NSString *) xErrorCode;
	-(NSString *) xRefNum;
	-(NSString *) xExp;
	-(NSString *) xDate;
	-(NSString *) xAuthCode;
	-(NSString *) xBatch;
	-(NSString *) xAvsResultCode;
	-(NSString *) xAvsResult;
	-(NSString *) xCvvResultCode;
	-(NSString *) xCvvResult;
	-(NSString *) xAuthAmount;
	-(NSString *) xToken;
	-(NSString *) xMaskedCardNumber;
	-(NSString *) xCardType;
	-(NSString *) xInvoice;
	-(NSString *) xName;
	-(NSString *) xCurrency;
	-(NSString *) xEntryMethod;
	-(NSString *) xRefNumCurrent;
	-(id) init;
@end

@interface CardknoxCardReaderCallback : NSObject {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(int) code;
	-(NSString *) name;
	-(NSString *) message;
	+(id) unwrap:(NSNotification *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardknoxCardReaderCallbackType : NSObject {
}
	@property (nonatomic, assign, readonly) int bluetoothDisabled;
	@property (nonatomic, assign, readonly) int transactionStartErrorDeviceDisconnected;
	@property (nonatomic, assign, readonly) int transactionStartErrorTimeout;
	@property (nonatomic, assign, readonly) int connected;
	@property (nonatomic, assign, readonly) int connecting;
	@property (nonatomic, assign, readonly) int disconnected;
	@property (nonatomic, assign, readonly) int transactionStarting;
	@property (nonatomic, assign, readonly) int transactionStarted;
	@property (nonatomic, assign, readonly) int transactionCancelled;
	@property (nonatomic, assign, readonly) int error;
	@property (nonatomic, assign, readonly) int waitingForDeviceResponse;
	@property (nonatomic, assign, readonly) int scanStop;
	@property (nonatomic, assign, readonly) int scanStart;
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	+(int) bluetoothDisabled;
	+(int) transactionStartErrorDeviceDisconnected;
	+(int) transactionStartErrorTimeout;
	+(int) connected;
	+(int) connecting;
	+(int) disconnected;
	+(int) transactionStarting;
	+(int) transactionStarted;
	+(int) transactionCancelled;
	+(int) error;
	+(int) waitingForDeviceResponse;
	+(int) scanStop;
	+(int) scanStart;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CardknoxSDKCustomUI : NSObject {
}
	@property (nonatomic, assign, readonly) NSString * customUI_scannedDevice_Subscription_NSNotificationCenterName;
	@property (nonatomic, assign, readonly) NSString * customUI_scanCompleted_Subscription_NSNotificationCenterName;
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	+(NSString *) customUI_scannedDevice_Subscription_NSNotificationCenterName;
	+(NSString *) customUI_scanCompleted_Subscription_NSNotificationCenterName;
	+(id) create;
	-(void) destroy;
	-(void) startScanningWithTimeout:(int)p0;
	-(void) stopScanning;
	-(void) connectWithName:(NSString *)p0;
	-(void) connectWithUUID:(NSString *)p0;
	-(void) disconnectFromCurrentDevice;
	-(void) startTransactionWithArgs:(id)p0;
	-(void) cancelTransaction;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardknoxSDKCustomUIScanCompleted : NSObject {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSArray *) scannedDevices;
	+(id) from:(NSNotification *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CardknoxSDKCustomUIScannedDevice : CardknoxPaymentsSDK_NSObjectBase {
}
	+(id) from:(NSNotification *)p0;
	-(NSString *) displayName;
	-(NSString *) name;
	-(NSString *) uuid;
@end

@interface CardknoxSDKDirect : NSObject {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	+(id) create;
	-(void) destroy;
	-(id) createRequestWithParameters:(id)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CardknoxSDKUI : CardknoxPaymentsSDK_NSObjectBase {
}
	@property (nonatomic, assign) BOOL EnableDeviceInsertSwipeTap;
	@property (nonatomic, assign) BOOL EnableKeyedEntry;
	@property (nonatomic, assign) BOOL CloseSDKUIOnProcessedTransaction;
	+(BOOL) EnableDeviceInsertSwipeTap;
	+(void) setEnableDeviceInsertSwipeTap:(BOOL)p0;
	+(BOOL) EnableKeyedEntry;
	+(void) setEnableKeyedEntry:(BOOL)p0;
	+(BOOL) CloseSDKUIOnProcessedTransaction;
	+(void) setCloseSDKUIOnProcessedTransaction:(BOOL)p0;
	+(id) create;
	-(void) destroy;
	-(id) createRequestWithParameters:(id)p0;
	-(id) init;
@end

@interface PaymentTransactionRequest : CardknoxPaymentsSDK_NSObjectBase {
}
	@property (nonatomic, assign, readonly) BOOL IsValid;
	@property (nonatomic, assign, readonly) NSArray * ValidationErrors;
	-(BOOL) IsValid;
	-(NSArray *) ValidationErrors;
@end

@interface PaymentTransactionRequestUI : PaymentTransactionRequest {
}
	-(void) process;
@end

@interface CardknoxSDKVersion : CardknoxPaymentsSDK_NSObjectBase {
}
	@property (nonatomic, assign) int major;
	@property (nonatomic, assign) int minor;
	@property (nonatomic, assign) int patch;
	@property (nonatomic, assign) int build;
	-(int) major;
	-(void) setMajor:(int)p0;
	-(int) minor;
	-(void) setMinor:(int)p0;
	-(int) patch;
	-(void) setPatch:(int)p0;
	-(int) build;
	-(void) setBuild:(int)p0;
	-(NSString *) description;
@end

@interface PaymentTransactionRequestDirect : PaymentTransactionRequest {
}
	-(id) process;
@end

@interface CardknoxSDK : NSObject {
}
	@property (nonatomic, assign, readonly) NSString * transactionResultSubscription_NSNotificationCenterName;
	@property (nonatomic, assign, readonly) NSString * cardreaderEventSubscription_NSNotificationCenterName;
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	+(NSString *) transactionResultSubscription_NSNotificationCenterName;
	+(NSString *) cardreaderEventSubscription_NSNotificationCenterName;
	+(void) setPrincipalKey:(NSString *)p0;
	+(void) enableLogging:(BOOL)p0;
	+(void) setxSoftwareName:(NSString *)p0 xSoftwareVersion:(NSString *)p1 xVersion:(NSString *)p2;
	+(id) getVersion;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface NSURLSessionDataDelegate : NSObject<NSURLSessionDataDelegate, NSURLSessionTaskDelegate, NSURLSessionDelegate> {
}
	-(id) init;
@end

@interface __UIGestureRecognizerToken : NSObject {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface __UIGestureRecognizerParameterlessToken : __UIGestureRecognizerToken {
}
	-(void) target;
@end

@interface Xamarin_Forms_Platform_iOS_VisualElementRenderer_1 : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIColor *) backgroundColor;
	-(void) setBackgroundColor:(UIColor *)p0;
	-(BOOL) canBecomeFirstResponder;
	-(NSArray *) keyCommands;
	-(void) tabForward:(UIKeyCommand *)p0;
	-(void) tabBackward:(UIKeyCommand *)p0;
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ViewRenderer_2 : Xamarin_Forms_Platform_iOS_VisualElementRenderer_1 {
}
	-(void) layoutSubviews;
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ViewRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_CellTableViewCell : UITableViewCell {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ActivityIndicatorRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) drawRect:(CGRect)p0;
	-(void) layoutSubviews;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_BoxRenderer : Xamarin_Forms_Platform_iOS_VisualElementRenderer_1 {
}
	-(void) drawRect:(CGRect)p0;
	-(void) layoutSubviews;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ButtonRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(void) drawRect:(CGRect)p0;
	-(void) layoutSubviews;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_DatePickerRendererBase_1 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_DatePickerRenderer : Xamarin_Forms_Platform_iOS_DatePickerRendererBase_1 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EditorRendererBase_1 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EditorRenderer : Xamarin_Forms_Platform_iOS_EditorRendererBase_1 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EntryRendererBase_1 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EntryRenderer : Xamarin_Forms_Platform_iOS_EntryRendererBase_1 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_FormsRefreshControl : UIRefreshControl {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) isHidden;
	-(void) setHidden:(BOOL)p0;
	-(void) beginRefreshing;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_PickerRendererBase_1 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PickerRenderer : Xamarin_Forms_Platform_iOS_PickerRendererBase_1 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ProgressBarRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ScrollViewRenderer : UIScrollView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_SearchBarRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_SliderRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_StepperRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_SwitchRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_TableViewModelRenderer : NSObject<UIScrollViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UITableViewCell *) tableView:(UITableView *)p0 cellForRowAtIndexPath:(NSIndexPath *)p1;
	-(CGFloat) tableView:(UITableView *)p0 heightForHeaderInSection:(NSInteger)p1;
	-(UIView *) tableView:(UITableView *)p0 viewForHeaderInSection:(NSInteger)p1;
	-(void) tableView:(UITableView *)p0 willDisplayHeaderView:(UIView *)p1 forSection:(NSInteger)p2;
	-(NSInteger) numberOfSectionsInTableView:(UITableView *)p0;
	-(void) tableView:(UITableView *)p0 didSelectRowAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) tableView:(UITableView *)p0 numberOfRowsInSection:(NSInteger)p1;
	-(NSArray *) sectionIndexTitlesForTableView:(UITableView *)p0;
	-(NSString *) tableView:(UITableView *)p0 titleForHeaderInSection:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_UnEvenTableViewModelRenderer : Xamarin_Forms_Platform_iOS_TableViewModelRenderer<UIScrollViewDelegate> {
}
	-(CGFloat) tableView:(UITableView *)p0 heightForRowAtIndexPath:(NSIndexPath *)p1;
@end

@interface Xamarin_Forms_Platform_iOS_TableViewRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) layoutSubviews;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_TimePickerRendererBase_1 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_TimePickerRenderer : Xamarin_Forms_Platform_iOS_TimePickerRendererBase_1 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ItemsViewDelegator_2 : NSObject<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) scrollViewDidScroll:(UIScrollView *)p0;
	-(UIEdgeInsets) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 insetForSectionAtIndex:(NSInteger)p2;
	-(CGFloat) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 minimumInteritemSpacingForSectionAtIndex:(NSInteger)p2;
	-(CGFloat) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 minimumLineSpacingForSectionAtIndex:(NSInteger)p2;
	-(void) collectionView:(UICollectionView *)p0 didEndDisplayingCell:(UICollectionViewCell *)p1 forItemAtIndexPath:(NSIndexPath *)p2;
	-(CGSize) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 sizeForItemAtIndexPath:(NSIndexPath *)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_CarouselViewDelegator : Xamarin_Forms_Platform_iOS_ItemsViewDelegator_2<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
}
	-(void) scrollViewDidScroll:(UIScrollView *)p0;
	-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)p0;
	-(void) scrollViewDidEndDecelerating:(UIScrollView *)p0;
	-(void) scrollViewWillBeginDragging:(UIScrollView *)p0;
	-(void) scrollViewDidEndDragging:(UIScrollView *)p0 willDecelerate:(BOOL)p1;
@end

@interface Xamarin_Forms_Platform_iOS_ItemsViewRenderer_2 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_CarouselViewRenderer : Xamarin_Forms_Platform_iOS_ItemsViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_StructuredItemsViewRenderer_2 : Xamarin_Forms_Platform_iOS_ItemsViewRenderer_2 {
}
	-(void) layoutSubviews;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_SelectableItemsViewRenderer_2 : Xamarin_Forms_Platform_iOS_StructuredItemsViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_GroupableItemsViewRenderer_2 : Xamarin_Forms_Platform_iOS_SelectableItemsViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_CollectionViewRenderer : Xamarin_Forms_Platform_iOS_GroupableItemsViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ItemsViewController_1 : UICollectionViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UICollectionViewCell *) collectionView:(UICollectionView *)p0 cellForItemAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) collectionView:(UICollectionView *)p0 numberOfItemsInSection:(NSInteger)p1;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewWillLayoutSubviews;
	-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_StructuredItemsViewController_1 : Xamarin_Forms_Platform_iOS_ItemsViewController_1 {
}
	-(void) viewWillLayoutSubviews;
@end

@interface Xamarin_Forms_Platform_iOS_SelectableItemsViewController_1 : Xamarin_Forms_Platform_iOS_StructuredItemsViewController_1 {
}
	-(void) collectionView:(UICollectionView *)p0 didSelectItemAtIndexPath:(NSIndexPath *)p1;
	-(void) collectionView:(UICollectionView *)p0 didDeselectItemAtIndexPath:(NSIndexPath *)p1;
@end

@interface Xamarin_Forms_Platform_iOS_GroupableItemsViewController_1 : Xamarin_Forms_Platform_iOS_SelectableItemsViewController_1 {
}
	-(UICollectionReusableView *) collectionView:(UICollectionView *)p0 viewForSupplementaryElementOfKind:(NSString *)p1 atIndexPath:(NSIndexPath *)p2;
@end

@interface Xamarin_Forms_Platform_iOS_SelectableItemsViewDelegator_2 : Xamarin_Forms_Platform_iOS_ItemsViewDelegator_2<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
}
	-(void) collectionView:(UICollectionView *)p0 didSelectItemAtIndexPath:(NSIndexPath *)p1;
	-(void) collectionView:(UICollectionView *)p0 didDeselectItemAtIndexPath:(NSIndexPath *)p1;
@end

@interface Xamarin_Forms_Platform_iOS_GroupableItemsViewDelegator_2 : Xamarin_Forms_Platform_iOS_SelectableItemsViewDelegator_2<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
}
	-(CGSize) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 referenceSizeForHeaderInSection:(NSInteger)p2;
	-(CGSize) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 referenceSizeForFooterInSection:(NSInteger)p2;
	-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)p0;
	-(UIEdgeInsets) collectionView:(UICollectionView *)p0 layout:(UICollectionViewLayout *)p1 insetForSectionAtIndex:(NSInteger)p2;
@end

@interface Xamarin_Forms_Platform_iOS_ItemsViewCell : UICollectionViewCell {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) initWithFrame:(CGRect)p0;
@end

@interface Xamarin_Forms_Platform_iOS_TemplatedCell : Xamarin_Forms_Platform_iOS_ItemsViewCell {
}
	-(UICollectionViewLayoutAttributes *) preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)p0;
	-(BOOL) isSelected;
	-(void) setSelected:(BOOL)p0;
	-(id) initWithFrame:(CGRect)p0;
@end

@interface Xamarin_Forms_Platform_iOS_DefaultCell : Xamarin_Forms_Platform_iOS_ItemsViewCell {
}
	-(id) initWithFrame:(CGRect)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ItemsViewLayout : UICollectionViewFlowLayout {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) flipsHorizontallyInOppositeLayoutDirection;
	-(BOOL) shouldInvalidateLayoutForPreferredLayoutAttributes:(UICollectionViewLayoutAttributes *)p0 withOriginalAttributes:(UICollectionViewLayoutAttributes *)p1;
	-(CGPoint) targetContentOffsetForProposedContentOffset:(CGPoint)p0 withScrollingVelocity:(CGPoint)p1;
	-(UICollectionViewLayoutInvalidationContext *) invalidationContextForPreferredLayoutAttributes:(UICollectionViewLayoutAttributes *)p0 withOriginalAttributes:(UICollectionViewLayoutAttributes *)p1;
	-(void) prepareLayout;
	-(void) prepareForCollectionViewUpdates:(NSArray *)p0;
	-(CGPoint) targetContentOffsetForProposedContentOffset:(CGPoint)p0;
	-(void) finalizeCollectionViewUpdates;
	-(BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_GridViewLayout : Xamarin_Forms_Platform_iOS_ItemsViewLayout {
}
	-(CGSize) collectionViewContentSize;
	-(NSArray *) layoutAttributesForElementsInRect:(CGRect)p0;
	-(UICollectionViewLayoutInvalidationContext *) invalidationContextForPreferredLayoutAttributes:(UICollectionViewLayoutAttributes *)p0 withOriginalAttributes:(UICollectionViewLayoutAttributes *)p1;
@end

@interface Xamarin_Forms_Platform_iOS_ListViewLayout : Xamarin_Forms_Platform_iOS_ItemsViewLayout {
}
@end

@interface Xamarin_Forms_Platform_iOS_FormsCAKeyFrameAnimation : CAKeyframeAnimation {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_FormsCheckBox : UIButton {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) isEnabled;
	-(void) setEnabled:(BOOL)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_FormsUIImageView : UIImageView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIImage *) image;
	-(void) setImage:(UIImage *)p0;
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(BOOL) isAnimating;
	-(void) startAnimating;
	-(void) stopAnimating;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_NativeViewWrapperRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) layoutSubviews;
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_CheckBoxRendererBase_1 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) layoutSubviews;
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PhoneFlyoutPageRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) willRotateToInterfaceOrientation:(NSInteger)p0 duration:(double)p1;
	-(UIViewController *) childViewControllerForStatusBarHidden;
	-(UIViewController *) childViewControllerForHomeIndicatorAutoHidden;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PhoneMasterDetailRenderer : Xamarin_Forms_Platform_iOS_PhoneFlyoutPageRenderer {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_TabletFlyoutPageRenderer : UISplitViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) viewWillDisappear:(BOOL)p0;
	-(void) viewWillLayoutSubviews;
	-(void) willRotateToInterfaceOrientation:(NSInteger)p0 duration:(double)p1;
	-(UIViewController *) childViewControllerForStatusBarHidden;
	-(UIViewController *) childViewControllerForHomeIndicatorAutoHidden;
	-(void) viewWillTransitionToSize:(CGSize)p0 withTransitionCoordinator:(id)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_TabletMasterDetailRenderer : Xamarin_Forms_Platform_iOS_TabletFlyoutPageRenderer {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShellItemRenderer : UITabBarController<UINavigationControllerDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIViewController *) selectedViewController;
	-(void) setSelectedViewController:(UIViewController *)p0;
	-(void) navigationController:(UINavigationController *)p0 didShowViewController:(UIViewController *)p1 animated:(BOOL)p2;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) viewWillLayoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellSearchResultsRenderer : UITableViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UITableViewCell *) tableView:(UITableView *)p0 cellForRowAtIndexPath:(NSIndexPath *)p1;
	-(void) tableView:(UITableView *)p0 didSelectRowAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) numberOfSectionsInTableView:(UITableView *)p0;
	-(NSInteger) tableView:(UITableView *)p0 numberOfRowsInSection:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellTableViewController : UITableViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_UIContainerCell : UITableViewCell {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_UIContainerView : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) willMoveToSuperview:(UIView *)p0;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_CheckBoxRenderer : Xamarin_Forms_Platform_iOS_CheckBoxRendererBase_1 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_CarouselViewLayout : Xamarin_Forms_Platform_iOS_ItemsViewLayout {
}
	-(void) prepareForCollectionViewUpdates:(NSArray *)p0;
	-(void) finalizeCollectionViewUpdates;
@end

@interface Xamarin_Forms_Platform_iOS_CarouselViewController : Xamarin_Forms_Platform_iOS_ItemsViewController_1 {
}
	-(UICollectionViewCell *) collectionView:(UICollectionView *)p0 cellForItemAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) collectionView:(UICollectionView *)p0 numberOfItemsInSection:(NSInteger)p1;
	-(void) viewDidLoad;
	-(void) viewWillLayoutSubviews;
	-(void) viewDidLayoutSubviews;
	-(void) scrollViewWillBeginDragging:(UIScrollView *)p0;
	-(void) scrollViewDidEndDragging:(UIScrollView *)p0 willDecelerate:(BOOL)p1;
@end

@interface Xamarin_Forms_Platform_iOS_CarouselTemplatedCell : Xamarin_Forms_Platform_iOS_TemplatedCell {
}
	-(id) initWithFrame:(CGRect)p0;
@end

@interface Xamarin_Forms_Platform_iOS_RefreshViewRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_IndicatorViewRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShapeRenderer_2 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShapeView : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShapeLayer : CALayer {
}
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) drawInContext:(id)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PathRenderer : Xamarin_Forms_Platform_iOS_ShapeRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PathView : Xamarin_Forms_Platform_iOS_ShapeView {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EllipseRenderer : Xamarin_Forms_Platform_iOS_ShapeRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EllipseView : Xamarin_Forms_Platform_iOS_ShapeView {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_LineRenderer : Xamarin_Forms_Platform_iOS_ShapeRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_LineView : Xamarin_Forms_Platform_iOS_ShapeView {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PolygonRenderer : Xamarin_Forms_Platform_iOS_ShapeRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PolygonView : Xamarin_Forms_Platform_iOS_ShapeView {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PolylineRenderer : Xamarin_Forms_Platform_iOS_ShapeRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PolylineView : Xamarin_Forms_Platform_iOS_ShapeView {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_RectangleRenderer : Xamarin_Forms_Platform_iOS_ShapeRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_RectangleView : Xamarin_Forms_Platform_iOS_ShapeView {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EntryCellRenderer_EntryCellTableViewCell : Xamarin_Forms_Platform_iOS_CellTableViewCell {
}
	-(void) layoutSubviews;
@end

@interface Xamarin_Forms_Platform_iOS_CarouselPageRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) didRotateFromInterfaceOrientation:(NSInteger)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) viewDidUnload;
	-(void) willRotateToInterfaceOrientation:(NSInteger)p0 duration:(double)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_FrameRenderer : Xamarin_Forms_Platform_iOS_VisualElementRenderer_1 {
}
	-(void) addSubview:(UIView *)p0;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(void) layoutSubviews;
	-(void) drawRect:(CGRect)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ImageRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_LabelRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) layoutSubviews;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ListViewRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) layoutSubviews;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_NavigationRenderer : UINavigationController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) didRotateFromInterfaceOrientation:(NSInteger)p0;
	-(NSArray *) popToRootViewControllerAnimated:(BOOL)p0;
	-(UIViewController *) popViewControllerAnimated:(BOOL)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(UIViewController *) childViewControllerForStatusBarHidden;
	-(UIViewController *) childViewControllerForHomeIndicatorAutoHidden;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_PageRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) loadView;
	-(void) viewWillLayoutSubviews;
	-(void) viewDidLayoutSubviews;
	-(void) viewSafeAreaInsetsDidChange;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLoad;
	-(void) viewWillDisappear:(BOOL)p0;
	-(NSInteger) preferredStatusBarUpdateAnimation;
	-(void) traitCollectionDidChange:(UITraitCollection *)p0;
	-(BOOL) prefersStatusBarHidden;
	-(BOOL) prefersHomeIndicatorAutoHidden;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_TabbedRenderer : UITabBarController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIViewController *) selectedViewController;
	-(void) setSelectedViewController:(UIViewController *)p0;
	-(void) didRotateFromInterfaceOrientation:(NSInteger)p0;
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewDidDisappear:(BOOL)p0;
	-(void) viewDidLayoutSubviews;
	-(UIViewController *) childViewControllerForStatusBarHidden;
	-(UIViewController *) childViewControllerForHomeIndicatorAutoHidden;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_EventedViewController_FlyoutView : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_WkWebViewRenderer : WKWebView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) didMoveToWindow;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShellFlyoutContentRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewWillLayoutSubviews;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewWillDisappear:(BOOL)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellFlyoutRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIViewController *) childViewControllerForStatusBarStyle;
	-(void) viewDidLayoutSubviews;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidLoad;
	-(NSArray *) keyCommands;
	-(void) tabForward:(UIKeyCommand *)p0;
	-(void) tabBackward:(UIKeyCommand *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShellPageRendererTracker_TitleViewContainer : Xamarin_Forms_Platform_iOS_UIContainerView {
}
	-(CGRect) frame;
	-(void) setFrame:(CGRect)p0;
	-(void) willMoveToSuperview:(UIView *)p0;
	-(CGSize) intrinsicContentSize;
	-(CGSize) sizeThatFits:(CGSize)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShellSectionRootHeader_ShellSectionHeaderCell : UICollectionViewCell {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) isSelected;
	-(void) setSelected:(BOOL)p0;
	-(void) layoutSubviews;
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
	-(id) initWithFrame:(CGRect)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellSectionRootHeader : UICollectionViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) collectionView:(UICollectionView *)p0 canMoveItemAtIndexPath:(NSIndexPath *)p1;
	-(UICollectionViewCell *) collectionView:(UICollectionView *)p0 cellForItemAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) collectionView:(UICollectionView *)p0 numberOfItemsInSection:(NSInteger)p1;
	-(void) collectionView:(UICollectionView *)p0 didDeselectItemAtIndexPath:(NSIndexPath *)p1;
	-(void) collectionView:(UICollectionView *)p0 didSelectItemAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)p0;
	-(BOOL) collectionView:(UICollectionView *)p0 shouldSelectItemAtIndexPath:(NSIndexPath *)p1;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_ShellSectionRootRenderer : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewSafeAreaInsetsDidChange;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellSectionRenderer : UINavigationController {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) navigationBar:(UINavigationBar *)p0 shouldPopItem:(UINavigationItem *)p1;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewDidLayoutSubviews;
	-(void) viewDidLoad;
	-(NSArray *) popToRootViewControllerAnimated:(BOOL)p0;
	-(NSArray *) viewControllers;
	-(void) setViewControllers:(NSArray *)p0;
	-(NSArray *) popToViewController:(UIViewController *)p0 animated:(BOOL)p1;
	-(void) pushViewController:(UIViewController *)p0 animated:(BOOL)p1;
	-(UIViewController *) popViewControllerAnimated:(BOOL)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ShellTableViewSource : NSObject<UIScrollViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(CGFloat) tableView:(UITableView *)p0 heightForRowAtIndexPath:(NSIndexPath *)p1;
	-(UITableViewCell *) tableView:(UITableView *)p0 cellForRowAtIndexPath:(NSIndexPath *)p1;
	-(CGFloat) tableView:(UITableView *)p0 heightForFooterInSection:(NSInteger)p1;
	-(UIView *) tableView:(UITableView *)p0 viewForFooterInSection:(NSInteger)p1;
	-(NSInteger) numberOfSectionsInTableView:(UITableView *)p0;
	-(void) tableView:(UITableView *)p0 didSelectRowAtIndexPath:(NSIndexPath *)p1;
	-(NSInteger) tableView:(UITableView *)p0 numberOfRowsInSection:(NSInteger)p1;
	-(void) scrollViewDidScroll:(UIScrollView *)p0;
	-(void) tableView:(UITableView *)p0 willDisplayCell:(UITableViewCell *)p1 forRowAtIndexPath:(NSIndexPath *)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface Xamarin_Forms_Platform_iOS_ImageButtonRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(CGSize) sizeThatFits:(CGSize)p0;
	-(id) init;
@end

@interface Xamarin_Forms_Platform_iOS_SwipeViewRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(void) willMoveToWindow:(UIWindow *)p0;
	-(void) layoutSubviews;
	-(void) touchesEnded:(NSSet *)p0 withEvent:(UIEvent *)p1;
	-(void) touchesCancelled:(NSSet *)p0 withEvent:(UIEvent *)p1;
	-(UIView *) hitTest:(CGPoint)p0 withEvent:(UIEvent *)p1;
	-(id) init;
@end

@interface OpenTK_Platform_iPhoneOS_iPhoneOSGameView : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	+(Class) layerClass;
	-(void) layoutSubviews;
	-(void) willMoveToWindow:(UIWindow *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) initWithCoder:(NSCoder *)p0;
	-(id) initWithFrame:(CGRect)p0;
@end

@interface TTG_TTGSnackbar : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface AIDatePickerController : UIViewController<UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) viewDidLoad;
	-(double) transitionDuration:(id)p0;
	-(void) animateTransition:(id)p0;
	-(id) animationControllerForPresentedController:(UIViewController *)p0 presentingController:(UIViewController *)p1 sourceController:(UIViewController *)p2;
	-(id) animationControllerForDismissedController:(UIViewController *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface BigTed_ProgressHUD : UIView {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) drawRect:(CGRect)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface SignaturePad_Forms_SignaturePadCanvasRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface SignaturePadView : UIView {
}
	@property (nonatomic, assign) UIColor * StrokeColor;
	@property (nonatomic, assign) float StrokeWidth;
	@property (nonatomic, assign) UIColor * SignatureLineColor;
	@property (nonatomic, assign) CGFloat SignatureLineWidth;
	@property (nonatomic, assign) CGFloat SignatureLineSpacing;
	@property (nonatomic, assign) NSString * CaptionText;
	@property (nonatomic, assign) CGFloat CaptionFontSize;
	@property (nonatomic, assign) UIColor * CaptionTextColor;
	@property (nonatomic, assign) NSString * SignaturePromptText;
	@property (nonatomic, assign) CGFloat SignaturePromptFontSize;
	@property (nonatomic, assign) UIColor * SignaturePromptTextColor;
	@property (nonatomic, assign) NSString * ClearLabelText;
	@property (nonatomic, assign) CGFloat ClearLabelFontSize;
	@property (nonatomic, assign) UIColor * ClearLabelTextColor;
	@property (nonatomic, assign) UIImage * BackgroundImage;
	@property (nonatomic, assign) NSInteger BackgroundImageContentMode;
	@property (nonatomic, assign) CGFloat BackgroundImageAlpha;
	@property (nonatomic, assign) UIEdgeInsets Padding;
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIColor *) StrokeColor;
	-(void) setStrokeColor:(UIColor *)p0;
	-(float) StrokeWidth;
	-(void) setStrokeWidth:(float)p0;
	-(UIColor *) SignatureLineColor;
	-(void) setSignatureLineColor:(UIColor *)p0;
	-(CGFloat) SignatureLineWidth;
	-(void) setSignatureLineWidth:(CGFloat)p0;
	-(CGFloat) SignatureLineSpacing;
	-(void) setSignatureLineSpacing:(CGFloat)p0;
	-(NSString *) CaptionText;
	-(void) setCaptionText:(NSString *)p0;
	-(CGFloat) CaptionFontSize;
	-(void) setCaptionFontSize:(CGFloat)p0;
	-(UIColor *) CaptionTextColor;
	-(void) setCaptionTextColor:(UIColor *)p0;
	-(NSString *) SignaturePromptText;
	-(void) setSignaturePromptText:(NSString *)p0;
	-(CGFloat) SignaturePromptFontSize;
	-(void) setSignaturePromptFontSize:(CGFloat)p0;
	-(UIColor *) SignaturePromptTextColor;
	-(void) setSignaturePromptTextColor:(UIColor *)p0;
	-(NSString *) ClearLabelText;
	-(void) setClearLabelText:(NSString *)p0;
	-(CGFloat) ClearLabelFontSize;
	-(void) setClearLabelFontSize:(CGFloat)p0;
	-(UIColor *) ClearLabelTextColor;
	-(void) setClearLabelTextColor:(UIColor *)p0;
	-(UIImage *) BackgroundImage;
	-(void) setBackgroundImage:(UIImage *)p0;
	-(NSInteger) BackgroundImageContentMode;
	-(void) setBackgroundImageContentMode:(NSInteger)p0;
	-(CGFloat) BackgroundImageAlpha;
	-(void) setBackgroundImageAlpha:(CGFloat)p0;
	-(UIEdgeInsets) Padding;
	-(void) setPadding:(UIEdgeInsets)p0;
	-(void) layoutSubviews;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface SignaturePadCanvasView : UIView {
}
	@property (nonatomic, assign) UIColor * StrokeColor;
	@property (nonatomic, assign) float StrokeWidth;
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(UIColor *) StrokeColor;
	-(void) setStrokeColor:(UIColor *)p0;
	-(float) StrokeWidth;
	-(void) setStrokeWidth:(float)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_BindablePopupPageRenderer : Xamarin_Forms_Platform_iOS_PageRenderer {
}
	-(NSInteger) preferredInterfaceOrientationForPresentation;
	-(NSUInteger) supportedInterfaceOrientations;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) didMoveToParentViewController:(UIViewController *)p0;
	-(void) viewWillDisappear:(BOOL)p0;
	-(void) viewDidLoad;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKBoxViewRenderer : Xamarin_Forms_Platform_iOS_BoxRenderer {
}
	-(void) drawRect:(CGRect)p0;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKExpDatePickerRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKExpDatePickerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardKnox_Mobile_iOS_SDK_CKEditorRenderer : Xamarin_Forms_Platform_iOS_EditorRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKFrameRenderer : Xamarin_Forms_Platform_iOS_FrameRenderer {
}
	-(void) touchesBegan:(NSSet *)p0 withEvent:(UIEvent *)p1;
	-(void) touchesEnded:(NSSet *)p0 withEvent:(UIEvent *)p1;
	-(void) touchesCancelled:(NSSet *)p0 withEvent:(UIEvent *)p1;
	-(void) layoutSubviews;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKImageRenderer : Xamarin_Forms_Platform_iOS_ImageRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKScrollViewRenderer : Xamarin_Forms_Platform_iOS_ScrollViewRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CollectionViewRenderer : Xamarin_Forms_Platform_iOS_GroupableItemsViewRenderer_2 {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CustomItemsViewController : Xamarin_Forms_Platform_iOS_GroupableItemsViewController_1 {
}
@end

@interface CardKnox_Mobile_iOS_SDK_GrossRevenueChartRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKSpinnerRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKGregorianAndHebrewDatePickerRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_GregorianAndHebrewDatePickerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardKnox_Mobile_iOS_SDK_CKAmountEditorRenderer : Xamarin_Forms_Platform_iOS_EditorRenderer {
}
	-(BOOL) isFocused;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_AutoFontSizeLabelRenderer : Xamarin_Forms_Platform_iOS_LabelRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKPickerRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKPickerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(UIView *) pickerView:(UIPickerView *)p0 viewForRow:(NSInteger)p1 forComponent:(NSInteger)p2 reusingView:(UIView *)p3;
	-(CGFloat) pickerView:(UIPickerView *)p0 rowHeightForComponent:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardKnox_Mobile_iOS_SDK_BindablePageRenderer : Xamarin_Forms_Platform_iOS_PageRenderer {
}
	-(void) viewDidAppear:(BOOL)p0;
	-(void) viewWillAppear:(BOOL)p0;
	-(void) viewWillDisappear:(BOOL)p0;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKEntryRenderer : Xamarin_Forms_Platform_iOS_EntryRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKLabelRenderer : Xamarin_Forms_Platform_iOS_LabelRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKSignaturePadRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CoreNavigationPageRenderer : Xamarin_Forms_Platform_iOS_NavigationRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_TabBarBackgroundRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKPieChartRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_SpinnerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardKnox_Mobile_iOS_SDK_iOSVP3300BluetoothScanner_CustomUICustomCBCentralDelegate : NSObject<CBCentralManagerDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) centralManagerDidUpdateState:(CBCentralManager *)p0;
	-(void) centralManager:(CBCentralManager *)p0 didDiscoverPeripheral:(CBPeripheral *)p1 advertisementData:(NSDictionary *)p2 RSSI:(NSNumber *)p3;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_VP3300ScanService_CustomCBCentralDelegate : NSObject<CBCentralManagerDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) centralManagerDidUpdateState:(CBCentralManager *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@protocol TBDIDTDeviceCallback
	@required -(void) TBDDevice_didPlugStatusChange:(BOOL)p0;
	@required -(void) TBDConnectionCallback_didConnectionResult:(id)p0;
	@required -(void) TBDDevice_didConnectionResponse:(unsigned int)p0;
	@required -(void) TBDDevice_didConfigLoaded:(unsigned int)p0 :(unsigned int)p1 :(NSString *)p2;
	@required -(void) TBDDevice_didInformationResponse:(id)p0;
	@required -(void) TBDDevice_didExecuteCommandAsyncResponse:(id)p0;
	@required -(void) TBDTransaction_didTransactionStart:(unsigned int)p0 :(int)p1 :(NSString *)p2;
	@required -(void) TBDTransaction_didMsrCardRead:(id)p0;
	@required -(void) TBDTransaction_didEmvCardRead:(id)p0;
@end

@interface TBDIDTDeviceCallback : NSObject<TBDIDTDeviceCallback> {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_VP3300ScanService : NSObject<TBDIDTDeviceCallback> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) TBDConnectionCallback_didConnectionResult:(id)p0;
	-(void) TBDDevice_didConnectionResponse:(unsigned int)p0;
	-(void) TBDDevice_didExecuteCommandAsyncResponse:(id)p0;
	-(void) TBDDevice_didInformationResponse:(id)p0;
	-(void) TBDDevice_didPlugStatusChange:(BOOL)p0;
	-(void) TBDDevice_didConfigLoaded:(unsigned int)p0 :(unsigned int)p1 :(NSString *)p2;
	-(void) TBDTransaction_didEmvCardRead:(id)p0;
	-(void) TBDTransaction_didMsrCardRead:(id)p0;
	-(void) TBDTransaction_didTransactionStart:(unsigned int)p0 :(int)p1 :(NSString *)p2;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@protocol TBDIDTDeviceCallback2
	@required -(void) TBDDevice_OnConnectionResponse2:(unsigned int)p0;
	@required -(void) TBDConnect_OnConnectError:(unsigned int)p0;
	@required -(void) TBDConnect_OnBluetoothReady;
	@required -(void) TBDTransaction_didTransactionStart2;
	@required -(void) TBDConnect_OnAttachDeviceName:(NSString *)p0;
	@required -(void) TBDConnect_OnDetachDeviceName;
	@required -(void) TBDTransaction_OnTransactionStartError:(unsigned int)p0;
	@required -(void) TBDTransaction_OnTransactionStop;
	@required -(void) TBDTransaction_OnTransactionStopError:(unsigned int)p0;
	@required -(void) TBDTransaction_didMsrCardRead:(id)p0;
	@required -(void) TBDTransaction_didEmvCardRead:(id)p0;
@end

@interface TBDIDTDeviceCallback2 : NSObject<TBDIDTDeviceCallback2> {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_VP3300DirectService : NSObject<TBDIDTDeviceCallback2> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(void) TBDConnect_OnAttachDeviceName:(NSString *)p0;
	-(void) TBDConnect_OnDetachDeviceName;
	-(void) TBDConnect_OnBluetoothReady;
	-(void) TBDConnect_OnConnectError:(unsigned int)p0;
	-(void) TBDDevice_OnConnectionResponse2:(unsigned int)p0;
	-(void) TBDTransaction_didEmvCardRead:(id)p0;
	-(void) TBDTransaction_didMsrCardRead:(id)p0;
	-(void) TBDTransaction_didTransactionStart2;
	-(void) TBDTransaction_OnTransactionStartError:(unsigned int)p0;
	-(void) TBDTransaction_OnTransactionStop;
	-(void) TBDTransaction_OnTransactionStopError:(unsigned int)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer4_CKObservablePickerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(UIView *) pickerView:(UIPickerView *)p0 viewForRow:(NSInteger)p1 forComponent:(NSInteger)p2 reusingView:(UIView *)p3;
	-(CGFloat) pickerView:(UIPickerView *)p0 rowHeightForComponent:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer4 : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer_CKObservablePickerViewModel : NSObject<UIPickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
}
	-(void) release;
	-(id) retain;
	-(uint32_t) xamarinGetGCHandle;
	-(bool) xamarinSetGCHandle: (uint32_t) gchandle flags: (enum XamarinGCHandleFlags) flags;
	-(enum XamarinGCHandleFlags) xamarinGetFlags;
	-(void) xamarinSetFlags: (enum XamarinGCHandleFlags) flags;
	-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)p0;
	-(NSInteger) pickerView:(UIPickerView *)p0 numberOfRowsInComponent:(NSInteger)p1;
	-(NSString *) pickerView:(UIPickerView *)p0 titleForRow:(NSInteger)p1 forComponent:(NSInteger)p2;
	-(void) pickerView:(UIPickerView *)p0 didSelectRow:(NSInteger)p1 inComponent:(NSInteger)p2;
	-(UIView *) pickerView:(UIPickerView *)p0 viewForRow:(NSInteger)p1 forComponent:(NSInteger)p2 reusingView:(UIView *)p3;
	-(CGFloat) pickerView:(UIPickerView *)p0 rowHeightForComponent:(NSInteger)p1;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface CardKnox_Mobile_iOS_SDK_CKObservablePickerRenderer : Xamarin_Forms_Platform_iOS_ViewRenderer_2 {
}
	-(id) init;
@end

@interface TBDBaseConnectionParams : NSObject {
}
	-(void) createConnection;
	-(id) connection;
	-(void) setConnection:(id)p0;
	-(NSString *) getType;
	-(id) init;
@end

@interface TBDAudioConnectionParams : TBDBaseConnectionParams {
}
	-(id) init;
@end

@interface TBDBaseCommand : NSObject {
}
	-(void) execute;
	-(unsigned int) errorCode;
	-(void) setErrorCode:(unsigned int)p0;
	-(NSString *) errorMessage;
	-(void) setErrorMessage:(NSString *)p0;
	-(unsigned int) result;
	-(void) setResult:(unsigned int)p0;
	-(id) init;
@end

@interface TBDBaseConnection : NSObject {
}
	-(void) deviceConnected;
	-(void) deviceDisconnected;
	-(void) disconnect;
	-(void) discoverAndConnect:(id)p0;
	-(void) plugStatusChange:(BOOL)p0;
	-(void) stopDiscovering;
	-(NSObject *) delegate;
	-(void) setDelegate:(NSObject *)p0;
	-(id) init;
@end

@interface TBDBatteryStatusCommand : NSObject {
}
	-(int) batteryLevel;
	-(void) setBatteryLevel:(int)p0;
	-(unsigned int) getBatteryStatus;
	-(unsigned int) responseCode;
	-(void) setResponseCode:(unsigned int)p0;
	-(id) init;
@end

@interface TBDBleDevice : NSObject {
}
	-(NSString *) frienlyName;
	-(void) setFrienlyName:(NSString *)p0;
	-(NSNumber *) RSSI;
	-(void) setRSSI:(NSNumber *)p0;
	-(id) init;
@end

@interface TBDBleMgr : NSObject {
}
	-(void) stopDiscovering;
	-(int) discoverDevices;
	-(NSObject *) delegate;
	-(void) setDelegate:(NSObject *)p0;
	-(id) init;
@end

@interface TBDBluetoothConnection : TBDBaseConnection {
}
	-(id) currentDevice;
	-(void) setCurrentDevice:(id)p0;
	-(id) init;
@end

@interface TBDBluetoothConnectionParams : TBDBaseConnectionParams {
}
	-(NSString *) prefix;
	-(void) setPrefix:(NSString *)p0;
	-(NSString *) serialNumber;
	-(void) setSerialNumber:(NSString *)p0;
	-(NSString *) UUID;
	-(void) setUUID:(NSString *)p0;
	-(id) init;
@end

@interface TBDConfigData : NSObject {
}
	-(NSString *) ctlsAppData;
	-(void) setCtlsAppData:(NSString *)p0;
	-(NSMutableArray *) ctlsCapks;
	-(void) setCtlsCapks:(NSMutableArray *)p0;
	-(NSString *) ctlsTerminalData;
	-(void) setCtlsTerminalData:(NSString *)p0;
	-(NSMutableDictionary *) emvAppData;
	-(void) setEmvAppData:(NSMutableDictionary *)p0;
	-(NSMutableArray *) emvCapks;
	-(void) setEmvCapks:(NSMutableArray *)p0;
	-(NSString *) emvTerminalData;
	-(void) setEmvTerminalData:(NSString *)p0;
	-(id) init;
@end

@protocol TBDConnectionCallback
	@required -(void) TBDConnectionCallback_didScanningStart;
	@required -(void) TBDConnectionCallback_didScanningEnd;
	@required -(void) TBDConnectionCallback_didConnectionResult:(id)p0;
@end

@interface TBDConnectionCallback : NSObject<TBDConnectionCallback> {
}
	-(id) init;
@end

@interface TBDDevice : NSObject {
}
	-(id) connectionParams;
	-(void) setConnectionParams:(id)p0;
	-(unsigned int) currentState;
	-(void) setCurrentState:(unsigned int)p0;
	-(NSString *) name;
	-(void) setName:(NSString *)p0;
	-(unsigned int) type;
	-(void) setType:(unsigned int)p0;
	-(id) init;
@end

@interface TBDDeviceInformation : NSObject {
}
	-(unsigned int) errorCode;
	-(void) setErrorCode:(unsigned int)p0;
	-(NSString *) errorMessage;
	-(void) setErrorMessage:(NSString *)p0;
	-(NSString *) firmwareVersion;
	-(void) setFirmwareVersion:(NSString *)p0;
	-(unsigned int) result;
	-(void) setResult:(unsigned int)p0;
	-(NSString *) serialNumber;
	-(void) setSerialNumber:(NSString *)p0;
	-(id) init;
@end

@interface TBDEmvData : NSObject {
}
	-(void) updateEncryptedTags:(NSDictionary *)p0;
	-(void) updateMaskedTags:(NSDictionary *)p0;
	-(void) updateUnencryptedTags:(NSDictionary *)p0;
	-(id) cardData;
	-(void) setCardData:(id)p0;
	-(unsigned int) cardType;
	-(void) setCardType:(unsigned int)p0;
	-(NSMutableDictionary *) encryptedTags;
	-(void) setEncryptedTags:(NSMutableDictionary *)p0;
	-(unsigned int) encryptionMode;
	-(void) setEncryptionMode:(unsigned int)p0;
	-(int) errorCode;
	-(void) setErrorCode:(int)p0;
	-(BOOL) hasAdvise;
	-(void) setHasAdvise:(BOOL)p0;
	-(BOOL) hasReversal;
	-(void) setHasReversal:(BOOL)p0;
	-(NSData *) KSN;
	-(void) setKSN:(NSData *)p0;
	-(NSMutableDictionary *) maskedTags;
	-(void) setMaskedTags:(NSMutableDictionary *)p0;
	-(unsigned int) result;
	-(void) setResult:(unsigned int)p0;
	-(unsigned int) resultCode;
	-(void) setResultCode:(unsigned int)p0;
	-(NSMutableDictionary *) unencryptedTags;
	-(void) setUnencryptedTags:(NSMutableDictionary *)p0;
	-(id) init;
@end

@interface TBDIDTDeviceController : NSObject {
}
	-(void) disconnect;
	-(void) discoverAndConnect:(id)p0;
	-(id) executeCommand:(id)p0;
	-(void) executeCommandAsync:(id)p0;
	-(void) getDeviceInformation;
	-(void) loadConfig:(id)p0;
	-(void) loadConfigWithXml:(NSString *)p0;
	-(void) setup:(id)p0;
	-(void) startTransaction:(id)p0;
	-(void) stopDiscovering;
	-(void) stopTransaction;
	-(void) TBDConnectionCallback_didConnectionResult:(id)p0;
	-(void) TBDConnectionCallback_didScanningEnd;
	-(void) TBDConnectionCallback_didScanningStart;
	-(id) currentDevice;
	-(void) setCurrentDevice:(id)p0;
	-(NSString *) frameworkVersion;
	-(BOOL) isAudiojackPlugged;
	-(id) getSettings;
	-(id) init;
@end

@interface TBDIDTDeviceController2 : NSObject {
}
	-(void) connectWithDeviceName:(NSString *)p0;
	-(void) connectWithUUID:(NSUUID *)p0;
	-(void) destroy;
	-(void) disconnect;
	-(BOOL) isConnected;
	-(BOOL) isScanning;
	-(void) loggingEnable:(BOOL)p0;
	-(void) setup:(id)p0;
	-(void) startTransaction:(id)p0;
	-(void) stopTransaction;
	-(id) getBatteryStatus;
	-(id) currentDevice;
	-(void) setCurrentDevice:(id)p0;
	-(id) getSettings;
	-(id) init;
@end

@interface TBDMsrData : NSObject {
}
	-(unsigned int) captureEncodeType;
	-(void) setCaptureEncodeType:(unsigned int)p0;
	-(unsigned int) captureEncryptType;
	-(void) setCaptureEncryptType:(unsigned int)p0;
	-(NSData *) cardData;
	-(void) setCardData:(NSData *)p0;
	-(NSData *) encTrack1;
	-(void) setEncTrack1:(NSData *)p0;
	-(NSData *) encTrack2;
	-(void) setEncTrack2:(NSData *)p0;
	-(NSData *) encTrack3;
	-(void) setEncTrack3:(NSData *)p0;
	-(NSDictionary *) encryptedTags;
	-(void) setEncryptedTags:(NSDictionary *)p0;
	-(NSData *) hashTrack1;
	-(void) setHashTrack1:(NSData *)p0;
	-(NSData *) hashTrack2;
	-(void) setHashTrack2:(NSData *)p0;
	-(NSData *) hashTrack3;
	-(void) setHashTrack3:(NSData *)p0;
	-(BOOL) iccPresent;
	-(void) setIccPresent:(BOOL)p0;
	-(NSData *) KSN;
	-(void) setKSN:(NSData *)p0;
	-(NSDictionary *) maskedTags;
	-(void) setMaskedTags:(NSDictionary *)p0;
	-(NSString *) RSN;
	-(void) setRSN:(NSString *)p0;
	-(unsigned char) readStatus;
	-(void) setReadStatus:(unsigned char)p0;
	-(unsigned int) result;
	-(void) setResult:(unsigned int)p0;
	-(NSData *) sessionID;
	-(void) setSessionID:(NSData *)p0;
	-(NSString *) track1;
	-(void) setTrack1:(NSString *)p0;
	-(int) track1Length;
	-(void) setTrack1Length:(int)p0;
	-(NSString *) track2;
	-(void) setTrack2:(NSString *)p0;
	-(int) track2Length;
	-(void) setTrack2Length:(int)p0;
	-(NSString *) track3;
	-(void) setTrack3:(NSString *)p0;
	-(int) track3Length;
	-(void) setTrack3Length:(int)p0;
	-(NSDictionary *) unencryptedTags;
	-(void) setUnencryptedTags:(NSDictionary *)p0;
	-(id) init;
@end

@interface TBDSettings : NSObject {
}
	-(int) timeout;
	-(void) setTimeout:(int)p0;
	-(NSObject *) delegate;
	-(void) setDelegate:(NSObject *)p0;
	-(id) init;
@end

@interface TBDSettings2 : NSObject {
}
	-(int) timeout;
	-(void) setTimeout:(int)p0;
	-(NSObject *) delegate;
	-(void) setDelegate:(NSObject *)p0;
	-(id) init;
@end

@interface TBDStartTransactionParams : NSObject {
}
	-(double) amount;
	-(void) setAmount:(double)p0;
	-(BOOL) fallback;
	-(void) setFallback:(BOOL)p0;
	-(BOOL) forceOnline;
	-(void) setForceOnline:(BOOL)p0;
	-(double) otherAmount;
	-(void) setOtherAmount:(double)p0;
	-(NSString *) tagsInHex;
	-(void) setTagsInHex:(NSString *)p0;
	-(double) transTimeout;
	-(void) setTransTimeout:(double)p0;
	-(unsigned int) transType;
	-(void) setTransType:(unsigned int)p0;
	-(id) init;
@end


